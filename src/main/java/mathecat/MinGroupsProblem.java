package mathecat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MinGroupsProblem {
    public static Integer[][] solve(int[][] input) {
        Map<Double, List<Integer>> indexesByAverage = new HashMap<>();

        for (int i = 0; i < input.length; i++) {
            Double avg = Arrays.stream(input[i]).average().getAsDouble();

            List<Integer> currentList = indexesByAverage.computeIfAbsent(avg, k -> new ArrayList<>());
            currentList.add(i);
        }

        List<List<Integer>> sortedLists = indexesByAverage.values().stream()
                .sorted(Comparator.comparing(currentList -> currentList.get(0), Comparator.comparingInt(s -> s)))
                .collect(Collectors.toList());

        Integer[][] returnMatrix = new Integer[sortedLists.size()][];
        for (int i = 0; i < sortedLists.size(); i++) {
            returnMatrix[i] = sortedLists.get(i).toArray(new Integer[0]);
        }

        return returnMatrix;
    }
}
