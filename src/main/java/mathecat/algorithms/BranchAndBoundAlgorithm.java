package mathecat.algorithms;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

// TODO
public class BranchAndBoundAlgorithm {
    public List<Edge> findShortestPath(String startNodeName, String endNodeName, Graph graph) {
        final Node startNode = graph.getNode(startNodeName);
        final Node endNode = graph.getNode(endNodeName);

        if (Objects.isNull(startNode) || Objects.isNull(endNode)) {
            throw new IllegalArgumentException("Unable to find node in graph");
        }

        List<SearchNode> nodeQueue = new ArrayList<>();
        nodeQueue.add(new SearchNode(startNode));

        while (!nodeQueue.isEmpty()) {
            SearchNode head = nodeQueue.remove(0);

            for (Edge edge : head.getCurrentNode().getEdgeList()) {

            }
        }

        return null;
    }

    private static class SearchNode {
        private final Node currentNode;
        private double accumulatedWeight;

        public SearchNode(Node currentNode) {
            this.currentNode = currentNode;
        }

        public Node getCurrentNode() {
            return currentNode;
        }

        public double getAccumulatedWeight() {
            return accumulatedWeight;
        }

        public void setAccumulatedWeight(double accumulatedWeight) {
            this.accumulatedWeight = accumulatedWeight;
        }
    }
}
