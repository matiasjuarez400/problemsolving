package mathecat.algorithms;

import java.util.List;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) {
        BruteForceSearch bruteForceSearch = new BruteForceSearch();

        Graph graph = new Graph();
        graph.joinNodes("s", "a", 3);
        graph.joinNodes("s", "b", 5);

        graph.joinNodes("a", "d", 3);
        graph.joinNodes("a", "b", 4);

        graph.joinNodes("b", "c", 4);

        graph.joinNodes("c", "e", 6);

        graph.joinNodes("d", "g", 5);

        List<List<Node>> successfulPaths = bruteForceSearch.findPath("s", "g", graph);

        for (List<Node> successfulPath : successfulPaths) {
            String pathRepresentation = successfulPath.stream()
                    .map(Node::getName)
                    .collect(Collectors.joining(" - "));

            System.out.println(pathRepresentation);
        }
    }
}
