package mathecat.algorithms;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Node {
    private final String name;
    private final Set<Edge> edgeList;

    public Node(String name) {
        this.name = name;
        this.edgeList = new HashSet<>();
    }

    public String getName() {
        return name;
    }

    public Set<Edge> getEdgeList() {
        return edgeList;
    }

    public void addEdge(Edge edge) {
        this.edgeList.add(edge);
    }

    public void join(Node other, double weight) {
        Edge edge = new Edge(this, other, weight);
        addEdge(edge);
        other.addEdge(edge);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return Objects.equals(name, node.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Node{" +
                "name='" + name + '\'' +
//                ", edgeList=" + edgeList.stream().map(Edge::toString).collect(Collectors.joining(" - ")) +
                '}';
    }
}
