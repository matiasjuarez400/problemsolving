package mathecat.algorithms;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

// After learning some theory, I know that it is not "BRUTE FORCE", but a Depth First Search implemented using recursion
public class BruteForceSearch {
    public List<List<Node>> findPath(String startNodeName, String endNodeName, Graph graph) {
        final Node startNode = graph.getNode(startNodeName);
        final Node endNode = graph.getNode(endNodeName);

        final List<List<Node>> successfulPaths = new ArrayList<>();

        expand(startNode, endNode, new ArrayList<>(), new HashSet<>(), successfulPaths);

        return successfulPaths;
    }

    private void expand(Node currentNode, Node endNode, List<Node> currentPath, Set<Node> visitedNodes, List<List<Node>> successfulPaths) {
        currentPath.add(currentNode);

        if (currentNode.equals(endNode)) {
            successfulPaths.add(new ArrayList<>(currentPath));
            return;
        }

        if (visitedNodes.contains(currentNode)) return;

        visitedNodes.add(currentNode);

        for (Edge edge : currentNode.getEdgeList()) {
            expand(edge.getFirst(), endNode, new ArrayList<>(currentPath), visitedNodes, successfulPaths);
            expand(edge.getSecond(), endNode, new ArrayList<>(currentPath), visitedNodes, successfulPaths);
        }
    }
}
