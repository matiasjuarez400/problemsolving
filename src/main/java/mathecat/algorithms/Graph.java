package mathecat.algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class Graph {
    private final Map<String, Node> nodesByName = new HashMap<>();
    private final List<List<Boolean>> adjacencyMatrix = new ArrayList<>();
    private final Map<String, Integer> indexByNodeName = new HashMap<>();

    public Node getNode(String nodeName) {
        return nodesByName.get(nodeName);
    }

    public void joinNodes(Node first, Node second, double weight) {
        Node firstWorkingNode = getWorkingNode(first);
        Node secondWorkingNode = getWorkingNode(second);

        nodesByName.put(firstWorkingNode.getName(), firstWorkingNode);
        nodesByName.put(secondWorkingNode.getName(), secondWorkingNode);

        firstWorkingNode.join(secondWorkingNode, weight);
    }

    private Node getWorkingNode(Node input) {
        return Optional.ofNullable(nodesByName.get(input.getName())).orElse(input);
    }

    private void createEntryForAdjacencyMatrix(Node node) {
        if (!indexByNodeName.containsKey(node.getName())) {
            indexByNodeName.put(node.getName(), indexByNodeName.size());
            adjacencyMatrix.add(new ArrayList<>());
        }
    }

    public boolean[][] getAdjacencyMatrix() {
        return null;
    }

    public void joinNodes(String first, String second, double weight) {
        joinNodes(new Node(first), new Node(second), weight);
    }
}
