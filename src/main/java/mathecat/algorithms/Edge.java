package mathecat.algorithms;

import java.util.Objects;

public class Edge {
    private final Node first;
    private final Node second;
    private final double weight;

    public Edge(Node first, Node second, double weight) {
        this.first = first;
        this.second = second;
        this.weight = weight;
    }

    public Node getFirst() {
        return first;
    }

    public Node getSecond() {
        return second;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edge edge = (Edge) o;
        return Double.compare(edge.weight, weight) == 0 &&
                first.equals(edge.first) &&
                second.equals(edge.second);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second, weight);
    }

    @Override
    public String toString() {
        return "Edge{" +
                "first=" + first +
                ", second=" + second +
                ", weight=" + weight +
                '}';
    }
}
