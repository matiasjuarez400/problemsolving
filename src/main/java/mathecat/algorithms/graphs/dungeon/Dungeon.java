package mathecat.algorithms.graphs.dungeon;

public class Dungeon {
    private Point exit;
    private final String[][] representation;
    public static final String EMPTY_SPACE = ".";
    public static final String OBSTACLE = "#";
    public static final String EXIT_POINT = "E";

    public Dungeon(String[][] representation) {
        this.representation = representation;

        for (int row = 0; row < representation.length; row++) {
            for (int col = 0; col < representation[row].length; col++) {
                if (EXIT_POINT.equals(representation[row][col])) {
                    exit = new Point(row, col);
                    break;
                }
            }
        }

        if (exit == null) {
            throw new IllegalStateException("The dungeon doesn't have an exit!");
        }
    }

    public Point getExit() {
        return exit;
    }

    public String[][] getRepresentation() {
        return representation;
    }

    public String get(Point point) {
        return representation[point.getRow()][point.getCol()];
    }

    public boolean isExit(Point point) {
        return get(point).equals(EXIT_POINT);
    }

    public boolean isObstacle(Point point) {
        return get(point).equals(OBSTACLE);
    }
}
