package mathecat.algorithms.graphs.dungeon;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class DungeonGenerator {
    private static final double DEFAULT_OBSTACLES_PROPORTION = 0.1;

    public static Dungeon generateDungeon(int rows, int cols, double obstaclesProportion) {
        final int dungeonSize = rows * cols;
        final int obstaclesAmount = (int) (obstaclesProportion * dungeonSize);

        final Set<Point> obstacles = new HashSet<>();
        final Random random = new Random();
        for (int i = 0; i < obstaclesAmount; i++) {
            Point newObstacle;
            do {
                newObstacle = new Point(random.nextInt(rows), random.nextInt(cols));
            } while (obstacles.contains(newObstacle));

            obstacles.add(newObstacle);
        }

        final String[][] dungeonMap = new String[rows][cols];

        for (String[] row : dungeonMap) {
            Arrays.fill(row, Dungeon.EMPTY_SPACE);
        }

        obstacles.forEach(obstacle -> {
            dungeonMap[obstacle.getRow()][obstacle.getCol()] = Dungeon.OBSTACLE;
        });

        dungeonMap[random.nextInt(rows)][random.nextInt(cols)] = Dungeon.EXIT_POINT;

        return new Dungeon(dungeonMap);
    }

    public static Dungeon generateDungeon(int rows, int cols) {
        return generateDungeon(rows, cols, DEFAULT_OBSTACLES_PROPORTION);
    }
}
