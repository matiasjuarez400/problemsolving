package mathecat.algorithms.graphs.dungeon;

public class DungeonException extends RuntimeException {
    public DungeonException(String msg) {
        super(msg);
    }
}
