package mathecat.algorithms.graphs.dungeon;

public class DungeonScape {
    public static void main(String[] args) {
        String[][] dungeonMatrix = {
                {".", "#", ".", "."},
                {".", ".", ".", "."},
                {"#", "#", ".", "#"},
                {".", ".", ".", "."},
                {"E", "#", "#", "."},
        };

        Dungeon dungeon = new Dungeon(dungeonMatrix);
        dungeon = DungeonGenerator.generateDungeon(10, 30, 0.3);

        System.out.println(DungeonPrinter.getDungeonRepresentation(dungeon));

        DungeonSolver solver = new DungeonSolver();

        System.out.println("Path to exit: " + solver.findPathToExit(dungeon, new Point(0, 0)));
    }
}
