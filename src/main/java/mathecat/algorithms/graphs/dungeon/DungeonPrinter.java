package mathecat.algorithms.graphs.dungeon;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class DungeonPrinter {
    private static final String SPIDER = "U+1F577";
    private static final String SCORPION = "U+1F982";
    private static final String DINO_THE_BEST = "U+1F996";
    private static final String DRAGON = "U+1F409";
    private static final String SNAKE = "U+1F40D";
    private static final String COCO = "U+1F40A";
    private static final String BAT = "U+1F987";

    private static final List<String> MONSTER_LIST = Arrays.asList(
            DINO_THE_BEST
    );

    private static final String DOOR = "U+1F6AA";

    private static String convertCodePointToString(String codePoint) {
        int codePointInt = Integer.parseInt(codePoint.substring(2), 16);
        char[] ch = Character.toChars(codePointInt);

        return new String(ch);
    }

    public static String getDungeonRepresentation(Dungeon dungeon) {
        final String[][] dungeonMatrix = dungeon.getRepresentation();

        final String[][] output = new String[dungeonMatrix.length][dungeonMatrix[0].length];

        Random random = new Random();

        boolean colsWithMonsters[] = new boolean[output[0].length];
        for (int col = 0; col < output[0].length; col++) {
            for (int row = 0; row < output.length; row++) {
                if (Dungeon.OBSTACLE.equals(dungeonMatrix[row][col])) {
                    colsWithMonsters[col] = true;
                    break;
                }
            }
        }

        for (int row = 0; row < output.length; row++) {
            for (int col = 0; col < output[row].length; col++) {
                String current = dungeonMatrix[row][col];

                if (Dungeon.OBSTACLE.equals(current)) {
                    output[row][col] = convertCodePointToString(MONSTER_LIST.get(random.nextInt(MONSTER_LIST.size())));
                } else {
                    String endChar = dungeonMatrix[row][col];
                    if (colsWithMonsters[col]) endChar += " ";

                    output[row][col] = endChar;
                }
            }
        }

        final Point dungeonExit = dungeon.getExit();
        output[dungeonExit.getRow()][dungeonExit.getCol()] = convertCodePointToString(DOOR) + (colsWithMonsters[dungeonExit.getCol()] ? " " : "");

        final StringBuilder sb = new StringBuilder();

        for (int row = 0; row < output.length; row++) {
            for (int col = 0; col < output[row].length; col++) {
                String currentChar = output[row][col];

                sb.append(currentChar).append(" | ");
            }
            sb.append("\n");
        }

        return sb.toString();
    }
}
