package mathecat.algorithms.graphs.dungeon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

public class DungeonSolver {
    private static final List<Point> MOVEMENTS = Arrays.asList(
            new Point(-1, 0), new Point(1, 0),
            new Point(0, -1), new Point(0, 1)
    );

    public List<Point> findPathToExit(Dungeon dungeon, Point start) {
        if (start.getCol() < 0 || start.getCol() >= dungeon.getRepresentation()[0].length ||
                start.getRow() < 0 || start.getRow() >= dungeon.getRepresentation().length) {
            throw new DungeonException("You are not even inside the dungeon, dude! That's why you are dead");
        }

        if (dungeon.getRepresentation()[start.getRow()][start.getCol()].equals(Dungeon.OBSTACLE)) {
            throw new DungeonException("You've fallen right into a trap and you died");
        }

        return findExitUsingBFS(dungeon, start);
    }

    private List<Point> findExitUsingBFS(Dungeon dungeon, Point start) {
        final String[][] matrix = dungeon.getRepresentation();

        final Queue<Point> queue = new LinkedList<>();
        queue.add(start);

        final Set<Point> visited = new HashSet<>();

        final Point[][] previousPoints = new Point[matrix.length][matrix[0].length];

        boolean foundExit = false;
        Point exit = null;

        while (!queue.isEmpty()) {
            final Point at = queue.poll();
            if (visited.contains(at)) continue;
            if (dungeon.isObstacle(at)) continue;

            System.out.println(at);

            visited.add(at);

            if (dungeon.isExit(at)) {
                foundExit = true;
                exit = at;
                break;
            }

            final List<Point> neighbours = findNeighbours(dungeon, at);

            neighbours.forEach(neighbour -> {
                if (!visited.contains(neighbour)) {
                    previousPoints[neighbour.getRow()][neighbour.getCol()] = at;
                    queue.add(neighbour);
                }
            });
        }

        if (foundExit) {
            return buildPath(previousPoints, start, exit);
        } else {
            return Collections.emptyList();
        }
    }

    private List<Point> buildPath(Point[][] previousPoints, Point start, Point exit) {
        final List<Point> reversedPath = new ArrayList<>();

        Point at = exit;

        while (at != null) {
            reversedPath.add(at);
            at = previousPoints[at.getRow()][at.getCol()];
        }

        Collections.reverse(reversedPath);

        return reversedPath;
    }

    private List<Point> findNeighbours(Dungeon dungeon, Point point) {
        return MOVEMENTS.stream()
                .map(movement -> new Point(point.getRow() + movement.getRow(), point.getCol() + movement.getCol()))
                .filter(neighbour -> neighbour.getCol() >= 0 && neighbour.getCol() < dungeon.getRepresentation()[0].length &&
                            neighbour.getRow() >= 0 && neighbour.getRow() < dungeon.getRepresentation().length
                        )
                .collect(Collectors.toList());
    }
}
