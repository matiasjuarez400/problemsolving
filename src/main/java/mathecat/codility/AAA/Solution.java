package mathecat.codility.AAA;

import java.util.ArrayList;
import java.util.List;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.test5();
        solution.test4();
        solution.test3();
        solution.test2();
        solution.test1();
    }

    // should be 30
    public void test5() {
        int[] H = {1, 1, 7, 6, 6, 6};

        int result = solution(H);

        validateResult(result, 30);
    }

    // should be 35
    public void test4() {
        int[] H = {7, 7, 3, 7, 7};

        int result = solution(H);

        validateResult(result, 35);
    }

    // should be 19
    public void test3() {
        int[] H = {5, 3, 5, 2, 1};

        int result = solution(H);

        validateResult(result, 19);
    }

    // should be 17
    public void test2() {
        int[] H = {5, 3, 2, 4};

        int result = solution(H);

        validateResult(result, 17);
    }

    // should be 10
    public void test1() {
        int[] H = {3, 1, 4};

        int result = solution(H);

        validateResult(result, 10);
    }

    private void validateResult(int result, int expected) {
        if (result != expected) {
            throw new IllegalStateException(String.format("Expected: %s. Result: %s", expected, result));
        } else {
            System.out.println("PASSED");
        }
    }

    public int solution(int[] H) {
        final int lastIndex = H.length - 1;

        int maxHeight = Integer.MIN_VALUE;
        int nextMaxHeight = Integer.MIN_VALUE;
        List<Integer> maxHeightIndexes = new ArrayList<>();
        List<Integer> nextMaxHeightIndexes = new ArrayList<>();

        for (int i = 0; i < H.length; i++) {
            int currentHeight = H[i];
            if (currentHeight > maxHeight) {
                nextMaxHeight = maxHeight;
                nextMaxHeightIndexes.clear();
                nextMaxHeightIndexes.addAll(maxHeightIndexes);

                maxHeight = currentHeight;
                maxHeightIndexes.clear();
                maxHeightIndexes.add(i);
            } else if (currentHeight == maxHeight) {
                maxHeightIndexes.add(i);
            } else if (currentHeight == nextMaxHeight) {
                nextMaxHeightIndexes.add(i);
            } else if (currentHeight > nextMaxHeight) {
                nextMaxHeight = currentHeight;
                nextMaxHeightIndexes.clear();
                nextMaxHeightIndexes.add(i);
            }
        }

        // Only one max height building at the beggining or at the end
        if (maxHeightIndexes.size() == 1) {
            if ((maxHeightIndexes.get(0) == 0 || maxHeightIndexes.get(0) == lastIndex)) {
                int result = maxHeight;
                if (!nextMaxHeightIndexes.isEmpty()) {
                    result += nextMaxHeight * (H.length - 1);
                }
                return result;
            } else {
                final int maxHeightIndex = maxHeightIndexes.get(0);
                return calculateMinPossibleArea(
                        0, maxHeightIndex - 1,
                        maxHeightIndex, maxHeightIndex,
                        maxHeightIndex + 1, lastIndex,
                        maxHeight,
                        H
                        );
            }
        } else if (maxHeightIndexes.size() > 1) {
            final int leftMaxHeightIndex = maxHeightIndexes.get(0);
            final int rightMaxHeightIndex = maxHeightIndexes.get(maxHeightIndexes.size() - 1);

            if (leftMaxHeightIndex > 0 && rightMaxHeightIndex < lastIndex) {
                return calculateMinPossibleArea(
                        0, leftMaxHeightIndex - 1,
                        leftMaxHeightIndex, rightMaxHeightIndex,
                        rightMaxHeightIndex + 1, lastIndex,
                        maxHeight,
                        H
                );
            } else if (leftMaxHeightIndex == 0 || rightMaxHeightIndex == lastIndex) {
                final int areaBetweenMaxHeights = calculateArea(leftMaxHeightIndex, rightMaxHeightIndex, maxHeight);

                final int leftArea = findMinPossibleAreaBetweenIndexes(0, leftMaxHeightIndex - 1, H);
                final int rightArea = findMinPossibleAreaBetweenIndexes(rightMaxHeightIndex + 1, lastIndex, H);

                return areaBetweenMaxHeights + leftArea + rightArea;
            }
        }

        return H.length * maxHeight;
    }

    public int calculateMinPossibleArea(int leftAreaBeginIndex, int leftAreaEndIndex,
                                        int middleAreaBeginIndex, int middleAreaEndIndex,
                                        int rightAreaBeginIndex, int rightAreaEndIndex, int maxHeight, int[] H) {
        final int leftMaxArea = calculateArea(0, leftAreaEndIndex, maxHeight);
        final int rightMaxArea = calculateArea(rightAreaBeginIndex, rightAreaEndIndex, maxHeight);
        final int leftMinArea = findMinPossibleAreaBetweenIndexes(0, leftAreaEndIndex, H);
        final int rightMinArea = findMinPossibleAreaBetweenIndexes(rightAreaBeginIndex, rightAreaEndIndex, H);

        final int middleArea = calculateArea(middleAreaBeginIndex, middleAreaEndIndex, maxHeight);

        final int leftWastedArea = leftMaxArea - leftMinArea;
        final int rightWastedArea = rightMaxArea - rightMinArea;

        if (rightWastedArea < leftWastedArea) {
            return rightMaxArea + middleArea + leftMinArea;
        } else {
            return leftMaxArea + middleArea + rightMinArea;
        }
    }

    public int calculateArea(int leftInclusive, int rightInclusive, int height) {
        if (leftInclusive > rightInclusive) return 0;

        return (rightInclusive - leftInclusive + 1) * height;
    }

    public int findMinPossibleAreaBetweenIndexes(int leftInclusive, int rightInclusive, int[] H) {
        if (leftInclusive > rightInclusive) return 0;

        int maxHeight = Integer.MIN_VALUE;

        for (int i = leftInclusive; i <= rightInclusive; i++) {
            int currentHeight = H[i];
            if (currentHeight > maxHeight) maxHeight = currentHeight;
        }

        return calculateArea(leftInclusive, rightInclusive, maxHeight);
    }
}
