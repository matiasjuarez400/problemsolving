package mathecat.codility.training._5_prefix_sums.genomic_range_query.crap;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
    }

    public int[] solutionExplorePositions_NeedPerformance(String S, int[] P, int[] Q) {
        char[] sortedGens = {'A', 'C', 'G', 'T'};

        char[] sequence = S.toCharArray();

        Map<Character, List<Integer>> positionsByGen = new HashMap<>();
        for (int i = 0; i < sequence.length; i++) {
            char gen = sequence[i];
            List<Integer> positions = positionsByGen.computeIfAbsent(gen, k -> new ArrayList<>());
            positions.add(i);
        }

        int[] answers = new int[P.length];

        for (int i = 0; i < P.length; i++) {
            int start = P[i];
            int end = Q[i];
            int answer = -1;

            for (int j = 0; j < sortedGens.length - 1; j++) {
                char gen = sortedGens[j];
                List<Integer> positions =  positionsByGen.get(gen);
                if (positions == null) continue;

                for (int position : positions) {
                    if (start <= position && position <= end) {
                        answer = j + 1;
                        break;
                    } else if (position > end) {
                        break;
                    }
                }

                if (answer != -1) break;
            }

            if (answer == -1) answer = 4;

            answers[i] = answer;
        }

        return answers;
    }

    public int[] solution(String S, int[] P, int[] Q) {
        char[] sortedGens = {'A', 'C', 'G', 'T'};

        char[] sequence = S.toCharArray();

        PositionStore positionStore = new PositionStore(3000);

        for (int i = 0; i < sequence.length; i++) {
            char gen = sequence[i];
            positionStore.store(gen, i);
        }

        int[] answers = new int[P.length];

        for (int i = 0; i < P.length; i++) {
            int start = P[i];
            int end = Q[i];
            int answer = -1;

            for (int j = 0; j < sortedGens.length - 1; j++) {
                char gen = sortedGens[j];

                for (int k = start; k <= end; k++) {
                    if (positionStore.contains(gen, k)) {
                        answer = j + 1;
                        break;
                    }
                }

                if (answer != -1) break;
            }

            if (answer == -1) answer = 4;

            answers[i] = answer;
        }

        return answers;
    }

    private static class PositionStore {
        private final int bucketSize;
        private final Map<Character, PositionBucket> bucketByGen = new HashMap<>();

        public PositionStore(int bucketSize) {
            this.bucketSize = bucketSize;
        }

        public void store(char gen, int position) {
            PositionBucket positionBucket = bucketByGen.computeIfAbsent(gen, k -> new PositionBucket(gen, bucketSize));
            positionBucket.store(position);
        }

        public boolean contains(char gen, int position) {
            PositionBucket positionBucket = bucketByGen.get(gen);
            if (positionBucket == null) return false;
            return positionBucket.contains(position);
        }
    }

    private static class PositionBucket {
        private char gen;
        private final int groupSize;
        private Map<Integer, List<Integer>> positionsGrouped = new HashMap<>();

        public PositionBucket(char gen, int groupSize) {
            this.gen = gen;
            this.groupSize = groupSize;
        }

        public void store(int value) {
            int group = calculateGroup(value);
            List<Integer> positions = positionsGrouped.computeIfAbsent(group, k -> new ArrayList<>());
            positions.add(value);
        }

        public boolean contains(int value) {
            int group = calculateGroup(value);
            List<Integer> positions = positionsGrouped.get(group);
            if (positions == null) return false;

            for (int position : positions) {
                if (position == value) return true;
                if (position > value) break;
            }

            return false;
        }

        private int calculateGroup(int value) {
            return value / groupSize;
        }
    }

    public int[] solutionNeedsPerformance(String S, int[] P, int[] Q) {
        // write your code in Java SE 8
        int[] impacts = new int[30];
        impacts[0] = 1;
        impacts['C' - 'A'] = 2;
        impacts['G' - 'A'] = 3;
        impacts['T' - 'A'] = 4;

        char[] sequence = S.toCharArray();

        int[] answers = new int[P.length];

        for (int i = 0; i < P.length; i++) {
            int minimalImpact = 5;
            for (int j = P[i]; j <= Q[i]; j++) {
                int nextImpact = impacts[sequence[j] - 'A'];
                if (nextImpact < minimalImpact) minimalImpact = nextImpact;
                if (minimalImpact == 1) break;
            }

            answers[i] = minimalImpact;
        }

        return answers;
    }

    public int[] solutionCacheIncomplete(String S, int[] P, int[] Q) {
        // write your code in Java SE 8
        int[] impacts = new int[30];
        impacts[0] = 1;
        impacts['C' - 'A'] = 2;
        impacts['G' - 'A'] = 3;
        impacts['T' - 'A'] = 4;

        char[] sequence = S.toCharArray();

        int[] answers = new int[P.length];

        Cache cache = new Cache();

        for (int i = 0; i < P.length; i++) {
            int minimalImpact = 5;
            int start = P[i];
            int end = Q[i];

            CacheEntry cacheEntry = cache.search(start, end);

            while (cacheEntry != null) {
                if (cacheEntry.getMinimalImpact() < minimalImpact) {
                    minimalImpact = cacheEntry.getMinimalImpact();
                }
                if (minimalImpact == 1) {
                    break;
                } else {
                    start = cacheEntry.getEnd() + 1;
                    cacheEntry = cache.search(start, end);
                }
            }

            if (minimalImpact == 1) {
                answers[i] = 1;
                continue;
            }

            for (int j = start; j <= end; j++) {
                int nextImpact = impacts[sequence[j] - 'A'];
                if (nextImpact < minimalImpact) minimalImpact = nextImpact;
                if (minimalImpact == 1) break;
            }

            answers[i] = minimalImpact;
        }

        return answers;
    }

    private static class Cache {
        private final Map<Integer, List<CacheEntry>> entryMap = new HashMap<>();

        public CacheEntry search(int start, int end) {
            List<CacheEntry> candidates = entryMap.get(start);
            if (candidates == null) return null;

            CacheEntry bestFit = null;
            for (CacheEntry cacheEntry : candidates) {
                if (cacheEntry.getEnd() <= end) bestFit = cacheEntry;
                else if (cacheEntry.getEnd() > end) break;
            }

            return bestFit;
        }

        public void store(int start, int end, int minimalImpact) {
            List<CacheEntry> entries = entryMap.computeIfAbsent(start, k -> new ArrayList<>());

            entries.add(new CacheEntry(start, end, minimalImpact));

            entries.sort(Comparator.comparing(CacheEntry::getEnd));
        }
    }

    private static class CacheEntry {
        private final int start;
        private final int end;
        private final int minimalImpact;

        public CacheEntry(int start, int end, int minimalImpact) {
            this.start = start;
            this.end = end;
            this.minimalImpact = minimalImpact;
        }

        public int getStart() {
            return start;
        }

        public int getEnd() {
            return end;
        }

        public int getMinimalImpact() {
            return minimalImpact;
        }
    }
}
