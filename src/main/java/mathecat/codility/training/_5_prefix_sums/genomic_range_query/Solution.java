package mathecat.codility.training._5_prefix_sums.genomic_range_query;

import java.util.HashMap;
import java.util.Map;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.solution("A", null, null);
    }

    public int[] solution(String S, int[] P, int[] Q) {
        Map<Character, Integer> impactByGen = new HashMap<>();
        impactByGen.put('A', 1);
        impactByGen.put('C', 2);
        impactByGen.put('G', 3);
        impactByGen.put('T', 4);

        int[][] segmentTree = generateSegmentTree(S);

        int[] output = new int[P.length];
        for (int i = 0; i < P.length; i++) {
            output[i] = impactByGen.get((char) findMinimum(segmentTree, 0, P[i], Q[i]));
        }

        return output;
    }

    public int findMinimum(int[][] segmentTree, int position, int left, int right) {
        int[] entry = segmentTree[position];

        if (left > entry[1] || right < entry[0]) return Integer.MAX_VALUE;
        if (left <= entry[0] && right >= entry[1]) return entry[2];

        return Integer.min(
                findMinimum(segmentTree, position * 2 + 1, left, right),
                findMinimum(segmentTree, position * 2 + 2, left, right)
        );
    }

    public int[][] generateSegmentTree(String sequence) {
        char[] genes = sequence.toCharArray();
        int[][] treeHolder = createTreeHolder(genes);

        doGenerate(genes, treeHolder, 0, 0, genes.length - 1);

        return treeHolder;
    }

    private int[][] createTreeHolder(char[] genes) {
        int target = 2 * genes.length - 1;

        int currentPower = 1;

        while (currentPower < target) {
            currentPower *= 2;
        }

        return new int[currentPower][];
    }

    private int[] doGenerate(char[] genes, int[][] treeHolder, int currentPosition, int low, int high) {
        if (low == high) {
            treeHolder[currentPosition] = new int[] {low, high, genes[low]};
        } else {
            int mid = (high + low) / 2;

            int[] left = doGenerate(genes, treeHolder, currentPosition * 2 + 1, low, mid);
            int[] right = doGenerate(genes, treeHolder, currentPosition * 2 + 2, mid + 1, high);

            treeHolder[currentPosition] = new int[] {low, high, Integer.min(left[2], right[2])};
        }

        return treeHolder[currentPosition];
    }
}
