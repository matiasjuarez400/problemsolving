package mathecat.codility.training._5_prefix_sums.count_div;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
    }

    public int solution(int A, int B, int K) {
        if (K > B) return A == 0 ? 1 : 0;

        int lowerLimit = A;
        if (K >= A && A != 0) lowerLimit = K;
        else {
            for (int i = A; i <= B; i++) {
                if (i % K == 0) {
                    lowerLimit = i;
                    break;
                }
            }
        }

        return (B - lowerLimit) / K + (lowerLimit % K == 0 ? 1 : 0);
    }
}
