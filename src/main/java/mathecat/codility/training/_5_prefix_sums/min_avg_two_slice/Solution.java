package mathecat.codility.training._5_prefix_sums.min_avg_two_slice;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
    }

    public int solution(int[] A) {
        // write your code in Java SE 8
        List<Result> results = prefixSumWithWindowSlide(A);
        results.sort(Comparator.comparing(Result::getStart));

        return results.get(0).getStart();
    }

    public List<Result> prefixSumWithWindowSlide(int[] A) {
        for (int i = 1; i < A.length; i++) {
            A[i] = A[i - 1] + A[i];
        }

        List<Result> minResults = new ArrayList<>();

        Result minResult = Result.worst();

        for (int windowSize = 2; windowSize <= 3; windowSize++) {
            int minAccumulated = Integer.MAX_VALUE;

            for (int i = 0; i <= A.length - windowSize; i++) {
                int rightIndex = i + windowSize - 1;

                int right = A[rightIndex];
                int left = i == 0 ? 0 : A[i - 1];

                int accumulated = right - left;

                if (accumulated < minAccumulated) {
                    minAccumulated = accumulated;
                }

                if (accumulated == minAccumulated) {
                    Result result = new Result(i, rightIndex, accumulated);

                    if (result.getAvg() < minResult.getAvg()) {
                        minResult = result;
                        minResults.clear();
                    }

                    if (result.getAvg() == minResult.getAvg()) {
                        minResults.add(result);
                    }
                }
            }
        }

        return minResults;
    }

    public List<Result> bruteForce(int[] A) {
        List<Result> minAvgs = new ArrayList<>();

        double minAvg = Double.MAX_VALUE;

        for (int i = 0; i < A.length; i++) {
            int acc = A[i];
            for (int j = i + 1; j < A.length; j++) {
                acc += A[j];
                double currentAvg = (acc * 1.0) / (j - i + 1);
                if (currentAvg < minAvg) {
                    minAvgs.clear();
                    minAvg = currentAvg;
                }

                if (currentAvg == minAvg) {
                    minAvgs.add(new Result(i, j, acc));
                }
            }
        }

        return minAvgs;
    }

    static class Result {
        int start;
        int end;
        int acc;
        int size;
        double avg;

        public Result(int start, int end, int acc) {
            this.start = start;
            this.end = end;
            this.acc = acc;
            this.size = end - start + 1;
            this.avg = (acc * 1.0) / this.size;
        }

        public static Result worst() {
            return new Result(0, 0, Integer.MAX_VALUE);
        }

        public int getStart() {
            return start;
        }

        public int getEnd() {
            return end;
        }

        public int getAcc() {
            return acc;
        }

        public double getAvg() {
            return avg;
        }

        public int getSize() {
            return end - start + 1;
        }
    }
}
