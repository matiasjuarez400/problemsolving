package mathecat.codility.training._1_iterations._1_binary_gap;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.solution(20));
        System.out.println(solution.solution(529));
        System.out.println(solution.solution(15));
        System.out.println(solution.solution(32));
        System.out.println(solution.solution(74901729));
        System.out.println(solution.solution(561892));
    }

    public int solution(int N) {
        String Ns = Integer.toBinaryString(N);

        int maxGap = 0;
        int currentGap = 0;
        for (char c : Ns.toCharArray()) {
            if (c == '1') {
                if (currentGap > maxGap) {
                    maxGap = currentGap;
                }
                currentGap = 0;
            } else if (c == '0') {
                currentGap++;
            }
        }

        log(N, Ns, maxGap);

        return maxGap;
    }

    private void log(int input, String binaryRepresentation, int maxGap) {
        System.out.printf("I: %s _ BR: %s _ G: %s%n", input, binaryRepresentation, maxGap);
    }
}
