package mathecat.codility.training._3_time_complexity._6_tape_equilibrium;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.solution(new int[]{
                3, 1, 2, 4, 3
        }));
    }

    public int solution(int[] A) {
        int left = A[0];
        int right = 0;

        for (int i = 1; i < A.length; i++) {
            right += A[i];
        }

        int minDiff = Integer.MAX_VALUE;

        int i = 1;
        do {
            int diff = Math.abs(left - right);
            if (diff < minDiff) {
                minDiff = diff;
            }

            int interchange = A[i];
            left += interchange;
            right -= interchange;

            i++;
        } while (i < A.length);

        return minDiff;
        // write your code in Java SE 8
    }
}
