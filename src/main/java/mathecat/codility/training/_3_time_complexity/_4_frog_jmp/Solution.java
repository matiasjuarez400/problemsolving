package mathecat.codility.training._3_time_complexity._4_frog_jmp;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.solution(10, 85, 30));
    }

    public int solution(int X, int Y, int D) {
        int distanceToCover = Y - X;

        int jumps = distanceToCover / D;
        if (distanceToCover % D != 0) {
            jumps++;
        }

        return jumps;
    }
}
