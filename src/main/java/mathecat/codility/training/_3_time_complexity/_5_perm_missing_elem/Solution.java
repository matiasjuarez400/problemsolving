package mathecat.codility.training._3_time_complexity._5_perm_missing_elem;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.solution(new int[] {
                2, 3, 1, 5
        }));
    }

    public int solution(int[] A) {
        if (A == null || A.length == 0) return 1;

        Arrays.sort(A);
        if (A[0] != 1) return 1;
        if (A[A.length - 1] != A.length + 1) return A.length + 1;

        for (int i = 1; i < A.length; i++) {
            if (A[i] - A[i - 1] != 1) {
                return A[i] - 1;
            }
        }

        return -1;
        // write your code in Java SE 8
    }
}
