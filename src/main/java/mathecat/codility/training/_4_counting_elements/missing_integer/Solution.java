package mathecat.codility.training._4_counting_elements.missing_integer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
    }

    public int solution(int[] A) {
        // write your code in Java SE 8
        Set<Integer> uniquePositives = new HashSet<>();
        for (int i : A) {
            if (i <= 0) continue;
            uniquePositives.add(i);
        }

        if (uniquePositives.size() == 0) return 1;

        List<Integer> sortedPositives = new ArrayList<>(uniquePositives);
        Collections.sort(sortedPositives);

        if (sortedPositives.size() == sortedPositives.get(sortedPositives.size() - 1)) return sortedPositives.get(sortedPositives.size() - 1) + 1;

        int previous = 0;

        for (int i : sortedPositives) {
            if (i - previous > 1) return previous + 1;
            else previous = i;
        }

        return -1;
    }

    public int solutionNeedsPerformance(int[] A) {
        // write your code in Java SE 8
        int[] positives = Arrays.stream(A)
                .filter(i -> i > 0)
                .sorted()
                .toArray();

        if (positives.length == 0) return 1;

        int previous = 0;

        for (int i : positives) {
            if (i - previous > 1) return previous + 1;
            else previous = i;
        }

        return positives[positives.length - 1] + 1;
    }
}
