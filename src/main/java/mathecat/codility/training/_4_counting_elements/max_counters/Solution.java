package mathecat.codility.training._4_counting_elements.max_counters;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
    }

    public int[] solution(int N, int[] A) {
        int[] counterRelativeValues = new int[N];
        int[] counterBases = new int[N];

        int realBaseValue = 0;
        int maxValue = 0;
        int realIndex;

        for (int i : A) {
            if (i == N + 1) {
                realBaseValue = maxValue;
            } else {
                realIndex = i - 1;

                if (counterBases[realIndex] != realBaseValue) {
                    counterBases[realIndex] = realBaseValue;
                    counterRelativeValues[realIndex] = 1;
                } else {
                    counterRelativeValues[realIndex]++;
                }

                int accumulated = counterBases[realIndex] + counterRelativeValues[realIndex];

                if (accumulated > maxValue) maxValue = accumulated;
            }
        }

        int[] countersFinalValues = new int[N];

        for (int i = 0; i < counterBases.length; i++) {
            if (counterBases[i] != realBaseValue) {
                countersFinalValues[i] = realBaseValue;
            } else {
                countersFinalValues[i] = realBaseValue + counterRelativeValues[i];
            }
        }

        return countersFinalValues;
    }

    public int[] solutionOld(int N, int[] A) {
        int[] counters = new int[N];

        int baseValue = 0;
        int relativeMax = 0;
        for (int i : A) {
            if (i == N + 1) {
                counters = new int[N];
                baseValue += relativeMax;
                relativeMax = 0;
            } else {
                int realIndex = i - 1;
                counters[realIndex]++;

                if (counters[realIndex] > relativeMax) relativeMax = counters[realIndex];
            }
        }

        final int finalBaseValue = baseValue;
        return Arrays.stream(counters)
                .map(i -> finalBaseValue + i)
                .toArray();
    }

    // Fill array with max value only when the next value is not another set counters to max
    public int[] solutionFillArrayWithMaxValueWhenNecessary(int N, int[] A) {
        int[] counters = new int[N];

        boolean pendingSetMax = false;
        int maxValue = 0;
        for (int i : A) {
            if (i == N + 1) {
                pendingSetMax = true;
            } else {
                if (pendingSetMax) {
                    pendingSetMax = false;
                    Arrays.fill(counters, maxValue);
                }

                int realIndex = i - 1;
                counters[realIndex]++;

                if (counters[realIndex] > maxValue) maxValue = counters[realIndex];
            }
        }

        if (pendingSetMax) {
            Arrays.fill(counters, maxValue);
        }

        return counters;
    }

    public int[] solutionWithCounter(int N, int[] A) {
        Counter[] counters = new Counter[N];
        for (int i = 0; i < counters.length; i++) {
            counters[i] = new Counter();
        }

        int baseValue = 0;
        int maxValue = 0;
        for (int i : A) {
            if (i == N + 1) {
                baseValue = maxValue;
            } else {
                int realIndex = i - 1;
                int accumulated = counters[realIndex].increaseAcc(baseValue);

                if (accumulated > maxValue) maxValue = accumulated;
            }
        }

        final int finalBaseValue = baseValue;
        return Arrays.stream(counters)
                .mapToInt(c -> c.getFinalValue(finalBaseValue))
                .toArray();
    }

    private static class Counter {
        int currentBase;
        int relativeAcc;

        public int increaseAcc(int base) {
            if (base == currentBase) relativeAcc++;
            else {
                currentBase = base;
                relativeAcc = 1;
            }

            return currentBase + relativeAcc;
        }

        public int getFinalValue(int base) {
            if (base != currentBase) {
                currentBase = base;
                relativeAcc = 0;
            }
            return currentBase + relativeAcc;
        }

        public int getCurrentBase() {
            return currentBase;
        }

        public int getRelativeAcc() {
            return relativeAcc;
        }

        @Override
        public String toString() {
            return Integer.toString(currentBase + relativeAcc);
        }
    }
}
