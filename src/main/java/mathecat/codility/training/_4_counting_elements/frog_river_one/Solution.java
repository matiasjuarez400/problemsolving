package mathecat.codility.training._4_counting_elements.frog_river_one;

import java.util.HashSet;
import java.util.Set;

class Solution {
    public int run(int riverSize, int[] leaves) {
        Set<Integer> filledPositions = new HashSet<>();

        for (int i = 0; i < leaves.length; i++) {
            int leave = leaves[i];

            filledPositions.add(leave);

            if (filledPositions.size() == riverSize) return i;
        }

        return -1;
    }
}
