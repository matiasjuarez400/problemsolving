package mathecat.codility.training._4_counting_elements.perm_check;

import java.util.HashSet;
import java.util.Set;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
    }

    public int solution(int[] A) {
        // write your code in Java SE 8
        Set<Integer> uniqueNumbers = new HashSet<>();

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        boolean containsOne = false;

        for (int i : A) {
            uniqueNumbers.add(i);

            if (i < min) min = i;
            if (i > max) max = i;

            if (i == 1) containsOne = true;
        }

        if (uniqueNumbers.size() != A.length) return 0;
        if (!containsOne) return 0;

        int minMaxDistance = max - min;

        return minMaxDistance + 1 == uniqueNumbers.size() ? 1 : 0;
    }
}
