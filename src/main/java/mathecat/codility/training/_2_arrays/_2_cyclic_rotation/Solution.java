package mathecat.codility.training._2_arrays._2_cyclic_rotation;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] A = {3, 8, 9, 7, 6};
        int K = 3;

        System.out.println(Arrays.toString(solution.solution(A, K)));
    }

    public int[] solution(int[] A, int K) {
        if (A == null || A.length == 0) return A;

        final int effectiveRotation = K % A.length;

        final int[] output = new int[A.length];

        int newPosition;
        for (int i = 0; i < A.length; i++) {
            newPosition = i + effectiveRotation;
            if (newPosition > output.length - 1) {
                newPosition -= output.length;
            }

            output[newPosition] = A[i];
        }

        return output;
    }
}
