package mathecat.codility.training._2_arrays._3_odd_occurrences_in_array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.sortingFirst(new int[] {9, 3, 9, 3, 9, 7, 9}));
    }

    public int solution_stream(int[] A) {
        final Map<Integer, Long> occurrencesByNumber = Arrays.stream(A)
                .boxed()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        return occurrencesByNumber.entrySet().stream()
                .filter(e -> e.getValue() % 2 != 0)
                .findFirst()
                .map(Map.Entry::getKey)
                .orElse(-1);
    }

    public int solution_parallelStream(int[] A) {
        final Map<Integer, Long> occurrencesByNumber = Arrays.stream(A).parallel()
                .boxed()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        return occurrencesByNumber.entrySet().parallelStream()
                .filter(e -> e.getValue() % 2 != 0)
                .findAny()
                .map(Map.Entry::getKey)
                .orElse(-1);
    }

    public int solution_setContainer(int[] A) {
        Set<Integer> unpaired = new HashSet<>();
        for (int i : A) {
            if (!unpaired.remove(i)) {
                unpaired.add(i);
            }
        }

        return unpaired.stream().findFirst().orElse(-1);
    }

    public int sortingFirst(int[] A) {
        Arrays.sort(A);

        int current = A[0];
        int count = 1;

        for (int i = 1; i < A.length; i++) {
            if (A[i] != current) {
                if (count % 2 != 0) return current;
                current = A[i];
                count = 1;
            } else {
                count++;
            }
        }

        return count % 2 != 0 ? current : -1;
    }

    public int multipleSets(int[] A) {
        final int steps = 4;
        final double stepSize = A.length * 1.0 / steps;

        int left = 0;
        int right = (int) stepSize;

        List<Set<Integer>> sets = new ArrayList<>();
        for (int i = 0; i < steps; i++) {
            Set<Integer> set = new HashSet<>();
            sets.add(set);
            for (int j = left; j < right; j++) {
                int next = A[j];
                if (!set.remove(next)) {
                    set.add(next);
                }
            }

            left = right;
            right = (int)(stepSize * (i + 2));
        }

        for (int i = 0; i < steps; i++) {
            Set<Integer> leftSet = sets.get(i);
            Set<Integer> rightSet;

            Iterator<Integer> it = leftSet.iterator();
            while (it.hasNext()) {
                Integer next = it.next();
                for (int j = i + 1; j < steps; j++) {
                    rightSet = sets.get(j);
                    if (rightSet.contains(next)) {
                        it.remove();
                        rightSet.remove(next);
                        break;
                    }
                }
            }
        }

        for (Set<Integer> set : sets) {
            if (set.size() == 1) return set.stream().findAny().orElse(-1);
        }

        return -1;
    }
}
