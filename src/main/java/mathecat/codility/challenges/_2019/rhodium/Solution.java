package mathecat.codility.challenges._2019.rhodium;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] T = {2, 0, 2, 2, 1, 0};

        System.out.println(solution.solution(T));
    }

    public int solution(int[] T) {
        // write your code in Java SE 8
        PathMemory pathMemory = new PathMemory(T.length);
        PathFinder pathFinder = new PathFinder(pathMemory);
        Graph graph = new Graph(pathFinder, pathMemory);

        for (int i = 0; i < T.length; i++) {
            graph.addNodesWithConnection(i, T[i]);
        }

        List<int[]> validPairs = new ArrayList<>();
        for (int A = 0; A < T.length; A++) {
            boolean isValidPair = true;
            for (int B = A; B < T.length; B++) {
                List<Node> path = graph.findPathBetweenNodes(A, B);
                if (path.isEmpty()) isValidPair = false;

                for (Node node : path) {
                    int J = node.getValue();
                    if (!(A <= J && J <= B)) {
                        isValidPair = false;
                        break;
                    }
                }

                if (isValidPair) {
                    validPairs.add(new int[] {A, B});
                } else {
                    break;
                }
            }
        }

        return validPairs.size();
    }

    private static class Graph {
        private final Map<Integer, Node> nodeByValue;
        private final PathFinder pathFinder;
        private final PathMemory pathMemory;

        public Graph(PathFinder pathFinder, PathMemory pathMemory) {
            this.pathFinder = pathFinder;
            this.pathMemory = pathMemory;
            this.nodeByValue = new HashMap<>();
        }

        public void addNodesWithConnection(int node1, int node2) {
            Node n1 = nodeByValue.compute(node1, (k, v) -> v == null ? new Node(node1) : v);
            Node n2 = nodeByValue.compute(node2, (k, v) -> v == null ? new Node(node2) : v);

            n1.addNode(n2);
            n2.addNode(n1);
        }

        public Node getNode(int node) {
            return this.nodeByValue.get(node);
        }

        public List<Node> findPathBetweenNodes(int node1, int node2) {
            List<Integer> memoryPath = pathMemory.getPathBetweenNodes(node1, node2);

            if (memoryPath != null) {
                return memoryPath.stream()
                        .map(integer -> nodeByValue.get(integer))
                        .collect(Collectors.toList());
            }

            Node n1 = nodeByValue.get(node1);
            Node n2 = nodeByValue.get(node2);

            List<Node> pathFound = pathFinder.findPathBetweenNodes(n1, n2);
            return pathFound;
        }
    }

    private static class PathFinder {
        private PathMemory pathMemory;

        public PathFinder(PathMemory pathMemory) {
            this.pathMemory = pathMemory;
        }

        public List<Node> findPathBetweenNodes(Node n1, Node n2) {
            List<Node> path = runSearch(n1, n2, new ArrayList<>(), new HashSet<>());
            return path;
        }

        private List<Node> runSearch(Node current, Node target, List<Node> currentPath, Set<Node> visitedNodes) {
            if (visitedNodes.contains(current)) return Collections.emptyList();
            visitedNodes.add(current);
            currentPath.add(current);
            pathMemory.processPath(currentPath);

            if (current.equals(target)) {
                return currentPath;
            }

            List<Node> pathResult = Collections.emptyList();
            for (Node nextNode : current.getDirectNodes()) {
                List<Node> currentPathCopy = new ArrayList<>(currentPath);
                pathResult = runSearch(nextNode, target, currentPathCopy, visitedNodes);
                if (!pathResult.isEmpty()) break;
            }

            return pathResult;
        }
    }

    private static class PathMemory {
        private Map<Integer, Map<Integer, List<Integer>>> learnedPaths;
        private int totalNodes;

        public PathMemory(int totalNodes) {
            this.learnedPaths = new HashMap<>();
            this.totalNodes = totalNodes;
        }

        public void processPath(List<Node> path) {
            storePathInfo(path);

            List<Node> reversed = new ArrayList<>(path);
            Collections.reverse(reversed);
            storePathInfo(reversed);
        }

        private void storePathInfo(List<Node> path) {
            for (int i = 0; i < path.size(); i++) {
//                Map<Integer, List<Integer>> pathsForN1 = learnedPaths.get(path.get(i).getValue());
//                if (pathsForN1 != null && pathsForN1.size() == totalNodes) break;

                for (int j = i; j < path.size(); j++) {
                    Map<Integer, List<Integer>> toEndNodePath = learnedPaths.compute(path.get(i).getValue(), (k, v) -> v == null ? new HashMap<>() : v);
                    toEndNodePath.put(path.get(j).getValue(), path.subList(i, j + 1).stream().map(Node::getValue).collect(Collectors.toList()));
                }
            }
        }

        public List<Integer> getPathBetweenNodes(int n1, int n2) {
            if (learnedPaths.get(n1) != null && learnedPaths.get(n1).get(n2) != null) {
                return learnedPaths.get(n1).get(n2);
            }
            return null;
        }
    }

    private static class Node {
        private Set<Node> directNodes;
        private int value;

        public Node(int value) {
            this.value = value;
            this.directNodes = new HashSet<>();
        }

        public void addNode(Node node) {
            this.directNodes.add(node);
        }

        public Set<Node> getDirectNodes() {
            return directNodes;
        }

        public int getValue() {
            return value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node node = (Node) o;
            return value == node.value;
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }

        @Override
        public String toString() {
            return "Node{" +
                    "value=" + value +
                    '}';
        }
    }
}
