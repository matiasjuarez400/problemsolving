package mathecat.codesignal.companyChallenges.dropbox.B_campusCup;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String[] emails = {"b@harvard.edu", "c@harvard.edu", "d@harvard.edu",
                "e@harvard.edu", "f@harvard.edu",
                "a@student.spbu.ru", "b@student.spbu.ru", "c@student.spbu.ru",
                "d@student.spbu.ru", "e@student.spbu.ru", "f@student.spbu.ru",
                "g@student.spbu.ru"};

        String[] sortedDomains = solution.campusCup(emails);

        System.out.println(Arrays.toString(sortedDomains));
    }

    String[] campusCup(String[] emails) {
        Map<String, Integer> pointsByDomain = countPointsByDomain(emails);

        Map<String, Integer> rewardsByDomain = calculateRewardByDomain(pointsByDomain);

        List<Map.Entry<String, Integer>> rewardEntries = rewardsByDomain.entrySet()
                .stream()
                .sorted((o1, o2) -> {
                    int rewardComparingResult = o2.getValue() - o1.getValue();
                    if (rewardComparingResult != 0) return rewardComparingResult;

                    return o1.getKey().compareTo(o2.getKey());
                })
                .collect(Collectors.toList());

        String[] output = new String[rewardEntries.size()];

        for (int i = 0; i < output.length; i++) {
            Map.Entry<String, Integer> entry = rewardEntries.get(i);
            output[i] = entry.getKey();
        }
        return output;
    }

    private Map<String, Integer> calculateRewardByDomain(Map<String, Integer> pointsByDomain) {
        Map<Integer, Integer> REWARD_BY_POINTS = new HashMap<>();
        REWARD_BY_POINTS.put(0, 0);
        REWARD_BY_POINTS.put(100, 3);
        REWARD_BY_POINTS.put(200, 8);
        REWARD_BY_POINTS.put(300, 15);
        REWARD_BY_POINTS.put(500, 25);

        Map<String, Integer> rewardByDomain = new HashMap<>();
        pointsByDomain.forEach((domain, domainPoints) -> {
            REWARD_BY_POINTS.forEach((pointsThreshold, reward) -> {
                if (domainPoints >= pointsThreshold) {
                    rewardByDomain.compute(domain, (k, v) -> v == null ? reward : v + reward);
                }
            });
        });

        return rewardByDomain;
    }

    private Map<String, Integer> countPointsByDomain(String[] emails) {
        Map<String, Integer> countersByDomain = new HashMap<>();

        Pattern DOMAIN_PATTERN = Pattern.compile("\\w+@([\\w\\.]+)");
        Matcher matcher = DOMAIN_PATTERN.matcher("");
        Arrays.stream(emails)
                .forEach(email -> {
                    matcher.reset(email);

                    if (matcher.find()) {
                        String domain = matcher.group(1);
                        countersByDomain.compute(domain, (k, v) -> v == null ? 20 : v + 20);
                    } else {
                        throw new RuntimeException("Unable to extract domain from email: " + email);
                    }
                });

        return countersByDomain;
    }
}
