package mathecat.codesignal.companyChallenges.dropbox.A_incorrectPasscodeAttempts;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
    }

    boolean incorrectPasscodeAttempts(String passcode, String[] attempts) {
        int acc = 0;
        for (String attempt : attempts) {
            if (passcode.equals(attempt)) {
                acc = 0;
            } else {
                acc++;
                if (acc >= 10) return true;
            }
        }
        return false;
    }
}
