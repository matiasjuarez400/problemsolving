package mathecat.codesignal.companyChallenges.uber.D_fancyRide;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int discount = 30;
        double[] fares = {0.3, 0.5, 0.7, 1, 1.3};

        solution.fancyRide(30, fares);
    }

    String fancyRide(int l, double[] fares) {
        final String[] uberNames = {"UberX", "UberXL", "UberPlus", "UberBlack", "UberSUV"};

        final double discount = 20;

        for (int i = fares.length - 1; i >= 0; i--) {
            if (l * fares[i] <= discount) {
                return uberNames[i];
            }
        }

        return "You shall not travel!";
    }
}
