package mathecat.codesignal.companyChallenges.uber.F_closestLocation;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.test3();
    }

    void test3() {
        String address = "Cat";
        int[][] objects = {
                {-3,0}, 
                 {1,3}, 
                 {2,1,2,4}, 
                 {-4,-3,6,-3}
        };
        String[] names = {"Bat building",
                "Cats exhibition",
                "Cramp",
                "cat avenue"};

        System.out.println(closestLocation(address, objects, names));
    }

    void test5() {
        String address = "Location";
        int[][] objects = {
                {1,1},
                {3,3}
        };
        String[] names = {"Locati",
                "Location"};

        System.out.println(closestLocation(address, objects, names));
    }

    void test4() {
        String address = "Location";
        int[][] objects = {
                {5,-10,5,10}, 
                {3,3}
        };
        String[] names = {"loocationnnn",
                "lcationaaaa"};

        System.out.println(closestLocation(address, objects, names));
    }

    void test1() {
        String address = "Cat";
        int[][] objects = {
                {-2, 0}, {1, 2}, {2, 1, 2, 4}, {-3, -1, 4, -1}
        };
        String[] names = {"Bat building", "Cast exhibition", "At street", "Cat avenue"};

        System.out.println(closestLocation(address, objects, names));
    }

    String closestLocation(String address, int[][] objects, String[] names) {
        StringComparator stringComparator = new StringComparator();

        int[] validIndexes = IntStream.range(0, names.length)
                .filter(i -> stringComparator.isSimilar(address, names[i]))
                .toArray();

        if (validIndexes.length == 0) return null;

        final Point center = new Point(0, 0);
        List<ObjectInMap> objectsInMap = Arrays.stream(validIndexes)
                .mapToObj(i -> new ObjectInMap(names[i], objects[i]))
                .sorted(Comparator.comparing(objectInMap -> objectInMap.calculateDistance(center)))
                .collect(Collectors.toList());

        List<Double> distancesToObjects = objectsInMap.stream()
                .map(objectInMap -> objectInMap.calculateDistance(center))
                .collect(Collectors.toList());

        return objectsInMap.get(0).getName();
    }

    private static class ObjectInMap {
        private final String name;
        private Point point;
        private Segment segment;

        public ObjectInMap(String name, int[] descriptor) {
            this.name = name;

            if (descriptor.length == 2) {
                this.point = new Point(descriptor[0], descriptor[1]);
            } else if (descriptor.length == 4) {
                this.segment = new Segment(
                    new Point(descriptor[0], descriptor[1]),
                    new Point(descriptor[2], descriptor[3])
                );
            } else {
                throw new IllegalArgumentException("Unexpected descriptor");
            }
        }

        public Double calculateDistance(Point p) {
            if (this.point != null) {
                return this.point.calculateDistance(p);
            } else {
                return this.segment.calculateDistanceToPoint(p);
            }
        }

        public String getName() {
            return name;
        }

        public Point getPoint() {
            return point;
        }

        public Segment getSegment() {
            return segment;
        }
    }

    private static void lineTestStringComparator() {
        StringComparator stringComparator = new StringComparator();

        System.out.println(stringComparator.hasOneExtraCharacter("Cataluña", "Caaluña"));
    }

    private static void segmentTesting() {
        Point center = new Point(0, 0);

        Segment segment1 = new Segment(
                new Point(2, 1),
                new Point(2, 3)
        );

        System.out.println(segment1.calculateDistanceToPoint(center));

        Segment segment2 = new Segment(
                new Point(2, -300),
                new Point(2, 300)
        );

        System.out.println(segment2.calculateDistanceToPoint(center));
    }

    private void lineTesting() {
        Line l1 = new Line(new Point(1, 0), new Point(1, 5));
        Line l2 = new Line(new Point(0, 3), new Point(4, 3));

        System.out.println(l1.evaluate(2.0));
        System.out.println(l1.evaluate(1.0));

        System.out.println(l2.evaluate(2.0));
        System.out.println(l2.evaluate(1.0));

        Point intersection = l1.calculateIntersectionPoint(l2);
        System.out.println(intersection);

        Line l3 = new Line(new Point(0, 0), new Point(4, 2.476));
        intersection = l1.calculateIntersectionPoint(l3);
        System.out.println(intersection);
        System.out.println(l3.calculateIntersectionPoint(l1));
    }

    private static class StringComparator {
        public boolean isSimilar(String base, String target) {
            base = base.toUpperCase();
            target = target.toUpperCase().substring(0, Integer.min(base.length(), target.length())).trim();

            return base.equals(target) ||
                    differsByCharacters(base, target, 1) ||
                    hasOneExtraCharacter(base, target) ||
                    hasOneExtraCharacter(target, base);
        }

        private boolean hasOneExtraCharacter(String base, String target) {
            List<Character> targetChars = target.chars()
                    .mapToObj(i -> (char) i)
                    .collect(Collectors.toList());

            int diffChars = 0;
            for (char c : base.toCharArray()) {
                if (targetChars.isEmpty()) {
                    diffChars++;
                } else if (targetChars.get(0) == c) {
                    targetChars.remove(0);
                } else {
                    diffChars++;
                }
            }

            return diffChars == 1;
        }

        private boolean differsByCharacters(String base, String target, int differentCharsAllowed) {
            if (base.length() == target.length()) {
                int differencesFound = 0;
                for (int i = 0; i < base.length(); i++) {
                    if (base.charAt(i) != target.charAt(i)) {
                        differencesFound++;
                        if (differencesFound > differentCharsAllowed) return false;
                    }
                }

                return true;
            }

            return false;
        }
    }

    private static class Segment {
        private Line line;
        private Point p1;
        private Point p2;
        private final static Double EPSILON = 0.001;

        public Segment(Point p1, Point p2) {
            this.p1 = p1;
            this.p2 = p2;
            this.line = new Line(p1, p2);
        }

        public Double calculateDistanceToPoint(Point p) {
            Line perpendicularLine = line.createPerpendicularLineWithPoint(p);
            Point intersectionPoint = this.line.calculateIntersectionPoint(perpendicularLine);

            if (isPointInsideSegment(intersectionPoint)) {
                return intersectionPoint.calculateDistance(p);
            } else {
                return Double.min(
                    p1.calculateDistance(p),
                    p2.calculateDistance(p)
                );
            }
        }

        private boolean isPointInsideSegment(Point p) {
            Double accumulatedDistance = p1.calculateDistance(p) + p.calculateDistance(p2);
            Double segmentSize = p1.calculateDistance(p2);
            double difference = accumulatedDistance - segmentSize;

            return -EPSILON < difference && difference < EPSILON;
        }
    }

    private static class Line {
        private Double slope;
        private Double yIntercept;
        private Double xIntercept;

        public Line(Double slope, Double yIntercept) {
            if (slope == null || yIntercept == null) throw new IllegalArgumentException("Slope and yIntercept must be specified to use this constructor");

            this.slope = slope;
            this.yIntercept = yIntercept;
            this.xIntercept = calculateXIntercept();
        }

        public Line(Double slope, Double yIntercept, Double xIntercept) {
            this.slope = slope;
            this.yIntercept = yIntercept;
            this.xIntercept = xIntercept;
        }

        public Line(Point p1, Point p2) {
            double xDistance = p1.getX() - p2.getX();
            double yDistance = p1.getY() - p2.getY();

            if (xDistance == 0) {
                this.xIntercept = p1.getX();
            } else if (yDistance == 0) {
                this.slope = 0.0;
                this.yIntercept = p1.getY();
            } else {
                this.slope = (p1.getY() - p2.getY()) / (p1.getX() - p2.getX());
                this.yIntercept = p1.getY() - this.slope * p1.getX();
                xIntercept = calculateXIntercept();
            }
        }

        public Point calculateIntersectionPoint(Line that) {
            if (this.slope == null && that.slope == null) return null;

            Double xIntercept = null;
            Double yIntercept = null;
            if (this.slope == null) {
                xIntercept = this.xIntercept;
                yIntercept = that.evaluate(xIntercept);
            }
            if (that.slope == null) {
                xIntercept = that.xIntercept;
                yIntercept = this.evaluate(xIntercept);
            }

            if (xIntercept != null && yIntercept != null) {
                return new Point(xIntercept, yIntercept);
            }

            xIntercept = (this.yIntercept - that.yIntercept) / (that.slope - this.slope);
            yIntercept = this.evaluate(xIntercept);

            return new Point(xIntercept, yIntercept);
        }

        public Line createPerpendicularLineWithPoint(Point p) {
            if (this.slope == null) {
                return new Line(0.0, p.getY(), null);
            } else if (this.slope == 0) {
                return new Line(null, null, p.getX());
            } else {
                Double newSlope = -1/this.slope;
                Double yIntercept = evaluate(0.0);
                return new Line(newSlope, yIntercept);
            }
        }

        private Double evaluate(Double x) {
            if (slope == null) {
                if (x.equals(this.xIntercept)) return xIntercept;
                else return null;
            }
            return slope * x + yIntercept;
        }

        public Double calculateXIntercept() {
            if (slope == 0) return null;
            return -yIntercept / slope;
        }
    }

    private static class Point {
        private final double x;
        private final double y;

        public Point(double x, double y) {
            this.x = x;
            this.y = y;
        }

        public double getX() {
            return x;
        }

        public double getY() {
            return y;
        }

        public double calculateDistance(Point another) {
            return Math.sqrt(Math.pow(this.x - another.x, 2) + Math.pow(this.y - another.y, 2));
        }

        @Override
        public String toString() {
            return "Point{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }
    }
}
