package mathecat.codesignal.companyChallenges.uber.A_fareEstimator;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int rideDistance = 7;
        int rideTime = 30;
        double[] costPerMinute = {0.2, 0.35, 0.4, 0.45};
        double[] costPerMile = {1.1, 1.8, 2.3, 3.5};

        System.out.println(Arrays.toString(solution.fareEstimator(rideTime, rideDistance, costPerMinute, costPerMile)));
    }

    double[] fareEstimator(int ride_time, int ride_distance, double[] cost_per_minute, double[] cost_per_mile) {
        double[] estimates = new double[cost_per_mile.length];
        for (int i = 0; i < cost_per_mile.length; i++) {
            estimates[i] = ride_distance * cost_per_mile[i] + ride_time * cost_per_minute[i];
        }
        return estimates;
    }
}
