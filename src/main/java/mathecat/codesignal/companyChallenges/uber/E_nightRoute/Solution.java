package mathecat.codesignal.companyChallenges.uber.E_nightRoute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.test5();
    }

    void test5() {
        int[][] city = {
                {-1,-1,19,8,18,-1,-1,-1,-1,-1}, 
                 {10,6,4,7,0,10,18,-1,0,-1}, 
                 {-1,-1,15,-1,17,3,-1,14,16,3}, 
                 {4,19,3,15,8,4,6,11,5,8}, 
                 {5,3,10,-1,0,14,15,1,16,5}, 
                 {-1,8,-1,-1,5,-1,5,0,1,-1}, 
                 {-1,18,-1,19,2,-1,10,-1,8,6}, 
                 {14,8,12,16,-1,-1,0,16,15,17}, 
                 {4,5,1,12,0,4,8,15,1,-1}, 
                 {13,7,17,-1,4,13,16,3,12,9}
        };

        // expected: 14
        System.out.println(nightRoute(city));
    }

    void test2() {
        int[][] city = {
                {-1,5,2,15}, 
                 {2,-1,0,3}, 
                 {1,-1,-1,9}, 
                 {0,0,0,-1}
        };

        System.out.println(nightRoute(city));
    }

    void test1() {
        int[][] city = {
                {-1, 5, 20},
                {21, -1, 10},
                {-1, 1, -1}
        };

        System.out.println(nightRoute(city));
    }

    int nightRoute(int[][] city) {
        Graph graph = GraphBuilder.buildGraph(city);
        PathFinder pathFinder = new PathFinder(graph);

        List<List<Node>> pathsBetweenCities = pathFinder.findAllPathsBetweenCities(0, city.length - 1);
        if (pathsBetweenCities.size() == 0) return -1;

        int minDistance = Integer.MAX_VALUE;
        for (List<Node> pathBetweenCities : pathsBetweenCities) {
            int accumulatedDistance = 0;
            for (int i = 0; i < pathBetweenCities.size() - 1; i++) {
                Node origin = pathBetweenCities.get(i);
                Node destination = pathBetweenCities.get(i + 1);

                Connection connectionUsed = origin.getConnectionList().stream()
                        .filter(connection -> connection.getDestination().equals(destination))
                        .findFirst().orElseThrow(() -> new IllegalStateException("Something is wrong with your algorithm"));

                accumulatedDistance += connectionUsed.getValue();
            }

            if (accumulatedDistance < minDistance) minDistance = accumulatedDistance;
        }

        return minDistance;
    }

    private static class PathFinder {
        private final Graph graph;

        public PathFinder(Graph graph) {
            this.graph = graph;
        }

        public List<List<Node>> findAllPathsBetweenCities(int orig, int dest) {
            Node origNode = graph.getNodeByCity(orig);
            Node destNode = graph.getNodeByCity(dest);
            return findAllPathsBetweenNodes(origNode, destNode);
        }

        public List<List<Node>> findAllPathsBetweenNodes(Node orig, Node dest) {
            List<List<Node>> pathsFound = new ArrayList<>();
            searchPaths(orig, dest, new ArrayList<>(), pathsFound, new HashSet<>());
            return pathsFound;
        }

        private void searchPaths(Node current, Node destination, List<Node> currentPath, List<List<Node>> pathsFound, Set<Node> visitedNodes) {
            currentPath.add(current);
            if (current.equals(destination)) {
                pathsFound.add(currentPath);
            } else {
                if (!visitedNodes.contains(current)) {
                    visitedNodes.add(current);
                    for (Connection connection : current.getConnectionList()) {
                        searchPaths(connection.getDestination(), destination, new ArrayList<>(currentPath), pathsFound, new HashSet<>(visitedNodes));
                    }
                }

            }
        }
    }

    private static class GraphBuilder {
        public static Graph buildGraph(int[][] city) {
            Graph graph = new Graph();

            for (int origin = 0; origin < city.length; origin++) {
                for (int destination = 0; destination < city[origin].length; destination++) {
                    int miles = city[origin][destination];
                    if (miles != -1) {
                        Node originNode = new Node(origin);
                        Node destinationNode = new Node(destination);
                        graph.createConnection(originNode, destinationNode, miles);
                    }
                }
            }

            return graph;
        }
    }

    private static class Graph {
        private final Map<Integer, Node> nodeByCity;

        public Graph() {
            this.nodeByCity = new HashMap<>();
        }

        public void createConnection(Node orig, Node dest, double value) {
            final Node storedOrigin = nodeByCity.computeIfAbsent(orig.getCity(), k -> orig);
            final Node storedDestination = nodeByCity.computeIfAbsent(dest.getCity(), k -> dest);

            final Connection connection = new Connection(storedOrigin, storedDestination, value);

            storedOrigin.addConnection(connection);
        }

        public Node getNodeByCity(int city) {
            return nodeByCity.get(city);
        }
    }

    private static class Connection {
        private final Node origin;
        private final Node destination;
        private final double value;

        public Connection(Node origin, Node destination, double value) {
            this.origin = origin;
            this.destination = destination;
            this.value = value;
        }

        public Node getOrigin() {
            return origin;
        }

        public Node getDestination() {
            return destination;
        }

        public double getValue() {
            return value;
        }
    }

    private static class Node {
        private final int city;
        private final List<Connection> connectionList;

        public Node(int city) {
            this.city = city;
            this.connectionList = new ArrayList<>();
        }

        public int getCity() {
            return city;
        }

        public List<Connection> getConnectionList() {
            return connectionList;
        }

        public void addConnection(Connection connection) {
            this.connectionList.add(connection);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node node = (Node) o;
            return city == node.city;
        }

        @Override
        public int hashCode() {
            return Objects.hash(city);
        }
    }
}
