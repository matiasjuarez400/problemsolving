package mathecat.codesignal.companyChallenges.uber.C_parkingSpot;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.test1();
        solution.test2();
        solution.test3();
    }

    void test2() {
        int[] carDimensions = {3, 2};
        int[][] parkingLot = {
                {1,0,1,0,1,0}, 
                 {1,0,0,0,1,0},
                 {0,0,0,0,0,1},
                 {1,0,0,0,1,1}
        };
        int[] luckySpot = {1, 1, 2, 3};

        boolean expected = false;
        boolean result = parkingSpot(carDimensions, parkingLot, luckySpot);

        if (expected == result) System.out.println("PASS");
        else System.out.println("FAIL");
    }

    void test3() {
        int[] carDimensions = {4, 1};
        int[][] parkingLot = {
                {1,0,1,0,1,0}, 
                 {1,0,0,0,1,0}, 
                 {0,0,0,0,0,1}, 
                 {1,0,0,0,1,1}
        };
        int[] luckySpot = {0, 3, 3, 3};

        boolean expected = true;
        boolean result = parkingSpot(carDimensions, parkingLot, luckySpot);

        if (expected == result) System.out.println("PASS");
        else System.out.println("FAIL");
    }

    void test1() {
        int[] carDimensions = {3, 2};
        int[][] parkingLot = {
                {1, 0, 1, 0, 1, 0},
                {0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 1},
                {1, 0, 1, 1, 1, 1}
        };
        int[] luckySpot = {1, 1, 2, 3};

        boolean expected = true;
        boolean result = parkingSpot(carDimensions, parkingLot, luckySpot);

        if (expected == result) System.out.println("PASS");
        else System.out.println("FAIL");
    }

    boolean parkingSpot(int[] carDimensions, int[][] parkingLotRaw, int[] luckySpot) {
        Car car = new Car(carDimensions);
        Spot spot = new Spot(luckySpot);
        ParkingLot parkingLot = new ParkingLot(parkingLotRaw);
        PathFinder pathFinder = new PathFinder();

        for (int row = spot.getMinY(); row <= spot.getMaxY(); row++) {
            for (int col = spot.getMinX(); col <= spot.getMaxX(); col++) {
                if (parkingLot.canPositionCarHorizontally(car, col, row, spot.getMaxX(), spot.getMaxY())) {
                    if (pathFinder.canGetToPositionHorizontally(car, parkingLot, col, row)) return true;
                } else if (parkingLot.canPositionCarVertically(car, col, row, spot.getMaxX(), spot.getMaxY())) {
                    if (pathFinder.canGetToPositionVertically(car, parkingLot, col, row)) return true;
                }
            }
        }

        return false;
    }

    private static class Spot {
        private final int minY;
        private final int maxY;
        private final int minX;
        private final int maxX;

        public Spot(int[] luckySpot) {
            minX = luckySpot[1];
            minY = luckySpot[0];
            maxX = luckySpot[3];
            maxY = luckySpot[2];
        }

        public int getMinY() {
            return minY;
        }

        public int getMaxY() {
            return maxY;
        }

        public int getMinX() {
            return minX;
        }

        public int getMaxX() {
            return maxX;
        }
    }

    private static class PathFinder {
        public boolean canGetToPositionHorizontally(Car car, ParkingLot parkingLot, int topLeftX, int topLeftY) {
            if (topLeftX < 0 || topLeftY < 0) return false;

            return canReachHorizontally(car, parkingLot, topLeftX, topLeftY, 1) ||
                    canReachHorizontally(car, parkingLot, topLeftX, topLeftY, -1);
        }

        public boolean canGetToPositionVertically(Car car, ParkingLot parkingLot, int topLeftX, int topLeftY) {
            if (topLeftX < 0 || topLeftY < 0) return false;

            return canReachVertically(car, parkingLot, topLeftX, topLeftY, 1) ||
                    canReachVertically(car, parkingLot, topLeftX, topLeftY, -1);
        }

        private boolean canReachHorizontally(Car car, ParkingLot parkingLot, int topLeftXDest, int topLeftYDest, int directionStep) {
            if (topLeftYDest + car.frontSize - 1 > parkingLot.getMaxValidY()) return false;

            int startingCol = directionStep > 0 ? 0 : parkingLot.getMaxValidX();

            for (int col = startingCol; col != topLeftXDest; col += directionStep) {
                for (int row = topLeftYDest; row < topLeftYDest + car.frontSize; row++) {
                    if (parkingLot.isOccupied(row, col)) return false;
                }
            }

            return true;
        }

        private boolean canReachVertically(Car car, ParkingLot parkingLot, int topLeftXDest, int topLeftYDest, int directionStep) {
            if (topLeftXDest + car.frontSize - 1 > parkingLot.getMaxValidX()) return false;

            int startingRow = directionStep > 0 ? 0 : parkingLot.getMaxValidY();

            for (int row = startingRow; row != topLeftYDest; row += directionStep) {
                for (int col = topLeftXDest; col < topLeftXDest + car.frontSize; col++) {
                    if (parkingLot.isOccupied(row, col)) return false;
                }
            }

            return true;
        }
    }

    private static class ParkingLot {
        private final boolean[][] currentState;

        public boolean isOccupied(int row, int col) {
            return currentState[row][col];
        }

        public int getMaxValidX() {
            return currentState[0].length - 1;
        }

        public int getMaxValidY() {
            return currentState.length - 1;
        }

        public ParkingLot(int[][] currentStateRaw) {
            this.currentState = new boolean[currentStateRaw.length][currentStateRaw[0].length];
            for (int row = 0; row < currentState.length; row++) {
                for (int col = 0; col < currentState[0].length; col++) {
                    currentState[row][col] = currentStateRaw[row][col] == 1;
                }
            }
        }

        public boolean canPositionCarHorizontally(Car car, int topLeftX, int topLeftY, int bottomRightX, int bottomRightY) {
            if (car.sideSize + topLeftX - 1 <= bottomRightX && car.frontSize + topLeftY - 1 <= bottomRightY) {
                for (int carRow = 0; carRow < car.frontSize; carRow++) {
                    for (int carCol = 0; carCol < car.sideSize; carCol++) {
                        if (currentState[carRow + topLeftY][carCol + topLeftX]) return false;
                    }
                }

                return true;
            }
            return false;
        }

        public boolean canPositionCarVertically(Car car, int topLeftX, int topLefty, int bottomRightX, int bottomRightY) {
            if (car.frontSize + topLeftX - 1 <= bottomRightX && car.sideSize + topLefty - 1 <= bottomRightY) {
                for (int carRow = 0; carRow < car.sideSize; carRow++) {
                    for (int carCol = 0; carCol < car.frontSize; carCol++) {
                        if (currentState[carRow + topLefty][carCol + topLeftX]) return false;
                    }
                }

                return true;
            }
            return false;
        }
    }

    private static class Car {
        private final int sideSize;
        private final int frontSize;

        public Car(int[] carDimensions) {
            this.sideSize = carDimensions[0];
            this.frontSize = carDimensions[1];
        }

        public int getSideSize() {
            return sideSize;
        }

        public int getFrontSize() {
            return frontSize;
        }
    }
}
