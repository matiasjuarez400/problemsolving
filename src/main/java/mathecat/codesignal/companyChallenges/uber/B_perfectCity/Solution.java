package mathecat.codesignal.companyChallenges.uber.B_perfectCity;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        double[] departure = {0.4, 1};
        double[] destination = {0.9, 3};

        System.out.println(solution.perfectCity(departure, destination));
    }

    double perfectCity(double[] departure, double[] destination) {
        double xMinDistance = calculateMinDistanceGoingToNearestIntegers(departure[0], destination[0]);
        double yMinDistance = calculateMinDistanceGoingToNearestIntegers(departure[1], destination[1]);
        return xMinDistance + yMinDistance;
    }

    private double calculateMinDistanceGoingToNearestIntegers(double origin, double destination) {
        int nearestBelowInteger = (int) origin;
        int nearestAboveInteger = ((int) (origin + 1));

        double distanceToBelowInteger = Math.abs(origin - nearestBelowInteger);
        double distanceToAboveInteger = Math.abs(nearestAboveInteger - origin);

        double distanceFromBelowIntegerToDestination = Math.abs(destination - nearestBelowInteger);
        double distanceFromAboveIntegerToDestination = Math.abs(nearestAboveInteger - destination);

        return Double.min(distanceFromAboveIntegerToDestination + distanceToAboveInteger, distanceFromBelowIntegerToDestination + distanceToBelowInteger);
    }
}
