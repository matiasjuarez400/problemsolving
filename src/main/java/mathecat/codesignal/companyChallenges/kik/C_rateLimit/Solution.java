package mathecat.codesignal.companyChallenges.kik.C_rateLimit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        
        int[][] sentBatches = {{1471040000, 736273, 827482, 2738283},
               {1471040005, 736273, 2738283},
               {1471040010, 827482, 2738283},
               {1471040015, 2738283},
               {1471040025, 827482},
               {1471046400, 736273, 827482, 2738283}};
        
        int[][] receivedMessages = {{1471040001, 2738283},
                    {1471040002, 2738283},
                    {1471040010, 827482},
                    {1471040020, 2738283}};

        int startingAllowance = 1;

        System.out.println(Arrays.toString(solution.rateLimit(sentBatches, receivedMessages, startingAllowance)));
    }

    int[] rateLimit(int[][] sentBatches, int[][] receivedMessages, int startingAllowance) {
        // Initialize events
        EventIdProvider eventIdProvider = new EventIdProvider();

        List<Event> events = new ArrayList<>();

        for(int[] sentEvent : sentBatches) {
            events.add(new Event(sentEvent, EventType.SENT, eventIdProvider.getNextSentId()));
        }

        for (int[] receivedEvent : receivedMessages) {
            events.add(new Event(receivedEvent, EventType.RECEIVED, eventIdProvider.getNextReceivedId()));
        }

        events = events.stream()
                .sorted(Comparator.comparing((Event event) -> event.getTime().getTimeInMillis()).thenComparing(event -> event.getEventType() == EventType.SENT ? 1 : -1))
                .collect(Collectors.toList());

        // Initialize users
        Set<Integer> userIds = new HashSet<>();
        for (Event event : events) {
            userIds.addAll(event.getUserIds());
        }

        Map<Integer, User> userById = new HashMap<>();
        for (Integer userId : userIds) {
            userById.putIfAbsent(userId, new User(userId, startingAllowance));
        }

        // Process events
        List<Integer> notSentMessages = new ArrayList<>();
        Integer currentDayOfYear = null;
        for (Event event : events) {
            if (currentDayOfYear == null) {
                currentDayOfYear = event.getTime().get(Calendar.DAY_OF_YEAR);
            } else {
                int eventDayOfYear = event.getTime().get(Calendar.DAY_OF_YEAR);
                if (eventDayOfYear > currentDayOfYear) {
                    userById.values().forEach(user -> user.setRateLimit(startingAllowance));
                }
                currentDayOfYear = eventDayOfYear;
            }

            if (event.getEventType() == EventType.RECEIVED) {
                for (Integer userId : event.getUserIds()) {
                    User user = userById.get(userId);
                    user.incrementLimit();
                }
            } else {
                List<User> affectedUssers = event.getUserIds().stream()
                        .map(userById::get)
                        .collect(Collectors.toList());

                if (affectedUssers.stream().anyMatch(user -> user.getRateLimit() == 0)) {
                    notSentMessages.add(event.getId());
                } else {
                    affectedUssers.forEach(User::decreaseLimit);
                }
            }
        }

        return notSentMessages.stream().mapToInt(i -> i).toArray();
    }

    private static class User {
        private final int id;
        private int rateLimit;

        public User(int id, int rateLimit) {
            this.id = id;
            this.rateLimit = rateLimit;
        }

        public void setRateLimit(int rateLimit) {
            this.rateLimit = rateLimit;
        }

        public int incrementLimit() {
            return ++rateLimit;
        }

        public int decreaseLimit() {
            return --rateLimit;
        }

        public int getRateLimit() {
            return this.rateLimit;
        }

        public int getId() {
            return id;
        }
    }

    private enum EventType {
        SENT, RECEIVED
    }

    private static class EventIdProvider {
        private int nextSentId = 0;
        private int nextReceivedId = 0;

        public int getNextSentId() {
            return nextSentId++;
        }

        public int getNextReceivedId() {
            return nextReceivedId++;
        }
    }

    private static class Event {
        private final Calendar time;
        private final List<Integer> userIds;
        private final EventType eventType;
        private final int id;

        public Event(int[] eventData, EventType eventType, int eventId) {
            time = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            time.setTimeInMillis(eventData[0] * 1000L);

            userIds = new ArrayList<>();
            for (int i = 1; i < eventData.length; i++) {
                userIds.add(eventData[i]);
            }

            this.eventType = eventType;

            this.id = eventId;
        }

        public Calendar getTime() {
            return time;
        }

        public boolean isMidnight() {
            return time.get(Calendar.HOUR_OF_DAY) == 0 && time.get(Calendar.MINUTE) == 0 && time.get(Calendar.SECOND) == 0;
        }

        public List<Integer> getUserIds() {
            return userIds;
        }

        public EventType getEventType() {
            return eventType;
        }

        public int getId() {
            return id;
        }

        @Override
        public String toString() {
            return String.format("Event{%s. %s. %s}", id, eventType, time.get(Calendar.HOUR_OF_DAY) + ":" + time.get(Calendar.MINUTE) + ":" + time.get(Calendar.SECOND));
        }
    }
}
