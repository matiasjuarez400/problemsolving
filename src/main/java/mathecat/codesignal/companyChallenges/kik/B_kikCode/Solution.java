package mathecat.codesignal.companyChallenges.kik.B_kikCode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[][][] result = solution.kikCode("1851027803204441");

        System.out.println(Arrays.deepToString(result));
    }

    int[][][] kikCode(String userId) {
        final List<Integer> chunkSizes = Arrays.asList(
            3, 4, 8, 10, 12, 15
        );

        final String binaryRepresentation = convertTo52BitsRepresentation(userId);
        final List<String> bitsPerCircumference = divide52BitsRepresentationInChuncks(chunkSizes, binaryRepresentation);
        final List<int[][]> outputList = createSectorsWithOutputNotation(bitsPerCircumference);

        int[][][] output = new int[outputList.size()][][];
        for (int i = 0; i < outputList.size(); i++) {
            output[i] = outputList.get(i);
        }

        return output;
    }

    private List<int[][]> createSectorsWithOutputNotation(List<String> bitesPerCircumference) {
        final List<int[][]> output = new ArrayList<>();

        for (int i = 0; i < bitesPerCircumference.size(); i++) {
            List<int[]> sectors = createSectors(bitesPerCircumference.get(i));
            List<int[][]> convertedSector = convertSectorsToOutputNotation(sectors, i + 1);
            output.addAll(convertedSector);
        }

        return output;
    }

    private List<int[][]> convertSectorsToOutputNotation(List<int[]> sectors, int circumferenceNumber) {
        final List<int[][]> output = new ArrayList<>();

        for (int[] sector : sectors) {
            int[][] translated = new int[2][2];
            translated[0][0] = circumferenceNumber;
            translated[0][1] = sector[0];
            translated[1][0] = circumferenceNumber;
            translated[1][1] = sector[1];
            output.add(translated);
        }

        return output;
    }

    private List<int[]> createSectors(String circumferenceBits) {
        final double sectorSize = 360f / circumferenceBits.length();

        final List<int[]> output = new ArrayList<>();

        int[] currentSector = null;
        for (int i = 0; i < circumferenceBits.length(); i++) {
            char current = circumferenceBits.charAt(i);

            if (current == '1') {
                if (currentSector == null) {
                    currentSector = new int[2];
                    currentSector[0] = i;
                    currentSector[1] = i + 1;
                } else {
                    currentSector[1]++;
                }
            } else {
                if (currentSector != null) {
                    output.add(currentSector);
                    currentSector = null;
                }
            }
        }

        if (currentSector != null) {
            output.add(currentSector);
        }

        // fix first sector joined with last sector
        if (output.size() >= 2) {
            final int lastIndex = output.size() - 1;
            final int[] firstSector = output.get(0);
            final int[] lastSector = output.get(lastIndex);

            if (firstSector[0] == 0 && lastSector[1] == circumferenceBits.length()) {
                output.remove(0);
                lastSector[1] += firstSector[1];
            }
        }

        // we convert the indexes we've stored in our array to polar coordinates
        for (int[] coordinates : output) {
            coordinates[0] *= sectorSize;
            coordinates[1] *= sectorSize;
        }

        return output;
    }

    private List<String> divide52BitsRepresentationInChuncks(List<Integer> chunkSizes, String representation) {
        int currentStart = 0;

        final List<String> output = new ArrayList<>();
        for (Integer chunkSize : chunkSizes) {
            output.add(representation.substring(currentStart, chunkSize + currentStart));
            currentStart += chunkSize;
        }

        return output;
    }

    private String convertTo52BitsRepresentation(String userId) {
        final long userIdLong = Long.parseLong(userId);

        final String binaryRepresentation = Long.toBinaryString(userIdLong);

        final int leadingZerosToAppend = 52 - binaryRepresentation.length();

        final String withLeadingZeros = repeatChar('0', leadingZerosToAppend) + binaryRepresentation;

        return new StringBuilder(withLeadingZeros).reverse().toString();
    }

    private String repeatChar(char c, int times) {
        return new String(new char[times]).replace('\0', c);
    }
}
