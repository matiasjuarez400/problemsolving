package mathecat.codesignal.companyChallenges.freedomFinantialNetwork.A_coverDebts;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.IntStream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int s = 50;
        int[] debts = {2, 2, 5};
        int[] interests = {200, 100, 150};

        System.out.println(solution.coverDebts(s, debts, interests));
    }

    double coverDebts(int s, final int[] debtsRaw, final int[] interests) {
        int[] descendingIndexInterest = IntStream.range(0, interests.length)
                .boxed()
                .sorted(Comparator.comparingInt(i -> interests[(int) i]).reversed())
                .mapToInt(i -> i)
                .toArray();

        double moneySpent = 0.0;
        double payDebtAmountPerMonth = s / 10.0;
        double[] debts = Arrays.stream(debtsRaw).mapToDouble(i -> i).toArray();

        while (Arrays.stream(debts).anyMatch(i -> i > 0)) {
            double payDebtAmountLeft = payDebtAmountPerMonth;

            for (int i = 0; i < descendingIndexInterest.length && payDebtAmountLeft > 0.0; i++) {
                int nextDebtIndex = descendingIndexInterest[i];
                double debt = debts[nextDebtIndex];
                double transactionAmount = Double.min(debt, payDebtAmountLeft);
                debts[nextDebtIndex] -= transactionAmount;
                payDebtAmountLeft -= transactionAmount;
            }

            for (int i = 0; i < debts.length; i++) {
                double debt = debts[i];
                int interest = interests[i];
                debts[i] += debt * (interest / 100.0);
            }

            moneySpent += payDebtAmountPerMonth - payDebtAmountLeft;
        }

        return moneySpent;
    }
}
