package mathecat.codesignal.companyChallenges.freedomFinantialNetwork.B_salesLeadScore;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String[] names = {"lead1", "lead2", "lead3", "lead4", "lead5"};
        int[] time = {250, 300, 250, 260, 310};
        int[] netValue = {1000, 800, 1100, 1200, 1000};
        boolean[] isOnVacation = {true, false, true, false, false};

        System.out.println(Arrays.toString(solution.salesLeadsScore(names, time, netValue, isOnVacation)));
    }

    String[] salesLeadsScore(String[] names, int[] time, int[] netValue, boolean[] isOnVacation) {
        List<SalesLeadStatistic> notOnVacationSalesLeads = IntStream.range(0, isOnVacation.length)
                .filter(i -> !isOnVacation[i])
                .mapToObj(i -> new SalesLeadStatistic(names[i], time[i], netValue[i]))
                .collect(Collectors.toList());

        return notOnVacationSalesLeads.stream()
                .sorted(SalesLeadStatistic.bestLeadComparator().reversed())
                .map(SalesLeadStatistic::getName)
                .toArray(String[]::new);
    }

    private static class SalesLeadStatistic {
        private final String name;
        private final int time;
        private final int netValue;
        private final double score;

        public SalesLeadStatistic(String name, int time, int netValue) {
            this.name = name;
            this.time = time;
            this.netValue = netValue;
            this.score = this.netValue * time / 365.0;
        }

        public static Comparator<SalesLeadStatistic> bestLeadComparator() {
            return (s1, s2) -> {
                if (s1.score > s2.score) return 1;
                else if (s1.score == s2.score) {
                    if (s1.time > s2.time) return 1;
                    else if (s1.time == s2.time) {
                        return s2.name.compareTo(s1.name);
                    }
                }

                return -1;
            };
        }

        public String getName() {
            return name;
        }
    }
}
