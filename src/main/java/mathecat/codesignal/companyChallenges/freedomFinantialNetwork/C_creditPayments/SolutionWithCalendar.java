package mathecat.codesignal.companyChallenges.freedomFinantialNetwork.C_creditPayments;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SolutionWithCalendar {
    private static final boolean SHOW_OUTPUT = false;
    private static final boolean ONLY_FIRST = false;

    public static void main(String[] args) throws ParseException {
        SolutionWithCalendar solution = new SolutionWithCalendar();

        List<Runnable> tests = Arrays.asList(
                solution::test5,
                solution::test4,
                solution::test2,
                solution::test1
        );

        boolean firstExecuted = false;
        for (Runnable runnable : tests) {
            if (firstExecuted && ONLY_FIRST) break;

            firstExecuted = true;

            runnable.run();
        }
    }

    void test5() {
        int creditAmount = 3550;
        int debtPerMonth = 250;
        String startDate = "01/2000",
                endDate = "01/2001";

        String[] history = {
                "$500 payment received on 01/01/2000",
                "$500 payment received on 02/02/2000",
                "$500 payment received on 03/03/2000",
                "$500 payment received on 04/04/2000",
                "$500 payment received on 05/05/2000",
                "$500 payment received on 06/06/2000",
                "$500 payment received on 07/07/2000",
                "$500 payment received on 09/09/2000"
        };

        String[] output = creditPayments(creditAmount, debtPerMonth, startDate, endDate, history);

        String[] expected = {
                "Leave as it is, 250.",
                "Leave as it is, 250.",
                "Remove one year, 214.",
                "Remove one year, 178.",
                "Remove one year, 142.",
                "Remove one year, 106.",
                "Remove one year, 70.",
                "Leave as it is, 70.",
                "Leave as it is, 70.",
                "Leave as it is, 70.",
                "Add one year, 106.",
                "Add one year, 142.",
                "Add one year, 178."
        };

        verifyOutput(output, expected);
    }

    void test4()  {
        int creditAmount = 3550;
        int debtPerMonth = 250;
        String startDate = "01/2000",
                endDate = "01/2001";

        String[] history = {
                "$500 payment received on 01/16/2000",
                "$500 payment received on 02/16/2000",
                "$500 payment received on 03/16/2000",
                "$500 payment received on 04/16/2000",
                "$500 payment received on 05/16/2000",
                "$500 payment received on 06/16/2000",
                "$500 payment received on 07/16/2000",
                "$500 payment received on 09/16/2000"
        };

        String[] output = creditPayments(creditAmount, debtPerMonth, startDate, endDate, history);

        String[] expected = {
                "Leave as it is, 250.",
                "Leave as it is, 250.",
                "Add one year, 286.",
                "Add one year, 322.",
                "Add one year, 358.",
                "Add one year, 394.",
                "Add one year, 430.",
                "Add one year, 466.",
                "Add one year, 502.",
                "Add one year, 538.",
                "Add one year, 574.",
                "Add one year, 610.",
                "Add one year, 646."
        };

        verifyOutput(output, expected);
    }

    void test2() {
        int creditAmount = 10000;
        int debtPerMonth = 1000;
        String startDate = "12/2018",
                endDate = "02/2019";

        String[] history = {
                "$3000 payment received on 01/16/2019"
        };

        String[] output = creditPayments(creditAmount, debtPerMonth, startDate, endDate, history);

        String[] expected = {
                "Leave as it is, 1000.",
                "Leave as it is, 1000.",
                "Add one year, 1100."
        };

        verifyOutput(output, expected);
    }

    void test1() {
        int creditAmount = 10000;
        int debtPerMonth = 1000;
        String startDate = "01/2019",
                endDate = "08/2019";

        String[] history = {
                "$1000 payment received on 01/16/2019",
                "$2400 payment received on 03/20/2019",
                "$4000 payment received on 04/25/2019",
                "$3300 payment received on 05/06/2019",
                "$1500 payment received on 08/15/2019"
        };

        String[] output = creditPayments(creditAmount, debtPerMonth, startDate, endDate, history);

        String[] expected = {
                "Leave as it is, 1000.",
                "Leave as it is, 1000.",
                "Add one year, 1100.",
                "Add one year, 1200.",
                "Remove one year, 1100.",
                "Leave as it is, 1100.",
                "Add one year, 1200.",
                "Add one year, 1300."
        };

        verifyOutput(output, expected);
    }

    void verifyOutput(String[] output, String[] expected) {
        if (SHOW_OUTPUT) {
            System.out.println("**********************OUTPUT*********************");
            for (String suggestion : output) {
                System.out.println(suggestion);
            }

            System.out.println("**********************EXPECTED*********************");
            for (String ex : expected) {
                System.out.println(ex);
            }
        }

        for (int i = 0; i < output.length; i++) {
            String outputValue = output[i];
            String expectedValue = expected[i];

            if (!outputValue.equals(expectedValue)) {
                System.out.println(String.format("INDEX[%s] _ OUTPUT: [%s] --- EXPECTED: [%s]", i, outputValue, expectedValue));
                return;
            }
        }

        System.out.println("PASSED");
    }

    String[] creditPayments(int a, int b, String startDateRaw, String endDateRaw, String[] history) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy");

        final Date startDate = parseDate(startDateRaw, sdf);
        final Date endDate = parseDate(endDateRaw, sdf);

        final List<Payment> payments = Arrays.stream(history)
                .map(Payment::new)
                .sorted(Comparator.comparing(Payment::getDate))
                .collect(Collectors.toList());

        final int historySize = 3;
        final PaymentHistory paymentHistory = new PaymentHistory(historySize);

        SuggestionSystem suggestionSystem = new SuggestionSystem(paymentHistory);

        return suggestionSystem.createSuggestions(payments, a, b, startDate, endDate);
    }



    private static class SuggestionSystem {
        private final PaymentHistory paymentHistory;
        private static final String LEAVE_MESSAGE = "Leave as it is, %s.";
        private static final String ADD_MESSAGE = "Add one year, %s.";
        private static final String REMOVE_MESSAGE = "Remove one year, %s.";

        private enum Suggestion {
            ADD, REMOVE, LEAVE
        }

        public SuggestionSystem(PaymentHistory paymentHistory) {
            this.paymentHistory = paymentHistory;
        }

        public String[] createSuggestions(final List<Payment> paymentsInput, double creditAmount, double debtPerMonth, Date startDate, Date endDate) {
            paymentHistory.clearHistory();
            final List<Payment> payments = fillMissingMonths(paymentsInput, startDate, endDate);

            double currentDebtPerMonth = debtPerMonth;

            List<String> suggestionsOutput = new ArrayList<>();

            suggestionsOutput.add(buildSuggestionMessage(currentDebtPerMonth, Suggestion.LEAVE));
            this.paymentHistory.addPayment(payments.get(0));

            for (int i = 1; i < payments.size(); i++) {
                Payment payment = payments.get(i);
                this.paymentHistory.addPayment(payment);
                double d = calculateD(payments);
                Suggestion suggestion = calculateSuggestion(currentDebtPerMonth);

                if (suggestion == Suggestion.ADD) {
                    creditAmount = increaseValueByPercentage(creditAmount, 1);
//                    currentDebtPerMonth = increaseValueByPercentage(currentDebtPerMonth, 1);
                    currentDebtPerMonth = calculateNewDebtPerMonth(currentDebtPerMonth, creditAmount, d);
                } else if (suggestion == Suggestion.REMOVE) {
                    creditAmount = increaseValueByPercentage(creditAmount, -1);
                    currentDebtPerMonth = calculateNewDebtPerMonth(currentDebtPerMonth, creditAmount, -d);
//                    currentDebtPerMonth = increaseValueByPercentage(currentDebtPerMonth, -1);
                }

                suggestionsOutput.add(buildSuggestionMessage(currentDebtPerMonth, suggestion));
            }

            return suggestionsOutput.toArray(new String[0]);
        }

        private double increaseValueByPercentage(double value, double percentage) {
            return (int) Math.round(value + (value * percentage ) / 100.0);
        }

        private double calculateD(List<Payment> payments) {
            if (this.paymentHistory.getLastPayments().isEmpty()) return 0;

            Payment lastPayment = paymentHistory.getLastPayment();
            int elapsedMonths = 0;
            int paidMonths = 0;
            for (int i = 0; i < payments.size(); i++) {
                Payment currentPayment = payments.get(i);
                elapsedMonths++;
                if (currentPayment.getAmount() != 0) paidMonths++;

                if (lastPayment.equals(currentPayment)) {
                    break;
                }
            }

            return ((paidMonths * 100.0) / elapsedMonths) / 100.0;
        }

        private List<Payment> fillMissingMonths(List<Payment> payments, Date startDate, Date endDate) {
            List<Payment> allMonthsPayment = new ArrayList<>();

            Calendar startDateCalendar = Calendar.getInstance();
            startDateCalendar.setTime(startDate);
            Calendar endDateCalendar = Calendar.getInstance();
            endDateCalendar.setTime(endDate);

            Payment firstPayment = payments.get(0);

            int distanceBetweenFirstPaymentAndStartDate = calculateMonthsBetweenDates(startDate, firstPayment.getDate());
            for (int i = 0; i < distanceBetweenFirstPaymentAndStartDate; i++) {
                allMonthsPayment.add(createFakePayment(startDate, i));
            }

            allMonthsPayment.add(firstPayment);

            for (int i = 1; i < payments.size(); i++) {
                Payment currentPayment = payments.get(i);
                Date currentDate = currentPayment.getDate();
                Payment lastPayment = allMonthsPayment.get(allMonthsPayment.size() - 1);
                Date lastDate = lastPayment.getDate();

                Calendar currentDateCalendar = Calendar.getInstance();
                currentDateCalendar.setTime(currentDate);

                Calendar lastDateCalendar = Calendar.getInstance();
                lastDateCalendar.setTime(lastDate);

                int monthDistanceBetweenLastAndCurrentPayment = currentDateCalendar.get(Calendar.MONTH) - lastDateCalendar.get(Calendar.MONTH);

                for (int j = 1; j < monthDistanceBetweenLastAndCurrentPayment; j++) {
                    allMonthsPayment.add(createFakePayment(lastDate, j));
                }

                allMonthsPayment.add(currentPayment);
            }

            Payment lastPayment = payments.get(payments.size() - 1);
            Date lastPaymentDate  = lastPayment.getDate();
            int distanceBetweenLastPaymentAndEndDate = calculateMonthsBetweenDates(lastPaymentDate, endDate);
            for (int i = 0; i < distanceBetweenLastPaymentAndEndDate; i++) {
                allMonthsPayment.add(createFakePayment(lastPaymentDate, i));
            }

            return allMonthsPayment;
        }

        private int calculateMonthsBetweenDates(Date start, Date end) {
            if (end.before(start)) {
                Date temp = start;
                start = end;
                end = temp;
            }

            Calendar beforeCalendar = Calendar.getInstance();
            beforeCalendar.setTime(start);

            Calendar endCalendar = Calendar.getInstance();
            endCalendar.setTime(end);

            int monthDistance = 0;
            while (!areEqualByField(beforeCalendar, endCalendar, Calendar.MONTH) || !areEqualByField(beforeCalendar, endCalendar, Calendar.YEAR)) {
                monthDistance++;
                beforeCalendar.add(Calendar.MONTH, 1);
            }

            return monthDistance;
        }

        private boolean areEqualByField(Calendar c1, Calendar c2, int field) {
            return c1.get(field) == c2.get(field);
        }

        private Payment createFakePayment(Date baseDate, int monthOffset) {
            Calendar fillingDateCalendar = Calendar.getInstance();
            fillingDateCalendar.setTime(baseDate);
            fillingDateCalendar.add(Calendar.MONTH, monthOffset);
            fillingDateCalendar.set(Calendar.DAY_OF_MONTH, 20);
            return new Payment(0, fillingDateCalendar.getTime());
        }

        private double calculateNewDebtPerMonth(double debtPerMonth, double creditAmount, double d) {
            double resultDouble = debtPerMonth + creditAmount * d / 100;
            return resultDouble;
        }

        private String buildSuggestionMessage(double debPerMonth, Suggestion suggestion) {
            String debtAsString = Integer.toString((int) debPerMonth);

            switch (suggestion) {
                case LEAVE: return String.format(LEAVE_MESSAGE, debtAsString);
                case REMOVE: return String.format(REMOVE_MESSAGE, debtAsString);
                case ADD: return String.format(ADD_MESSAGE, debtAsString);
                default: throw new IllegalArgumentException("Suggestion param is needed");
            }
        }

        private Suggestion calculateSuggestion(double debtPerMonth) {
            int totalPaidInLastMonths = 0;
            boolean alwaysLate = true;
            boolean didntPayLastTwoMonths = true;
            int monthsWithPaymentMoreThanNeeded = 0;

            final List<Payment> lastPayments = paymentHistory.getLastPayments();
            for (int i = 0; i < lastPayments.size(); i++) {
                Payment currentPayment = lastPayments.get(i);
                totalPaidInLastMonths += currentPayment.getAmount();
                alwaysLate = alwaysLate && currentPayment.isLate();

                if (currentPayment.getAmount() >= debtPerMonth * 2) monthsWithPaymentMoreThanNeeded++;

                if (i >= lastPayments.size() - 2) {
                    didntPayLastTwoMonths = didntPayLastTwoMonths && currentPayment.getAmount() == 0;
                }
            }

            if (lastPayments.size() == 3) {
                if (totalPaidInLastMonths < 3 * debtPerMonth || alwaysLate || didntPayLastTwoMonths) {
                    return Suggestion.ADD;
                } else if (monthsWithPaymentMoreThanNeeded == 3) {
                    return Suggestion.REMOVE;
                } else {
                    return Suggestion.LEAVE;
                }
            } else if (lastPayments.size() == 2 && didntPayLastTwoMonths) {
                return Suggestion.ADD;
            } else {
                return Suggestion.LEAVE;
            }
        }
    }

    private static class PaymentHistory {
        private final List<Payment> lastPayments;
        private final int historySize;

        public PaymentHistory(int historySize) {
            lastPayments = new ArrayList<>(historySize);
            this.historySize = historySize;
        }

        public List<Payment> getLastPayments() {
            // I did this to guarantee that this guy will always have a maximum amount of entries == historySize
            return new ArrayList<>(lastPayments);
        }

        public Payment getLastPayment() {
            return this.lastPayments.get(lastPayments.size() - 1);
        }

        public void addPayment(Payment payment) {
            if (lastPayments.size() == historySize) {
                this.lastPayments.remove(0);
            }
            this.lastPayments.add(payment);
        }

        public void clearHistory() {
            this.lastPayments.clear();
        }
    }

    private static Date parseDate(String dateString, SimpleDateFormat simpleDateFormat) {
        try {
            return simpleDateFormat.parse(dateString);
        } catch (ParseException e) {
            throw new IllegalArgumentException(String.format("Date with invalid format: %s", dateString));
        }
    }

    private static class Payment {
        private final int amount;
        private final Date date;
        private final boolean isLate;
        private static final String PAYMENT_STRING_REGEX = "^\\$(\\d+)\\s+payment received on\\s+([0-9/]+$)";
        private static final Pattern PAYMENT_STRING_PATTERN = Pattern.compile(PAYMENT_STRING_REGEX);
        private static final String PAYMENT_DATE_FORMAT = "MM/dd/yyyy";
        private static final SimpleDateFormat DATE_TIME_FORMATTER = new SimpleDateFormat(PAYMENT_DATE_FORMAT);

        public Payment(String paymentString) {
            Matcher matcher = PAYMENT_STRING_PATTERN.matcher(paymentString);

            if (matcher.matches()) {
                this.amount = Integer.parseInt(matcher.group(1));
                this.date = parseDate(matcher.group(2), DATE_TIME_FORMATTER);
                this.isLate = calculateIfItsLate();
            } else {
                throw new IllegalArgumentException(String.format("Received payment with invalid format: [%s]", paymentString));
            }
        }

        public Payment(int amount, Date date) {
            this.amount = amount;
            this.date = date;
            this.isLate = calculateIfItsLate();
        }

        private boolean calculateIfItsLate() {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar.get(Calendar.DAY_OF_MONTH) > 15;
        }

        public int getAmount() {
            return amount;
        }

        public Date getDate() {
            return date;
        }

        public boolean isLate() {
            return isLate;
        }
    }
}
