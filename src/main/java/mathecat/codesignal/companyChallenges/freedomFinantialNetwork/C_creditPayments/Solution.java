package mathecat.codesignal.companyChallenges.freedomFinantialNetwork.C_creditPayments;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

class Solution {
//    public static void main(String[] args) throws ParseException {
//        Solution solution = new Solution();
//
//        int creditAmount = 10000;
//        int debtPerMonth = 1000;
//        String startDate = "01/2019",
//                endDate = "08/2019";
//
//        String[] history = {
//                "$1000 payment received on 01/16/2019",
//                "$2400 payment received on 03/20/2019",
//                "$4000 payment received on 04/25/2019",
//                "$3300 payment received on 05/06/2019",
//                "$1500 payment received on 08/15/2019"
//        };
//
//        String[] suggestions = solution.creditPayments(creditAmount, debtPerMonth, startDate, endDate, history);
//        for (String suggestion : suggestions) {
//            System.out.println(suggestion);
//        }
//    }
//
//    String[] creditPayments(int a, int b, String startDateRaw, String endDateRaw, String[] history) throws ParseException {
//        final DateTimeFormatter dtf = new DateTimeFormatterBuilder()
//                .appendPattern("MM/yyyy")
//                .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
//                .toFormatter();
//
//        final LocalDate startDate = LocalDate.parse(startDateRaw, dtf);
//        final LocalDate endDate = LocalDate.parse(endDateRaw, dtf);
//
//        final List<Payment> payments = Arrays.stream(history)
//                .map(Payment::new)
//                .sorted(Comparator.comparing(Payment::getDate))
//                .collect(Collectors.toList());
//
//        final int historySize = 3;
//        final PaymentHistory paymentHistory = new PaymentHistory(historySize);
//
//        SuggestionSystem suggestionSystem = new SuggestionSystem(paymentHistory);
//
//        return suggestionSystem.createSuggestions(payments, a, b, startDate, endDate);
//    }
//
//    private static class SuggestionSystem {
//        private final PaymentHistory paymentHistory;
//        private static final String LEAVE_MESSAGE = "Leave as it is, %s.";
//        private static final String ADD_MESSAGE = "Add one year, %s.";
//        private static final String REMOVE_MESSAGE = "Remove one year, %s.";
//
//        private enum Suggestion {
//            ADD, REMOVE, LEAVE
//        }
//
//        public SuggestionSystem(PaymentHistory paymentHistory) {
//            this.paymentHistory = paymentHistory;
//        }
//
//        public String[] createSuggestions(final List<Payment> paymentsInput, int creditAmount, int debtPerMonth, LocalDate startDate, LocalDate endDate) {
//            paymentHistory.clearHistory();
//            final List<Payment> payments = fillMissingMonths(paymentsInput, startDate, endDate);
//
//            int currentDebtPerMonth = debtPerMonth;
//            int d = 1;
//
//            List<String> suggestionsOutput = new ArrayList<>();
//
//            suggestionsOutput.add(buildSuggestionMessage(currentDebtPerMonth, Suggestion.LEAVE));
//            this.paymentHistory.addPayment(payments.get(0));
//
//            for (int i = 1; i < payments.size(); i++) {
//                Payment payment = payments.get(i);
//                this.paymentHistory.addPayment(payment);
//                Suggestion suggestion = calculateSuggestion(currentDebtPerMonth);
//
//                if (suggestion == Suggestion.ADD) {
//                    currentDebtPerMonth = calculateNewDebtPerMonth(currentDebtPerMonth, creditAmount, 1);
//                } else if (suggestion == Suggestion.REMOVE) {
//                    currentDebtPerMonth = calculateNewDebtPerMonth(currentDebtPerMonth, creditAmount, -1);
//                }
//
//                suggestionsOutput.add(buildSuggestionMessage(currentDebtPerMonth, suggestion));
//            }
//
//            return suggestionsOutput.toArray(new String[0]);
//        }
//
//        private List<Payment> fillMissingMonths(List<Payment> payments, LocalDate startDate, LocalDate endDate) {
//            List<Payment> allMonthsPayment = new ArrayList<>();
//
//            allMonthsPayment.add(payments.get(0));
//
//            for (int i = 1; i < payments.size(); i++) {
//                Payment currentPayment = payments.get(i);
//                LocalDate currentDate = currentPayment.getDate();
//                Payment lastPayment = allMonthsPayment.get(allMonthsPayment.size() - 1);
//
//                int monthDistanceBetweenLastAndCurrentPayment = currentPayment.getDate().getMonthValue() - allMonthsPayment.get(allMonthsPayment.size() - 1).getDate().getMonthValue();
//
//                for (int j = 1; j < monthDistanceBetweenLastAndCurrentPayment; j++) {
//                    allMonthsPayment.add(new Payment(0, LocalDate.of(currentDate.getYear(), lastPayment.getDate().getMonthValue() + j, 20)));
//                }
//
//                allMonthsPayment.add(currentPayment);
//            }
//
//            return allMonthsPayment;
//        }
//
//        private int calculateNewDebtPerMonth(int debtPerMonth, int creditAmount, int d) {
//            return debtPerMonth + creditAmount * d / 100;
//        }
//
//        private String buildSuggestionMessage(int debPerMonth, Suggestion suggestion) {
//            String debtAsString = Integer.toString(debPerMonth);
//
//            switch (suggestion) {
//                case LEAVE: return String.format(LEAVE_MESSAGE, debtAsString);
//                case REMOVE: return String.format(REMOVE_MESSAGE, debtAsString);
//                case ADD: return String.format(ADD_MESSAGE, debtAsString);
//                default: throw new IllegalArgumentException("Suggestion param is needed");
//            }
//        }
//
//        private Suggestion calculateSuggestion(int debtPerMonth) {
//            int totalPaidInLastMonths = 0;
//            boolean alwaysLate = true;
//            boolean didntPayLastTwoMonths = true;
//            int monthsWithPaymentMoreThanNeeded = 0;
//
//            final List<Payment> lastPayments = paymentHistory.getLastPayments();
//            for (int i = 0; i < lastPayments.size(); i++) {
//                Payment currentPayment = lastPayments.get(i);
//                totalPaidInLastMonths += currentPayment.getAmount();
//                alwaysLate = alwaysLate && currentPayment.isLate();
//
//                if (currentPayment.getAmount() >= debtPerMonth * 2) monthsWithPaymentMoreThanNeeded++;
//
//                if (i >= lastPayments.size() - 2) {
//                    didntPayLastTwoMonths = didntPayLastTwoMonths && currentPayment.getAmount() == 0;
//                }
//            }
//
//            if (lastPayments.size() == 3) {
//                if (totalPaidInLastMonths < 3 * debtPerMonth || alwaysLate || didntPayLastTwoMonths) {
//                    return Suggestion.ADD;
//                } else if (monthsWithPaymentMoreThanNeeded == 3) {
//                    return Suggestion.REMOVE;
//                } else {
//                    return Suggestion.LEAVE;
//                }
//            } else if (lastPayments.size() == 2 && didntPayLastTwoMonths) {
//                return Suggestion.ADD;
//            } else {
//                return Suggestion.LEAVE;
//            }
//        }
//    }
//
//    private static class PaymentHistory {
//        private final List<Payment> lastPayments;
//        private final int historySize;
//
//        public PaymentHistory(int historySize) {
//            lastPayments = new ArrayList<>(historySize);
//            this.historySize = historySize;
//        }
//
//        public List<Payment> getLastPayments() {
//            // I did this to guarantee that this guy will always have a maximum amount of entries == historySize
//            return new ArrayList<>(lastPayments);
//        }
//
//        public void addPayment(Payment payment) {
//            if (lastPayments.size() == historySize) {
//                this.lastPayments.remove(0);
//            }
//            this.lastPayments.add(payment);
//        }
//
//        public void clearHistory() {
//            this.lastPayments.clear();
//        }
//    }
//
//    private static class Payment {
//        private final int amount;
//        private final LocalDate date;
//        private final boolean isLate;
//        private static final String PAYMENT_STRING_REGEX = "^\\$(\\d+)\\s+payment received on\\s+([0-9/]+$)";
//        private static final Pattern PAYMENT_STRING_PATTERN = Pattern.compile(PAYMENT_STRING_REGEX);
//        private static final String PAYMENT_DATE_FORMAT = "MM/dd/yyyy";
//        private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(PAYMENT_DATE_FORMAT);
//
//        public Payment(String paymentString) {
//            Matcher matcher = PAYMENT_STRING_PATTERN.matcher(paymentString);
//
//            if (matcher.matches()) {
//                this.amount = Integer.parseInt(matcher.group(1));
//                this.date = LocalDate.parse(matcher.group(2), DATE_TIME_FORMATTER);
//                this.isLate = calculateIfItsLate();
//            } else {
//                throw new IllegalArgumentException(String.format("Received payment with invalid format: [%s]", paymentString));
//            }
//        }
//
//        public Payment(int amount, LocalDate date) {
//            this.amount = amount;
//            this.date = date;
//            this.isLate = calculateIfItsLate();
//        }
//
//        private boolean calculateIfItsLate() {
//            return date.getDayOfMonth() > 15;
//        }
//
//        public int getAmount() {
//            return amount;
//        }
//
//        public LocalDate getDate() {
//            return date;
//        }
//
//        public boolean isLate() {
//            return isLate;
//        }
//    }
}
