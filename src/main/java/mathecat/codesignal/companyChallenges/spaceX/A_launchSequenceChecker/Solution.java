package mathecat.codesignal.companyChallenges.spaceX.A_launchSequenceChecker;

import java.util.HashMap;
import java.util.Map;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String[] systemNames = {"stage_1", "stage_2", "dragon", "stage_1", "stage_2", "dragon"};
        int[] stepNumbers = {1, 10, 11, 2, 12, 111};

        System.out.println(solution.launchSequenceChecker(systemNames, stepNumbers));
    }

    boolean launchSequenceChecker(String[] systemNames, int[] stepNumbers) {
        Map<String, Integer> maxSequenceBySystem = new HashMap<>();
        for (int i = 0; i < systemNames.length; i++) {
            String systemName = systemNames[i];
            Integer stepNumber = stepNumbers[i];

            Integer currentStepNumber = maxSequenceBySystem.get(systemName);
            if (currentStepNumber == null) maxSequenceBySystem.put(systemName, stepNumber);
            else {
                if (currentStepNumber >= stepNumber) return false;
                else maxSequenceBySystem.put(systemName, stepNumber);
            }
        }

        return true;
    }
}
