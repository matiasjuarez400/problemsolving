package mathecat.codesignal.companyChallenges.spaceX.B_packetDescrambler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        List<Supplier<String>> tests = Arrays.asList(
            solution::test1,
            solution::test2,
            solution::test3,
            solution::test3,
            solution::test5,
            solution::test5,
            solution::test7,
            solution::test8
        );

        for (int i = 0; i < tests.size(); i++) {
            System.out.println(String.format("%s - %s", i + 1, tests.get(i).get()));
        }
    }

    private String test8() {
        int[] seq = {0, 0};
        char[] fragmentData = {'#', '#'};
        int n = 3;
        String expected = "#";

        String output = packetDescrambler(seq, fragmentData, n);

        return validateOutput(output, expected);
    }

    private String test7() {
        int[] seq = {0};
        char[] fragmentData = {'#'};
        int n = 3;
        String expected = "";

        String output = packetDescrambler(seq, fragmentData, n);

        return validateOutput(output, expected);
    }

    private String test5() {
        int[] seq = {1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0};
        char[] fragmentData = {'X',
                '#',
                'Y',
                'x',
                '#',
                'Y',
                'x',
                '#',
                'Y',
                'x',
                '#',
                'Y',
                'X',
                '#',
                'Y',
                'W',
                '#',
                'Y'};
        int n = 6;
        String expected = "";

        String output = packetDescrambler(seq, fragmentData, n);

        return validateOutput(output, expected);
    }

    private String test3() {
        int[] seq = {1, 1, 2, 2, 2, 0, 0, 0};
        char[] fragmentData = {'+',
                '+',
                'A',
                'A',
                'B',
                '#',
                '#',
                '#'};
        int n = 3;
        String expected = "";

        String output = packetDescrambler(seq, fragmentData, n);

        return validateOutput(output, expected);
    }

    private String test2() {
        int[] seq = {1, 1, 0, 0, 0, 2, 2, 2};
        char[] fragmentData = {'+',
                '+',
                'A',
                'A',
                'B',
                '#',
                '#',
                '#'};
        int n = 4;
        String expected = "";

        String output = packetDescrambler(seq, fragmentData, n);

        return validateOutput(output, expected);
    }

    private String test1() {
        int[] seq = {1, 1, 0, 0, 0, 2, 2, 2};
        char[] fragmentData = {'+', '+', 'A', 'A', 'B', '#', '#', '#'};
        int n = 3;
        String expected = "A+#";

        String output = packetDescrambler(seq, fragmentData, n);

        return validateOutput(output, expected);
    }

    private String validateOutput(String output, String expected) {
        if (!output.equalsIgnoreCase(expected)) {
            return String.format("OUTPUT [%s] - EXPECTED [%s]", output, expected);
        } else {
            return "PASSED";
        }
    }

    String packetDescrambler(int[] seq, char[] fragmentData, int n) {
        List<Fragment> fragments = IntStream.range(0, seq.length)
                .mapToObj(i -> new Fragment(seq[i], fragmentData[i]))
                .collect(Collectors.toList());

        FragmentsCurator fragmentsCurator = new FragmentsCurator();

        List<Fragment> curatedFragments = fragmentsCurator.curate(fragments, n);

        MessageValidator messageValidator = new MessageValidator(curatedFragments);

        boolean isValid = messageValidator.validate();
        if (!isValid) return "";

        return curatedFragments.stream()
                .map(Fragment::getCharacter)
                .map(Object::toString)
                .collect(Collectors.joining());
    }

    private static class FragmentsCurator {
        public List<Fragment> curate(List<Fragment> fragments, int repetitionsPerFragment) {
            Map<Integer, List<Fragment>> fragmentsBySequenceNumber = fragments.stream()
                    .collect(Collectors.groupingBy(Fragment::getSequence));

            List<Fragment> curatedFragments = new ArrayList<>();

            fragmentsBySequenceNumber.forEach((sequence, fragmentsForSequence) -> {
                Map<Character, Long> ocurrencesByCharacter = fragmentsForSequence.stream()
                        .collect(Collectors.groupingBy(Fragment::getCharacter, Collectors.counting()));

                Long higherOcurrence = Long.MIN_VALUE;
                Character bestCandidate = null;
                for (Character c : ocurrencesByCharacter.keySet()) {
                    Long currentOcurrence = ocurrencesByCharacter.get(c);
                    if (currentOcurrence > higherOcurrence) {
                        higherOcurrence = currentOcurrence;
                        bestCandidate = c;
                    }
                }

                double ocurrencePercentage = (higherOcurrence * 100.0) / repetitionsPerFragment;

                if (ocurrencePercentage > 50) {
                    curatedFragments.add(new Fragment(sequence, bestCandidate));
                }
            });

            return curatedFragments;
        }
    }

    private static class MessageValidator {
        private List<Fragment> fragments;

        public MessageValidator(List<Fragment> fragments) {
            this.fragments = fragments.stream()
                    .sorted(Comparator.comparing(Fragment::getSequence))
                    .collect(Collectors.toList());
        }

        public boolean validate() {
            return hasContent() &&
                    validateEndCharacterAtTheEnd() &&
                    valideOnlyOneEndCharacter() &&
                    validateNoGapsPresent();
        }

        private boolean hasContent() {
            return !fragments.isEmpty();
        }

        private boolean validateEndCharacterAtTheEnd() {
            return fragments.get(fragments.size() - 1).getCharacter() == '#';
        }

        private boolean valideOnlyOneEndCharacter() {
            long endCharacterCount = fragments.stream()
                    .map(Fragment::getCharacter)
                    .filter(character -> '#' == character)
                    .count();

            return endCharacterCount == 1;
        }

        private boolean validateNoGapsPresent() {
            for (int i = 0; i < fragments.size(); i++) {
                Fragment currentFragment = fragments.get(i);
                if (i != currentFragment.getSequence()) return false;
            }
            return true;
        }
    }

    private static class Fragment {
        private final int sequence;
        private final char character;

        public Fragment(int sequence, char character) {
            this.sequence = sequence;
            this.character = character;
        }

        public int getSequence() {
            return sequence;
        }

        public char getCharacter() {
            return character;
        }
    }
}
