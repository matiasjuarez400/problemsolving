package mathecat.codesignal.companyChallenges.spaceX.C_cpuEmulator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.test4();
    }

    private void test4() {
        String[] subroutine = {
                "INV R41",
                "ADD R42,R41"
        };

        System.out.println(cpuEmulator(subroutine));
    }

    private void test1() {
        String[] subroutine = {
                "MOV 5,R00",
                "MOV 10,R01",
                "JZ 7",
                "ADD R02,R01",
                "DEC R00",
                "JMP 3",
                "MOV R02,R42"
        };

        System.out.println(cpuEmulator(subroutine));
    }

    String cpuEmulator(String[] subroutine) {
        RawInstructionParser parser = new RawInstructionParser();

        List<RawInstruction> rawInstructions = Arrays.stream(subroutine)
                .map(parser::parseInstruction)
                .collect(Collectors.toList());

        CPUSimulator simulator = new CPUSimulator(43);
        simulator.processInstructions(rawInstructions);

        return Long.toString(simulator.getResult());
    }

    private static class CPUSimulator {
        private final Map<String, Register> registerByName;
        private final List<InstructionProcessor> instructionProcessors;
        private final Register R00;
        private final Register RLast;
        private static final long INTEGER_LIMIT = (long) Math.pow(2, 32);
        private long nextInstructionIndex;

        public CPUSimulator(int registerAmount) {
            this.registerByName = new HashMap<>();
            this.R00 = createRegister(0);
            this.RLast = createRegister(registerAmount - 1);
            this.registerByName.put(R00.getName(), R00);
            this.registerByName.put(RLast.getName(), RLast);

            for (int i = 1; i < registerAmount - 1; i++) {
                Register nextRegister = createRegister(i);
                registerByName.put(nextRegister.getName(), nextRegister);
            }

            this.instructionProcessors = createInstructionProcessors();
        }

        public void processInstructions(List<RawInstruction> rawInstructions) {
            nextInstructionIndex = 0;

            List<Instruction> instructions = rawInstructions.stream()
                    .map(this::convertRawInstruction)
                    .collect(Collectors.toList());

            while (nextInstructionIndex <= instructions.size() - 1) {
                Instruction nextInstruction = instructions.get((int) nextInstructionIndex);
                nextInstructionIndex++;
                processInstruction(nextInstruction);
            }
        }

        public long getResult() {
            return RLast.getValue();
        }

        private Instruction convertRawInstruction(RawInstruction rawInstruction) {
            Register leftRegister = this.registerByName.get(rawInstruction.getLeftRegister());
            Register rightRegister = this.registerByName.get(rawInstruction.getRightRegister());
            Long constant = rawInstruction.getConstant();

            return new Instruction(rawInstruction.getOperation(), leftRegister, rightRegister, constant);
        }

        private void registerWithNameNotFound(String name) {
            throw new IllegalArgumentException(String.format("There is no register with name [%s]", name));
        }

        private void processInstruction(Instruction instruction) {
            boolean instructionHandled = this.instructionProcessors.stream()
                    .anyMatch(instructionProcessor -> instructionProcessor.process(instruction));

            if (!instructionHandled) {
                throw new IllegalStateException("Unable to handle instruction: " + instruction);
            }
        }

        private Register createRegister(int registerNumber) {
            String registerName = String.format("R%02d", registerNumber);
            return new Register(registerName);
        }

        private List<InstructionProcessor> createInstructionProcessors() {
            return Arrays.asList(
                    createMoveProcessor(),
                    createAddProcessor(),
                    createDecrementProcessor(),
                    createIncrementProcessor(),
                    createBitInverter(),
                    createJumpProcessor(),
                    createJumpConditionalProcessor(),
                    createNoOperationProcessor()
            );
        }

        private InstructionProcessor createMoveProcessor() {
            return instruction -> {
                if (instruction.getOperation() != Operation.MOV) return false;

                final Register left = instruction.getLeftRegister();
                final Register right = instruction.getRightRegister();
                final Long constant = instruction.getConstant();

                if (left != null && right != null) {
                    right.setValue(left.getValue());
                } else if (left != null && constant != null) {
                    left.setValue(constant);
                } else {
                    throw new WrongInstructionException();
                }

                return true;
            };
        }

        private InstructionProcessor createAddProcessor() {
            return instruction -> {
                if (instruction.getOperation() != Operation.ADD) return false;

                final Register left = instruction.getLeftRegister();
                final Register right = instruction.getRightRegister();

                if (left != null && right != null) {
                    long sum = right.getValue() + left.getValue();
                    left.setValue(sum % INTEGER_LIMIT);
                } else {
                    throw new WrongInstructionException();
                }

                return true;
            };
        }

        private InstructionProcessor createDecrementProcessor() {
            return instruction -> {
                if (instruction.getOperation() != Operation.DEC) return false;

                final Register left = instruction.getLeftRegister();

                if (left == null) {
                    throw new WrongInstructionException();
                } else {
                    if (left.getValue() == 0) {
                        left.setValue(INTEGER_LIMIT - 1);
                    } else {
                        left.setValue(left.getValue() - 1);
                    }
                }

                return true;
            };
        }

        private InstructionProcessor createIncrementProcessor() {
            return instruction -> {
                if (instruction.getOperation() != Operation.INC) return false;

                final Register left = instruction.getLeftRegister();

                if (left == null) {
                    throw new WrongInstructionException();
                } else {
                    left.setValue(left.getValue() + 1);
                    if (left.getValue() == INTEGER_LIMIT) {
                        left.setValue(0);
                    }
                }

                return true;
            };
        }

        private InstructionProcessor createBitInverter() {
            return instruction -> {
                if (instruction.getOperation() != Operation.INV) return false;

                final Register left = instruction.getLeftRegister();

                if (left == null) {
                    throw new WrongInstructionException();
                } else {
                    long newValue = ~left.getValue();
                    if (newValue < 0) newValue = INTEGER_LIMIT + newValue;
                    left.setValue(newValue);
                }

                return true;
            };
        }

        public InstructionProcessor createJumpProcessor() {
            return instruction -> {
                if (instruction.getOperation() != Operation.JMP) return false;

                Long constant = instruction.getConstant();

                if (constant == null) {
                    throw new WrongInstructionException();
                } else {
                    nextInstructionIndex = constant - 1;
                }

                return true;
            };
        }

        public InstructionProcessor createJumpConditionalProcessor() {
            return instruction -> {
                if (instruction.getOperation() != Operation.JZ) return false;

                Long constant = instruction.getConstant();

                if (constant == null) {
                    throw new WrongInstructionException();
                } else {
                    if (R00.getValue() == 0) {
                        nextInstructionIndex = constant - 1;
                    }
                }

                return true;
            };
        }

        public InstructionProcessor createNoOperationProcessor() {
            return instruction -> instruction.getOperation() == Operation.NOP;
        }

        private interface InstructionProcessor {
            boolean process(Instruction instruction);
        }

        private static class WrongInstructionException extends RuntimeException {
            public WrongInstructionException(String message) {
                super(message);
            }

            public WrongInstructionException() {
                this("Received unexpected instruction");
            }
        }
    }

    private static class RawInstructionParser {
        private static final String OPERATION_REGEX = "(\\w{2,3})";
        private static final String REGISTER_REGEX = "(R\\d{2})";
        private static final String CONSTANTE_REGEX = "(\\d+)";
        private static final String WHITE_SPACES_REGEX = "\\s*?";

        private static final Pattern DOUBLE_REGISTER_PATTERN = buildDoubleRegisterPattern();
        private static final Pattern SINGLE_REGISTER_PATTERN = buildSingleRegisterPattern();
        private static final Pattern CONSTANT_REGISTER_PATTERN = buildConstantAndRegisterPattern();
        private static final Pattern SINGLE_CONSTANT_PATTERN = buildSingleConstantPattern();
        private static final Pattern ONLY_OPERATION_PATTERN = Pattern.compile(OPERATION_REGEX);

        private static final String MOV_INSTRUCTION = "MOV";
        private static final String ADD_INSTRUCTION = "ADD";
        private static final String DEC_INSTRUCTION = "DEC";
        private static final String INC_INSTRUCTION = "INC";
        private static final String INV_INSTRUCTION = "INV";
        private static final String JMP_INSTRUCTION = "JMP";
        private static final String JZ_INSTRUCTION = "JZ";
        private static final String NOP_INSTRUCTION = "NOP";

        private static final List<Function<String, RawInstruction>> PARSERS = buildInstructionParsers();

        private static Pattern buildDoubleRegisterPattern() {
            String exp = OPERATION_REGEX + WHITE_SPACES_REGEX + REGISTER_REGEX + WHITE_SPACES_REGEX + "," + WHITE_SPACES_REGEX + REGISTER_REGEX;
            return Pattern.compile(exp);
        }

        private static Pattern buildConstantAndRegisterPattern() {
            String exp = OPERATION_REGEX + WHITE_SPACES_REGEX + CONSTANTE_REGEX + WHITE_SPACES_REGEX + "," + WHITE_SPACES_REGEX + REGISTER_REGEX;
            return Pattern.compile(exp);
        }

        private static Pattern buildSingleRegisterPattern() {
            String exp = OPERATION_REGEX + WHITE_SPACES_REGEX + REGISTER_REGEX;
            return Pattern.compile(exp);
        }

        private static Pattern buildSingleConstantPattern() {
            String exp = OPERATION_REGEX + WHITE_SPACES_REGEX + CONSTANTE_REGEX;
            return Pattern.compile(exp);
        }

        public RawInstruction parseInstruction(String instruction) {
            RawInstruction rawInstruction = PARSERS.stream()
                    .map(parser -> parser.apply(instruction))
                    .filter(Objects::nonNull)
                    .findFirst().orElseThrow(() -> new IllegalArgumentException(String.format("Received unparseable instruction: %s", instruction)));

            return rawInstruction;
        }

        private static List<Function<String, RawInstruction>> buildInstructionParsers() {
            return Arrays.asList(
                    buildDoubleRegisterParser(),
                    buildSingleRegisterParser(),
                    buildSingleConstantParser(),
                    buildConstantAndRegisterParser(),
                    buildOnlyOperationParser()
            );
        }

        private static Function<String, RawInstruction> buildOnlyOperationParser() {
            return s -> {
                Matcher matcher = ONLY_OPERATION_PATTERN.matcher(s);
                if (!matcher.matches()) return null;

                String rawOperation = matcher.group(1);
                Operation operation = transformToOperation(rawOperation);

                return new RawInstruction(operation);
            };
        }

        private static Function<String, RawInstruction> buildConstantAndRegisterParser() {
            return s -> {
                Matcher matcher = CONSTANT_REGISTER_PATTERN.matcher(s);

                if (!matcher.matches()) return null;

                String rawOperation = matcher.group(1);
                Long constant = Long.parseLong(matcher.group(2));
                String leftRegister = matcher.group(3);

                Operation operation = transformToOperation(rawOperation);

                return new RawInstruction(operation, leftRegister, constant);
            };
        }

        private static Function<String, RawInstruction> buildSingleConstantParser() {
            return s -> {
                Matcher matcher = SINGLE_CONSTANT_PATTERN.matcher(s);

                if (!matcher.matches()) return null;

                String rawOperation = matcher.group(1);
                Long constant = Long.parseLong(matcher.group(2));

                Operation operation = transformToOperation(rawOperation);

                return new RawInstruction(operation, constant);
            };
        }

        private static Function<String, RawInstruction> buildSingleRegisterParser() {
            return s -> {
                Matcher matcher = SINGLE_REGISTER_PATTERN.matcher(s);

                if (!matcher.matches()) return null;

                String rawOperation = matcher.group(1);
                String leftRegister = matcher.group(2);

                Operation operation = transformToOperation(rawOperation);

                return new RawInstruction(operation, leftRegister);
            };
        }

        private static Function<String, RawInstruction> buildDoubleRegisterParser() {
            return s -> {
                Matcher matcher = DOUBLE_REGISTER_PATTERN.matcher(s);

                if (!matcher.matches()) return null;

                String rawOperation = matcher.group(1);
                String leftRegister = matcher.group(2);
                String rightRegister = matcher.group(3);

                Operation operation = transformToOperation(rawOperation);

                return new RawInstruction(operation, leftRegister, rightRegister);
            };
        }

        private static Operation transformToOperation(String rawOperation) {
            switch (rawOperation) {
                case MOV_INSTRUCTION: return Operation.MOV;
                case ADD_INSTRUCTION: return Operation.ADD;
                case DEC_INSTRUCTION: return Operation.DEC;
                case INC_INSTRUCTION: return Operation.INC;
                case INV_INSTRUCTION: return Operation.INV;
                case JMP_INSTRUCTION: return Operation.JMP;
                case JZ_INSTRUCTION: return Operation.JZ;
                case NOP_INSTRUCTION: return Operation.NOP;
                default: throw new IllegalArgumentException(String.format("Received unparseable operation: %s", rawOperation));
            }
        }
    }

    private static class RawInstruction {
        private Operation operation;
        private String leftRegister;
        private String rightRegister;
        private Long constant;

        public RawInstruction(Operation operation) {
            this.operation = operation;
        }

        public RawInstruction(Operation operation, String leftRegister, String rightRegister) {
            this.operation = operation;
            this.leftRegister = leftRegister;
            this.rightRegister = rightRegister;
        }

        public RawInstruction(Operation operation, String leftRegister) {
            this.operation = operation;
            this.leftRegister = leftRegister;
        }

        public RawInstruction(Operation operation, Long constant) {
            this.operation = operation;
            this.constant = constant;
        }

        public RawInstruction(Operation operation, String leftRegister, Long constant) {
            this.operation = operation;
            this.leftRegister = leftRegister;
            this.constant = constant;
        }

        public Operation getOperation() {
            return operation;
        }

        public String getLeftRegister() {
            return leftRegister;
        }

        public String getRightRegister() {
            return rightRegister;
        }

        public Long getConstant() {
            return constant;
        }
    }

    private static class Instruction {
        private Operation operation;
        private Register leftRegister;
        private Register rightRegister;
        private Long constant;

        public Instruction(Operation operation, Register leftRegister, Register rightRegister, Long constant) {
            this.operation = operation;
            this.leftRegister = leftRegister;
            this.rightRegister = rightRegister;
            this.constant = constant;
        }

        public Instruction(Operation operation, Register leftRegister, Register rightRegister) {
            this.operation = operation;
            this.leftRegister = leftRegister;
            this.rightRegister = rightRegister;
        }

        public Instruction(Operation operation, Register leftRegister, Long constant) {
            this.operation = operation;
            this.leftRegister = leftRegister;
            this.constant = constant;
        }

        public Instruction(Operation operation, Register leftRegister) {
            this.operation = operation;
            this.leftRegister = leftRegister;
        }

        public Instruction(Operation operation, Long constant) {
            this.operation = operation;
            this.constant = constant;
        }

        public Instruction(Operation operation) {
            this.operation = operation;
        }

        public Operation getOperation() {
            return operation;
        }

        public Register getLeftRegister() {
            return leftRegister;
        }

        public Register getRightRegister() {
            return rightRegister;
        }

        public Long getConstant() {
            return constant;
        }

        @Override
        public String toString() {
            return "Instruction{" +
                    "operation=" + operation +
                    ", leftRegister=" + leftRegister +
                    ", rightRegister=" + rightRegister +
                    ", constant=" + constant +
                    '}';
        }
    }

    private static class Register {
        private String name;
        private long value;

        public Register(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public long getValue() {
            return value;
        }

        public void setValue(long value) {
            this.value = value;
        }
    }

    private enum Operation {
        MOV, ADD, DEC, INC, INV, JMP, JZ, NOP
    }
}
