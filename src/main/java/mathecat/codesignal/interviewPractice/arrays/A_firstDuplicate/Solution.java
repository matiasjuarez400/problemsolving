package mathecat.codesignal.interviewPractice.arrays.A_firstDuplicate;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.firstDuplicate(new int[] {2, 1, 3, 5, 3, 2}));
    }

    int firstDuplicate(int[] a) {
        boolean[] foundNumbers = new boolean[a.length + 1];
        for (int i : a) {
            if (foundNumbers[i]) return i;
            foundNumbers[i] = true;
        }

        return -1;
    }
}
