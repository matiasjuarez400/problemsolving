package mathecat.codesignal.interviewPractice.arrays.C_rotateImage;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        
        int[][] a = {{1, 2, 3},
                     {4, 5, 6},
                     {7, 8, 9}};

        int[][] result = solution.rotateImage(a);

        for(int[] row : result) {
            System.out.println(Arrays.toString(row));
        }
    }

    int[][] rotateImage(int[][] a) {
        int[][] output = new int[a.length][a[0].length];

        for (int row = 0; row < a.length; row++) {
            for (int col = 0; col < a[row].length; col++) {
                output[col][a[row].length - 1 - row] = a[row][col];
            }
        }

        return output;
    }
}
