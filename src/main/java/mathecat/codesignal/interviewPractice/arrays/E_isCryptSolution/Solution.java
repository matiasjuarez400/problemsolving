package mathecat.codesignal.interviewPractice.arrays.E_isCryptSolution;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.test1();
    }

    void test1() {
        String[] crypt = {"TEN", "TWO", "ONE"};
        char[][] mapping = {{'O', '1'},
                        {'T', '0'},
                        {'W', '9'},
                        {'E', '5'},
                        {'N', '4'}};

        System.out.println(isCryptSolution(crypt, mapping));
    }

    boolean isCryptSolution(String[] crypt, char[][] solution) {
        if (Arrays.stream(crypt).anyMatch(s -> s == null || s.length() == 0)) return false;

        Map<Character, Character> mapping = new HashMap<>();

        for (int i = 0; i < solution.length; i++) {
            mapping.put(solution[i][0], solution[i][1]);
        }

        for (int i = 0; i < crypt.length; i++) {
            String decoded = decodeString(crypt[i], mapping);
            if (decoded.contains("_")) return false;
            if (decoded.startsWith("0") && decoded.length() > 1) return false;
            crypt[i] = decoded;
        }

        return parseString(crypt[0]) + parseString(crypt[1]) == parseString(crypt[2]);
    }

    private int parseString(String s) {
        try {
            return Integer.parseInt(s);
        } catch (Exception e) {
            return 0;
        }
    }

    private String decodeString(String input, Map<Character, Character> mapping) {
        StringBuilder output = new StringBuilder();
        for (char c : input.toCharArray()) {
            Character mappedDigit = mapping.get(c);
            if (mappedDigit == null) output.append("_");
            else output.append(mapping.get(c));
        }
        return output.toString();
    }
}
