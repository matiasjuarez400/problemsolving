package mathecat.codesignal.interviewPractice.arrays.D_sudoku2;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        
        char[][] grid = {{'.', '.', '.', '1', '4', '.', '.', '2', '.'},
                        {'.', '.', '6', '.', '.', '.', '.', '.', '.'},
                        {'.', '.', '.', '.', '.', '.', '.', '.', '.'},
                        {'.', '.', '1', '.', '.', '.', '.', '.', '.'},
                        {'.', '6', '7', '.', '.', '.', '.', '.', '9'},
                        {'.', '.', '.', '.', '.', '.', '8', '1', '.'},
                        {'.', '3', '.', '.', '.', '.', '.', '.', '6'},
                        {'.', '.', '.', '.', '.', '7', '.', '.', '.'},
                        {'.', '.', '.', '5', '.', '.', '.', '7', '.'}};

        System.out.println(solution.sudoku2(grid));
    }

    boolean sudoku2(char[][] grid) {
        String[] rows = initArray();
        String[] cols = initArray();
        String[] regions = initArray();

        for (int row = 0; row < grid.length; row++) {
            for (int col = 0; col < grid[row].length; col++) {
                char currentChar = grid[row][col];
                String value = Character.isDigit(currentChar) ? Character.toString(currentChar) : "";
                rows[row] += value;
                cols[col] += value;
                int region = (row / 3) * 3 + (col / 3);
                regions[region] += value;
            }
        }

        return validateArrayForDifferentStrings(rows) &&
                validateArrayForDifferentStrings(cols) &&
                validateArrayForDifferentStrings(regions);
    }

    private String[] initArray() {
        String[] a = new String[9];
        Arrays.fill(a, "");
        return a;
    }

    private boolean validateArrayForDifferentStrings(String[] a) {
        for (String s : a) {
            if (hasRepeatedChars(s)) return false;
        }
        return true;
    }

    private boolean hasRepeatedChars(String s) {
        for (char c : s.toCharArray()) {
            if (s.indexOf(c) != s.lastIndexOf(c)) return true;
        }
        return false;
    }
}
