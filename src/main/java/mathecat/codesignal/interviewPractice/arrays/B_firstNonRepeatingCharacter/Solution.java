package mathecat.codesignal.interviewPractice.arrays.B_firstNonRepeatingCharacter;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.firstNotRepeatingCharacter("abacabad"));
    }

    char firstNotRepeatingCharacter(String s) {
        int[] indexByNonRepeatingChar = new int['z' - 'a' + 1];

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int indexInArray = c - 'a';

            if (indexByNonRepeatingChar[indexInArray] == 0) {
                indexByNonRepeatingChar[indexInArray] = i + 1;
            } else {
                indexByNonRepeatingChar[indexInArray] = -1;
            }
        }

        int firstNonRepeatingIndex = Arrays.stream(indexByNonRepeatingChar)
                .filter(i -> i > 0)
                .min().orElse(-1);

        if (firstNonRepeatingIndex == -1) return '_';
        else return s.charAt(firstNonRepeatingIndex - 1);
    }

}
