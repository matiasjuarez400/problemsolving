package mathecat.codesignal.interviewPractice.linkedLists.A_removeKFromList;

import mathecat.codesignal.common.ListNode2;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        ListNode2<Integer> listNode2 = ListNode2.createFromArray(3, 1, 2, 3, 4, 5);

        ListNode2<Integer> result = solution.removeKFromList(listNode2, 3);

        System.out.println(result);
    }

    ListNode2<Integer> removeKFromList(ListNode2<Integer> l, int k) {
        ListNode2<Integer> current = l;
        ListNode2<Integer> previous = null;
        while (current != null) {
            if (current.value == k) {
                if (previous == null) {
                    current = current.next;
                    l = current;
                } else {
                    previous.next = current.next;
                    current = current.next;
                }
            } else {
                previous = current;
                current = previous.next;
            }
        }

        return l;
    }
}
