package mathecat.codesignal.interviewPractice.linkedLists.B_isListPalindrome;

import mathecat.codesignal.common.ListNode;
import mathecat.codesignal.common.ListNode2;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        ListNode<Integer> l = ListNode.createFromArray(3, 2, 2, 3);

        System.out.println(solution.isListPalindrome(l));
    }

    boolean isListPalindrome(ListNode<Integer> lo) {
        if (lo == null || lo.value == null) return true;

        ListNode2<Integer> l = new ListNode2<>(lo);
        ListNode2<Integer> reversed = new ListNode2<>(l).reverse();

        ListNode2<Integer> currentL = l;
        ListNode2<Integer> currentReversed = reversed;

        while (currentL != null && currentReversed != null) {
            if (!currentL.value.equals(currentReversed.value)) return false;
            currentL = currentL.next;
            currentReversed = currentReversed.next;
        }

        return true;
    }
}
