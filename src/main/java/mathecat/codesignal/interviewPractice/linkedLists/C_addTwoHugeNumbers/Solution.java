package mathecat.codesignal.interviewPractice.linkedLists.C_addTwoHugeNumbers;

import mathecat.codesignal.common.ListNode;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
    }

    ListNode<Integer> addTwoHugeNumbers(ListNode<Integer> a, ListNode<Integer> b) {
        return null;
    }

    String convertToString(ListNode<Integer> l) {
        String output = "";
        while (l != null) {
            output += String.format("%d4", l.value);
            l = l.next;
        }

        return output;
    }
}
