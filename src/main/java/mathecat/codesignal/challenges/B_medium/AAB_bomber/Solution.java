package mathecat.codesignal.challenges.B_medium.AAB_bomber;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Each cell in a 2D grid contains either a wall ('W') or an enemy ('E'), or is empty ('0'). Bombs can destroy enemies, but walls are too strong to be destroyed.
 * A bomb placed in an empty cell destroys all enemies in the same row and column, but the destruction stops once it hits a wall.
 *
 * Return the maximum number of enemies you can destroy using one bomb.
 *
 * Note that your solution should have O(field.length · field[0].length) complexity because this is what you will be asked during an interview.
 */
class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        char[][] field = {{'0', '0', 'E', '0'},
                         {'W', '0', 'W', 'E'},
                         {'0', 'E', '0', 'W'},
                         {'0', 'W', '0', 'E'}};
        System.out.println(solution.solution(field));
    }

    int solution(char[][] field) {
        Storage enemies = new Storage();
        Storage empty = new Storage();
        Storage walls = new Storage();

        for (int y = 0; y < field.length; y++) {
            for (int x = 0; x < field[y].length; x++) {
                if (field[y][x] == 'W') walls.add(x, y);
                else if (field[y][x] == 'E') enemies.add(x, y);
                else empty.add(x, y);
            }
        }

        List<Pair> maxEnemyList = new ArrayList<>();
        for (Pair emptyPair : empty.getAll()) {
            EmptySpace emptySpace = new EmptySpace(emptyPair);
            List<Pair> currentEnemies = emptySpace.calculateEnemiesToHit(enemies, walls);
            if (currentEnemies.size() > maxEnemyList.size()) maxEnemyList = currentEnemies;
        }

        return maxEnemyList.size();
    }

    private class EmptySpace {
        private Pair location;

        public EmptySpace(Pair location) {
            this.location = location;
        }

        public List<Pair> calculateEnemiesToHit(Storage enemies, Storage walls) {
            List<Pair> enemiesList = new ArrayList<>();

            Pair leftWall = null;
            Pair rightWall = null;
            Pair upWall = null;
            Pair downWall = null;

            List<Pair> horizontalWalls = walls.getByRow(this.location.y);
            if (horizontalWalls != null) {
                for (Pair wall : horizontalWalls) {
                    int diff = wall.x - location.x;
                    if (diff < 0) {
                        if (leftWall == null) leftWall = wall;
                        else if (wall.x > leftWall.x) leftWall = wall;
                    } else {
                        if (rightWall == null) rightWall = wall;
                        else if (wall.x < rightWall.x) rightWall = wall;
                    }
                }
            }

            List<Pair> verticalWalls = walls.getByColumn(this.location.x);
            if (verticalWalls != null) {
                for (Pair wall : verticalWalls) {
                    int diff = wall.y - location.y;
                    if (diff < 0) {
                        if (upWall == null) upWall = wall;
                        else if (wall.y > upWall.y) upWall = wall;
                    } else {
                        if (downWall == null) downWall = wall;
                        else if (wall.y < downWall.y) downWall = wall;
                    }
                }
            }

            List<Pair> horizontalEnemies = enemies.getByRow(this.location.y);
            if (horizontalEnemies != null) {
                for (Pair enemy : horizontalEnemies) {
                    boolean leftWallOk = leftWall == null || enemy.x > leftWall.x;
                    boolean rightWallOk = rightWall == null || enemy.x < rightWall.x;
                    if (leftWallOk && rightWallOk) enemiesList.add(enemy);
                }
            }

            List<Pair> verticalEnemies = enemies.getByColumn(this.location.x);
            if (verticalEnemies != null) {
                for (Pair enemy : verticalEnemies) {
                    boolean upWallOk = upWall == null || enemy.y > upWall.y;
                    boolean downWallOk = downWall == null || enemy.y < downWall.y;
                    if (upWallOk && downWallOk) enemiesList.add(enemy);
                }
            }

            return enemiesList;
        }
    }

    private class Storage {
        private Map<Integer, List<Pair>> pairsByRow = new HashMap<>();
        private Map<Integer, List<Pair>> pairsByCol = new HashMap<>();

        public void add(Pair pair) {
            List<Pair> rowPairs = pairsByRow.computeIfAbsent(pair.y, k -> new ArrayList<>());
            rowPairs.add(pair);

            List<Pair> colPairs = pairsByCol.computeIfAbsent(pair.x, k -> new ArrayList<>());
            colPairs.add(pair);
        }

        public void add(int x, int y) {
            add(new Pair(x, y));
        }

        public List<Pair> getByColumn(int col) {
            return pairsByCol.get(col);
        }

        public List<Pair> getByRow(int row) {
            return pairsByRow.get(row);
        }

        public Pair get(int col, int row) {
            List<Pair> pairs = getByColumn(col);
            if (pairs == null || pairs.isEmpty()) return null;
            return pairs.stream().filter(pair -> pair.y == row).findFirst().orElse(null);
        }

        public Set<Pair> getAll() {
            Set<Pair> pairs = new HashSet<>();
            pairsByRow.values().forEach(pairs::addAll);
            pairsByCol.values().forEach(pairs::addAll);
            return pairs;
        }
    }

    private class Pair {
        int x;
        int y;

        public Pair(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Pair pair = (Pair) o;
            return x == pair.x &&
                    y == pair.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }
    }
}
