package mathecat.codesignal.challenges.B_medium.AAO_isbeautifulString;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.isBeautifulString("bbbaacdafe"));
    }

    boolean isBeautifulString(String inputString) {
        Map<Integer, Long> counterByCharacter = inputString.chars()
                .boxed()
                .collect(Collectors.groupingBy((Integer c) -> c, Collectors.counting()));

        for (int i = 'a'; i <= 'z'; i++) {
            counterByCharacter.putIfAbsent(i, 0L);
        }

        List<LetterCounter> letterCounters = new ArrayList<>();
        counterByCharacter.forEach((character, counter) -> {
            letterCounters.add(new LetterCounter((char) (int) character, counter));
        });

        List<LetterCounter> letterCountersSorted = letterCounters.stream()
                .sorted(Comparator.comparing(LetterCounter::getLetter))
                .collect(Collectors.toList());

        long previous = Integer.MAX_VALUE;
        for (LetterCounter letterCounter : letterCountersSorted) {
            if (letterCounter.getAcc() > previous) return false;
            previous = letterCounter.getAcc();
        }
        return true;
    }

    private static class LetterCounter {
        private long acc = 0;
        private char letter;

        public LetterCounter(char letter, long acc) {
            this.acc = acc;
            this.letter = letter;
        }

        public long getAcc() {
            return acc;
        }

        public char getLetter() {
            return letter;
        }
    }
}
