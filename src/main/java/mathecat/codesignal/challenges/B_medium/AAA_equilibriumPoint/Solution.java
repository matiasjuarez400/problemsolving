package mathecat.codesignal.challenges.B_medium.AAA_equilibriumPoint;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] arr = {10, 5, 3, 5, 2, 2, 6, 8};

        System.out.println(solution.equilibriumPoint(arr));
    }

    int equilibriumPoint(int[] arr) {
        int sum = Arrays.stream(arr).sum();

        int acc = 0;
        for (int i = 0; i < arr.length; i++) {
            acc += arr[i];

            if (acc == sum - acc + arr[i]) return i + 1;
        }

        return -1;
    }
}
