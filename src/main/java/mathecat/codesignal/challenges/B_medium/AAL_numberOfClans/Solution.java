package mathecat.codesignal.challenges.B_medium.AAL_numberOfClans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.test1();
    }

    void test1() {
        int[] divisors = {2, 3};
        int k = 6;

        System.out.println(numberOfClans(divisors, k));
    }

    int numberOfClans(int[] divisors, int k) {
        Map<Integer, List<Integer>> divisorsByNumber = new HashMap<>();

        for (int i = 1; i <= k; i++) {
            divisorsByNumber.put(i, getDivisorsOfNumber(divisors, i));
        }

        Set<String> clans = new HashSet<>();
        for (Integer number : divisorsByNumber.keySet()) {
            List<Integer> clanMembers = new ArrayList<>();
            clanMembers.add(number);

            List<Integer> numberDivisors = divisorsByNumber.get(number);

            for (Integer comparingNumber : divisorsByNumber.keySet()) {
                if (number.equals(comparingNumber)) continue;
                List<Integer> comparingNumberDivisors = divisorsByNumber.get(comparingNumber);

                if (numberDivisors.size() != comparingNumberDivisors.size()) continue;

                if (numberDivisors.containsAll(comparingNumberDivisors)) clanMembers.add(comparingNumber);
            }

            clans.add(clanMembers.stream().sorted().map(Object::toString).collect(Collectors.joining()));
        }

        return clans.size();
    }

    private List<Integer> getDivisorsOfNumber(int[] divisors, int n) {
        List<Integer> result = new ArrayList<>();
        for (int d : divisors) {
            if (n % d == 0) result.add(d);
        }
        return result;
    }
}
