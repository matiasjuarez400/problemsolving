package mathecat.codesignal.challenges.B_medium.AAQ_optimalStockBasket;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[][] stocks = {
                {-1, 2}, {10, 15}, {11, 13}, {9, 10}
        };
        int riskBudget = 30;

        System.out.println(solution.optimalStockBasket(stocks, riskBudget));

//        System.out.println(solution.convertIntToCombination(3, 15));
    }

    int optimalStockBasket(int[][] stocksRaw, int riskBudget) {
        List<Stock> stocks = Arrays.stream(stocksRaw)
                .map(Stock::new)
                .filter(stock -> stock.getEstimatedReturn() > 0)
                .collect(Collectors.toList());

        if (stocks.size() == 0) return 0;

        int maxReturn = Integer.MIN_VALUE;

        int maxCombination = (int) Math.pow(2, stocks.size());

        int currentCombination = 0;

        int accumulatedRisk;
        int accumulatedReturn;
        while (currentCombination <= maxCombination) {
            String binaryRepresentation = convertIntToCombination(currentCombination++, stocks.size());
            accumulatedRisk = 0;
            accumulatedReturn = 0;

            boolean validCombination = true;
            for (int i = 0; i < binaryRepresentation.length(); i++) {
                if (binaryRepresentation.charAt(i) == '1') {
                    Stock currentStock = stocks.get(i);
                    accumulatedRisk += currentStock.getRisk();
                    accumulatedReturn += currentStock.getEstimatedReturn();

                    if (accumulatedRisk > riskBudget) {
                        validCombination = false;
                        break;
                    }
                }
            }

            if (validCombination && accumulatedReturn > maxReturn) {
                maxReturn = accumulatedReturn;
            }
        }

        return maxReturn;
    }

    private String convertIntToCombination(int n, int bits) {
        return String.format("%0"+ bits + "d", new BigInteger(Integer.toBinaryString(n)));
    }

    private static class Stock {
        private final int estimatedReturn;
        private final int risk;

        public Stock(int[] stockInfo) {
            this.estimatedReturn = stockInfo[0];
            this.risk = stockInfo[1];
        }

        public int getEstimatedReturn() {
            return estimatedReturn;
        }

        public int getRisk() {
            return risk;
        }
    }
}
