package mathecat.codesignal.challenges.B_medium.AAF_maximalSquare;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        
        char[][] matrix = {
                    {'1', '0', '1', '1', '1'},
                    {'1', '0', '1', '1', '1'},
                    {'1', '1', '1', '1', '1'},
                    {'1', '0', '0', '1', '0'},
                    {'1', '0', '0', '1', '0'}
                };

        System.out.println(solution.maximalSquare(matrix));
    }

    int maximalSquare(char[][] matrix) {
        int biggestSize = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                int size = 0;

                boolean validForSquare = true;
                while (validForSquare) {
                    int ki = i + size;
                    int li = j + size;

                    int kf = ki;
                    while (kf >= i && validForSquare) {
                        validForSquare = checkIfIsValidForSquare(matrix, kf, li);
                        kf--;
                    }

                    int lf = li;
                    while (lf >= j && validForSquare) {
                        validForSquare = checkIfIsValidForSquare(matrix, ki, lf);
                        lf--;
                    }

                    if (validForSquare) size++;
                }

                biggestSize = Integer.max(biggestSize, size);
            }
        }
        return biggestSize * biggestSize;
    }

    boolean checkIfIsValidForSquare(char[][] matrix, int i, int j) {
        return i < matrix.length && j < matrix[i].length && matrix[i][j] == '1';
    }
}
