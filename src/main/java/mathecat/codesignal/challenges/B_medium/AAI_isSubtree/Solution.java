package mathecat.codesignal.challenges.B_medium.AAI_isSubtree;

import mathecat.codesignal.common.Tree;

import java.util.ArrayList;
import java.util.List;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.test4();
    }

    void test4() {
        String t1Raw = "{\n" +
                "    \"value\": 1,\n" +
                "    \"left\": {\n" +
                "        \"value\": 2,\n" +
                "        \"left\": null,\n" +
                "        \"right\": null\n" +
                "    },\n" +
                "    \"right\": {\n" +
                "        \"value\": 2,\n" +
                "        \"left\": null,\n" +
                "        \"right\": null\n" +
                "    }\n" +
                "}";

        Tree<Integer> t1 = Tree.parse(t1Raw, Integer.class);

        System.out.println(isSubtree(t1, null));
    }

    boolean isSubtree(Tree<Integer> t1, Tree<Integer> t2) {
        if (t2 == null) return true;
        if (t1 == null) return false;

        List<Tree<Integer>> t1Nodes = getAllTreeNodes(t1);

        return t1Nodes.stream()
                .filter(node -> node.value.equals(t2.value))
                .anyMatch(node -> areEqual(node, t2));
    }

    private List<Tree<Integer>> getAllTreeNodes(Tree<Integer> tree) {
        List<Tree<Integer>> nodes = new ArrayList<>();
        nodes.add(tree);

        if (tree.left != null) {
            nodes.addAll(getAllTreeNodes(tree.left));
        }

        if (tree.right != null) {
            nodes.addAll(getAllTreeNodes(tree.right));
        }

        return nodes;
    }

    private boolean areEqual(Tree<Integer> t1, Tree<Integer> t2) {
        if (t1 == null && t2 == null) return true;
        if (t1 == null ^ t2 == null) return false;
        if (!t1.value.equals(t2.value)) return false;

        return areEqual(t1.left, t2.left) && areEqual(t1.right, t2.right);
    }
}
