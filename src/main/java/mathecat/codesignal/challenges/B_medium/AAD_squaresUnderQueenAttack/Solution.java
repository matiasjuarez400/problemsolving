package mathecat.codesignal.challenges.B_medium.AAD_squaresUnderQueenAttack;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[][] queens = {{1, 1}, {3, 2}};
        int[][] queries = {{1, 1}, {0, 3}, {0, 4}, {3, 4}, {2, 0}, {4, 3}, {4, 0}};
        int n = 5;

        boolean[] result = solution.squaresUnderQueenAttack(n, queens, queries);

        System.out.println(Arrays.toString(result));
    }


    boolean[] squaresUnderQueenAttack(int n, int[][] queens, int[][] queries) {
        List<Queen> queenList = Arrays.stream(queens).map(queen -> new Queen(queen[1], queen[0])).collect(Collectors.toList());
        List<Square> squareList = Arrays.stream(queries).map(square -> new Square(square[1], square[0])).collect(Collectors.toList());

        List<Boolean> preresult = squareList.stream().map(square -> queenList.stream().anyMatch(queen -> queen.isUnderAttack(square))).collect(Collectors.toList());

        boolean[] result = new boolean[preresult.size()];

        for (int i = 0; i < squareList.size(); i++) {
            result[i] = preresult.get(i);
        }
        return result;
    }

    private class Queen {
        private int x;
        private int y;

        public Queen(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public boolean isUnderAttack(Square square) {
            if (square.x == this.x || square.y == this.y) return true;

            return Math.abs(square.x - this.x) == Math.abs(square.y - this.y);
        }
    }

    private class Square {
        private int x;
        private int y;

        public Square(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }
}
