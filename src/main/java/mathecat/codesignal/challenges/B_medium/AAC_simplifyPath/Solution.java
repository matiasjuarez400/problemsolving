package mathecat.codesignal.challenges.B_medium.AAC_simplifyPath;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String path = "/home/a/./x/../b//c/";

        System.out.println(solution.simplifyPath(path));
    }

    String simplifyPath(String path) {
        Pattern pattern = Pattern.compile("([\\w\\.]+)");
        Matcher matcher = pattern.matcher(path);

        List<String> elements = new ArrayList<>();
        while (matcher.find()) {
            elements.add(matcher.group(1));
        }

        List<String> result = new ArrayList<>();
        for (String element : elements) {
            if (element.equals(".")) continue;
            if (element.equals("..")) {
                if (result.size() == 0) continue;
                result.remove(result.size() - 1);
            } else {
                result.add(element);
            }
        }

        return "/" + String.join("/", result);
    }
}
