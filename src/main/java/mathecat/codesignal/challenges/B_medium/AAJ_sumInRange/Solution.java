package mathecat.codesignal.challenges.B_medium.AAJ_sumInRange;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.test1();
    }

    void test1() {
        int[] nums = {3, 0, -2, 6, -3, 2};
        int[][] queries = {
                {0,2}, 
                 {2,5},
                 {0,5}
        };

        System.out.println(sumInRange(nums, queries));
    }

    void test2() {
        int[] nums = {-1000};
        int[][] queries = {
                {0, 0}
        };

        System.out.println(sumInRange(nums, queries));
    }

    int sumInRange(int[] nums, int[][] queries) {
        queries = sortQueries(queries);
        long module = (long) Math.pow(10, 9) + 7;
        int acc = 0;
        Cache cache = new Cache();
        for (int[] query : queries) {
            int left = query[0];
            int right = query[1];
            long currentSum = 0;

            Pair closestPair = cache.getClosestPair(left);
            if (closestPair != null) {
                left = closestPair.rightIndex + 1;
                currentSum = closestPair.sum;
            }

            for (int i = left; i <= right; i++) {
                currentSum += nums[i];
            }

            cache.saveResult(left, right, currentSum);
            acc += currentSum % module;
        }

        return (acc > 0 ? acc : (int) (module + acc));
    }

    private static int[][] sortQueries(int[][] queries) {
        return Arrays.stream(queries)
                .sorted(Comparator.comparing((int[] a) -> a[0]).thenComparingInt((int[] a) -> a[1]))
                .toArray(int[][]::new);
    }

    private static class Cache {
        Map<Integer, List<Pair>> queriesByLeftIndex = new HashMap<>();

        public Pair getClosestPair(int left) {
            List<Pair> pairs = queriesByLeftIndex.get(left);
            if (pairs == null) return null;
            return pairs.get(pairs.size() - 1);
        }

        public void saveResult(int left, int right, long sum) {
            queriesByLeftIndex.compute(left, (k, v) -> v == null ? new ArrayList<>() : v).add(new Pair(right, sum));
        }
    }

    private static class Pair {
        int rightIndex;
        long sum;

        Pair(){}
        Pair(int rightIndex, long sum) {
            this.rightIndex = rightIndex;
            this.sum = sum;
        }
    }
}
