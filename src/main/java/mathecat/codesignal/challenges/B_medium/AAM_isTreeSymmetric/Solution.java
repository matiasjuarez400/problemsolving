package mathecat.codesignal.challenges.B_medium.AAM_isTreeSymmetric;

import mathecat.codesignal.common.Tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.test2();
    }

    void test2() {
        String input= "{\n" +
                "    \"value\": 1,\n" +
                "    \"left\": {\n" +
                "        \"value\": 2,\n" +
                "        \"left\": null,\n" +
                "        \"right\": {\n" +
                "            \"value\": 3,\n" +
                "            \"left\": null,\n" +
                "            \"right\": null\n" +
                "        }\n" +
                "    },\n" +
                "    \"right\": {\n" +
                "        \"value\": 2,\n" +
                "        \"left\": null,\n" +
                "        \"right\": {\n" +
                "            \"value\": 3,\n" +
                "            \"left\": null,\n" +
                "            \"right\": null\n" +
                "        }\n" +
                "    }\n" +
                "}";

        Tree<Integer> t = Tree.parse(input, Integer.class);

        System.out.println(isTreeSymmetric(t));
    }

    void test1() {
        String input = "{\n" +
                "    \"value\": 1,\n" +
                "    \"left\": {\n" +
                "        \"value\": 2,\n" +
                "        \"left\": {\n" +
                "            \"value\": 3,\n" +
                "            \"left\": null,\n" +
                "            \"right\": null\n" +
                "        },\n" +
                "        \"right\": {\n" +
                "            \"value\": 4,\n" +
                "            \"left\": null,\n" +
                "            \"right\": null\n" +
                "        }\n" +
                "    },\n" +
                "    \"right\": {\n" +
                "        \"value\": 2,\n" +
                "        \"left\": {\n" +
                "            \"value\": 4,\n" +
                "            \"left\": null,\n" +
                "            \"right\": null\n" +
                "        },\n" +
                "        \"right\": {\n" +
                "            \"value\": 3,\n" +
                "            \"left\": null,\n" +
                "            \"right\": null\n" +
                "        }\n" +
                "    }\n" +
                "}";

        Tree<Integer> t = Tree.parse(input, Integer.class);

        System.out.println(isTreeSymmetric(t));
    }

    boolean isTreeSymmetric(Tree<Integer> t) {
        Map<Integer, List<Integer>> nodesByVerticalLevel = new HashMap<>();

        calculateNodeInfo(t, nodesByVerticalLevel, 0, 0);

        if (nodesByVerticalLevel.size() <= 1) return true;

        for (Integer verticalLevel : nodesByVerticalLevel.keySet()) {
            List<Integer> currentLevel = nodesByVerticalLevel.get(verticalLevel);
            final int currentLevelSize = currentLevel.size();

            int forLimit = currentLevelSize / 2;
            for (int i = 0; i < forLimit; i++) {
                final Integer leftValue = currentLevel.get(i);
                final Integer rightValue = currentLevel.get(currentLevelSize - 1 - i);

                if (leftValue == null && rightValue == null) continue;
                if (leftValue == null ^ rightValue == null) return false;

                if (!currentLevel.get(i).equals(currentLevel.get(currentLevelSize - 1 - i))) return false;
            }
        }
        return true;
    }

    private void calculateNodeInfo(Tree<Integer> t, Map<Integer, List<Integer>> nodeMap, int verticalLevel, int horizontalLevel) {
        nodeMap.compute(verticalLevel, (k, v) -> v == null ? new ArrayList<>() : v).add(t == null ? null : t.value);
        if (t == null) return;

        calculateNodeInfo(t.left, nodeMap, verticalLevel + 1, horizontalLevel - 1);
        calculateNodeInfo(t.right, nodeMap, verticalLevel + 1, horizontalLevel);
    }
}
