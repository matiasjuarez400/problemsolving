package mathecat.codesignal.challenges.B_medium.AAG_prductExceptSelf;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] nums = {37, 50, 50, 6, 8, 54, 7, 64, 2, 64, 67, 59, 22, 73, 33, 53, 43, 77, 56, 76, 59, 96, 64, 100, 89, 38, 64, 73, 85, 96, 65, 50, 62, 4, 82, 57, 98, 61, 92, 55, 80, 53, 80, 55, 94, 9, 73, 89, 83, 80};
        int m = 67;

        System.out.println(solution.productExceptSelf(nums, m));
        System.out.println(solution.productExceptSelfOld(nums, m));
    }

    int productExceptSelfOld(int[] nums, int m) {
        BigInteger bigM = BigInteger.valueOf(m);

        BigInteger baseMultiplication = BigInteger.ONE;

        for (int i : nums) {
            baseMultiplication = baseMultiplication.multiply(BigInteger.valueOf(i));
        }

        System.out.println("BASE MULTIPLICATION: " + baseMultiplication);

        BigInteger acc = BigInteger.ZERO;
        for (int i = 0; i < nums.length; i++) {
            acc = acc.add(baseMultiplication.divide(BigInteger.valueOf(nums[i])).mod(bigM));
        }

        return acc.mod(bigM).intValue();
    }

    int productExceptSelf(int[] nums, int m) {
        BigInteger bigM = BigInteger.valueOf(m);
        Map<Integer, Long> countByNumber = Arrays.stream(nums)
                .boxed()
                .collect(Collectors.groupingBy(i -> i, Collectors.counting()));

        final Counter baseMultiplication = new Counter(BigInteger.ONE);
        countByNumber.forEach((k, v) -> {
            BigInteger currentMultiplication = BigInteger.valueOf(k).pow(v.intValue());

            baseMultiplication.multiply(currentMultiplication);
        });

        System.out.println("BASE MULTIPLICATION: " + baseMultiplication);

        final Counter acc = new Counter(BigInteger.ZERO);
        countByNumber.forEach((k, v) -> {
            BigInteger multiplicationForK = baseMultiplication.getCounter().divide(BigInteger.valueOf(k));
            BigInteger modForK = multiplicationForK.mod(bigM);
            acc.add(modForK.multiply(BigInteger.valueOf(v)));
        });

        return acc.getCounter().mod(bigM).intValue();
    }

    private static class Counter {
        private BigInteger counter;

        public Counter(BigInteger initValue) {
            counter = initValue;
        }

        public void add(BigInteger value) {
            counter = counter.add(value);
        }

        public void multiply(BigInteger value) {
            counter = counter.multiply(value);
        }

        public BigInteger getCounter() {
            return counter;
        }

        @Override
        public String toString() {
            return getCounter().toString();
        }
    }
}
