package mathecat.codesignal.challenges.B_medium.AAN_sortZeroOneTwoList;

import mathecat.codesignal.common.ListNode2;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.test2();
    }

    void test1() {
        Integer[] array = {2, 1, 0};
        ListNode2<Integer> listNode2 = ListNode2.createFromArray(array);

        System.out.println(sortZeroOneTwoList(listNode2));
    }

    void test2() {
        Integer[] array = {0, 1, 0, 1, 2, 0};
        ListNode2<Integer> listNode2 = ListNode2.createFromArray(array);

        System.out.println(sortZeroOneTwoList(listNode2));
    }

    ListNode2<Integer> sortZeroOneTwoList(ListNode2<Integer> l) {
        return sortMethod3(l);
    }

    private ListNode2<Integer> sortMethod3(ListNode2<Integer> l) {
        if (l == null) return null;

        int[] counter = new int[3];

        ListNode2<Integer> current = l;

        do {
            counter[current.value]++;
        } while ((current = current.next) != null);

        current = l;
        for (int nextNumberIndex = 0; nextNumberIndex < counter.length; nextNumberIndex++) {
            int ocurrences = counter[nextNumberIndex];
            for (int acc = 0; acc < ocurrences; acc++) {
                current.value = nextNumberIndex;
                current = current.next;
            }
        }

        return l;
    }

    private ListNode2<Integer> sortMethod2(ListNode2<Integer> l) {
        int itCount = 0;

        if (l == null) return null;

        boolean keepSorting = true;
        ListNode2<Integer> current = l;
        while (keepSorting) {
            itCount++;
            keepSorting = false;

            do {
                if (current.next != null) {
                    if (current.value > current.next.value) {
                        Integer temp = current.value;
                        current.value = current.next.value;
                        current.next.value = temp;
                        keepSorting = true;
                    }
                }
            } while ((current = current.next) != null);

            current = l;
        }

        System.out.println(itCount);
        return l;
    }

    private ListNode2<Integer> sortMethod1(ListNode2<Integer> l) {
        if (l == null) return null;

        ListNode2<Integer> current = l;
        while (current != null) {
            ListNode2<Integer> comparing = current;
            while ((comparing = comparing.next) != null) {
                if (comparing.value < current.value) {
                    Integer temp = current.value;
                    current.value = comparing.value;
                    comparing.value = temp;
                }
            }
            current = current.next;
        }

        return l;
    }
}
