package mathecat.codesignal.challenges.B_medium.AAS_shoppingList;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String items = "Doughnuts, 4; doughnuts holes, 0.08; glue, 3.4";

        System.out.println(solution.shoppingList(items));;
    }

    double shoppingList(String items) {
        final Pattern pattern = Pattern.compile("(\\d+(?:\\.\\d{1,2})?)");

        final Matcher matcher = pattern.matcher(items);

        double acc = 0;

        while (matcher.find()) {
            acc += Double.parseDouble(matcher.group(1));
        }

        return acc;
    }
}
