package mathecat.codesignal.challenges.B_medium.AAK_sumSubsets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.test8();

        System.out.println(Arrays.deepToString(new int[1][0]));
        System.out.println(Arrays.deepToString(new int[1][]));
        System.out.println(Arrays.deepToString(new int[0][]));
    }

    void test8() {
        int[] arr = {1, 1, 500, 500, 500, 1000, 1000};
        int num = 5501;
        int testNumber = 8;
        String result = Arrays.deepToString(sumSubsets(arr, num));

        System.out.println(String.format("Test %s)\n %s", testNumber, result.replace("],", "],\n")));
    }

    void test7() {
        int[] arr = {1, 1, 2, 11, 11, 13, 13};
        int num = 24;
        int testNumber = 7;
        String result = Arrays.deepToString(sumSubsets(arr, num));

        System.out.println(String.format("Test %s)\n %s", testNumber, result.replace("],", "],\n")));
    }

    void test6() {
        int[] arr = {1, 1, 2, 4, 4, 4, 7, 9, 9, 13, 13, 13, 15, 15, 16, 16, 16, 19, 19, 20};
        int num = 36;
        int testNumber = 6;
        String result = Arrays.deepToString(sumSubsets(arr, num));

        System.out.println(String.format("Test %s)\n %s", testNumber, result.replace("],", "],\n")));
    }

    void test4() {
        int[] arr = {1, 1, 1, 1, 1, 1, 1, 1, 1};
        int num = 9;
        int testNumber = 4;
        String result = Arrays.deepToString(sumSubsets(arr, num));

        System.out.println(String.format("Test %s) %s", testNumber, result));
    }

    void test3() {
        int[] arr = {};
        int num = 0;
        int testNumber = 3;
        String result = Arrays.deepToString(sumSubsets(arr, num));

        System.out.println(String.format("Test %s) %s", testNumber, result));
    }

    void test2() {
        int[] arr = {1, 2, 2, 3, 4, 5};
        int num = 5;
        int testNumber = 2;
        String result = Arrays.deepToString(sumSubsets(arr, num));

        System.out.println(String.format("Test %s) %s", testNumber, result));
    }

    void test1() {
        int[] arr = {1, 2, 3, 4, 5};
        int num = 5;
        int testNumber = 1;
        String result = Arrays.deepToString(sumSubsets(arr, num));

        System.out.println(String.format("Test %s) %s", testNumber, result));
    }

    int[][] sumSubsets(int[] arr, int num) {
        int[][] defaultResult = new int[1][0];

        if (arr == null || arr.length == 0) return defaultResult;

        List<List<Integer>> validCombinationIndexes = new ArrayList<>();

        final List<Integer> currentCombination = new ArrayList<>();
        currentCombination.add(0);

        while (!currentCombination.isEmpty()) {
            int currentSum = currentCombination.stream()
                    .mapToInt(i -> arr[i])
                    .sum();

            if (currentSum == num) {
                validCombinationIndexes.add(new ArrayList<>(currentCombination));
                removeLastIndexAndIncrementPrevious(currentCombination, arr.length - 1);
            } else if (currentSum > num) {
                removeLastIndexAndIncrementPrevious(currentCombination, arr.length - 1);
            } else {
                int lastIndex = getLastElement(currentCombination);
                if (lastIndex < arr.length - 1) {
                    currentCombination.add(lastIndex + 1);
                } else {
                    removeLastIndexAndIncrementPrevious(currentCombination, arr.length - 1);
                }
            }
        }

        if (validCombinationIndexes.isEmpty()) return defaultResult;

        List<List<Integer>> validSets = validCombinationIndexes.stream()
                .map(indexes -> {
                    List<Integer> inputArrayValues = new ArrayList<>();
                    for (Integer index : indexes) {
                        inputArrayValues.add(arr[index]);
                    }
                    return inputArrayValues;
                }).collect(Collectors.toList());

        List<List<Integer>> validSetsWithoutDuplicates = removeDuplicateSets(validSets);
        // This call to sortLexicographically is unnecessary because the input array is sorted
        validSetsWithoutDuplicates = sortLexicographically(validSetsWithoutDuplicates);

        int[][] outputArray = new int[validSetsWithoutDuplicates.size()][];
        for (int i = 0; i < validSetsWithoutDuplicates.size(); i++) {
            List<Integer> outputSet = validSetsWithoutDuplicates.get(i);
            outputArray[i] = new int[outputSet.size()];
            for (int j = 0; j < outputSet.size(); j++) {
                outputArray[i][j] = outputSet.get(j);
            }
        }
        return outputArray;
    }

    private Integer getLastElement(List<Integer> combination) {
        return combination.get(getLastIndex(combination));
    }

    private Integer getLastIndex(List<Integer> combination) {
        return combination.size() - 1;
    }

    private void removeLastIndexAndIncrementPrevious(List<Integer> combination, int maxIndex) {
        combination.remove((int) getLastIndex(combination));

        while (!combination.isEmpty()) {
            int lastIndex = getLastElement(combination);
            int newIndex = lastIndex + 1;
            if (newIndex > maxIndex) {
                combination.remove(getLastIndex(combination));
                continue;
            }
            combination.set(getLastIndex(combination), newIndex);
            break;
        }
    }

    private List<List<Integer>> removeDuplicateSets(List<List<Integer>> sets) {
        List<List<Integer>> validSetsWithoutDuplicates = new ArrayList<>();
        for (List<Integer> validSet : sets) {
            if (validSetsWithoutDuplicates.isEmpty()) {
                validSetsWithoutDuplicates.add(validSet);
            } else {
                boolean isContainedSet = false;
                for (List<Integer> notDuplicatedSet : validSetsWithoutDuplicates) {
                    if (notDuplicatedSet.size() == validSet.size()) {
                        boolean foundDifferentElement = false;
                        for (int i = 0; i < notDuplicatedSet.size(); i++) {
                            if (!notDuplicatedSet.get(i).equals(validSet.get(i))) {
                                foundDifferentElement = true;
                                break;
                            }
                        }

                        if (!foundDifferentElement) {
                            isContainedSet = true;
                            break;
                        }
                    }
                }

                if (!isContainedSet) {
                    validSetsWithoutDuplicates.add(validSet);
                }
            }
        }

        return validSetsWithoutDuplicates;
    }

    private List<List<Integer>> sortLexicographically(List<List<Integer>> result) {
        return result.stream()
                .sorted((o1, o2) -> {
                    int limit = Integer.min(o1.size(), o2.size());
                    for (int i = 0; i < limit; i++) {
                        if (o1.get(i) < o2.get(i)) return -1;
                        if (o1.get(i) > o2.get(i)) return 1;
                    }

                    return o1.size() - o2.size();
                }).collect(Collectors.toList());
    }
}
