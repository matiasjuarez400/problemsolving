package mathecat.codesignal.challenges.B_medium.AAH_findSubarrayBySum;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
    }

    int[] findSubarrayBySum(int s, int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            int acc = 0;
            for (int j = i; j < arr.length; j++) {
                acc += arr[j];
                if (acc == s) return new int[] {i + 1, j + 1};
                if (acc > s) break;
            }
        }

        return new int[] {-1};
    }
}
