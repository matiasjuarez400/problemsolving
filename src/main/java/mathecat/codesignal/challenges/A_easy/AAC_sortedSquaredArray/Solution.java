package mathecat.codesignal.challenges.A_easy.AAC_sortedSquaredArray;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(Arrays.toString(solution.sortedSquaredArray(new int[] {-6, -4, 1, 2, 3, 5})));
    }

    int[] sortedSquaredArray(int[] array) {
        return Arrays.stream(array).map(i -> i * i).sorted().toArray();
    }
}
