package mathecat.codesignal.challenges.A_easy.AAA_mostFrequentSum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.test2();
    }

    void test1() {
        Tree<Integer> root = new Tree<>(1);

        root.setLeft(2);
        root.setRight(3);

        System.out.println(Arrays.toString(mostFrequentSum(root)));
    }

    void test2() {
        Tree<Integer> root = new Tree<>(-2);

        root.setLeft(-3);
        root.setRight(2);

        System.out.println(Arrays.toString(mostFrequentSum(root)));
    }

    int[] mostFrequentSum(Tree<Integer> t) {
        if (t == null) return new int[0];

        List<Integer> sums = new ArrayList<>();
        calculateTreeSum(t, sums);

        Map<Integer, Long> ocurrencesBySum = sums.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        int maxSumOcurrence = ocurrencesBySum.values().stream()
                .mapToInt(Long::intValue).max().orElse(Integer.MIN_VALUE);

        List<Integer> sumsWithMaxOcurrence = new ArrayList<>();
        ocurrencesBySum.forEach((sum, ocurrence) -> {
            if (ocurrence == maxSumOcurrence) sumsWithMaxOcurrence.add(sum);
        });

        return sumsWithMaxOcurrence.stream().mapToInt(i -> i).sorted().toArray();
    }

    int calculateTreeSum(Tree<Integer> tree, List<Integer> sums) {
        int result = Optional.ofNullable(tree.value).orElse(0);

        if (tree.left != null) {
            result += calculateTreeSum(tree.left, sums);
        }

        if (tree.right != null) {
            result += calculateTreeSum(tree.right, sums);
        }

        sums.add(result);

        return result;
    }

    static class Tree<T> {
        Tree(T x) {
            value = x;
        }

        T value;
        Tree<T> left;
        Tree<T> right;

        Tree<T> setLeft(T value) {
            left = new Tree<>(value);
            return left;
        }

        Tree<T> setRight(T value) {
            right = new Tree<>(value);
            return right;
        }
    }
}
