package mathecat.codesignal.challenges.A_easy.AAB_phoneCall;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.test2();
    }

    void test2() {
        System.out.println(phoneCall(1, 2, 1, 6));
    }

    void test1() {
        System.out.println(phoneCall(3, 1, 2, 20));
    }

    int phoneCall(int min1, int min2_10, int min11, int s) {
        int callDuration = 0;

        s = s - min1;
        if (s >= 0) callDuration++;

        if (s <= 0) return callDuration;

        int min2_10Minutes = Integer.min(s / min2_10, 9);
        s = s - min2_10Minutes * min2_10;
        callDuration += min2_10Minutes;

        if (min2_10Minutes == 9) {
            int min11Minutes = s / min11;
            callDuration += min11Minutes;
        }

        return callDuration;
    }
}
