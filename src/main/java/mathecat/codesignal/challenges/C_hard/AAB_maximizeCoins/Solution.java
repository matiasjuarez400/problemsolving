package mathecat.codesignal.challenges.C_hard.AAB_maximizeCoins;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        
        solution.test1();
        solution.test2();
        solution.test3();
        solution.test4();
        solution.test5();
    }

    void test5() {
        int[][] coins = {
                {8,3}, 
             {6,7}, 
             {1,5}, 
             {10,4}, 
             {6,2}, 
             {5,1}, 
             {8,0}, 
             {10,7}, 
             {8,6}, 
             {0,10}
        };

        int expected = 5;
        int testNumber = 5;

        runTest(coins, expected, testNumber);
    }

    void test4() {
        int[][] coins = {
                {3,3}, 
             {1,3}, 
             {7,4}, 
             {4,0}, 
             {2,2}, 
             {7,3}, 
             {8,8}, 
             {3,9}, 
             {1,8}, 
             {6,7}
        };

        int expected = 5;
        int testNumber = 4;

        runTest(coins, expected, testNumber);
    }

    void test3() {
        int[][] coins = {
                {0,0}, 
                 {0,1}, 
                 {1,2}, 
                 {4,3}, 
                 {8,3}, 
                 {4,6}, 
                 {7,6}
        };

        int expected = 6;
        int testNumber = 3;

        runTest(coins, expected, testNumber);
    }
    
    void test2() {
        int[][] coins = {
                {0,0}
        };

        int expected = 1;
        int testNumber = 2;

        runTest(coins, expected, testNumber);
    }

    void test1() {
        int[][] coins = {
                {0,1},
                {1,1},
                {2,0},
                {1,2},
                {2,2}
        };

        int expected = 4;
        int testNumber = 1;

        runTest(coins, expected, testNumber);
    }

    void runTest(int[][] coins, int expected, int testNumber) {
        int result = maximizeCoins(coins);

        if (result != expected) {
            System.out.println(String.format("%s) Expected: [%s]. Result: [%s].", testNumber, expected, result));
        } else {
            System.out.println("PASSED");
        }
    }

    int maximizeCoins(int[][] coins) {
        Memory memory = new Memory(coins);
        LocalMemory localMemory = new LocalMemory(0, 0, 0, 0);
        findMaxCoins2(memory, localMemory, 0, 0);

        return memory.getMaxAccumulated();
    }

    private void findMaxCoins(Memory memory, LocalMemory previousStepMemory, int row, int col) {
        if (row > memory.getRowLimit() || col > memory.getColLimit()) return;

        int accInPosition = memory.getCoins(row, col) + previousStepMemory.getAccumulated();
        if (accInPosition > memory.getMaxAccumulated()) {
            memory.setMaxAccumulated(accInPosition);
        }

        findMaxCoinsInDirection(memory, previousStepMemory, accInPosition, row, col, 0, 1);
        findMaxCoinsInDirection(memory, previousStepMemory, accInPosition, row, col, 1, 0);
    }

    private void findMaxCoins2(Memory memory, LocalMemory previousStepMemory, int row, int col) {
        if (row > memory.getRowLimit() || col > memory.getColLimit()) return;

        int accInPosition = memory.getCoins(row, col) + previousStepMemory.getAccumulated();
        if (accInPosition > memory.getMaxAccumulated()) {
            memory.setMaxAccumulated(accInPosition);
        }

        int nextCoinInRow = memory.getNextCoinInRow(col, row);
        int nextCoinInCol = memory.getNextCoinInCol(row, col);

        if (nextCoinInRow > 0) {
            findMaxCoinsInDirection2(memory, previousStepMemory, accInPosition, row, col, 0, nextCoinInCol - col);
        } else {
            while (!memory.isCoinInRow(col, row) && row < memory.getRowLimit()) {
                row++;
            }
        }

        if (nextCoinInCol > 0) {
            findMaxCoinsInDirection2(memory, previousStepMemory, accInPosition, row, col, nextCoinInRow - row, 0);
        } else {
            while (!memory.isCoinInRow(row, col) && col < memory.getColLimit()) {
                col++;
            }
        }
    }

    private void findMaxCoinsInDirection2(Memory memory, LocalMemory previousStepMemory, int accInPosition, int row, int col, int dxRow, int dxCol) {
        int coinsLostWithTransition;
        if (dxCol != 0) {
            coinsLostWithTransition = getCoinsLostWithTransition2(memory, row, col, accInPosition, previousStepMemory, dxRow, dxCol);
        } else if (dxRow != 0) {
            coinsLostWithTransition = getCoinsLostWithTransition2(memory, row, col, accInPosition, previousStepMemory, dxRow, dxCol);
        } else {
            throw new IllegalStateException("No transition in column or row");
        }

        int coinsLeft = getCoinsLeft(memory.getTotalCoins(), coinsLostWithTransition, accInPosition);
        LocalMemory currentLocalMemory = new LocalMemory(accInPosition, row, col, coinsLostWithTransition);

        if (coinsLeft + accInPosition < memory.getMaxAccumulated()) {
//            System.out.println(buildBreakingMessage(currentLocalMemory, memory, coinsLeft, row, col, dxCol != 0));
        } else {
            findMaxCoins2(memory, currentLocalMemory, row + dxRow, col + dxCol);
        }
    }

    private void findMaxCoinsInDirection(Memory memory, LocalMemory previousStepMemory, int accInPosition, int row, int col, int dxRow, int dxCol) {
        int coinsLostWithTransition;
        if (dxCol != 0) {
            coinsLostWithTransition = getCoinsLostWithTransition(memory.getCoinsInCol(col), accInPosition, previousStepMemory);
        } else if (dxRow != 0) {
            coinsLostWithTransition = getCoinsLostWithTransition(memory.getCoinsInRow(row), accInPosition, previousStepMemory);
        } else {
            throw new IllegalStateException("No transition in column or row");
        }

        int coinsLeft = getCoinsLeft(memory.getTotalCoins(), coinsLostWithTransition, accInPosition);
        LocalMemory currentLocalMemory = new LocalMemory(accInPosition, row, col, coinsLostWithTransition);

        if (coinsLeft + accInPosition < memory.getMaxAccumulated()) {
//            System.out.println(buildBreakingMessage(currentLocalMemory, memory, coinsLeft, row, col, dxCol != 0));
        } else {
            findMaxCoins(memory, currentLocalMemory, row + dxRow, col + dxCol);
        }
    }

    private String buildBreakingMessage(LocalMemory localMemory, Memory memory, int coinsLeft, int row, int col, boolean colTransition) {
        return String.format("Breaking at position [%s, %s]. Coins left: [%s]. Accumulated: [%s]. Max accumulated: [%s]. ColTransition: [%s]",
                    row, col, coinsLeft, localMemory.getAccumulated(), memory.getMaxAccumulated(), colTransition
                );
    }

    private int getCoinsLeft(int totalCoins, int lostCoins, int accumulated) {
        return totalCoins - lostCoins - accumulated;
    }

    private int getCoinsLostWithTransition2(Memory memory, int row, int col, int accumulatedInPosition, LocalMemory previousMemory, int dxRow, int dxCol) {
        int accumulatedLost = 0;
        for (int ri = row; ri < row + dxRow; ri++) {
            accumulatedLost += memory.getCoinsInRow(ri);
        }

        for (int ci = col; ci < col + dxCol; ci++) {
            accumulatedLost += memory.getCoinsInCol(ci);
        }

        return accumulatedLost - accumulatedInPosition + previousMemory.getLostCoins();
    }

    private int getCoinsLostWithTransition(int coinsInDirection, int accumulatedInPosition, LocalMemory previousMemory) {
        return coinsInDirection - accumulatedInPosition + previousMemory.getLostCoins();
    }

    private static class LocalMemory {
        private int accumulated;
        private int row;
        private int col;
        private int lostCoins;

        public LocalMemory(int accumulated, int row, int col, int lostCoins) {
            this.accumulated = accumulated;
            this.row = row;
            this.col = col;
            this.lostCoins = lostCoins;
        }

        public int getAccumulated() {
            return accumulated;
        }

        public int getRow() {
            return row;
        }

        public int getCol() {
            return col;
        }

        public int getLostCoins() {
            return lostCoins;
        }
    }

    private static class Memory {
        private Map<Integer, List<Integer>> coinsByRow = new HashMap<>();
        private Map<Integer, List<Integer>> coinsByCol = new HashMap<>();
        private Map<Integer, Map<Integer, Integer>> coinsInPosition = new HashMap<>();
        private int maxAccumulated = Integer.MIN_VALUE;
        private int dRow = 1;
        private int dCol = 1;
        private int totalCoins;
        private int rowLimit;
        private int colLimit;

        public Memory(int[][] coins) {
            this.totalCoins = coins.length;
            for (int[] coin : coins) {
                int row = coin[0];
                int col = coin[1];

                coinsByRow.compute(row, (k, v) -> v == null ? new ArrayList<>() : v).add(col);
                coinsByCol.compute(col, (k, v) -> v == null ? new ArrayList<>() : v).add(row);

                coinsInPosition.compute(row, (k, v) -> v == null ? new HashMap<>() : v)
                        .compute(col, (k, v) -> v == null ? 1 : v + 1);

                if (row > this.rowLimit) this.rowLimit = row;
                if (col > this.colLimit) this.colLimit = col;
            }
        }

        public Map<Integer, List<Integer>> getCoinsByRow() {
            return coinsByRow;
        }

        public Map<Integer, List<Integer>> getCoinsByCol() {
            return coinsByCol;
        }

        public int getMaxAccumulated() {
            return maxAccumulated;
        }

        public void setMaxAccumulated(int maxAccumulated) {
            this.maxAccumulated = maxAccumulated;
        }

        public int getdRow() {
            return dRow;
        }

        public int getdCol() {
            return dCol;
        }

        public int getCoins(int row, int col) {
            return coinsInPosition.getOrDefault(row, Collections.emptyMap()).getOrDefault(col, 0);
        }

        public int getCoinsInRow(int row) {
            return this.coinsByRow.getOrDefault(row, Collections.emptyList()).size();
        }

        public int getCoinsInCol(int col) {
            return this.coinsByCol.getOrDefault(col, Collections.emptyList()).size();
        }

        public int getNextCoinInRow(int initCol, int row) {
            return getNextInMap(initCol, row, coinsByRow);
        }

        public boolean isCoinInRow(int initCol, int row) {
            return isCoinInDirection(initCol, row, this.coinsByRow);
        }

        public boolean isCoinInCol(int initRow, int col) {
            return isCoinInDirection(initRow, col, this.coinsByCol);
        }

        private boolean isCoinInDirection(int initPostion, int mapKey, Map<Integer, List<Integer>> map) {
            List<Integer> coins = map.get(mapKey);
            if (coins == null) return false;
            return coins.get(coins.size() - 1) >= initPostion;
        }

        public int getNextCoinInCol(int initRow, int col) {
            return getNextInMap(initRow, col, coinsByRow);
        }

        private int getNextInMap(int initialPosition, int mapKey, Map<Integer, List<Integer>> map) {
            List<Integer> coins = map.get(mapKey);
            if (coins == null) return -1;
            return coins.stream().filter(v -> v > initialPosition).findFirst().orElse(-1);
        }

        public int getTotalCoins() {
            return totalCoins;
        }

        public int getColLimit() {
            return colLimit;
        }

        public int getRowLimit() {
            return rowLimit;
        }
    }
}
