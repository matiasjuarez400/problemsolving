package mathecat.codesignal.challenges.C_hard.AAC_seriesEpisodes;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.test3();
    }

    private void test1() {
        int[] duration = {30, 35, 32, 30},
                l = {0, 0, 1, 0, 3, 2},
                r = {2, 0, 3, 3, 3, 2},
                freeTime = {62, 30, 60, 60, 32, 29};

        System.out.println(seriesEpisodes(duration, l, r, freeTime));
    }

    private void test2() {
        int[] duration = {4, 2, 2, 6, 1},
                l = {0, 0, 1, 2},
                r = {3, 3, 4, 4},
                freeTime = {3, 14, 3, 7};

        System.out.println(seriesEpisodes(duration, l, r, freeTime));
    }

    private void test3() {
        int[] duration = {1, 2, 1, 2, 1},
                l = {0, 0, 0, 0, 0, 0, 0, 0},
                r = {4, 4, 4, 4, 4, 4, 4, 4},
                freeTime = {1, 2, 3, 4, 5, 6, 7, 8};

        System.out.println(seriesEpisodes(duration, l, r, freeTime));
    }

    int seriesEpisodes(int[] duration, int[] l, int[] r, int[] freeTime) {
        int acc = 0;


        Map<String, RangeProcessingKnowledge> knowledgeByRange = new HashMap<>();
        for (int i = 0; i < freeTime.length; i++) {
            int currentFreeTime = freeTime[i];
            int leftEpisode = l[i];
            int rightEpisode = r[i];

            String key = leftEpisode + "" + rightEpisode;
            RangeProcessingKnowledge knowledge = knowledgeByRange.computeIfAbsent(key,
                    k -> new RangeProcessingKnowledge(duration, leftEpisode, rightEpisode));

            if (knowledge.canGetFreeTime(currentFreeTime)) {
                acc++;
            }
        }

        return acc;
    }

    private static class RangeProcessingKnowledge {
        private Set<Integer> knownCombinations;
        private int[] sortedDurations;
        private String key;
        private Map<Integer, RecursionInfo> recursionInfoByLevel = new HashMap<>();

        public RangeProcessingKnowledge(int[] durations, int left, int right) {
            this.knownCombinations = new HashSet<>();
            this.sortedDurations = Arrays.copyOfRange(durations, left, right + 1);
            Arrays.sort(this.sortedDurations);
            this.key = left + "" + right;
        }

        public boolean canGetFreeTime(int freeTime) {
            if (knownCombinations.contains(freeTime)) return true;
            boolean canCombine = combine(0, freeTime, 0, 0);
            return canCombine;
        }

        private boolean combine(int left, int freeTime, int lastAccumulated, int recursionLevel) {
            boolean couldCombine = false;

            RecursionInfo recursionInfo = this.recursionInfoByLevel.get(recursionLevel);
            if (recursionInfo != null) {
                left = recursionInfo.getI();
                lastAccumulated = recursionInfo.getAccumulated();
            } else {
                recursionInfo = new RecursionInfo(recursionLevel);
                recursionInfo.setI(left);
                recursionInfo.setAccumulated(lastAccumulated);
                this.recursionInfoByLevel.put(recursionLevel, recursionInfo);
            }

            for (int i = left; i < sortedDurations.length; i++) {
                int currentDuration = sortedDurations[i];
                int newAccumulated = currentDuration + lastAccumulated;

                knownCombinations.add(newAccumulated);

                if (newAccumulated == freeTime) {
                    couldCombine = true;
                    break;
                } else if (newAccumulated < freeTime) {
                    couldCombine = combine(i + 1, freeTime, newAccumulated, recursionLevel + 1);
                    if (couldCombine) {
                        break;
                    } else {
                        this.recursionInfoByLevel.remove(recursionLevel + 1);
                    }
                } else {
                    this.recursionInfoByLevel.remove(recursionLevel);
                    break;
                }
            }

            return couldCombine;
        }

        @Override
        public String toString() {
            return "RangeProcessingKnowledge{" +
                    "key='" + key + '\'' +
                    '}';
        }
    }

    private static class RecursionInfo {
        private int level;
        private int i;
        private int accumulated;

        public RecursionInfo(int level) {
            this.level = level;
        }

        public int getLevel() {
            return level;
        }

        public int getI() {
            return i;
        }

        public void setI(int i) {
            this.i = i;
        }

        public int getAccumulated() {
            return accumulated;
        }

        public void setAccumulated(int accumulated) {
            this.accumulated = accumulated;
        }
    }
}
