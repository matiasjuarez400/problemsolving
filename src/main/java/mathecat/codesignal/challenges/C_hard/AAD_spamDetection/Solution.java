package mathecat.codesignal.challenges.C_hard.AAD_spamDetection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        
        String[][] messages = {
                {"Sale today!", "2837273"},
                {"Unique offer!", "3873827"},
                {"Only today and only for you!", "2837273"},
                {"Sale today!", "2837273"},
                {"Unique offer!", "3873827"}
        };

        String[] signals = {"sale", "discount", "offer"};

        String[] output = solution.spamDetection(messages, signals);

        System.out.println(Arrays.toString(output));
    }

    String[] spamDetection(String[][] rawMessages, String[] spamSignals) {
        final List<Message> messages = Arrays.stream(rawMessages)
                .map(Message::new)
                .collect(Collectors.toList());

        for (int i = 0; i < spamSignals.length; i++) {
            spamSignals[i] = spamSignals[i].toLowerCase();
        }

        final List<Processor> processors = Arrays.asList(
                getMessageLengthProcessor(),
                getPerUserRepetitionProcessor(),
                getAllMessagesRepetitionProcessor(),
                getSpamSignalsProcessor(spamSignals)
        );

        messages.forEach(message ->
                processors.forEach(processor -> processor.process(message))
                );

        final String[] output = new String[processors.size()];

        for (int i = 0; i < processors.size(); i++) {
            output[i] = processors.get(i).getResult();
        }

        return output;
    }

    private static class Message {
        private final String message;
        private final String targetUser;

        public Message(String[] rawData) {
            this.message = rawData[0];
            this.targetUser = rawData[1];
        }

        public String getMessage() {
            return message;
        }

        public String getTargetUser() {
            return targetUser;
        }
    }

    private static abstract class Processor {
        protected static final String PASSED = "passed";
        protected static final String FAILED = "failed";
        protected static final String FAILED_MODEL = FAILED + ": %s";
        private final Matcher matcher = Pattern.compile("([a-zA-Z]+)").matcher("");

        public abstract void process(Message message);
        public abstract String getResult();

        protected double calculateProportion(int total, int count) {
            return (count * 1.0) / total;
        }

        protected String buildFailedMessage(String failedContent) {
            return String.format(FAILED_MODEL, failedContent);
        }

        protected List<String> getWordsFromMessage(Message message) {
            return getWordsFromMessage(message, false);
        }

        protected List<String> getWordsFromMessage(Message message, boolean toLower) {
            matcher.reset(message.getMessage());

            List<String> words = new ArrayList<>();
            while (matcher.find()) {
                String currentWord = matcher.group(1);
                if (toLower) currentWord = currentWord.toLowerCase();

                words.add(currentWord);
            }

            return words;
        }
    }

    private Processor getMessageLengthProcessor() {
        return new Processor() {
            private static final int WORD_COUNT_THRESHOLD = 5;
            private static final double PROPORTION_THRESHOLD = 0.9;
            private int totalMessages;
            private int wrongMessages;

            @Override
            public void process(Message message) {
                List<String> words = getWordsFromMessage(message);

                if (words.size() < WORD_COUNT_THRESHOLD) {
                    wrongMessages++;
                }

                totalMessages++;
            }

            @Override
            public String getResult() {
                double wrongMessagesProportion = calculateProportion(totalMessages, wrongMessages);

                if (wrongMessagesProportion <= PROPORTION_THRESHOLD) return PASSED;

                final String failProportion = calculateReducedFraction(wrongMessages, totalMessages);

                return buildFailedMessage(failProportion);
            }
        };
    }

    private String calculateReducedFraction(final int numerator, final int denominator) {
        int currentPrime = 2;
        int limit = Integer.min(numerator, denominator);

        int currentNumerator = numerator;
        int currentDenominator = denominator;

        while (currentPrime <= limit) {
            if (currentDenominator % currentPrime == 0 && currentNumerator % currentPrime == 0) {
                currentDenominator /= currentPrime;
                currentNumerator /= currentPrime;
            } else {
                if (currentPrime == 2) currentPrime = 3;
                else currentPrime += 2;
            }
        }

        return currentNumerator + "/" + currentDenominator;
    }

    private Processor getPerUserRepetitionProcessor() {
        return new Processor() {
            private static final double PROPORTION_THRESHOLD = 0.5;

            private final Map<String, List<Message>> messagesPerUser = new HashMap<>();

            @Override
            public void process(Message message) {
                messagesPerUser.compute(message.getTargetUser(), (k, v) -> v == null ? new ArrayList<>() : v).add(message);
            }

            @Override
            public String getResult() {
                final List<String> userIdsWithRepeatedMessages = new ArrayList<>();
                messagesPerUser.forEach((userId, messages) -> {
                    if (messages.size() > 1) {
                        Map<String, Long> countByMessage = messages.stream()
                                .collect(Collectors.groupingBy(Message::getMessage, Collectors.counting()));

                        for (Long messageCount : countByMessage.values()) {
                            double proportion = calculateProportion(messages.size(), messageCount.intValue());
                            if (proportion > PROPORTION_THRESHOLD) {
                                userIdsWithRepeatedMessages.add(userId);
                                break;
                            }
                        }
                    }
                });

                if (userIdsWithRepeatedMessages.isEmpty()) return PASSED;

                userIdsWithRepeatedMessages.sort(Comparator.comparingLong(Long::parseLong));

                return buildFailedMessage(String.join(" ", userIdsWithRepeatedMessages));
            }
        };
    }

    private Processor getAllMessagesRepetitionProcessor() {
        return new Processor() {
            private static final double PROPORTION_THRESHOLD = 0.5;

            private final Map<String, Integer> countByMessage = new HashMap<>();
            private int totalMessages;

            @Override
            public void process(Message message) {
                countByMessage.compute(message.getMessage(), (k, v) -> v == null ? 1 : v + 1);
                totalMessages++;
            }

            @Override
            public String getResult() {
                if (totalMessages > 1) {
                    for (Map.Entry<String, Integer> entry : countByMessage.entrySet()) {
                        double proportion = calculateProportion(totalMessages, entry.getValue());
                        if (proportion > PROPORTION_THRESHOLD) return buildFailedMessage(entry.getKey());
                    }
                }

                return PASSED;
            }
        };
    }

    private Processor getSpamSignalsProcessor(String[] spamSignals) {
        return new Processor() {
            private static final double PROPORTION_THRESHOLD = 0.5;

            private final String[] signals;
            private int totalMessages;
            private int messagesWithSpamSignals;

            private final Set<String> spamSignalsFound = new HashSet<>();

            {
                if (spamSignals == null) {
                    signals = new String[0];
                } else {
                    signals = Arrays.copyOf(spamSignals, spamSignals.length);
                }
            }

            @Override
            public void process(Message message) {
                totalMessages++;

                final List<String> words = getWordsFromMessage(message, true);

                boolean foundSpamSignal = false;
                for (String spamSignal : signals) {
                    if (words.contains(spamSignal)) {
                        foundSpamSignal = true;
                        spamSignalsFound.add(spamSignal);
                    }
                }

                if (foundSpamSignal) messagesWithSpamSignals++;
            }

            @Override
            public String getResult() {
                double proportion = calculateProportion(totalMessages, messagesWithSpamSignals);

                if (proportion <= PROPORTION_THRESHOLD) return PASSED;

                final String spamSignals = spamSignalsFound.stream()
                        .sorted()
                        .collect(Collectors.joining(" "));

                return buildFailedMessage(spamSignals);
            }
        };
    }
}
