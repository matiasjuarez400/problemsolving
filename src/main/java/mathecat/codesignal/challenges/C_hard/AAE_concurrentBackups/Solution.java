package mathecat.codesignal.challenges.C_hard.AAE_concurrentBackups;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

// This solution needs some optimization skills related to graph exploration that I still don't have
class Solution {
    private static final Solution solution = new Solution();

    public static void main(String[] args) {
        test1();
        test2();
        test3();
        test4();
        test5();
        test6();
        test7();
        stressBySizeTest();
    }

    private static void stressBySizeTest() {
        final long start = System.currentTimeMillis();

        int threads = 5;

        Random random = new Random();
        int[] documents = new int[15];
        for (int i = 0; i < documents.length; i++) {
            documents[i] = random.nextInt(100);
        }

        int expected = 0;

        String testName = "Random test";

        try {
            validateTest(threads, documents, expected, testName);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        final long end = System.currentTimeMillis();

        System.out.printf("Time spend: %d%n", end-start);
    }

    private static void test7() {
        int threads = 5;

        int[] documents = {2, 1, 1, 3, 5, 3, 4, 2, 3, 1};

        int expected = 5;

        String testName = "Test 7";

        validateTest(threads, documents, expected, testName);
    }

    private static void test6() {
        int threads = 5;

        int[] documents = {2, 1, 1, 3, 5, 3, 4, 2, 4, 1};

        int expected = 6;

        String testName = "Test 6";

        validateTest(threads, documents, expected, testName);
    }

    private static void test5() {
        int threads = 5;

        int[] documents = {10};

        int expected = 10;

        String testName = "Test 5";

        validateTest(threads, documents, expected, testName);
    }

    private static void test4() {
        int threads = 3;

        int[] documents = {};

        int expected = 0;

        String testName = "Test 4";

        validateTest(threads, documents, expected, testName);
    }

    private static void test3() {
        int threads = 1;

        int[] documents = {5};

        int expected = 5;

        String testName = "Test 3";

        validateTest(threads, documents, expected, testName);
    }

    private static void test2() {
        int threads = 2;

        int[] documents = {5, 3, 5, 3, 7};

        int expected = 12;

        String testName = "Test 2";

        validateTest(threads, documents, expected, testName);
    }

    private static void test1() {
        int threads = 2;

        int[] documents = {4, 2, 5};

        int expected = 6;

        String testName = "Test 1";

        validateTest(threads, documents, expected, testName);
    }

    private static void validateTest(int threads, int[] documents, int expectedValue, String testName) {
        final int result = solution.concurrentBackups(threads, documents);

        if (result != expectedValue) {
            throw new IllegalStateException(String.format("Result[%s] for test %s is different than the expected one [%s]", result, testName, expectedValue));
        } else {
            System.out.printf("%s. PASSED%n", testName);
        }
    }

    int concurrentBackups(int threads, int[] documents) {
        final List<Document> documentList = IntStream.range(0, documents.length)
                .mapToObj(i -> new Document(i, documents[i]))
                .collect(Collectors.toList());

        BackupScheduler scheduler = new BackupScheduler(threads, documentList);

        List<ThreadWorker> bestAssignation = scheduler.scheduleBackups();

        return bestAssignation.stream()
                .mapToInt(ThreadWorker::getWorkingTimeNeeded)
                .max()
                .orElseThrow(() -> new IllegalStateException("Couldn't find any Thread assignation"));
    }

    private static class BackupScheduler {
        private final int threads;
        private final List<Document> documentList;
        private int bestTimeSoFar;
        private List<ThreadWorker> bestAssignationSoFar;

        public BackupScheduler(int threads, List<Document> documentList) {
            this.threads = threads;
            this.documentList = documentList;
        }

        public List<ThreadWorker> scheduleBackups() {
            calculateOracleTime();

            final List<ThreadWorker> threadWorkerList = createThreadWorkers(this.threads);
            executeDepthSearchExploration(0, threadWorkerList);

            return bestAssignationSoFar;
        }

        private void executeDepthSearchExploration(int nextDocumentIndex, List<ThreadWorker> threadWorkers) {
            // If we enter this IF, it means that all documents have been assigned to a thread
            if (nextDocumentIndex >= documentList.size()) {
                int currentAssignationMaxTime = threadWorkers.stream()
                        .mapToInt(ThreadWorker::getWorkingTimeNeeded)
                        .max()
                        .orElse(Integer.MAX_VALUE);

                if (currentAssignationMaxTime < bestTimeSoFar) {
                    bestTimeSoFar = currentAssignationMaxTime;
                    bestAssignationSoFar = copyThreadWorkers(threadWorkers);
                }

                return;
            }

            // check if any threadWorker has a worst time than the bestTimeSoFar and exit
            for (ThreadWorker threadWorker : threadWorkers) {
                if (threadWorker.getWorkingTimeNeeded() > bestTimeSoFar) return;
            }

            final Document nextDocument = this.documentList.get(nextDocumentIndex);
            for (ThreadWorker threadWorker : threadWorkers) {
                threadWorker.addTask(nextDocument);
                executeDepthSearchExploration(nextDocumentIndex + 1, threadWorkers);
                threadWorker.removeLastTask();
            }
        }

        private List<ThreadWorker> copyThreadWorkers(List<ThreadWorker> threadWorkers) {
            final List<ThreadWorker> copies = new ArrayList<>();

            for (ThreadWorker threadWorker : threadWorkers) {
                ThreadWorker copy = new ThreadWorker();
                threadWorker.getTasks().forEach(copy::addTask);
                copies.add(copy);
            }

            return copies;
        }

        private List<ThreadWorker> createThreadWorkers(int size) {
            return IntStream.range(0, size)
                    .mapToObj(i -> new ThreadWorker())
                    .collect(Collectors.toList());
        }

        private void calculateOracleTime() {
            final List<ThreadWorker> threadWorkerList = createThreadWorkers(threads);

            documentList.forEach(document -> {
                ThreadWorker threadWithLeastWork = threadWorkerList.stream()
                        .min(Comparator.comparing(ThreadWorker::getWorkingTimeNeeded))
                        .orElseThrow(() -> new IllegalStateException("we should never be here"));

                threadWithLeastWork.addTask(document);
            });

            bestTimeSoFar = threadWorkerList.stream()
                    .max(Comparator.comparing(ThreadWorker::getWorkingTimeNeeded))
                    .map(ThreadWorker::getWorkingTimeNeeded)
                    .orElseThrow(() -> new IllegalStateException("we should never be here"));
            bestAssignationSoFar = threadWorkerList;
        }
    }

    private static class Document {
        private final int index;
        private final int size;

        public Document(int index, int size) {
            this.index = index;
            this.size = size;
        }

        public int getIndex() {
            return index;
        }

        public int getSize() {
            return size;
        }

        @Override
        public String toString() {
            return "Task{" +
                    "index=" + index +
                    ", size=" + size +
                    '}';
        }
    }

    private static class ThreadWorker {
        private static int nextWorkerId = 1;
        private final int workerId;
        private final List<Document> documents;
        private int workingTimeNeeded = 0;

        public ThreadWorker() {
            this.workerId = nextWorkerId++;
            this.documents = new ArrayList<>();
        }

        public int getWorkerId() {
            return workerId;
        }

        public List<Document> getTasks() {
            return documents;
        }

        public void addTask(Document document) {
            this.documents.add(document);
            this.workingTimeNeeded += calculateTimeNeeded(document);
        }

        private int calculateTimeNeeded(Document document) {
            return document.getSize();
        }

        public Document removeLastTask() {
            if (documents.isEmpty()) return null;
            Document removedDocument = documents.remove(documents.size() - 1);
            this.workingTimeNeeded -= calculateTimeNeeded(removedDocument);
            return removedDocument;
        }

        public int getWorkingTimeNeeded() {
            return workingTimeNeeded;
        }

        private int calculateNecessaryTime() {
            return documents.stream()
                    .mapToInt(this::calculateTimeNeeded)
                    .sum();
        }

        @Override
        public String toString() {
            return "ThreadWorker{" +
                    "workerId=" + workerId +
                    ", tasks=" + documents.stream().map(Document::toString).collect(Collectors.joining(" , ")) +
                    '}';
        }
    }
}
