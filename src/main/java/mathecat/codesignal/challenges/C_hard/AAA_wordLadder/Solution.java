package mathecat.codesignal.challenges.C_hard.AAA_wordLadder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String beginWord = "hit";
        String endWord = "cog";
        String[] wordList = {"hot", "dot", "dog", "lot", "log", "cog"};

        System.out.println(solution.wordLadder(beginWord, endWord, wordList));
    }

    int wordLadder(String beginWord, String endWord, String[] wordList) {
//        RecursiveSolution recursiveSolution = new RecursiveSolution();
        ExpansionSolution expansionSolution = new ExpansionSolution();

        return expansionSolution.wordLadder(beginWord, endWord, wordList);
    }

    private static class ExpansionSolution {
        int wordLadder(String beginWord, String endWord, String[] wordList) {
            List<String> allWordsList = new ArrayList<>(Arrays.asList(wordList));
            if (!allWordsList.contains(endWord)) return 0;

            allWordsList.add(beginWord);

            Map<String, Set<String>> validPermutationsMap = learnValidPermutations(allWordsList);

            List<Set<String>> currentExpansion = new ArrayList<>();
            currentExpansion.add(new HashSet<>(Collections.singletonList(beginWord)));

            while (expand(currentExpansion, validPermutationsMap)) {
                Set<String> lastLevel = currentExpansion.get(currentExpansion.size() - 1);
                if (lastLevel.contains(endWord)) return currentExpansion.size();
            }

            return 0;
        }

        private boolean expand(List<Set<String>> currentExpansion, Map<String, Set<String>> validPermutationsMap) {
            final Set<String> expansionLastLevel = currentExpansion.get(currentExpansion.size() - 1);

            final Set<String> levelValidPermutations = new HashSet<>();
            for (String currentLevelMember : expansionLastLevel) {
                Set<String> validPermutationsForMember = validPermutationsMap.get(currentLevelMember);
                if (validPermutationsForMember != null) {
                    levelValidPermutations.addAll(validPermutationsForMember);
                }
            }

            if (expansionLastLevel.containsAll(levelValidPermutations)) return false;

            return currentExpansion.add(levelValidPermutations);
        }

        private Map<String, Set<String>> learnValidPermutations(List<String> wordList) {
            Map<String, Set<String>> map = new HashMap<>();

            for (int i = 0; i < wordList.size(); i++) {
                String currentWord = wordList.get(i);
                for (int j = i + 1; j < wordList.size(); j++) {
                    String comparingWord = wordList.get(j);
                    if (isOneCharDifferent(currentWord, comparingWord)) {
                        map.computeIfAbsent(currentWord, k -> new HashSet<>()).add(comparingWord);
                        map.computeIfAbsent(comparingWord, k -> new HashSet<>()).add(currentWord);
                    }
                }
            }

            return map;
        }

        private boolean isOneCharDifferent(String s1, String s2) {
            if (s1.equals(s2) || Math.abs(s1.length() - s2.length()) > 1) return false;

            int diffCount = 0;
            final int minLength = Integer.min(s1.length(), s2.length());

            for (int i = 0; i < minLength; i++) {
                if (s1.charAt(i) != s2.charAt(i)) {
                    diffCount++;
                    if (diffCount > 1) return false;
                }
            }

            return s1.length() == s2.length() || diffCount == 0;
        }
    }

    // This one works only when the amount of words in wordList is not too big
    private static class RecursiveSolution {
        int wordLadder(String beginWord, String endWord, String[] wordList) {
            List<String> allWordsList = new ArrayList<>(Arrays.asList(wordList));
            if (!allWordsList.contains(endWord)) return 0;

            allWordsList.add(beginWord);

            Map<String, Set<String>> validPermutationsMap = learnValidPermutations(allWordsList);

            List<String> permutationList = new ArrayList<>();
            permutationList.add(beginWord);

            RecursionCommonInfo recursionCommonInfo = new RecursionCommonInfo(
                    validPermutationsMap,
                    endWord,
                    5
            );

            findNextVariation(new HashMap<>(), recursionCommonInfo, beginWord, permutationList);

            if (recursionCommonInfo.getValidPermutations().size() == 0) return 0;

            List<String> shortestPermutation = recursionCommonInfo.getValidPermutations().get(0);

            for (List<String> permutation : recursionCommonInfo.getValidPermutations()) {
                if (permutation.size() < shortestPermutation.size()) {
                    shortestPermutation = permutation;
                }
            }

            return shortestPermutation.size();
        }

        private Map<String, Set<String>> learnValidPermutations(List<String> wordList) {
            Map<String, Set<String>> map = new HashMap<>();

            for (int i = 0; i < wordList.size(); i++) {
                String currentWord = wordList.get(i);
                for (int j = i + 1; j < wordList.size(); j++) {
                    String comparingWord = wordList.get(j);
                    if (isOneCharDifferent(currentWord, comparingWord)) {
                        map.computeIfAbsent(currentWord, k -> new HashSet<>()).add(comparingWord);
                        map.computeIfAbsent(comparingWord, k -> new HashSet<>()).add(currentWord);
                    }
                }
            }

            return map;
        }

        private class RecursionCommonInfo {
            private Map<String, Set<String>> validPermutationMap;
            private String endWord;
            private List<List<String>> permutationsFound;
            private List<Set<String>> permutationsFoundAsSet;
            private Set<String> wordWithUsageLimitReached;
            private int wordUseLimit;

            public RecursionCommonInfo(Map<String, Set<String>> validPermutationMap, String endWord, int wordUseLimit) {
                this.validPermutationMap = validPermutationMap;
                this.endWord = endWord;
                this.permutationsFound = new ArrayList<>();
                this.permutationsFoundAsSet = new ArrayList<>();
                this.wordWithUsageLimitReached = new HashSet<>();
                this.wordUseLimit = wordUseLimit;
            }

            public void addPermutation(List<String> permutation) {
                permutationsFound.add(permutation);
                permutationsFoundAsSet.add(new HashSet<>(permutation));
            }

            public List<List<String>> getValidPermutationsWithWord(String word) {
                List<List<String>> permutationsWithWord = new ArrayList<>();
                for(int i = 0; i < permutationsFoundAsSet.size(); i++) {
                    if (permutationsFoundAsSet.get(i).contains(word)) {
                        permutationsWithWord.add(permutationsFound.get(i));
                    }
                }

                return permutationsWithWord;
            }

            public Map<String, Set<String>> getValidPermutationMap() {
                return validPermutationMap;
            }

            public String getEndWord() {
                return endWord;
            }

            public List<List<String>> getValidPermutations() {
                return permutationsFound;
            }

            public Set<String> getWordWithUsageLimitReached() {
                return wordWithUsageLimitReached;
            }

            public int getWordUseLimit() {
                return wordUseLimit;
            }
        }

        private void findNextVariation(Map<String, Integer> usesByWord, RecursionCommonInfo commonInfo,
                                       String currentWord, List<String> permutations) {
            Set<String> validPermutations = Optional.ofNullable(commonInfo.getValidPermutationMap().get(currentWord)).orElse(Collections.emptySet());

            for (String validPermutation : validPermutations) {
                if (commonInfo.getWordWithUsageLimitReached().contains(validPermutation)) continue;

                Map<String, Integer> usesByWordCopy = new HashMap<>(usesByWord);
                usesByWordCopy.compute(validPermutation, (k, v) -> v == null ? 1 : v + 1);

                if (usesByWordCopy.get(validPermutation) > commonInfo.getWordUseLimit()) {
                    commonInfo.getWordWithUsageLimitReached().add(validPermutation);
                    continue;
                }

                List<String> permutationsCopy = new ArrayList<>(permutations);
                permutationsCopy.add(validPermutation);

                if (validPermutation.equals(commonInfo.getEndWord())) {
                    commonInfo.addPermutation(permutationsCopy);
                    break;
                } else {
                    findNextVariation(usesByWordCopy, commonInfo, validPermutation, permutationsCopy);
                }
            }
        }

        private boolean isOneCharDifferent(String s1, String s2) {
            if (s1.equals(s2) || Math.abs(s1.length() - s2.length()) > 1) return false;

            int diffCount = 0;
            final int minLength = Integer.min(s1.length(), s2.length());

            for (int i = 0; i < minLength; i++) {
                if (s1.charAt(i) != s2.charAt(i)) {
                    diffCount++;
                    if (diffCount > 1) return false;
                }
            }

            return s1.length() == s2.length() || diffCount == 0;
        }
    }
}
