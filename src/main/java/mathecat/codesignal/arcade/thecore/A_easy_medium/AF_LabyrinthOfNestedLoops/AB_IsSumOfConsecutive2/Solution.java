package mathecat.codesignal.arcade.thecore.A_easy_medium.AF_LabyrinthOfNestedLoops.AB_IsSumOfConsecutive2;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.isSumOfConsecutive2(9));
    }

    int isSumOfConsecutive2(int n) {
        int count = 0;

        for (int i = 1; i <= n / 2; i++) {
            int acc = i;
            for (int j = i + 1; acc < n; j++) {
                acc += j;
                if (acc == n) {
                    count++;
                    break;
                }
            }
        }

        return count;
    }
}
