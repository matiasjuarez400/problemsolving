package mathecat.codesignal.arcade.thecore.A_easy_medium.AB_AtTheCrossroads.AG_WillYou;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.willYou(true, true, true));
        System.out.println(solution.willYou(true, false, true));
    }

    boolean willYou(boolean young, boolean beautiful, boolean loved) {
        return (young && beautiful && !loved) || (loved && (!young || !beautiful));
    }
}
