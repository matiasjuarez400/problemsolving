package mathecat.codesignal.arcade.thecore.A_easy_medium.AG_BookMarket.AG_IsMAC48Address;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.isMAC48Address("01-23-45-67-89-AB"));
        System.out.println(solution.isMAC48Address("Z1-1B-63-84-45-E6"));
    }

    boolean isMAC48Address(String inputString) {
        return inputString.matches("[0-9A-F]{2}-[0-9A-F]{2}-[0-9A-F]{2}-[0-9A-F]{2}-[0-9A-F]{2}-[0-9A-F]{2}");
    }
}
