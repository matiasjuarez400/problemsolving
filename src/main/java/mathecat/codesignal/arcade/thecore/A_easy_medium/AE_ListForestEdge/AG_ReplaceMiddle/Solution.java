package mathecat.codesignal.arcade.thecore.A_easy_medium.AE_ListForestEdge.AG_ReplaceMiddle;

import java.util.Arrays;
import java.util.stream.IntStream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] arr = {7, 2, 2, 5, 10, 7};

        System.out.println(Arrays.toString(solution.replaceMiddle(arr)));
    }

    int[] replaceMiddle(int[] arr) {
        if (arr.length % 2 == 0) {
            int middle = arr.length / 2;
            int replace = arr[middle] + arr[middle - 1];

            return IntStream.range(0, arr.length)
                    .filter(i -> i != middle - 1)
                    .map(i -> i == middle ? replace : arr[i])
                    .toArray();
        }

        return arr;
    }
}
