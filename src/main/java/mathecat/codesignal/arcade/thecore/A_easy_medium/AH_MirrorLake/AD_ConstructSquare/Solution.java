package mathecat.codesignal.arcade.thecore.A_easy_medium.AH_MirrorLake.AD_ConstructSquare;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.constructSquare("ab"));
    }

    int constructSquare(String s) {
        int currentSquare = (int) Math.pow(10, s.length());
        Map<Integer, List<Integer>> mapByChar = s.chars().boxed().collect(Collectors.groupingBy(c -> c));

        while((currentSquare = getPreviousSquareOfSize(currentSquare, s.length())) != -1) {
            String squareString = Integer.toString(currentSquare);
            Map<Integer, List<Integer>> mapByDigit = squareString.chars().boxed().collect(Collectors.groupingBy(c -> c));

            mapByChar.forEach((k, v) -> {
                for (Integer digit : mapByDigit.keySet()) {
                    if (mapByDigit.get(digit).size() == v.size()) {
                        mapByDigit.remove(digit);
                        break;
                    }
                }
            });

            if (mapByDigit.size() == 0) return currentSquare;
        }

        return -1;
    }

    int getPreviousSquareOfSize(int current, int size) {
        while (Math.sqrt(--current) % 1 != 0);

        if (current < Math.pow(10, size - 1)) return -1;

        return current;
    }
}
