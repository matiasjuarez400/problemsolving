package mathecat.codesignal.arcade.thecore.A_easy_medium.AG_BookMarket.AA_EncloseInBrackets;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.encloseInBrackets("abacaba"));
    }

    String encloseInBrackets(String inputString) {
        return String.format("(%s)", inputString);
    }

}
