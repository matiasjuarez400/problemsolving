package mathecat.codesignal.arcade.thecore.A_easy_medium.AH_MirrorLake.AF_DifferentSquares;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        
        int[][] matrix = {
                  {1, 2, 1},
                  {2, 2, 2},
                  {2, 2, 2},
                  {1, 2, 3},
                  {2, 2, 1}
        };

        System.out.println(solution.differentSquares(matrix));
    }

    int differentSquares(int[][] matrix) {
        List<int[]> differentSquares = new ArrayList<>();
        for (int i = 0; i < matrix.length - 1; i++) {
            for (int j = 0; j < matrix[i].length - 1; j++) {
                int[] elements = new int[4];
                elements[0] = matrix[i][j];
                elements[1] = matrix[i][j + 1];
                elements[2] = matrix[i + 1][j];
                elements[3] = matrix[i + 1][j + 1];

                if (differentSquares.stream().noneMatch(currentArray -> Arrays.equals(currentArray, elements))) {
                    differentSquares.add(elements);
                    System.out.println(Arrays.toString(elements));
                }
            }
        }

        return differentSquares.size();
    }
}
