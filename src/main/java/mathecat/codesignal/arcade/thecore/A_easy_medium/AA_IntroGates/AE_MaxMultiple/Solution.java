package mathecat.codesignal.arcade.thecore.A_easy_medium.AA_IntroGates.AE_MaxMultiple;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.maxMultiple(7, 100));
    }

    int maxMultiple(int divisor, int bound) {
        for (int i = bound; i > 0; i--) {
            if (i % divisor == 0) return i;
        }
        return -1;
    }
}
