package mathecat.codesignal.arcade.thecore.A_easy_medium.AB_AtTheCrossroads.AF_TennisSet;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.tennisSet(8, 5));
        System.out.println(solution.tennisSet(6, 5));
        System.out.println(solution.tennisSet(3, 6));
    }

    boolean tennisSet(int score1, int score2) {
        int dist = Math.abs(score1 - score2);

        if (score1 > 7 || score2 > 7) return false;
        if (score1 == 7 ^ score2 == 7) {
            if (dist == 1 || dist == 2) return true;
        }

        if (score1 == 6 ^ score2 == 6) {
            if (Math.abs(score1 - score2) >= 2) return true;
        }

        return false;
    }
}
