package mathecat.codesignal.arcade.thecore.A_easy_medium.AB_AtTheCrossroads.AB_KnapsackLight;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.knapsackLight(10, 5, 6, 4, 8));
        System.out.println(solution.knapsackLight(10, 5, 6, 4, 9));
    }

    int knapsackLight(int value1, int weight1, int value2, int weight2, int maxW) {
        return (weight1 + weight2 <= maxW) ? value1 + value2 :
                (weight1 <= maxW && weight2 <= maxW) ?
                        Integer.max(value1, value2) :
                        (weight1 <= maxW) ?
                                value1 :
                                (weight2 <= maxW) ?
                                        value2 : 0;}
}
