package mathecat.codesignal.arcade.thecore.A_easy_medium.AI_WellOfIntegration.AG_SwitchLights;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(Arrays.toString(solution.switchLights(new int[] {1, 1, 1, 1, 1})));
        System.out.println(Arrays.toString(solution.switchLights(new int[] {1, 1, 0, 0, 1})));
    }

    int[] switchLights(int[] a) {
        int length = a.length;
        int lit = (int) Arrays.stream(a).filter(i -> i == 1).count();

        for (int i = 0; i < length; i++) {
            if (a[i] == 1) {
                if (--lit % 2 == 0) a[i] = 0;
            } else {
                a[i] = lit % 2 == 0 ? 0 : 1;
            }
        }
        return a;
    }

}
