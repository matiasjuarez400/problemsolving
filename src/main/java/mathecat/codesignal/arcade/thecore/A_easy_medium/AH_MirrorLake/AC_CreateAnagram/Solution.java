package mathecat.codesignal.arcade.thecore.A_easy_medium.AH_MirrorLake.AC_CreateAnagram;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.createAnagram("AABAA", "BBAAA"));
        System.out.println(solution.createAnagram("OVGHK", "RPGUC"));
    }

    int createAnagram(String s, String t) {
        int[] sc = getCountArray(s);
        int[] tc = getCountArray(t);
        int[] diff = new int[sc.length];

        for (int i = 0; i < sc.length; i++) {
            diff[i] = sc[i] - tc[i];
        }

        int changes = 0;

        for (int i = 0; i < sc.length; i++) {
            if (diff[i] == 0) continue;
            for (int j = i + 1; j < sc.length; j++) {
                if (diff[j] == 0) continue;
                if (diff[i] > 0) {
                    if (diff[j] > 0) {
                        if (Arrays.stream(diff).anyMatch(in -> in < 0)) continue;

                        int minIndex = diff[i] < diff[j] ? i : j;
                        int maxIndex = minIndex == j ? i : j;

                        changes += Math.abs(diff[minIndex]);

                        diff[maxIndex] = diff[maxIndex] - diff[minIndex];
                        diff[minIndex] = 0;

                    } else {
                        int minIndex = diff[i] < Math.abs(diff[j]) ? i : j;
                        int maxIndex = minIndex == j ? i : j;

                        changes += Math.abs(diff[minIndex]);

                        diff[maxIndex] = diff[i] + diff[j];
                        diff[minIndex] = 0;
                    }
                } else if (diff[i] < 0){
                    if (diff[j] < 0) {
                        if (Arrays.stream(diff).anyMatch(in -> in > 0)) continue;

                        int minIndex = Math.abs(diff[i]) < Math.abs(diff[j]) ? i : j;
                        int maxIndex = minIndex == j ? i : j;

                        changes += Math.abs(diff[minIndex]);

                        diff[maxIndex] = diff[maxIndex] - diff[minIndex];
                        diff[minIndex] = 0;

                    } else {
                        int minIndex = Math.abs(diff[i]) < Math.abs(diff[j]) ? i : j;
                        int maxIndex = minIndex == j ? i : j;

                        changes += Math.abs(diff[minIndex]);

                        diff[maxIndex] = diff[i] + diff[j];
                        diff[minIndex] = 0;
                    }
                }
            }
        }
        return changes;
    }

    int[] getCountArray(String s) {
        int[] counter = new int['Z' - 'A' + 1];
        s.chars().forEach(c -> counter[c - 'A']++);
        return counter;
    }
}
