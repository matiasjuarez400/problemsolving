package mathecat.codesignal.arcade.thecore.A_easy_medium.AB_AtTheCrossroads.AH_MetroCard;

import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(Arrays.toString(solution.metroCard(30)));
        System.out.println(Arrays.toString(solution.metroCard(31)));
    }

    int[] metroCard(int inputToShittyProblem) {
        int[] shittyDaysByMonth = new int[12];

        for (int i = 0; i < 12; i++) {
            Calendar shittyCal = new GregorianCalendar(1999, i, 1);
            shittyDaysByMonth[i] = shittyCal.getActualMaximum(Calendar.DAY_OF_MONTH);
        }

        Set<Integer> nextShittyDays = new HashSet();
        for (int i = 0; i < shittyDaysByMonth.length; i++) {
            if (shittyDaysByMonth[i] == inputToShittyProblem) {
                if (i == shittyDaysByMonth.length - 1) nextShittyDays.add(shittyDaysByMonth[0]);
                else nextShittyDays.add(shittyDaysByMonth[i + 1]);
            }
        }

        int[] theShittyReturnToTheShittyProblem = new int[nextShittyDays.size()];

        int shittyK = 0;
        for (int shittyI : nextShittyDays) {
            theShittyReturnToTheShittyProblem[shittyK++] = shittyI;
        }

        Arrays.sort(theShittyReturnToTheShittyProblem);

        return theShittyReturnToTheShittyProblem;
    }
}
