package mathecat.codesignal.arcade.thecore.A_easy_medium.AF_LabyrinthOfNestedLoops.AC_SquareDigitsSequence;

import java.util.HashSet;
import java.util.Set;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.squareDigitsSequence(16));
    }

    int squareDigitsSequence(int a0) {
        Set<Integer> set = new HashSet<>();

        int nextNumber = a0;

        do {
            set.add(nextNumber);

            String nextNumberString = Integer.toString(nextNumber);
            nextNumber = 0;
            for (char c : nextNumberString.toCharArray()) {
                int currentDigit = Character.getNumericValue(c);
                nextNumber += currentDigit * currentDigit;
            }
        } while (!set.contains(nextNumber));

        return set.size() + 1;
    }
}
