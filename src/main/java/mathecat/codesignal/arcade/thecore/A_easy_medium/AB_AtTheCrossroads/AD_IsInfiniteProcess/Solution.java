package mathecat.codesignal.arcade.thecore.A_easy_medium.AB_AtTheCrossroads.AD_IsInfiniteProcess;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.isInfiniteProcess(2, 6));
        System.out.println(solution.isInfiniteProcess(2, 3));
    }

    boolean isInfiniteProcess(int a, int b) {
        return a > b || (b - a) % 2 != 0;
    }
}
