package mathecat.codesignal.arcade.thecore.A_easy_medium.AA_IntroGates.AC_Candies;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.candies(3, 9));
    }

    int candies(int n, int m) {
        return (m / n) * n;
    }
}
