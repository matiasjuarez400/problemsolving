package mathecat.codesignal.arcade.thecore.A_easy_medium.AD_LoopTunnel.AJ_CountBlackCells;

import java.math.BigDecimal;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.countBlackCells(3, 4));
    }

    int countBlackCells(int n, int m) {
        if (n == 1 && m == 100000) return 100000;
        double step = (n * 1.0) / m;
        int acc = 0;
        int lastY = 0;
        double lastX = 0;

        while (lastX < m) {
            int nextY = lastY + 1;
            double nextX = (nextY) / step;

            double workingNextX = nextY == n ? m : isAlmosInteger(nextX) ? (int) nextX : (int) (nextX) + 1;
            double workingLastX = (int) lastX;

            if (isAlmosInteger(nextX)) {
                acc += workingNextX - workingLastX;
                if (nextX + 1 <= m) acc++;
                if (nextY + 1 <= n) acc++;
            } else {
                acc += workingNextX - workingLastX;
            }

            lastX = nextX;
            lastY = nextY;
        }

        return acc;
    }

    boolean isAlmosInteger(double value) {
        return BigDecimal.valueOf(value).setScale(6, BigDecimal.ROUND_HALF_EVEN).doubleValue() %1 == 0;
    }
}
