package mathecat.codesignal.arcade.thecore.A_easy_medium.AC_CornerOfZerosAndOnes.AG_DifferentRightmostBit;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.differentRightmostBit(11, 13));
        System.out.println(solution.differentRightmostBit(7, 23));
    }

    int differentRightmostBit(int n, int m) {
        return solve(n, m);
    }

    int solve(int n, int m) {
        int diff = n ^ m;
        for (int i = 0; i < 32; i++) {
            if ((diff & (1 << i)) != 0) return 1 << i;
        }

        return -1;
    }
}
