package mathecat.codesignal.arcade.thecore.A_easy_medium.AH_MirrorLake.AG_MostFrequentDigitSum;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.mostFrequentDigitSum(88));
    }

    int mostFrequentDigitSum(int n) {
        List<Integer> stepResult = step(n);

        List<Integer> secondStep = stepResult.stream()
                .map(stepResultNumber -> s(stepResultNumber))
                .collect(Collectors.toList());

        Map<Integer, List<Integer>> listBySecondStepNumber = secondStep.stream()
                .collect(Collectors.groupingBy(i -> i));

        int frequency = Integer.MIN_VALUE;
        int mostFrequentNumber = -1;

        for (Integer key : listBySecondStepNumber.keySet()) {
            int currentFrequency = listBySecondStepNumber.get(key).size();

            if (currentFrequency > frequency) {
                frequency = currentFrequency;
                mostFrequentNumber = key;
            } else if (currentFrequency == frequency) {
                mostFrequentNumber = Integer.max(mostFrequentNumber, key);
            }
        }

        return mostFrequentNumber;
    }

    List<Integer> step(int number) {
        int nextNumber = number;

        List<Integer> sequence = new ArrayList();
        sequence.add(nextNumber);
        while (nextNumber != 0) {
            nextNumber -= s(nextNumber);
            sequence.add(nextNumber);
        }

        return sequence;
    }

    int s(int number) {
        String numberString = Integer.toString(number);

        int acc = 0;

        for (char c : numberString.toCharArray()) {
            acc += Character.getNumericValue(c);
        }

        return acc;
    }

}
