package mathecat.codesignal.arcade.thecore.A_easy_medium.AE_ListForestEdge.AF_IsSmooth;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] arr = {7, 2, 2, 5, 10, 7};

        System.out.println(solution.isSmooth(arr));
    }

    boolean isSmooth(int[] arr) {
        int size = arr.length;
        if (arr[0] != arr[size - 1]) return false;

        int middle;
        if (size % 2 == 0) {
            middle = arr[size / 2] + arr[(size / 2) - 1];
        } else {
            middle = arr[size / 2];
        }

        return arr[0] == middle;
    }
}
