package mathecat.codesignal.arcade.thecore.A_easy_medium.AD_LoopTunnel.AD_Lineup;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.lineUp("LLARL"));
    }

    int lineUp(String commands) {
        StateMachine m1 = new StateMachine(true);
        StateMachine m2 = new StateMachine(false);

        int acc = 0;
        for (int i = 0; i < commands.length(); i++) {
            char nextCommand = commands.charAt(i);
            m1.handleOrder(nextCommand);
            m2.handleOrder(nextCommand);

            if (m1.current.dir == m2.current.dir) acc++;
        }

        return acc;
    }

    private class StateMachine {
        State current;

        StateMachine(boolean reversed) {
            State s1 = new State(Direction.FRONT);
            State s2 = new State(Direction.RIGHT);
            State s3 = new State(Direction.BACK);
            State s4 = new State(Direction.LEFT);

            if (reversed) {
                s2 = new State(Direction.LEFT);
                s4 = new State(Direction.RIGHT);
            }

            s1.setNext(s2);
            s2.setNext(s3);
            s3.setNext(s4);
            s4.setNext(s1);

            current = s1;
        }

        void handleOrder(char order) {
            if (order == 'R') current = current.next;
            if (order == 'L') current = current.prev;
            if (order == 'A') current = current.prev.prev;
        }
    }

    private enum Direction {
        FRONT, RIGHT, BACK, LEFT
    }

    private class State {
        Direction dir;
        State next;
        State prev;

        State(Direction dir) {
            this.dir = dir;
        }

        void setNext(State state) {this.next = state; state.prev = this;}
    }
}
