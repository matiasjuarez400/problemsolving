package mathecat.codesignal.arcade.thecore.A_easy_medium.AI_WellOfIntegration.AA_HouseNumbersSum;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.houseNumbersSum(new int[] {5, 1, 2, 3, 0, 1, 5, 0, 2}));
    }

    int houseNumbersSum(int[] inputArray) {
        int acc = 0;
        for (int n : inputArray) {
            if (n == 0) break;
            acc += n;
        }
        return acc;
    }
}
