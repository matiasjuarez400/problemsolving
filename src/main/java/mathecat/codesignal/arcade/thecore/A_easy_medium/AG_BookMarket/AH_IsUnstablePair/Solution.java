package mathecat.codesignal.arcade.thecore.A_easy_medium.AG_BookMarket.AH_IsUnstablePair;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
    }

    boolean isUnstablePair(String filename1, String filename2) {
        int a = filename1.toLowerCase().compareTo(filename2.toLowerCase());
        int b = filename2.compareTo(filename1);
        return a * b >= 0;
    }
}
