package mathecat.codesignal.arcade.thecore.A_easy_medium.AH_MirrorLake.AE_NumbersGrouping;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.numbersGrouping(new int[] {20000, 239, 10001, 999999, 10000, 20566, 29999}));
    }

    int numbersGrouping(int[] a) {
        final int groupSize = (int) Math.pow(10, 4);

        final Map<Integer, List<Integer>> counterByGroup = Arrays.stream(a).boxed()
                .collect(Collectors.groupingBy(i -> calculateGroup(i - 1, groupSize)));

        int acc = 0;
        for (Integer group : counterByGroup.keySet()) {
            acc += counterByGroup.get(group).size() + 1;
        }

        return acc;
    }

    Integer calculateGroup(int number, int groupSize) {
        return number / groupSize;
    }
}
