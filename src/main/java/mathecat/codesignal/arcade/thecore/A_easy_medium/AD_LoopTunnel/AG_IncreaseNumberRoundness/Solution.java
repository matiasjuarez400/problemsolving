package mathecat.codesignal.arcade.thecore.A_easy_medium.AD_LoopTunnel.AG_IncreaseNumberRoundness;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.increaseNumberRoundness(902200100));
    }

    boolean increaseNumberRoundness(int n) {
        return Integer.toString(n).matches("[1-9]+0+[1-9]+[0-9]*");
    }

}
