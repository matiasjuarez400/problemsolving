package mathecat.codesignal.arcade.thecore.A_easy_medium.AG_BookMarket.AC_IsTandemRepeat;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.isTandemRepeat("tandemtandem"));
        System.out.println(solution.isTandemRepeat("2w2ww"));
    }

    boolean isTandemRepeat(String inputString) {
        return inputString.length() % 2 == 0 && inputString.substring(0, inputString.length() / 2).equals(inputString.substring(inputString.length() / 2));
    }
}
