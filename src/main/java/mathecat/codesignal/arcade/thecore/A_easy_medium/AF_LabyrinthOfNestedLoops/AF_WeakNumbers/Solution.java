package mathecat.codesignal.arcade.thecore.A_easy_medium.AF_LabyrinthOfNestedLoops.AF_WeakNumbers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(Arrays.toString(solution.weakNumbers(9)));
        System.out.println(Arrays.toString(solution.weakNumbers(500)));
    }

    int[] weakNumbers(int n) {
        Map<Integer, Integer> divisorsByNumber = new HashMap<>();
        for (int i = 1; i <= n; i++) {
            divisorsByNumber.put(i, calculateDivisorForNumber(i));
        }

        int[] out = new int[2];
        for (int i = 1; i <= n; i++) {
            int iDivisors = divisorsByNumber.get(i);
            int iWeakness = 0;
            for (int j = 1; j < i; j++) {
                int jDivisors = divisorsByNumber.get(j);
                if (jDivisors > iDivisors) iWeakness++;
            }

            if (iWeakness > out[0]) {
                out[0] = iWeakness;
                out[1] = 1;
            } else if (iWeakness == out[0]) {
                out[1]++;
            }
        }

        return out;
    }

    int calculateDivisorForNumber(int n) {
        int divisionResult = n / 2;
        int acc = 0;
        for (int i = 1; i <= divisionResult; i++) {
            if (n % i == 0) acc++;
        }

        return acc;
    }
}
