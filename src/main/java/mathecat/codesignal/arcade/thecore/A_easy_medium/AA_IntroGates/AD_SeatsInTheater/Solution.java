package mathecat.codesignal.arcade.thecore.A_easy_medium.AA_IntroGates.AD_SeatsInTheater;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.seatsInTheater(60, 100, 60, 1));
    }

    int seatsInTheater(int nCols, int nRows, int col, int row) {
        return (nCols - col + 1) * (nRows - row);
    }
}
