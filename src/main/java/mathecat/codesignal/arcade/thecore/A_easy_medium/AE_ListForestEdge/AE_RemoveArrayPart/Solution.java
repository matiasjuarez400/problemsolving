package mathecat.codesignal.arcade.thecore.A_easy_medium.AE_ListForestEdge.AE_RemoveArrayPart;

import java.util.Arrays;
import java.util.stream.IntStream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] inputArray = {2, 3, 2, 3, 4, 5};
        int l = 2;
        int r = 4;

        System.out.println(Arrays.toString(solution.removeArrayPart(inputArray, l, r)));
    }

    int[] removeArrayPart(int[] inputArray, int l, int r) {
        return IntStream.concat(
                Arrays.stream(Arrays.copyOfRange(inputArray, 0, l)),
                Arrays.stream(Arrays.copyOfRange(inputArray, r + 1, inputArray.length))
        ).toArray();
    }
}
