package mathecat.codesignal.arcade.thecore.A_easy_medium.AH_MirrorLake.AA_StringConstruction;

import java.util.HashMap;
import java.util.Map;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.stringsConstruction("abc", "abccba"));
    }

    int stringsConstruction(String a, String b) {
        Map<Character, Integer> target = getCountByCharMap(a);
        Map<Character, Integer> source = getCountByCharMap(b);

        int minOcurrences = Integer.MAX_VALUE;
        for (char c : target.keySet()) {
            Integer sourceCounter = source.get(c);
            if (sourceCounter == null) return 0;

            Integer targetCounter = target.get(c);
            minOcurrences = Integer.min(minOcurrences, sourceCounter / targetCounter);
        }

        return minOcurrences;
    }

    private Map<Character, Integer> getCountByCharMap(String s) {
        Map<Character, Integer> countByChar = new HashMap<>();

        s.chars().forEach(c -> {
            countByChar.computeIfPresent((char) c, (k, v) -> v + 1);
            countByChar.putIfAbsent((char) c, 1);
        });

        return countByChar;
    }
}
