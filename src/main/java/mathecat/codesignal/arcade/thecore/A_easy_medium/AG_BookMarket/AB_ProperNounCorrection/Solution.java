package mathecat.codesignal.arcade.thecore.A_easy_medium.AG_BookMarket.AB_ProperNounCorrection;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.properNounCorrection("pARiS"));
    }

    String properNounCorrection(String noun) {
        return noun.toUpperCase().charAt(0) + noun.toLowerCase().substring(1);
    }

}
