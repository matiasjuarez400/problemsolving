package mathecat.codesignal.arcade.thecore.A_easy_medium.AB_AtTheCrossroads.AC_ExtraNumber;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.extraNumber(2, 7, 2));

    }

    int extraNumber(int a, int b, int c) {
        return a == b ? c : a == c ? b : a;
    }
}
