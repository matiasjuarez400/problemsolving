package mathecat.codesignal.arcade.thecore.A_easy_medium.AA_IntroGates.AG_LateRide;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.lateRide(1439));
    }

    int lateRide(int n) {
        int hours = n / 60;
        int minutes = n % 60;

        String hs = ""+hours;
        String ms = ""+minutes;

        int acc = 0;

        for (int i = 0; i < hs.length(); i++) {
            acc += Character.getNumericValue(hs.charAt(i));
        }

        for (int i = 0; i < ms.length(); i++) {
            acc += Character.getNumericValue(ms.charAt(i));
        }

        return acc;
    }

}
