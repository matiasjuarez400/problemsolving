package mathecat.codesignal.arcade.thecore.A_easy_medium.AI_WellOfIntegration.AB_AllLongestStrings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(Arrays.toString(solution.allLongestStrings(new String[]{"aba", "aa", "ad", "vcd", "aba"})));
    }

    String[] allLongestStrings(String[] inputArray) {
        List<String> longest = new ArrayList<>();

        for (String s : inputArray) {
            if (longest.isEmpty()) {
                longest.add(s);
            } else {
                if (longest.get(0).length() < s.length()) {
                    longest.clear();
                    longest.add(s);
                } else if (longest.get(0).length() == s.length()) {
                    longest.add(s);
                }
            }
        }

        return longest.toArray(new String[0]);
    }

}
