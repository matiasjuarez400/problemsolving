package mathecat.codesignal.arcade.thecore.A_easy_medium.AI_WellOfIntegration.AE_MinimalNumberOfCoins;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.minimalNumberOfCoins(new int[] {1, 2, 10}, 28));
    }

    int minimalNumberOfCoins(int[] coins, int price) {
        int acc = 0;
        int coinsCounter = 0;
        for (int i = coins.length - 1; i >= 0; i--) {
            while (acc + coins[i] <= price) {
                acc += coins[i];
                coinsCounter++;
            }
        }
        return coinsCounter;
    }

}
