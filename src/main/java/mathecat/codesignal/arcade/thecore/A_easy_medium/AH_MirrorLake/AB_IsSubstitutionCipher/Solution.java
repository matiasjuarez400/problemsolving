package mathecat.codesignal.arcade.thecore.A_easy_medium.AH_MirrorLake.AB_IsSubstitutionCipher;

import java.util.HashMap;
import java.util.Map;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.isSubstitutionCipher("aacb", "aabc"));
        System.out.println(solution.isSubstitutionCipher("aa", "bc"));
    }

    boolean isSubstitutionCipher(String string1, String string2) {
        return isSubstitutionCipherDelegate(string1, string2);
    }

    boolean isSubstitutionCipherDelegate(String string1, String string2) {
        Map<Character, Character> cypheringMap = new HashMap<>();
        int limit = Integer.min(string1.length(), string2.length());
        for (int i = 0; i < limit; i++) {
            char c1 = string1.charAt(i);
            char c2 = string2.charAt(i);

            Character cypheringChar = cypheringMap.get(c1);
            if (cypheringChar == null) {
                if (cypheringMap.containsValue(c2)) return false;
                cypheringMap.put(c1, c2);
            } else {
                if (c2 != cypheringChar) return false;
            }
        }

        return true;
    }
}
