package mathecat.codesignal.arcade.thecore.A_easy_medium.AI_WellOfIntegration.AF_AddBorder;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(Arrays.toString(solution.addBorder(new String[] {"abc",
                "ded"})));
    }

    String[] addBorder(String[] picture) {
        String fullAsteriskLine = new String(new char[picture[0].length() + 2]).replace('\0', '*');
        String surroundingModel = "*%s*";

        String[] output = new String[picture.length + 2];
        output[0] = fullAsteriskLine;
        for (int i = 0; i < picture.length; i++) {
            output[i + 1] = String.format(surroundingModel, picture[i]);
        }
        output[output.length - 1] = fullAsteriskLine;

        return output;
    }

}
