package mathecat.codesignal.arcade.thecore.A_easy_medium.AF_LabyrinthOfNestedLoops.AH_CrosswordFormation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.crosswordFormation(new String[] {
                "crossword", "square", "formation", "something"
        }));

        System.out.println(solution.crosswordFormation(new String[] {
                "phenomenon",
                "remuneration",
                "particularly",
                "pronunciation"
        }));
    }

    public int crosswordFormation(String[] words) {
        List<int[]> combinations = getAllPossibleCombinations(new int[]{0, 1, 2, 3});
        int counter = 0;

        int loopCount = 0;
        for (int[] combination : combinations) {
            loopCount++;
            String upWord = words[combination[0]];
            String leftWord = words[combination[1]];
            String downWord = words[combination[2]];
            String rightWord = words[combination[3]];

            for (int ul = 0; ul < upWord.length() - 2; ul++) {
                char upLeftChar = upWord.charAt(ul);

                for (int l = 0; l < leftWord.length() - 2; l++) {
                    if (leftWord.charAt(l) != upLeftChar) continue;

                    for (int dl = l + 2; dl < leftWord.length(); dl++) {
                        char downLeftChar = leftWord.charAt(dl);

                        for (int d = 0; d < downWord.length(); d++) {
                            if (downWord.charAt(d) != downLeftChar) continue;

                            for (int ur = ul + 2; ur < upWord.length(); ur++) {
                                char upRightChar = upWord.charAt(ur);

                                if (d + ur - ul >= downWord.length()) break;
                                char downRightChar = downWord.charAt(d + ur - ul);

                                int upDownDistance = dl - l;
                                for (int r = 0; r < rightWord.length(); r++) {
                                    if (rightWord.length() <= upDownDistance + r) break;
                                    if (rightWord.charAt(r) != upRightChar) continue;
                                    if (rightWord.charAt(r + upDownDistance) != downRightChar) continue;
                                    counter++;
                                }
                            }
                        }
                    }
                }
            }
        }

//        System.out.println("Loop count: " + loopCount);
        return counter;
    }

    private List<int[]> getAllPossibleCombinations(int[] baseArray) {
        List<int[]> combinationList = new ArrayList<>();

        int[] currentCombination = new int[baseArray.length];
        for (int i = 0; i < currentCombination.length; i++) currentCombination[i] = i;

        combinationList.add(currentCombination);

        while ((currentCombination = getNextCombination(currentCombination)) != null) {
            combinationList.add(currentCombination);
        }

        return combinationList;
    }

    private int[] getNextCombination(int[] current) {
        int currentIndex = current.length - 1;
        int[] currentCopy = Arrays.copyOf(current, current.length);
        int[] end = new int[current.length];

        int lastIndex = currentCopy.length - 1;
        while (!Arrays.equals(currentCopy, end)) {
            int carryIndex;

            if (currentCopy[lastIndex] == lastIndex) {
                currentCopy[lastIndex] = 0;
                carryIndex = lastIndex - 1;

                while (carryIndex >= 0) {
                    if (currentCopy[carryIndex] == lastIndex) {
                        currentCopy[carryIndex] = 0;
                        carryIndex--;

                        if (carryIndex < 0) return null;
                    } else {
                        currentCopy[carryIndex]++;
                        break;
                    }
                }
            } else {
                currentCopy[lastIndex]++;
            }

            Set<Integer> validCombinationVerifier = new HashSet<>();
            for (int number : currentCopy) validCombinationVerifier.add(number);
            if (validCombinationVerifier.size() == currentCopy.length) {
                break;
            }
        }

        if (currentIndex < 0) return null;

        return currentCopy;
    }
}
