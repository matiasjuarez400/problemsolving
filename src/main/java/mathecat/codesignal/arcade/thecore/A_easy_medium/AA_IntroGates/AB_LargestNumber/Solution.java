package mathecat.codesignal.arcade.thecore.A_easy_medium.AA_IntroGates.AB_LargestNumber;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.largestNumber(7));
    }

    int largestNumber(int n) {
        int result = 0;
        for (int i = 0; i < n; i++) {
            result += 9 * Math.pow(10, i);
        }
        return result;
    }
}
