package mathecat.codesignal.arcade.thecore.A_easy_medium.AB_AtTheCrossroads.AA_ReachNextLevel;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.reachNextLevel(10, 15, 5));
        System.out.println(solution.reachNextLevel(10, 15, 4));
    }

    boolean reachNextLevel(int experience, int threshold, int reward) {
        return experience + reward >= threshold;
    }
}
