package mathecat.codesignal.arcade.thecore.A_easy_medium.AC_CornerOfZerosAndOnes.AC_RangeBitCount;

import java.util.stream.IntStream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.rangeBitCount(2, 7));
    }

    int rangeBitCount(int a, int b) {
        return (int) IntStream.range(a, b + 1)
                .boxed()
                .flatMap(i -> Integer.toBinaryString(i).chars().boxed())
                .filter(i -> i == '1')
                .count();
    }
}
