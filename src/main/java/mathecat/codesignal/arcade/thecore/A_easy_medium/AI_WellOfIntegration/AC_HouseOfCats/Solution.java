package mathecat.codesignal.arcade.thecore.A_easy_medium.AI_WellOfIntegration.AC_HouseOfCats;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(Arrays.toString(solution.houseOfCats(6)));
    }

    int[] houseOfCats(int legs) {
        int maxPeople = legs/2;
        List<Integer> l = new ArrayList<>();
        int next = maxPeople;
        while (next >= 0) {
            l.add(next);
            next -= 2;
        }

        int[] a = new int[l.size()];
        for (int i = 0, j = a.length - 1; i < a.length; i++, j--) {
            a[i] = l.get(j);
        }

        return a;
    }

}
