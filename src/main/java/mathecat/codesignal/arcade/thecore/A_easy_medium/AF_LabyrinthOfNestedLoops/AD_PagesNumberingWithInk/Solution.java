package mathecat.codesignal.arcade.thecore.A_easy_medium.AF_LabyrinthOfNestedLoops.AD_PagesNumberingWithInk;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.pagesNumberingWithInk(1, 5));
        System.out.println(solution.pagesNumberingWithInk(21, 5));
        System.out.println(solution.pagesNumberingWithInk(8, 4));
    }

    int pagesNumberingWithInk(int current, int numberOfDigits) {
        int digitsLeft = numberOfDigits;
        int currentDigitsBase = current;

        int currentNumberOfDigits = calculateDigits(current);
        int printedPages = 0;

        while (digitsLeft > 0 && currentNumberOfDigits <= digitsLeft) {
            int limitForNextNumberOfDigits = (int) Math.pow(10, currentNumberOfDigits);

            int pagesThatCanBeNumberedWithCurrentDigitSize = digitsLeft / currentNumberOfDigits;

            int numbersWithCurrentDigits = limitForNextNumberOfDigits - currentDigitsBase;

            int pagesThatWillBeNumbered = Integer.min(numbersWithCurrentDigits, pagesThatCanBeNumberedWithCurrentDigitSize);

            digitsLeft -= pagesThatWillBeNumbered * currentNumberOfDigits;
            currentDigitsBase = (int) Math.pow(10, currentNumberOfDigits);
            currentNumberOfDigits++;

            printedPages += pagesThatWillBeNumbered;
        }

        return printedPages + current - 1;
    }

    int calculateDigits(int number) {
        return (int) Math.log10(number) + 1;
    }
}
