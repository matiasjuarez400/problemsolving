package mathecat.codesignal.arcade.thecore.A_easy_medium.AC_CornerOfZerosAndOnes.AA_KillKthBit;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.killKthBit(37, 3));
        System.out.println(solution.killKthBit(37, 4));
    }

    int killKthBit(int n, int k) {
        return k == 1 ? n & ~1 : n & ~(1 << k - 1);
    }
}
