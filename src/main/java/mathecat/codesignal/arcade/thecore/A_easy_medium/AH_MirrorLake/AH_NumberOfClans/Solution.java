package mathecat.codesignal.arcade.thecore.A_easy_medium.AH_MirrorLake.AH_NumberOfClans;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.numberOfClans(new int[] {2, 3}, 6));
    }

    int numberOfClans(int[] divisors, int k) {
        boolean[][] clansInfo = new boolean[k][divisors.length];

        for (int i = 0; i < k; i++) {
            for (int j = 0; j < divisors.length; j++) {
                int divisor = divisors[j];
                int currentNumber = i + 1;

                clansInfo[i][j] = currentNumber % divisor == 0;
            }
        }

        Set<Integer> numbersAlreadyInClans = new HashSet<>();

        int clanCount = 0;
        for (int i = 0; i < k; i++) {
            if (numbersAlreadyInClans.contains(i)) continue;
            clanCount++;
            boolean[] clanInfoI = clansInfo[i];
            for (int j = i + 1; j < k; j++) {
                boolean[] clanInfoJ = clansInfo[j];

                if (Arrays.equals(clanInfoI, clanInfoJ)) {
                    numbersAlreadyInClans.add(j);
                }
            }
        }

        return clanCount;
    }

}
