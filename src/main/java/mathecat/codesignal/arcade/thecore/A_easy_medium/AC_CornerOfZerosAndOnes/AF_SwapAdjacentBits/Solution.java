package mathecat.codesignal.arcade.thecore.A_easy_medium.AC_CornerOfZerosAndOnes.AF_SwapAdjacentBits;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.swapAdjacentBits(13));
        System.out.println(solution.swapAdjacentBits(133));
    }

    int swapAdjacentBits(int n) {
        return solve(n);
    }

    int solve(int n) {
        int filter = Integer.parseInt("1010101010101010101010101010101", 2);
        return ((n>>1) & filter | (n << 1) & (filter << 1));
    }
}
