package mathecat.codesignal.arcade.thecore.A_easy_medium.AE_ListForestEdge.AA_CreateArray;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(Arrays.toString(solution.createArray(4)));
    }

    int[] createArray(int size) {
        int[] a = new int[size];
        Arrays.fill(a, 1);
        return a;
    }

}
