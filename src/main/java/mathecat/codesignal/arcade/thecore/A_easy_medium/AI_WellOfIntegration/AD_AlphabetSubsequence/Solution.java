package mathecat.codesignal.arcade.thecore.A_easy_medium.AI_WellOfIntegration.AD_AlphabetSubsequence;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.alphabetSubsequence("effg"));
        System.out.println(solution.alphabetSubsequence("bxz"));
    }

    boolean alphabetSubsequence(String s) {
        for (int i = 0; i < s.length() - 1; i++) {
            if (s.charAt(i) >= s.charAt(i + 1)) return false;
        }
        return true;
    }

}
