package mathecat.codesignal.arcade.thecore.A_easy_medium.AC_CornerOfZerosAndOnes.AB_ArrayPacking;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.arrayPacking(new int[] {24, 85, 0}));
        System.out.println(solution.arrayPacking(new int[] {23, 45, 39}));
    }

    int arrayPacking(int[] a) {
        int result = 0;

        for (int i = 0; i < a.length; i++) {
            result += a[i] << i * 8;
        }

        return result;
    }
}
