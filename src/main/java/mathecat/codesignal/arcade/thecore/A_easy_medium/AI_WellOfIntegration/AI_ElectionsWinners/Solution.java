package mathecat.codesignal.arcade.thecore.A_easy_medium.AI_WellOfIntegration.AI_ElectionsWinners;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.electionsWinners(new int[] {2, 3, 5, 2}, 3));
    }

    int electionsWinners(int[] votes, int k) {
        int max = Arrays.stream(votes).max().orElse(-1);
        if (k == 0) return (int) Arrays.stream(votes).filter(v -> v == max).count() > 1 ? 0 : 1;
        int winners = 0;
        for (int i = 0; i < votes.length; i++) {
            if (votes[i] + k > max) winners++;
        }

        return winners;
    }
}
