package mathecat.codesignal.arcade.thecore.A_easy_medium.AF_LabyrinthOfNestedLoops.AA_IsPower;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.isPower(125));
        System.out.println(solution.isPower(72));
    }

    boolean isPower(int n) {
        if (n == 1) return true;

        int limit = (int) Math.sqrt(n);
        for (int i = 2; i <= limit; i++) {
            int newValue = n;
            while (newValue % i == 0) newValue = newValue / i;

            if (newValue == 1) return true;

            continue;
        }
        return false;
    }

}
