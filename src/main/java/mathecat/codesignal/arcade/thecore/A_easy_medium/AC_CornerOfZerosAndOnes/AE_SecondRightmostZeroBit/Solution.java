package mathecat.codesignal.arcade.thecore.A_easy_medium.AC_CornerOfZerosAndOnes.AE_SecondRightmostZeroBit;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.secondRightmostZeroBit(37));
        System.out.println(solution.secondRightmostZeroBit(1073741824));
    }

    int secondRightmostZeroBit(int n) {
        return asd(n);
    }

    int asd(int n) {
        for (int i = 0, acc = 0; i < 32; i++) {
            if (((n >> i) & 1) == 0) {
                acc++;
                if (acc == 2) return (int) Math.pow(2, i);
            }
        }

        return -1;
    }
}
