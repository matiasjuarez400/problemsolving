package mathecat.codesignal.arcade.thecore.A_easy_medium.AI_WellOfIntegration.AK_AreSimilar;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.areSimilar(new int[] {1, 2, 3}, new int[] {1, 2, 3}));
    }

    boolean areSimilar(int[] a, int[] b) {
        int swapps = 0;

        boolean swapPending = false;
        for (int i = 0; i < a.length; i++) {
            int ae = a[i];
            int be = b[i];

            if (ae != be) {
                swapPending = true;
                for (int j = i + 1; j < a.length; j++) {
                    int aej = a[j];
                    int bej = b[j];

                    if (aej != bej) {
                        if (aej != be || bej != ae) return false;
                        else {
                            swapps++;
                            swapPending = false;
                            i = j;
                            break;
                        }
                    }
                }
            }
        }

        return !swapPending && swapps <= 1;
    }

}
