package mathecat.codesignal.arcade.thecore.A_easy_medium.AA_IntroGates.AH_PhoneCall;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.phoneCall(10, 1, 2, 22));
    }

    int phoneCall(int min1, int min2_10, int min11, int s) {
        int[] operators = new int[4];
        operators[0] = s;
        operators[1] = min1;
        operators[2] = 0;
        operators[3] = 1;

        operate(operators);

        operators[1] = min2_10;
        operators[3] = 9;
        if (operators[2] > 0) operate(operators);

        operators[1] = min11;
        operators[3] = Integer.MAX_VALUE;
        if (operators[2] > 9) operate(operators);

        return operators[2];
    }

    void operate(int[] values) {
        int temp = Integer.min(values[3], values[0] / values[1]);
        values[2] += temp;
        values[0] -= temp * values[1];
    }
}
