package mathecat.codesignal.arcade.thecore.A_easy_medium.AD_LoopTunnel.AI_Candles;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.candles(5, 2));
    }

    int candles(int candlesNumber, int makeNew) {
        int remainingCandles = candlesNumber;
        int result = remainingCandles;
        while (remainingCandles / makeNew > 0) {
            int newCandles = remainingCandles / makeNew;
            remainingCandles = remainingCandles + newCandles - newCandles * makeNew;
            result += newCandles;
        }

        return result;
    }

}
