package mathecat.codesignal.arcade.thecore.A_easy_medium.AD_LoopTunnel.AB_CountSumOfTwoRepresentations2;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.countSumOfTwoRepresentations2(6, 2, 4));
    }

    int countSumOfTwoRepresentations2(int n, int l, int r) {
        int acc = 0;
        for (int A = l; A <= r; A++) {
            int diff = n - A;
            if (diff >= A && diff <= r) acc++;
        }

        return acc;
    }
}
