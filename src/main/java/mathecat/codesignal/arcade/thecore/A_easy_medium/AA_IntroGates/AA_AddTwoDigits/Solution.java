package mathecat.codesignal.arcade.thecore.A_easy_medium.AA_IntroGates.AA_AddTwoDigits;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.addTwoDigits(29));
    }

    int addTwoDigits(int n) {
        String ns = Integer.toString(n);
        return Character.getNumericValue(ns.charAt(0)) + Character.getNumericValue(ns.charAt(1));
    }
}
