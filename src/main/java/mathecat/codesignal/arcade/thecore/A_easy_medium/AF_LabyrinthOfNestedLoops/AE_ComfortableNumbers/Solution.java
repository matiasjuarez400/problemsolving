package mathecat.codesignal.arcade.thecore.A_easy_medium.AF_LabyrinthOfNestedLoops.AE_ComfortableNumbers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.comfortableNumbers(10, 12));
        System.out.println(solution.comfortableNumbers(12, 108));
    }

    int comfortableNumbers(int l, int r) {
        Map<Integer, List<Integer>> numbersInSegmentByNumber = new HashMap<>();

        IntStream.range(l, r + 1).boxed()
                .forEach(i -> numbersInSegmentByNumber.put(i, getNumbersInSegmentForNumber(i, l, r)));

        Counter acc = new Counter();
        numbersInSegmentByNumber.forEach((k1, l1) -> {
            numbersInSegmentByNumber.forEach((k2, l2) -> {
                if (k1 < k2) {
                    if (l1.contains(k2) && l2.contains(k1)) acc.increment();
                }
            });
        });

        return acc.c;
    }

    class Counter {
        int c = 0;
        void increment() {
            this.c++;
        }
    }

    List<Integer> getNumbersInSegmentForNumber(int number, int l, int r) {
        List<Integer> numbers = new ArrayList<>();

        int digitsSum = calculateDigitsSum(number);

        for (int i = number - digitsSum; i <= number + digitsSum; i++) {
            if (i == number) continue;

            if (i >= l && i <= r) numbers.add(i);
        }

        return numbers;
    }

    int calculateDigitsSum(int number) {
        return Integer.toString(number).chars().map(Character::getNumericValue).sum();
    }
}
