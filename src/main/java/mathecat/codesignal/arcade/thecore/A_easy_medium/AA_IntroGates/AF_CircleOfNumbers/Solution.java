package mathecat.codesignal.arcade.thecore.A_easy_medium.AA_IntroGates.AF_CircleOfNumbers;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.circleOfNumbers(6, 3));
    }

    int circleOfNumbers(int n, int firstNumber) {
        int half = n / 2;

        return firstNumber + half >= n ? firstNumber - half : firstNumber + half;
    }

}
