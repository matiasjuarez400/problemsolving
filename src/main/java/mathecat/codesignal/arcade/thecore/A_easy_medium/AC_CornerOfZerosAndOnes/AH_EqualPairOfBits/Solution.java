package mathecat.codesignal.arcade.thecore.A_easy_medium.AC_CornerOfZerosAndOnes.AH_EqualPairOfBits;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
    }

    int equalPairOfBits(int n, int m) {
        return Integer.lowestOneBit(~n ^ m);
    }
}
