package mathecat.codesignal.arcade.thecore.A_easy_medium.AI_WellOfIntegration.AM_ThreeSplit;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.threeSplit(new int[] {0, -1, 0, -1, 0, -1}));
    }

    int threeSplit(int[] a) {
        int groups = 3;
        long arraySum = 0;
        for (int i : a) arraySum += i;

        long sumPerGroup = arraySum / groups;

        int acc = 0;

        long firstGroup = 0;
        for (int i = 0; i < a.length - 2; i++) {
            firstGroup += a[i];
            if (firstGroup != sumPerGroup) continue;

            long secondGroup = 0;
            for (int j = i + 1; j < a.length - 1; j++) {
                secondGroup += a[j];
                if (secondGroup != sumPerGroup) continue;

                acc++;
            }
        }

        return acc;
    }
}
