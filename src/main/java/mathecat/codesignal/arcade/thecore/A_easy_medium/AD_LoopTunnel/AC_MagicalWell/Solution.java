package mathecat.codesignal.arcade.thecore.A_easy_medium.AD_LoopTunnel.AC_MagicalWell;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.magicalWell(1, 2, 2));
    }

    int magicalWell(int a, int b, int n) {
        int acc = 0;
        for (int i = 0; i < n; i++) {
            acc += a++ * b++;
        }
        return acc;
    }

}
