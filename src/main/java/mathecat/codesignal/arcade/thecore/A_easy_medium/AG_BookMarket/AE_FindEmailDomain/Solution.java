package mathecat.codesignal.arcade.thecore.A_easy_medium.AG_BookMarket.AE_FindEmailDomain;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.findEmailDomain("John.Smith@example.com"));
        System.out.println(solution.findEmailDomain("\" \"@xample.org"));
        System.out.println(solution.findEmailDomain("\"very.unusual.@.unusual.com\"@usual.com"));
    }

    String findEmailDomain(String address) {
        Pattern pattern = Pattern.compile(".+@(.+)");
        Matcher matcher = pattern.matcher(address);

        matcher.find();
        return matcher.group(1);
    }

}
