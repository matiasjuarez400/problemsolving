package mathecat.codesignal.arcade.thecore.A_easy_medium.AF_LabyrinthOfNestedLoops.AG_RectangleRotation;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.rectangleRotation(6, 4));
        System.out.println(solution.rectangleRotation(50, 4));
        System.out.println(solution.rectangleRotation(38, 42));
    }

    int rectangleRotation(int a, int b) {
        double distanceBetweenIntegers = Math.sqrt(1 + 1);
        double halfDistanceBetweenIntegers = distanceBetweenIntegers / 2;
        double halfA = a / 2.0;
        double halfB = b / 2.0;

        int halfDistanceStepsToA = (int) (halfA / halfDistanceBetweenIntegers);
        int distanceStepsToA = halfDistanceStepsToA / 2;
        int halfDistanceStepsToB = (int) (halfB / halfDistanceBetweenIntegers);
        int distanceStepsToB = halfDistanceStepsToB / 2;

        int pointsInCentralADiagonal = (distanceStepsToA * 2 + 1);
        int centralDiagonalSiblings = distanceStepsToB * 2;
        int pointsInMiddleDiagonalAndSiblings = pointsInCentralADiagonal * (1 + centralDiagonalSiblings);

        boolean extraAMiddlePoint = distanceStepsToA * 2 < halfDistanceStepsToA;
        boolean extraBMiddlePoint = distanceStepsToB * 2 < halfDistanceStepsToB;

        int middlePoints = extraAMiddlePoint ? pointsInCentralADiagonal + 1 : pointsInCentralADiagonal - 1;
        int middlePointsLevels = extraBMiddlePoint ? centralDiagonalSiblings + 2 : centralDiagonalSiblings;

        int pointInIntermediateDiagonals = middlePoints * middlePointsLevels;

        return pointInIntermediateDiagonals + pointsInMiddleDiagonalAndSiblings;
    }
}
