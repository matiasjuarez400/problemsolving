package mathecat.codesignal.arcade.thecore.A_easy_medium.AD_LoopTunnel.AE_AdditionWithoutCarrying;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.additionWithoutCarrying(456, 1734));
    }

    int additionWithoutCarrying(int param1, int param2) {
        int result = 0;
        int limit = Math.max(Integer.toString(param1).length(), Integer.toString(param2).length());
        for (int i = 0; i < limit; i++) {
            int p1d = getDigitAtPosition(param1, i);
            int p2d = getDigitAtPosition(param2, i);

            int digitToAdd = p1d + p2d;
            if (digitToAdd >= 10) digitToAdd -= 10;

            result += digitToAdd * Math.pow(10, i);
        }
        return result;
    }

    int getDigitAtPosition(int number, int position) {
        return (int) ((number % Math.pow(10, position + 1)) / (Math.pow(10, position)));
    }

}
