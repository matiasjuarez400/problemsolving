package mathecat.codesignal.arcade.thecore.A_easy_medium.AC_CornerOfZerosAndOnes.AD_MirrorBits;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.mirrorBits(97));
        System.out.println(solution.mirrorBits(8));
        System.out.println(solution.mirrorBits(1));
    }

    int mirrorBits(int a) {
        return Integer.parseInt(
                new StringBuilder(Integer.toBinaryString(a))
                        .reverse().toString(),
                2);
    }
}
