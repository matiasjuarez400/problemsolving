package mathecat.codesignal.arcade.thecore.A_easy_medium.AI_WellOfIntegration.AJ_IntegerToStringOfFixedWidth;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.integerToStringOfFixedWidth(1234, 2));
        System.out.println(solution.integerToStringOfFixedWidth(1234, 5));
    }

    String integerToStringOfFixedWidth(int number, int width) {
        String numberString = Integer.toString(number);
        int l = numberString.length();

        if (l == width) return numberString;
        if (l > width) return numberString.substring(l - width);
        else return new String(new char[width - l]).replace('\0', '0') + numberString;
    }
}
