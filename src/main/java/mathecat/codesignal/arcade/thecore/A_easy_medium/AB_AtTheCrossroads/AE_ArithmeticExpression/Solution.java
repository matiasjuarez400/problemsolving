package mathecat.codesignal.arcade.thecore.A_easy_medium.AB_AtTheCrossroads.AE_ArithmeticExpression;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
    }

    boolean arithmeticExpression(int a, int b, int c) {
        return a + b == c || a - b == c || a * b == c || a / b == c && a % b == 0;
    }
}
