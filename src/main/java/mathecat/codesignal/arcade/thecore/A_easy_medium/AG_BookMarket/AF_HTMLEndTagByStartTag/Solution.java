package mathecat.codesignal.arcade.thecore.A_easy_medium.AG_BookMarket.AF_HTMLEndTagByStartTag;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.htmlEndTagByStartTag("<button type='button' disabled>"));
    }

    String htmlEndTagByStartTag(String startTag) {
        Pattern pattern = Pattern.compile("<(\\w+)");
        Matcher matcher = pattern.matcher(startTag);
        matcher.find();
        return String.format("</%s>", matcher.group(1));
    }

}
