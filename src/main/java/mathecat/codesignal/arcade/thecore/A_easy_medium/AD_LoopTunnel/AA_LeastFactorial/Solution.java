package mathecat.codesignal.arcade.thecore.A_easy_medium.AD_LoopTunnel.AA_LeastFactorial;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.leastFactorial(17));
    }

    int leastFactorial(int n) {
        for (int k = 1, acc = 1;; acc *= ++k) if (acc >= n) return acc;
    }
}
