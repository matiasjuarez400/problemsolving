package mathecat.codesignal.arcade.thecore.A_easy_medium.AD_LoopTunnel.AH_Rounders;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.rounders(15));
    }

    int rounders(int n) {
        int carry = 0;
        int next = n;
        int multiplier = 1;
        while (next > 9) {
            int lastDigit = next % 10;
            int lastDigitWithCarry = lastDigit + carry;
            if (lastDigitWithCarry >= 5) carry = 1;
            else carry = 0;
            multiplier *= 10;
            next = next / 10;
        }

        return (next + carry) * multiplier;
    }

}
