package mathecat.codesignal.arcade.thecore.A_easy_medium.AE_ListForestEdge.AD_ConcatenateArrays;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] a = {2, 2, 1};
        int[] b = {10, 11};

        System.out.println(Arrays.toString(solution.concatenateArrays(a, b)));
    }

    int[] concatenateArrays(int[] a, int[] b) {
        int[] c = new int[a.length + b.length];

        for (int i = 0; i < a.length; i++) {
            c[i] = a[i];
        }

        for (int i = a.length; i < a.length + b.length; i++) {
            c[i] = b[i - a.length];
        }

        return c;
    }
}
