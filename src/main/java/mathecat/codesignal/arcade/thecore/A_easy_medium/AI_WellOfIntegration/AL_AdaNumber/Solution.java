package mathecat.codesignal.arcade.thecore.A_easy_medium.AI_WellOfIntegration.AL_AdaNumber;

import java.math.BigInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.adaNumber("123_456_789"));
        System.out.println(solution.adaNumber("10#123abc#"));
    }

    boolean adaNumber(String line) {
        line = line.replace("_", "").toUpperCase();

        if (line.matches("\\d+")) return true;

        Pattern pattern = Pattern.compile("(\\d+)#([0-9A-F]+)#");
        Matcher matcher = pattern.matcher(line);

        if (!matcher.matches()) return false;

        String base = matcher.group(1);
        String number = matcher.group(2);

        int baseInt = Integer.parseInt(base);
        if (baseInt < 2 || baseInt > 16) return false;

        try {
            new BigInteger(number, baseInt);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
