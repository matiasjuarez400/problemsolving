package mathecat.codesignal.arcade.thecore.A_easy_medium.AI_WellOfIntegration.AH_TimedReading;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.timedReading(4, "The Fox asked the stork, 'How is the soup?'"));
    }

    int timedReading(int maxLength, String text) {
        return (int) Arrays.stream(text.split("\\W+")).filter(w -> w.length() <= maxLength).count();
    }

}
