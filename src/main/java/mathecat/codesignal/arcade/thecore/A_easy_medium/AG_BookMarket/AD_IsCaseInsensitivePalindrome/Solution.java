package mathecat.codesignal.arcade.thecore.A_easy_medium.AG_BookMarket.AD_IsCaseInsensitivePalindrome;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.isCaseInsensitivePalindrome("AaBaa"));
        System.out.println(solution.isCaseInsensitivePalindrome("abac"));
    }

    boolean isCaseInsensitivePalindrome(String inputString) {
        int middle = inputString.length() / 2;
        int size = inputString.length();

        return inputString.substring(0, middle).equalsIgnoreCase(new StringBuilder(inputString.substring(size % 2 == 0 ? middle : middle + 1)).reverse().toString());
    }

}
