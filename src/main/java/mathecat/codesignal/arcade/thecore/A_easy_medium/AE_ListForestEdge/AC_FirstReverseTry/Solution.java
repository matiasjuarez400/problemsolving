package mathecat.codesignal.arcade.thecore.A_easy_medium.AE_ListForestEdge.AC_FirstReverseTry;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] arr = {1, 2, 3, 4, 5};

        System.out.println(Arrays.toString(solution.firstReverseTry(arr)));
    }

    int[] firstReverseTry(int[] arr) {
        if (arr.length == 0) return arr;

        int temp = arr[0];
        arr[0] = arr[arr.length - 1];
        arr[arr.length - 1] = temp;
        return arr;
    }

}
