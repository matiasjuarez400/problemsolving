package mathecat.codesignal.arcade.thecore.A_easy_medium.AE_ListForestEdge.AH_MakeArrayConsecutive2;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] statues = {6, 2, 3, 8};

        System.out.println(solution.makeArrayConsecutive2(statues));
    }

    int makeArrayConsecutive2(int[] statues) {
        int max = -1;
        int min = Integer.MAX_VALUE;

        for (int i = 0; i < statues.length; i++) {
            max = Integer.max(max, statues[i]);
            min = Integer.min(min, statues[i]);
        }

        return max - min + 1 - statues.length;
    }

}
