package mathecat.codesignal.arcade.thecore.A_easy_medium.AE_ListForestEdge.AB_ArrayReplace;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] inputArray = {1, 2, 1};
        int elemToReplace = 1;
        int substitutionElem = 3;

        System.out.println(Arrays.toString(solution.arrayReplace(inputArray, elemToReplace, substitutionElem)));
    }

    int[] arrayReplace(int[] inputArray, int elemToReplace, int substitutionElem) {
        for (int i = 0; i < inputArray.length; i++) {
            if (inputArray[i] == elemToReplace) inputArray[i] = substitutionElem;
        }
        return inputArray;
    }

}
