package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.List;
import java.util.stream.Collectors;

class FractalUnionPointsInformationRelative {
    private final FractalUnionPointsInformation fractalUnionPointsInformation;
    private final Point globalRelativePosition;
    private final FractalModelRelative fractalModelRelative;

    public FractalUnionPointsInformationRelative(FractalUnionPointsInformation fractalUnionPointsInformation, Point globalRelativePosition) {
        this.fractalUnionPointsInformation = fractalUnionPointsInformation;
        this.globalRelativePosition = globalRelativePosition;
        this.fractalModelRelative = new FractalModelRelative(fractalUnionPointsInformation.getFractalModel(), globalRelativePosition);
    }

    public List<UnionPointWithAnchorsRelative> getUnionsWithRelativePosition() {
        return fractalUnionPointsInformation.getUnionPointsWithAnchors().stream()
                .map(origUnion -> {
                    final UnionPointWithAnchorsRelative relativeUnion = new UnionPointWithAnchorsRelative(origUnion, globalRelativePosition);
                    return relativeUnion;
                }).collect(Collectors.toList());
    }

    public FractalModelRelative getFractalModelRelative() {
        return fractalModelRelative;
    }

    public FractalModel getFractal() {
        return fractalUnionPointsInformation.getFractalModel();
    }

    public Point getGlobalRelativePosition() {
        return globalRelativePosition;
    }
}
