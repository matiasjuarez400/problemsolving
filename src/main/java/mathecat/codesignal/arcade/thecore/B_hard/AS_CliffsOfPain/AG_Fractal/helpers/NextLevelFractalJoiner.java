package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class NextLevelFractalJoiner {
    private final FractalJoiner fractalJoiner;
    private static final int[] CLOCK_WISE_INDEXES = {0, 3, 2, 1};
    private static final int[] ANTI_CLOCK_WISE_INDEX = {0, 1, 2, 3};

    public NextLevelFractalJoiner(FractalJoiner fractalJoiner) {
        this.fractalJoiner = fractalJoiner;
    }

    public NextLevelFractalData join(List<FractalUnionPointsInformationRelative> fractalsToBeJoined) {
        List<BridgeInformation> bridgeInformationList = createBridges(fractalsToBeJoined);

        if (bridgeInformationList == null) return null;

        List<Bridge> usedBridges = chooseBridges(bridgeInformationList);

        if (usedBridges == null || usedBridges.isEmpty()) return null;

        return new NextLevelFractalData(bridgeInformationList, usedBridges);
    }

    private List<Bridge> chooseBridges(List<BridgeInformation> bridgeInformationList) {
        return bridgeInformationList.stream().map(BridgeInformation::getShortestBridge).collect(Collectors.toList());
    }

    private List<BridgeInformation> createBridges(List<FractalUnionPointsInformationRelative> fractalUnionPointsInformationRelativeList) {
        List<BridgeInformation> unionsCreated;

        unionsCreated = createBridges(fractalUnionPointsInformationRelativeList, CLOCK_WISE_INDEXES);

        if (unionsCreated == null) {
            unionsCreated = createBridges(fractalUnionPointsInformationRelativeList, ANTI_CLOCK_WISE_INDEX);
        }

        return unionsCreated;
    }

    private List<BridgeInformation> createBridges(List<FractalUnionPointsInformationRelative> fractalUnionPointsInformationRelativeList,
                                                  int[] indexes) {
        final List<BridgeInformation> unionsCreated = new ArrayList<>();

        for (int i = 1; i < indexes.length; i++) {
            FractalUnionPointsInformationRelative first = fractalUnionPointsInformationRelativeList.get(indexes[i - 1]);
            FractalUnionPointsInformationRelative second = fractalUnionPointsInformationRelativeList.get(indexes[i]);

            BridgeInformation bridgeInformation = fractalJoiner.joinFractals(first, second);

            if (bridgeInformation == null || !bridgeInformation.bridgeExists()) return null;

            unionsCreated.add(bridgeInformation);
        }

        return unionsCreated;
    }
}
