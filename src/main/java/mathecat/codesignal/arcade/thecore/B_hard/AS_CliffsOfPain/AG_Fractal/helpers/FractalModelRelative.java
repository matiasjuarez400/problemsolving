package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.ArrayList;
import java.util.List;

public class FractalModelRelative implements RelativePositionable {
    private final FractalModel fractalModel;
    private final Point relativePosition;

    public FractalModelRelative(FractalModel fractalModel, Point relativePosition) {
        this.fractalModel = fractalModel;
        this.relativePosition = relativePosition;
    }

    public boolean isConnectorInPosition(int col, int row) {
        Character charInPosition = fractalModel.get(col - relativePosition.getCol(), row - relativePosition.getRow());
        return CharManager.isConnectorChar(charInPosition);
    }

    public boolean isConnectorInPosition(Point position) {
        return isConnectorInPosition(position.getCol(), position.getRow());
    }

    public FractalModelElement getElementFromRelativePosition(int col, int row) {
        return fractalModel.getFractalModelElement(col - relativePosition.getCol(), row - relativePosition.getRow());
    }

    public FractalModelElement getElementFromRelativePosition(Point point) {
        return getElementFromRelativePosition(point.getCol(), point.getRow());
    }

    public List<FractalModelElement> getAllFractalModelElementsWithRelativePosition() {
        final List<FractalModelElement> fractalModelElementList = new ArrayList<>();

        for (int localRow = 0; localRow < fractalModel.getRows(); localRow++) {
            for (int localCol = 0; localCol < fractalModel.getCols(); localCol++) {
                fractalModelElementList.add(new FractalModelElement(fractalModel.get(localCol, localRow),
                        localCol + relativePosition.getCol(),
                        localRow + relativePosition.getRow())
                );
            }
        }

        return fractalModelElementList;
    }

    public FractalModel getFractalModel() {
        return fractalModel;
    }

    @Override
    public Point getLocalPosition() {
        return new Point(0, 0);
    }

    @Override
    public Point getGlobalRelativePosition() {
        return relativePosition;
    }
}
