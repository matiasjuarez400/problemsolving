package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class FractalModel {
    private char[][] model;
    private int rotation = 0;

    public FractalModel(char[][] rawModel) {
        this.model = new char[rawModel.length][rawModel[0].length];

        for (int row = 0; row < model.length; row++) {
            for (int col = 0; col < model[row].length; col++) {
                char nextChar = rawModel[row][col];
                if (nextChar == ' ') {
                    nextChar = CharManager.getModelEmpty();
                } else if (!CharManager.isValidChar(nextChar)) {
                    throw new IllegalArgumentException("Unexpected char: " + nextChar);
                }

                this.model[row][col] = nextChar;
            }
        }
    }

    public FractalModel(FractalModel fractalModel) {
        this(fractalModel.getModel());
        this.rotation = fractalModel.rotation;
    }

    public int getRotation() {
        return rotation;
    }

    public int getRows() {
        return model.length;
    }

    public int getCols() {
        return model[0].length;
    }

    public Character get(Point point) {
        return get(point.getCol(), point.getRow());
    }

    public Character get(int col, int row) {
        if (col < 0 || row < 0 || col >= this.model[0].length || row >= this.model.length) return null;
        return model[row][col];
    }

    public FractalModelElement getFractalModelElement(int col, int row) {
        return getFractalModelElement(new Point(col, row));
    }

    public FractalModelElement getFractalModelElement(Point point) {
        Character character = get(point);

        if (character == null) return null;

        return new FractalModelElement(character, point);
    }

    public FractalModelElementWithSurroundings getFractalModelElementWithSurroundings(int col, int row) {
        return getFractalModelElementWithSurroundings(new Point(col, row));
    }

    public FractalModelElementWithSurroundings getFractalModelElementWithSurroundings(Point point) {
        Character character = get(point);

        if (character == null) return null;

        return new FractalModelElementWithSurroundings(this, point);
    }

    public List<FractalModel> getRotations() {
        List<FractalModel> allRotations = new ArrayList<>();
        allRotations.add(new FractalModel(this));

        for (int i = 0; i < 3; i++) {
            FractalModel newFractalRotation = allRotations.get(allRotations.size() - 1);
            newFractalRotation = new FractalModel(newFractalRotation);
            newFractalRotation.breath();
            newFractalRotation.rotation++;
            allRotations.add(newFractalRotation);
        }

//        fixFractalRotation(allRotations.get(1));
//        fixFractalRotation(allRotations.get(3));

        return allRotations;
    }

    private void fixFractalRotation(FractalModel fractalModel) {
        ConnectionAnalyzer connectionAnalyzer = ConnectionAnalyzer.getInstance();

        for (int rowIndex = 0; rowIndex < fractalModel.model.length; rowIndex++) {
            char[] row = fractalModel.model[rowIndex];
            for (int leftCol = 0; leftCol < row.length - 2; leftCol++) {
                final int middleCol = leftCol + 1;
                final int rightCol = leftCol + 2;

                if (CharManager.isHorizontal(row[leftCol]) && !CharManager.isConnectorChar(row[middleCol]) && CharManager.isHorizontal(row[rightCol])) {
                    FractalModelElementWithSurroundings leftElement = fractalModel.getFractalModelElementWithSurroundings(leftCol, rowIndex);
                    FractalModelElementWithSurroundings rightElement = fractalModel.getFractalModelElementWithSurroundings(rightCol, rowIndex);

                    FractalModelElement middleElement = new FractalModelElement(CharManager.getModelHorizontal(), middleCol, rowIndex);

                    if (connectionAnalyzer.canConnectWithElement(leftElement, middleElement) &&
                            connectionAnalyzer.canConnectWithElement(rightElement, middleElement)) {
                        fractalModel.model[rowIndex][middleCol] = middleElement.getSymbol();
                    }
                }
            }
        }
    }

    private char[][] invertHorizontally(char[][] input) {
        char[][] output = new char[input.length][input[0].length];

        for (int row = 0; row < input.length; row++) {
            for (int col = 0; col < input[row].length; col++) {
                output[row][input[row].length - 1 - col] = input[row][col];
            }
        }

        return output;
    }

    private char[][] invertVertically(char[][] input) {
        char[][] output = new char[input.length][input[0].length];

        for (int row = 0; row < input.length; row++) {
            for (int col = 0; col < input[row].length; col++) {
                output[input.length - 1 - row][col] = input[row][col];
            }
        }

        return output;
    }

    public char[][] getModel() {
        return model;
    }

    char[][] breath() {
        return breath(this.model);
    }

    private char[][] breath(char[][] input) {
        final char[][] expandedModel = expand(input);
        final char[][] rotatedModel = rotate(expandedModel);
        final char[][] compressedModel = compress(rotatedModel);
        this.model = compressedModel;
        return compressedModel;
    }

    char[][] compress(char[][] input) {
        final int firstRow = findFirstUsefulRow(input, 0, 1);
        final int lastRow = findFirstUsefulRow(input, input.length - 1, -1);
        final int firstCol = findFirstUsefulCol(input, 0, 1);
        final int lastCol = findFirstUsefulCol(input, input[0].length - 1, -1);

        final char[][] middleModel = new char[lastRow - firstRow + 1][lastCol - firstCol + 1];

        for (int row = firstRow; row <= lastRow; row++) {
            for (int col = firstCol; col <= lastCol; col++) {
                middleModel[row - firstRow][col - firstCol] = input[row][col];
            }
        }

        final char[][] compressedModel = compressMiddleModel(middleModel);
        return replaceExpansionCharacters(compressedModel);
    }

    private char[][] replaceExpansionCharacters(char[][] input) {
        for (char[] row : input) {
            for (int col = 0; col < row.length; col++) {
                char c = row[col];
                if (c == CharManager.getModelExpansion()) {
                    row[col] = CharManager.getModelEmpty();
                }
            }
        }
        return input;
    }

    private char[][] compressMiddleModel(char[][] input) {
        List<char[]> rowList = new ArrayList<>();

        char[] lastHorizontalRow = null;
        for (int row = input.length - 1; row >= 0; row--) {
            boolean emptyRow = true;
            boolean verticalRow = false;
            boolean horizontalRow = false;

            for (int col = 0; col < input[row].length; col++) {
                char c = input[row][col];
                if (c == CharManager.getModelHorizontal()) {
                    emptyRow = false;
                    horizontalRow = true;
                    break;
                } else if (c == CharManager.getModelVertical()) {
                    emptyRow = false;
                    verticalRow = true;
                    break;
                } else if (c == CharManager.getModelEmpty()) {
                    emptyRow = false;
                }
            }

            if (horizontalRow) {
                lastHorizontalRow = input[row];
                rowList.add(lastHorizontalRow);
            } else if (verticalRow) {
                char[] currentRow = input[row];
                if (lastHorizontalRow != null) {
                    for (int col = 0; col < currentRow.length; col++) {
                        if (currentRow[col] == CharManager.getModelVertical()) {
                            lastHorizontalRow[col] = CharManager.getModelVertical();
                        }
                    }
                } else {
                    rowList.add(currentRow);
                }

                lastHorizontalRow = null;
            } else if (!emptyRow) {
                char[] currentRow = input[row];
                lastHorizontalRow = currentRow;
                rowList.add(lastHorizontalRow);
            }
        }

        Collections.reverse(rowList);

        char[][] output = new char[rowList.size()][];
        for (int row = 0; row < rowList.size(); row++) {
            output[row] = rowList.get(row);
        }

        return output;
    }

    private int findFirstUsefulCol(char[][] input, int initialCol, int colStep) {
        for (int col = initialCol; col >= 0 && col < input[0].length; col += colStep) {
            for (int row = 0; row < input.length; row++) {
                char c = input[row][col];
                if (!(isEmptyChar(c))) {
                    return col;
                }
            }
        }

        throw new IllegalStateException("Whaaat??!!! No valid column found");
    }

    private int findFirstUsefulRow(char[][] input, int initialRow, int rowStep) {
        for (int row = initialRow; row >= 0 && row < input.length; row += rowStep) {
            char[] currentRow = input[row];
            if (isEmptyRow(currentRow)) continue;
            return row;
        }
        throw new IllegalArgumentException("Whaaat?? No useful row was found");
    }

    private boolean isEmptyRow(char[] row) {
        for (char c : row) {
            if (isEmptyChar(c)) return false;
        }
        return true;
    }

    private boolean isEmptyChar(char c) {
        return c == CharManager.getModelExpansion();
    }

    char[][] expand(char[][] input) {
        final int expandedRows = input.length * 2;
        final int rowColDiff = expandedRows - input[0].length;
        final int rawModelCols = input[0].length;

        int newModelRows = expandedRows;
        int newModelCols = rawModelCols;
        int initialRowIteration = 0;
        if (rowColDiff > 0) {
            newModelCols = input[0].length + rowColDiff;
        } else if (rowColDiff < 0) {
            initialRowIteration = Math.abs(rowColDiff);
            newModelRows = expandedRows + initialRowIteration;
        }

        final char[][] expandedModel = new char[newModelRows][newModelCols];
        for (char[] row : expandedModel) {
            Arrays.fill(row, CharManager.getModelExpansion());
        }

        for (int row = 0; row < input.length; row++) {
            int expandedModelRow = initialRowIteration + row * 2;

            for (int col = 0; col < rawModelCols; col++) {
                char current = input[row][col];
                int finalExpandadModelRow = expandedModelRow;
                if (current != CharManager.getModelVertical()) finalExpandadModelRow++;

                expandedModel[finalExpandadModelRow][col] = current;
            }
        }

        return expandedModel;
    }

    char[][] rotate(char[][] input) {
        char[][] output = new char[input[0].length][input.length];

        for (int row = 0; row < input.length; row++) {
            for (int col = 0; col < input[row].length; col++) {
                char currentChar = input[row][col];
                if (currentChar == CharManager.getModelVertical()) currentChar = CharManager.getModelHorizontal();
                else if (currentChar == CharManager.getModelHorizontal()) currentChar = CharManager.getModelVertical();

                output[input[row].length - 1 - col][row] = currentChar;
            }
        }

        return output;
    }

    @Override
    public String toString() {
        return FractalModelPrinter.printModelRepresentation(this);
    }
}
