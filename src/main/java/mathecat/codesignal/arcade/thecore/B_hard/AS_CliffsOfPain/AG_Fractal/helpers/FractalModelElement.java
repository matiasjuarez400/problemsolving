package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

class FractalModelElement {
    private final char symbol;
    private int col;
    private int row;

    public FractalModelElement(char symbol, int col, int row) {
        this.symbol = symbol;
        this.col = col;
        this.row = row;
    }

    public FractalModelElement(char symbol, Point point) {
        this(symbol, point.getCol(), point.getRow());
    }

    public FractalModelElement(FractalModelElement that) {
        this.symbol = that.symbol;
        this.col = that.col;
        this.row = that.row;
    }

    public FractalModelElement moveTo(int col, int row) {
        this.col = col;
        this.row = row;
        return this;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public Point getPosition() {
        return new Point(col, row);
    }

    @Override
    public String toString() {
        return "FractalModelElement{" +
                "symbol=" + symbol +
                ", col=" + col +
                ", row=" + row +
                '}';
    }
}
