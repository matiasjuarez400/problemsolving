package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.List;

public class AnchorRelative implements RelativePositionable {
    private Anchor anchor;
    private UnionPointWithAnchorsRelative unionPointWithAnchorsRelative;
    private Point globalRelativePosition;

    public AnchorRelative(Anchor anchor, UnionPointWithAnchorsRelative unionPointWithAnchorsRelative, Point globalRelativePosition) {
        this.anchor = anchor;
        this.unionPointWithAnchorsRelative = unionPointWithAnchorsRelative;
        this.globalRelativePosition = globalRelativePosition;
    }

    public List<Character> getValidConnectors() {
        return this.anchor.getValidConnectors();
    }

    @Override
    public Point getGlobalRelativePosition() {
        return anchor.getAnchorLocation().add(globalRelativePosition);
    }

    @Override
    public Point getLocalPosition() {
        return this.anchor.getAnchorLocation();
    }

    public UnionPointWithAnchorsRelative getAnchorOwner() {
        return unionPointWithAnchorsRelative;
    }

    @Override
    public String toString() {
        return "AnchorRelative{" +
                "local=" + getLocalPosition() +
                "relative=" + getGlobalRelativePosition() +
                '}';
    }
}
