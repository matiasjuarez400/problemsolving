package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.ArrayList;
import java.util.List;

class FractalUnionsFinder {
    private final ConnectionAnalyzer connectionAnalyzer;

    public FractalUnionsFinder(ConnectionAnalyzer connectionAnalyzer) {
        this.connectionAnalyzer = connectionAnalyzer;
    }

    public FractalUnionPointsInformation analyzeFractalModel(FractalModel fractalModel) {
        final boolean[][] unionMatrix = findUnionPoints(fractalModel);
        final List<UnionPointWithAnchors> unionPointsWithAnchors = findUnionPointsWithAnchors(fractalModel, unionMatrix);

        return new FractalUnionPointsInformation(fractalModel, unionMatrix, unionPointsWithAnchors);
    }

    private boolean[][] findUnionPoints(char[][] model) {
        boolean[][] unionPoints = new boolean[model.length][model[0].length];

        for (int row = 0; row < unionPoints.length; row++) {
            for (int col = 0; col < unionPoints[row].length; col++) {
                final char symbol = model[row][col];
                if (!CharManager.isConnectorChar(symbol)) continue;

                final FractalModelElement base = new FractalModelElement(symbol, col, row);
                final int connectionsForCurrentPoint = connectionAnalyzer.countConnectionsAroundPoint(base, model);
                if (connectionsForCurrentPoint <= 1) unionPoints[row][col] = true;
            }
        }

        return unionPoints;
    }

    private boolean[][] findUnionPoints(FractalModel fractalModel) {
        return findUnionPoints(fractalModel.getModel());
    }

    private List<UnionPointWithAnchors> findUnionPointsWithAnchors(FractalModel fractalModel, boolean[][] unionPointsMatrix) {
        final List<Point> unionPointList = new ArrayList<>();
        for (int row = 0; row < unionPointsMatrix.length; row++) {
            for (int col = 0; col < unionPointsMatrix[row].length; col++) {
                if (unionPointsMatrix[row][col]) {
                    unionPointList.add(new Point(col, row));
                }
            }
        }

        final List<UnionPointWithAnchors> unionPointWithAnchorsList = new ArrayList<>();
        for (Point point : unionPointList) {
            UnionPointWithAnchors unionPointWithAnchors = new UnionPointWithAnchors(point, fractalModel.get(point));
            unionPointWithAnchorsList.add(unionPointWithAnchors);

            for (int row = point.getRow() - 1; row <= point.getRow() + 1; row++) {
                for (int col = point.getCol() - 1; col <= point.getCol() + 1; col++) {
                    if (row == point.getRow() && col == point.getCol()) continue;

                    Character fractalChar = fractalModel.get(col, row);

                    if (CharManager.isConnectorChar(fractalChar)) continue;

                    Point anchorPosition = new Point(col, row);
                    List<Character> anchorPossibleCharacters = analyzePossibleValuesForAnchor(
                            anchorPosition, unionPointWithAnchors.getUnionLocation(), fractalModel);

                    if (!anchorPossibleCharacters.isEmpty()) {
                        unionPointWithAnchors.addAnchor(anchorPosition, anchorPossibleCharacters);
                    }
                }
            }
        }

        return unionPointWithAnchorsList;
    }

    List<Character> analyzePossibleValuesForAnchor(Point anchorPosition, Point unionPosition, FractalModel fractalModel) {
        List<Character> characters = new ArrayList<>();

        FractalModelElementWithSurroundings unionFractalModelElementWithSurroundings = fractalModel.getFractalModelElementWithSurroundings(unionPosition);
        for (Character connectorChar : CharManager.getConnectionChars()) {
            FractalModelElement newConnectorFractalModelElement = new FractalModelElement(connectorChar, anchorPosition);

            if (!connectionAnalyzer.canConnectWithElement(unionFractalModelElementWithSurroundings, newConnectorFractalModelElement)) continue;

            int connectionsForCurrentBase = connectionAnalyzer.countConnectionsAroundPoint(newConnectorFractalModelElement, fractalModel);

            /*
             * If connectionCount is 0, that means that if use the current connectorChar in the anchorPosition, we won't be able to generate a connection
             * to the current fractal model.
             * If connectionCount is 2 or more, that means that the current connectorChar would generate a close connection with the current fractal. That is,
             * the anchor can not be used to connect to a neighbour fractal
             */

            if (connectionsForCurrentBase != 1) continue;

            boolean neighbourHoodAnalysisPass = true;
            for (int row = -1; row <= 1 && neighbourHoodAnalysisPass; row++) {
                for (int col = -1; col <= 1; col++) {
                    final int effectiveRow = anchorPosition.getRow() + row;
                    final int effectiveCol = anchorPosition.getCol() + col;
                    FractalModelElementWithSurroundings currentNeighbourFractalModelElement = fractalModel
                            .getFractalModelElementWithSurroundings(effectiveCol, effectiveRow);

                    if (currentNeighbourFractalModelElement == null) continue;
                    if (connectionAnalyzer.canConnectWithElement(newConnectorFractalModelElement, currentNeighbourFractalModelElement.getCentralElement())) {
                        if (!connectionAnalyzer.canConnectWithElement(currentNeighbourFractalModelElement, newConnectorFractalModelElement))  {
                            neighbourHoodAnalysisPass = false;
                        }
                    }

//                    int connectionForCurrentNeighbour = connectionAnalyzer.countConnectionsAroundPoint(currentNeighbourFractalModelElement, fractalModel);
//
//                    if (connectionForCurrentNeighbour > 1) {
//                        neighbourHoodAnalysisPass = false;
//                        break;
//                    }
                }
            }

            if (neighbourHoodAnalysisPass) {
                characters.add(connectorChar);
            }
        }

        return characters;
    }
}
