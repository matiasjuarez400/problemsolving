package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.Objects;

class Point {
    private final int col;
    private final int row;

    public Point(int col, int row) {
        this.col = col;
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public Point add(Point that) {
        return new Point(this.col + that.col, this.row + that.row);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return col == point.col &&
                row == point.row;
    }

    @Override
    public int hashCode() {
        return Objects.hash(col, row);
    }

    @Override
    public String toString() {
        return "Point{" +
                "col=" + col +
                ", row=" + row +
                '}';
    }
}
