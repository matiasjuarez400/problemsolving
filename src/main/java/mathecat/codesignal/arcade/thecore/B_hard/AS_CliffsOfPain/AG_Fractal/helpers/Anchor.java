package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.List;

class Anchor {
    private final Point anchorLocation;
    private final UnionPointWithAnchors anchorOwner;
    private final List<Character> validConnectors;

    public Anchor(Point anchorLocation, UnionPointWithAnchors anchorOwner, List<Character> validConnectors) {
        this.anchorLocation = anchorLocation;
        this.anchorOwner = anchorOwner;
        this.validConnectors = validConnectors;
    }

    public Point getAnchorLocation() {
        return anchorLocation;
    }

    public UnionPointWithAnchors getAnchorOwner() {
        return anchorOwner;
    }

    public List<Character> getValidConnectors() {
        return validConnectors;
    }

    @Override
    public String toString() {
        return "Anchor{" +
                "anchorLocation=" + anchorLocation +
                '}';
    }
}
