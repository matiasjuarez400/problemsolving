package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AB_Game2048;

import java.util.Iterator;
import java.util.function.IntPredicate;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        
        int[][] grid = {
                 {0,2,0,2},
                 {0,4,4,2},
                 {0,0,4,2},
                 {0,0,4,2}
        };

        String moves = "LLU";

        int[][] result = solution.game2048(grid, moves);

        Board board = new Board(result);
        BoardPrinter printer = new BoardPrinter();

        printer.print(board);
    }

    int[][] game2048(int[][] grid, String path) {
        Board board = new Board(grid);
        BoardPrinter printer = new BoardPrinter();
//        printer.disable();
        printer.print(board);

        MoveInputIterator iterator = new MoveInputIterator(path);

        while (iterator.hasNext()) {
            Direction direction = iterator.next();
            board.processMove(direction);
            printer.print(board);
        }

        return board.convertToGrid();
    }

    private static class MoveInputIterator implements Iterator<Direction> {
        private String moves;
        private int moveIndex = 0;

        public MoveInputIterator(String moves) {
            this.moves = moves;
        }

        private Direction mapInputChar(char c) {
            switch (c) {
                case 'L': return Direction.LEFT;
                case 'R': return Direction.RIGHT;
                case 'U': return Direction.UP;
                case 'D': return Direction.DOWN;
                default: throw new IllegalArgumentException("Direction not supported: " + c);
            }
        }

        @Override
        public boolean hasNext() {
            return moveIndex < moves.length();
        }

        @Override
        public Direction next() {
            return mapInputChar(moves.charAt(moveIndex++));
        }
    }

    private enum Direction {
        UP, DOWN, LEFT, RIGHT
    }

    private static class Board {


        private int turn = 1;
        private Square[][] squares;

        public Board(int[][] grid) {
            this.squares = new Square[grid.length][grid[0].length];
            for (int row = 0; row < grid.length; row++) {
                for (int col = 0; col < grid[0].length; col++) {
                    int gridValue = grid[row][col];
                    squares[row][col] = new Square(gridValue);
                }
            }
        }

        public Square[][] getSquares() {
            return squares;
        }

        public void processMove(Direction direction) {
            int limit = (direction == Direction.RIGHT || direction == Direction.LEFT) ? squares.length : squares[0].length;

            for (int i = 0; i < limit; i++) {
                while (merge(i, direction));
            }

            this.turn++;
        }

        public int[][] convertToGrid() {
            int[][] grid = new int[squares.length][squares[0].length];

            for (int i = 0; i < grid.length; i++) {
                for (int c = 0; c < grid[i].length; c++) {
                    grid[i][c] = squares[i][c].getValue();
                }
            }

            return grid;
        }

        private boolean merge(int indexInput, Direction direction) {
            boolean somethingMerged = false;
            boolean isRow = direction.equals(Direction.RIGHT) || direction.equals(Direction.LEFT);

            IntPredicate insideRangePredicate = isRow ?
                    (i -> i >= 0 && i < this.squares[indexInput].length) :
                    (i -> i >= 0 && i < this.squares.length);

            int mainSquareIndex;
            int mainSquareStep;
            int mergingStep;

            if (direction == Direction.DOWN || direction == Direction.RIGHT) {
                mainSquareIndex  = this.squares.length - 1;
                mainSquareStep = -1;
                mergingStep = 1;
            } else if (direction == Direction.UP || direction == Direction.LEFT) {
                mainSquareIndex = 0;
                mainSquareStep = 1;
                mergingStep = -1;
            } else throw new IllegalArgumentException("Invalid direction");

            while (insideRangePredicate.test(mainSquareIndex)) {
                Square mainSquare = isRow ?
                        (this.squares[indexInput][mainSquareIndex]) :
                        (this.squares[mainSquareIndex][indexInput]);

                if (!mainSquare.isEmpty()) {
                    int lastFreeSquareIndex = -1;
                    for (int mergeSquareIndex = mainSquareIndex + mergingStep; insideRangePredicate.test(mergeSquareIndex); mergeSquareIndex += mergingStep) {
                        Square mergeSquare = isRow ?
                                (this.squares[indexInput][mergeSquareIndex]) :
                                (this.squares[mergeSquareIndex][indexInput]);

                        if (mergeSquare.isEmpty()) {
                            lastFreeSquareIndex = mergeSquareIndex;
                        } else {
                            if (mainSquare.canMerge(mergeSquare, this.turn)) {
                                mergeSquare.merge();
                                mergeSquare.setLastTurnMerged(this.turn);
                                mainSquare.free();
                                somethingMerged = true;
                                lastFreeSquareIndex = -1;
                                break;
                            } else if (lastFreeSquareIndex >= 0) {
                                if (isRow) {
                                    this.squares[indexInput][lastFreeSquareIndex].setValue(mainSquare.getValue());
                                } else {
                                    this.squares[lastFreeSquareIndex][indexInput].setValue(mainSquare.getValue());
                                }

                                mainSquare.free();
                                lastFreeSquareIndex = -1;
                                break;
                            } else {
                                break;
                            }
                        }
                    }

                    if (lastFreeSquareIndex >= 0) {
                        if (isRow) {
                            this.squares[indexInput][lastFreeSquareIndex].setValue(mainSquare.getValue());
                        } else {
                            this.squares[lastFreeSquareIndex][indexInput].setValue(mainSquare.getValue());
                        }
                        mainSquare.free();
                    }
                }

                mainSquareIndex += mainSquareStep;
            }

            return somethingMerged;
        }
    }

    private static class Square {
        private int value;
        private int lastTurnMerged;

        public Square(int value) {
            this.value = value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public int getLastTurnMerged() {
            return lastTurnMerged;
        }

        public void setLastTurnMerged(int lastTurnMerged) {
            this.lastTurnMerged = lastTurnMerged;
        }

        public boolean canMerge(Square anotherSquare, int currentTurn) {
            return anotherSquare != null &&
                    anotherSquare.getValue() == this.getValue() &&
                    anotherSquare.getLastTurnMerged() < currentTurn &&
                    this.getLastTurnMerged() < currentTurn;
        }

        public static Square empty() {
            return new Square(0);
        }

        public boolean isEmpty() {
            return this.value == 0;
        }

        public void merge() {
            this.value *= 2;
        }

        public void free() {
            this.value = 0;
        }
    }

    private static class BoardPrinter {
        private final char boardDelimeterSymbol = '*';
        private final char rowDelimeterSymbol = '-';
        private final char colDelimeterSymbol = '|';
        private final int cellRows = 5;
        private final int cellCols = 6;
        private final int numberOfCells = 4;
        private final int boardCols = (cellCols + 2 ) * numberOfCells;
        private final String cellEmptyRow = freeSpace( cellCols);
        private boolean enabled = true;

        public void print(Board board) {
            if (enabled) {
                System.out.println(generateBoardRepresentation(board));
            }
        }

        public String generateBoardRepresentation(Board board) {
            StringBuilder sb = new StringBuilder();
            String rowInitialContent = boardDelimeterSymbol + " ";
            String rowEndContent = " " + boardDelimeterSymbol;

            sb.append(rowInitialContent).append(repeat(boardDelimeterSymbol, boardCols)).append(rowEndContent).append("\n");

            Square[][] grid = board.getSquares();

            for (int row = 0; row < numberOfCells; row++) {
                for (int subrow = 0; subrow < cellRows; subrow++) {
                    sb.append(rowInitialContent);
                    for (int col = 0; col < numberOfCells; col++) {
                        if (subrow == cellRows / 2) {
                            sb.append(generateRowForCell(grid[row][col].getValue()));
                        } else {
                            if (subrow == 0 || subrow == numberOfCells) sb.append(repeat(rowDelimeterSymbol, cellCols + 2));
                            else sb.append(generateRowForCell(""));
                        }
                    }
                    sb.append(rowEndContent);
                    sb.append("\n");
                }
            }

            sb.append(rowInitialContent).append(repeat(boardDelimeterSymbol, boardCols)).append(rowEndContent).append("\n");

            return sb.toString();
        }

        public void disable() {
            this.enabled = false;
        }

        private String generateRowForCell(String value) {
            String returnValue = cellEmptyRow;

            if (value != null) {
                int freeSpace = cellEmptyRow.length() - value.length();
                int leftFreeSpace = freeSpace / 2;
                int rightFreeSpace = freeSpace - leftFreeSpace;

                returnValue = freeSpace(leftFreeSpace) + value + freeSpace(rightFreeSpace);
            }

            return colDelimeterSymbol + returnValue + colDelimeterSymbol;
        }

        private String generateRowForCell(Integer number) {
            if (number == null) return generateRowForCell("");
            return generateRowForCell(number.toString());
        }

        private String repeat(char c, int times) {
            return new String(new char[times]).replace('\0', c);
        }

        private String freeSpace(int times) {
            return repeat(' ', times);
        }
    }
}
