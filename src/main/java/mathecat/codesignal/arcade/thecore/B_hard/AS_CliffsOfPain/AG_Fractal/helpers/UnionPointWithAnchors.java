package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.ArrayList;
import java.util.List;

class UnionPointWithAnchors {
    private final Point unionLocation;
    private final Character unionSymbol;
    private final List<Anchor> anchors;
    private final FractalModelElement fractalModelElement;

    public UnionPointWithAnchors(Point unionLocation, Character unionSymbol) {
        this.unionLocation = unionLocation;
        this.unionSymbol = unionSymbol;
        this.fractalModelElement = new FractalModelElement(unionSymbol, unionLocation);
        this.anchors = new ArrayList<>();
    }

    public void addAnchor(Point p, List<Character> validCharacters) {
        this.anchors.add(new Anchor(p, this, validCharacters));
    }

    public Point getUnionLocation() {
        return unionLocation;
    }

    public List<Anchor> getAnchors() {
        return anchors;
    }

    public Character getUnionSymbol() {
        return unionSymbol;
    }

    public FractalModelElement getFractalModelElement() {
        return fractalModelElement;
    }
}
