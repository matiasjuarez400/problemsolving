package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.Arrays;
import java.util.List;

class CharManager {
    private static final char MODEL_HORIZONTAL = '_';
    private static final char MODEL_VERTICAL = '|';
    private static final char MODEL_EMPTY = '.';
    private static final char MODEL_EXPANSION = '°';
    private static final List<Character> CONNECTION_CHARS = Arrays.asList(MODEL_HORIZONTAL, MODEL_VERTICAL);

    public static List<Character> getConnectionChars() {
        return CONNECTION_CHARS;
    }

    public static char getModelHorizontal() {
        return MODEL_HORIZONTAL;
    }

    public static char getModelVertical() {
        return MODEL_VERTICAL;
    }

    public static char getModelEmpty() {
        return MODEL_EMPTY;
    }

    public static char getModelExpansion() {
        return MODEL_EXPANSION;
    }

    public static boolean isConnectorChar(Character c) {
        return c != null && (c == MODEL_HORIZONTAL || c == MODEL_VERTICAL);
    }

    public static boolean isValidChar(char c) {
        return isConnectorChar(c) || isEmpty(c);
    }

    public  static boolean isVertical(char c) {
        return c == MODEL_VERTICAL;
    }

    public  static boolean isHorizontal(char c) {
        return c == MODEL_HORIZONTAL;
    }

    public static boolean isEmpty(char c) {
        return !isConnectorChar(c);
    }
}
