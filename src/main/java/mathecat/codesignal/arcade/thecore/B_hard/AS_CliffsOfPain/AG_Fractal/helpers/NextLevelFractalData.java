package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class NextLevelFractalData {
    private final List<BridgeInformation> bridgeInformationList;
    private final List<Bridge> usedBridges;
    private FractalUnionPointsInformationRelative topLeft;
    private FractalUnionPointsInformationRelative topRight;
    private FractalUnionPointsInformationRelative downLeft;
    private FractalUnionPointsInformationRelative downRight;

    public NextLevelFractalData(List<BridgeInformation> bridgeInformationList, List<Bridge> usedBridges) {
        this.bridgeInformationList = bridgeInformationList;
        this.usedBridges = usedBridges;
        assignFractalUnionPointInformationRelativeFields();
    }

    private void assignFractalUnionPointInformationRelativeFields() {
        final List<FractalUnionPointsInformationRelative> allFractalUnionPointsInformationRelative = this.bridgeInformationList.stream()
                .map(bridgeInformation -> Arrays.asList(bridgeInformation.getFirstFractal(), bridgeInformation.getSecondFractal()))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        final Set<String> seen = new HashSet<>();
        final List<FractalUnionPointsInformationRelative> uniqueFractalUnionPointsInformationRelative = allFractalUnionPointsInformationRelative
                .stream()
                .filter(fractalUnionPointInformationRelative -> {
                    String id = fractalUnionPointInformationRelative.getGlobalRelativePosition().toString();
                    return seen.add(id);
                }).collect(Collectors.toList());

        final Map<Boolean, List<FractalUnionPointsInformationRelative>> fractalUnionPointInformationRelativeInTopRow =
                uniqueFractalUnionPointsInformationRelative.stream()
                .collect(Collectors.partitioningBy(fractalUnionPointInformationRelative ->
                        fractalUnionPointInformationRelative.getGlobalRelativePosition().getRow() == 0));

        final Comparator<FractalUnionPointsInformationRelative> comparatorByColumn =
                Comparator.comparing(fractalUnionPointInformationRelative -> fractalUnionPointInformationRelative.getGlobalRelativePosition().getCol());

        fractalUnionPointInformationRelativeInTopRow.get(true).sort(comparatorByColumn);
        fractalUnionPointInformationRelativeInTopRow.get(false).sort(comparatorByColumn);

        this.topLeft = fractalUnionPointInformationRelativeInTopRow.get(true).get(0);
        this.topRight = fractalUnionPointInformationRelativeInTopRow.get(true).get(1);
        this.downLeft = fractalUnionPointInformationRelativeInTopRow.get(false).get(0);
        this.downRight = fractalUnionPointInformationRelativeInTopRow.get(false).get(1);
    }

    public List<BridgeInformation> getBridgeInformationList() {
        return bridgeInformationList;
    }

    public FractalUnionPointsInformationRelative getTopLeft() {
        return topLeft;
    }

    public FractalUnionPointsInformationRelative getTopRight() {
        return topRight;
    }

    public FractalUnionPointsInformationRelative getDownLeft() {
        return downLeft;
    }

    public FractalUnionPointsInformationRelative getDownRight() {
        return downRight;
    }

    public List<Bridge> getUsedBridges() {
        return usedBridges;
    }
}
