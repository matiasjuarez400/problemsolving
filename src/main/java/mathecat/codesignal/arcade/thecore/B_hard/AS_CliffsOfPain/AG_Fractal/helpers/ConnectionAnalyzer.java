package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.List;

class ConnectionAnalyzer {
    private static ConnectionAnalyzer instance;

    public static ConnectionAnalyzer getInstance() {
        if (instance == null) {
            instance = new ConnectionAnalyzer();
        }

        return instance;
    }

    public boolean canConnectWithElement(FractalModelElementWithSurroundings base, FractalModelElementWithSurroundings target) {
        return canConnectWithElement(base, target.getCentralElement()) && canConnectWithElement(target, base.getCentralElement());
    }

    public boolean areBlocking(FractalModelElement base, FractalModelElement target) {
        if (base == null || target == null) return false;

        FractalModelElement horizontalElement = null;
        FractalModelElement verticalElement = null;

        if (base.getSymbol() == CharManager.getModelHorizontal()) {
            horizontalElement = base;
        } else if (base.getSymbol() == CharManager.getModelVertical()) {
            verticalElement = base;
        }

        if (target.getSymbol() == CharManager.getModelHorizontal()) {
            horizontalElement = target;
        } else if (target.getSymbol() == CharManager.getModelVertical()) {
            verticalElement = target;
        }

        if (horizontalElement == null || verticalElement == null) return false;

        return isVerticallyAligned(horizontalElement, verticalElement) && (isUp(verticalElement, horizontalElement));
    }

    public boolean canConnectWithElement(FractalModelElementWithSurroundings base, FractalModelElement target) {
        if (base == null) throw new IllegalStateException("base FractalElementModelWithSurroundings should never be null");
        if (target == null) return false;

        FractalModelElement baseFractalModelElement = base.getCentralElement();

        if (baseFractalModelElement == null) {
            throw new IllegalStateException("baseFractalModelElement should never be null");
        }

        if (!canConnectWithElement(target, baseFractalModelElement)) return false;

        if (isHorizontal(baseFractalModelElement)) {
            if (isRight(baseFractalModelElement, target)) {
                List<FractalModelElement> rightColumnElements = base.getRightColumnElements();
                return !canBeConnectedWithAnyElement(baseFractalModelElement, rightColumnElements);
            } else if (isLeft(baseFractalModelElement, target)) {
                List<FractalModelElement> leftColumnElements = base.getLeftColumnElements();
                return !canBeConnectedWithAnyElement(baseFractalModelElement, leftColumnElements);
            }
        } else if (isVertical(baseFractalModelElement)) {
            if (isUp(baseFractalModelElement, target)) {
                List<FractalModelElement> topRowElements = base.getTopRowElements();
                return !canBeConnectedWithAnyElement(baseFractalModelElement, topRowElements);
            } else {
                List<FractalModelElement> downRowElements = base.getDownRowElements();
                downRowElements.addAll(base.getMiddleRowElements());
                return !canBeConnectedWithAnyElement(baseFractalModelElement, downRowElements);
            }
        }

        return false;
    }

    public boolean canConnectWithElement(FractalModelElement base, FractalModelElement target) {
        if (base == null || target == null) return false;
        if (isEmpty(base) || isEmpty(target)) return false;

        if (isHorizontal(base)) {
            if (isHorizontallyAround(base, target)) {
                if (isHorizontallyAligned(base, target)) {
                    return true;
                } else if (isDown(base, target)) {
                    return isVertical(target);
                }
            }
            return false;
        } else if (isVertical(base)) {
            if (isUp(base, target)) {
                if (isVerticallyAligned(base, target)) {
                    return isVertical(target);
                } else if (isHorizontallyAround(base, target)) {
                    return isHorizontal(target);
                }
            } else if (isHorizontallyAligned(base, target)) {
                if (isHorizontallyAround(base, target)) {
                    return isHorizontal(target);
                }
            } else if (isDown(base, target)) {
                if (isVerticallyAligned(base, target)) {
                    return isVertical(target);
                }
            }
        }

        return false;
    }

    public int countConnectionsAroundPoint(FractalModelElement base, char[][] fractalModel) {
        final int baseRow = base.getRow();
        final int baseCol = base.getCol();

        int connectionsFound = 0;
        for (int row = baseRow - 1; row <= baseRow + 1; row++) {
            if (row < 0 || row > fractalModel.length - 1) continue;
            for (int col = baseCol - 1; col <= baseCol + 1; col++) {
                if (col < 0 || col > fractalModel[row].length - 1) continue;
                if (col == baseCol && row == baseRow) continue;

                char currentChar = fractalModel[row][col];
                if (!CharManager.isConnectorChar(currentChar)) continue;

                FractalModelElement target = new FractalModelElement(currentChar, col, row);

                if (canConnectWithElement(base, target)) {
                    connectionsFound++;
                }
            }
        }

        return connectionsFound;
    }

    public int countConnectionsAroundPoint(FractalModelElement base, FractalModel fractalModel) {
        return countConnectionsAroundPoint(base, fractalModel.getModel());
    }

    private boolean canBeConnectedWithAnyElement(FractalModelElement base, List<FractalModelElement> targets) {
        return targets.stream().anyMatch(target -> canConnectWithElement(base, target));
    }

    private boolean isHorizontallyAround(FractalModelElement base, FractalModelElement target) {
        return isLeft(base, target) || isRight(base, target);
    }

    private boolean isEmpty(FractalModelElement fractalModelElement) {
        return !isVertical(fractalModelElement) && !isHorizontal(fractalModelElement);
    }

    private boolean isVertical(FractalModelElement fractalModelElement) {
        return CharManager.isVertical(fractalModelElement.getSymbol());
    }

    private boolean isHorizontal(FractalModelElement fractalModelElement) {
        return CharManager.isHorizontal(fractalModelElement.getSymbol());
    }

    private boolean isLeft(FractalModelElement base, FractalModelElement target) {
        return target.getCol() == base.getCol() - 1;
    }

    private boolean isRight(FractalModelElement base, FractalModelElement target) {
        return target.getCol() == base.getCol() + 1;
    }

    private boolean isUp(FractalModelElement base, FractalModelElement target) {
        return target.getRow() == base.getRow() - 1;
    }

    private boolean isDown(FractalModelElement base, FractalModelElement target) {
        return target.getRow() == base.getRow() + 1;
    }

    private boolean isVerticallyAligned(FractalModelElement base, FractalModelElement target) {
        return base.getCol() == target.getCol();
    }

    private boolean isHorizontallyAligned(FractalModelElement base, FractalModelElement target) {
        return base.getRow() == target.getRow();
    }
}

