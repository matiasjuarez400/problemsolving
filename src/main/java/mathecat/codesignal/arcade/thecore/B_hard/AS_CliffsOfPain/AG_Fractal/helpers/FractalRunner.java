package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

public class FractalRunner {
    public static void main(String[] args) {
        FractalRunner fractalRunner = new FractalRunner();

        int desiredLevel = 4;
        char[][] result = fractalRunner.fractal(desiredLevel);

        System.out.println(FractalModelPrinter.printModelRepresentation(result));
    }

    char[][] fractal(int n) {
        long startTime = System.currentTimeMillis();

        ConnectionAnalyzer connectionAnalyzer = new ConnectionAnalyzer();
        FractalUnionsFinder fractalUnionsFinder = new FractalUnionsFinder(connectionAnalyzer);
        FractalJoiner fractalJoiner = new FractalJoiner(connectionAnalyzer);
        NextLevelFractalJoiner nextLevelFractalJoiner = new NextLevelFractalJoiner(fractalJoiner);

        FractalExpansionLogic fractalExpansionLogic = new FractalExpansionLogic(fractalUnionsFinder, nextLevelFractalJoiner);

        char[][] matrix = getFractal1();

        FractalModel fractalModel = new FractalModel(matrix);
        int currentLevel = 1;
        while (currentLevel < n) {
            fractalModel = fractalExpansionLogic.expand(fractalModel);
            System.out.println(fractalModel);
            currentLevel++;
        }

        long endTime = System.currentTimeMillis();

        System.out.println("Total time: " + (endTime - startTime));

        return fractalModel.getModel();
    }

    private static char[][] getFractal1() {
        return new char[][]{
                {' ', '_', ' '},       
                {' ', '_', '|'}
        };
    }

    private static char[][] getFractal2() {
        return new char[][]{
                {'.', '_', '.', '.', '.', '_', '.'},
                {'.', '_', '|', '.', '|', '_', '.'},
                {'|', '.', '.', '_', '.', '.', '|'},
                {'|', '_', '|', '.', '|', '_', '|'}
        };
    }
}
