package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AC_SnakeGame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        char[][] result = solution.example1();

        for (char[] row : result) {
            for (int col = 0; col < row.length; col++) {
                System.out.print(row[col] + " ");
            }
            System.out.println();
        }
    }

    char[][] example1() {
        char[][] gameBoard = {
                {'.','.','.','.'}, 
                 {'.','.','<','*'}, 
                 {'.','.','.','*'}
        };

        String commands = "R";

        return snakeGame(gameBoard, commands);
    }

    char[][] example2() {
        char[][] gameBoard = {
                {'.', '.', '^', '.', '.'},
                {'.', '.', '*', '*', '.'},
                {'.', '.', '.', '*', '*'}
        };

        String commands = "RFRF";

        return snakeGame(gameBoard, commands);
    }

    char[][] snakeGame(char[][] gameBoard, String commands) {
        InputProcessor inputProcessor = new InputProcessor();

        Snake snake = inputProcessor.readGameBoard(gameBoard);
        InputProcessor.CommandReadOutput commandReadOutput = inputProcessor.readCommands(commands, snake.getLastDirection());

        System.out.println(snake);

        for (Direction direction : commandReadOutput.getDirections()) {
            if (!snake.move(direction)) break;

            System.out.println(snake);
        }

        if (commandReadOutput.getUselessRotation() != null) {
            snake.setLastDirection(commandReadOutput.getUselessRotation());
        }

        for (char[] row : gameBoard) {
            Arrays.fill(row, '.');
        }

        for (Point bodyPart : snake.getSnakeBody()) {
            if (snake.isDead()) {
                gameBoard[bodyPart.getRow()][bodyPart.getCol()] = 'X';
            } else {
                if (bodyPart.equals(snake.getSnakeBody().get(0))) {
                    gameBoard[bodyPart.getRow()][bodyPart.getCol()] = InputProcessor.getHeadSymbol(snake.getLastDirection());
                } else {
                    gameBoard[bodyPart.getRow()][bodyPart.getCol()] = '*';
                }
            }
        }

        return gameBoard;
    }

    private static class InputProcessor {
        private static Map<Direction, Character> symbolByDirection;
        private static Map<Character, Direction> directionBySymbol;

        public InputProcessor() {
            initializeMaps();
        }

        public CommandReadOutput readCommands(String commands, final Direction initialDirection) {
            Direction currentDirection = initialDirection;

            List<Direction> directions = new ArrayList<>();

            boolean uselessDirection = false;
            for (char c : commands.toCharArray()) {
                switch (c) {
                    case 'F': {
                        directions.add(currentDirection);
                        uselessDirection = false;
                        break;
                    }
                    case 'L': {
                        currentDirection = rotateDirection(currentDirection, Direction.LEFT);
                        uselessDirection = true;
                        break;
                    }
                    case 'R': {
                        currentDirection = rotateDirection(currentDirection, Direction.RIGHT);
                        uselessDirection = true;
                        break;
                    }
                    default: throw new IllegalArgumentException("Invalid command: " + c);
                }
            }

            CommandReadOutput commandReadOutput = new CommandReadOutput(directions, uselessDirection ? currentDirection : null);

            return commandReadOutput;
        }

        private class CommandReadOutput {
            private List<Direction> directions;
            private Direction uselessRotation;

            public CommandReadOutput(List<Direction> directions, Direction uselessRotation) {
                this.directions = directions;
                this.uselessRotation = uselessRotation;
            }

            public List<Direction> getDirections() {
                return directions;
            }

            public Direction getUselessRotation() {
                return uselessRotation;
            }
        }

        public Snake readGameBoard(char[][] gameBoard) {
            List<Point> bodyPartsFound = new ArrayList<>();
            Point head = null;
            Direction headDirection = null;

            for (int row = 0; row < gameBoard.length; row++) {
                for (int col = 0; col < gameBoard[row].length; col++) {
                    char current = gameBoard[row][col];

                    if (current == '*') bodyPartsFound.add(new Point(row, col));
                    else if (directionBySymbol.containsKey(current)) {
                        head = new Point(row, col);
                        headDirection = directionBySymbol.get(current);
                    }
                }
            }

            if (head == null || headDirection == null) {
                throw new IllegalStateException("Unable to find snake head");
            }

            Snake snake = new Snake(headDirection, gameBoard.length - 1, gameBoard[0].length - 1);
            snake.addBodyPart(head);

            if (!bodyPartsFound.isEmpty()) {
                Direction searchDirection = invertDirection(headDirection);
                Point nextBodyPart = head.move(searchDirection);

                snake.addBodyPart(nextBodyPart);

                if (!bodyPartsFound.remove(nextBodyPart)) {
                    throw new IllegalStateException("Added body part to snake that doesn't exist");
                }

                while (!bodyPartsFound.isEmpty()) {
                    if (bodyPartsFound.size() > 1) {
                        nextBodyPart = lookAround(nextBodyPart, bodyPartsFound);
                        snake.addBodyPart(nextBodyPart);
                        if (!bodyPartsFound.remove(nextBodyPart)) {
                            throw new IllegalStateException("Added body part to snake that doesn't exist");
                        }
                    } else {
                        snake.addBodyPart(bodyPartsFound.remove(0));
                    }
                }
            }

            return snake;
        }

        private Point lookAround(Point basePoint, List<Point> bodyParts) {
            Direction searchDirection = symbolByDirection.keySet().stream()
                    .filter(direction -> bodyParts.contains(basePoint.move(direction)))
                    .findFirst().orElseThrow(() -> new IllegalStateException("Unable to find next body part"));

            return basePoint.move(searchDirection);
        }

        private static void initializeMaps() {
            symbolByDirection = new HashMap<>();
            symbolByDirection.put(Direction.RIGHT, '>');
            symbolByDirection.put(Direction.LEFT, '<');
            symbolByDirection.put(Direction.UP, '^');
            symbolByDirection.put(Direction.DOWN, 'v');

            directionBySymbol = new HashMap<>();
            symbolByDirection.forEach((k, v) -> {
                directionBySymbol.put(v, k);
            });
        }

        public static char getHeadSymbol(Direction direction) {
            return symbolByDirection.get(direction);
        }

        public static Direction getHeadDirection(char head) {
            return directionBySymbol.get(head);
        }

        public Direction invertDirection(Direction direction) {
            switch (direction) {
                case DOWN: return Direction.UP;
                case UP: return Direction.DOWN;
                case LEFT: return Direction.RIGHT;
                case RIGHT: return Direction.LEFT;
                default: throw new IllegalArgumentException("BLA");
            }
        }

        public Direction rotateDirection(Direction current, Direction rotation) {
            if (Stream.of(Direction.LEFT, Direction.RIGHT).noneMatch(d -> d == rotation)) {
                throw new IllegalArgumentException("rotation can only be left or right");
            }

            switch (current) {
                case LEFT: {
                    if (rotation == Direction.LEFT) return Direction.DOWN;
                    if (rotation == Direction.RIGHT) return Direction.UP;
                }
                case UP: {
                    if (rotation == Direction.LEFT) return Direction.LEFT;
                    if (rotation == Direction.RIGHT) return Direction.RIGHT;
                }
                case RIGHT: {
                    if (rotation == Direction.LEFT) return Direction.UP;
                    if (rotation == Direction.RIGHT) return Direction.DOWN;
                }
                case DOWN: {
                    if (rotation == Direction.LEFT) return Direction.RIGHT;
                    if (rotation == Direction.RIGHT) return Direction.LEFT;
                }
                default: throw new IllegalArgumentException("BLA");
            }
        }
    }

    private static class Snake {
        private List<Point> snakeBody;
        private Direction lastDirection;
        private final int rowLimit;
        private final int colLimit;
        private boolean isDead = false;

        public Snake(Direction direction, int rowLimit, int colLimit) {
            this.snakeBody = new ArrayList<>();
            this.lastDirection = direction;
            this.rowLimit = rowLimit;
            this.colLimit = colLimit;
        }

        public void setLastDirection(Direction lastDirection) {
            this.lastDirection = lastDirection;
        }

        public void addBodyPart(Point bodyPart) {
            this.snakeBody.add(bodyPart);
        }

        public List<Point> getSnakeBody() {
            return snakeBody;
        }

        public Direction getLastDirection() {
            return lastDirection;
        }

        private boolean collisionAfterMove(Direction direction) {
            Point head = this.snakeBody.get(0);

            Point headAfter = head.move(direction);

            if (headAfter.getRow() < 0 || headAfter.getRow() > rowLimit) return true;
            if (headAfter.getCol() < 0 || headAfter.getCol() > colLimit) return true;

            return this.snakeBody.contains(headAfter);
        }

        public boolean move(final Direction direction) {
            if (collisionAfterMove(direction) || isDead) {
                isDead = true;
                return false;
            }

            this.lastDirection = direction;

            List<Point> newBodySetup = new ArrayList<>();
            newBodySetup.add(this.snakeBody.get(0).move(direction));

            for (int i = 0; i < this.snakeBody.size() - 1; i++) {
                newBodySetup.add(this.snakeBody.get(i));
            }

            this.snakeBody = newBodySetup;

            return true;
        }

        public boolean isDead() {
            return isDead;
        }

        @Override
        public String toString() {
            int snakeSize = snakeBody.size();
            int minRow = getSnakeBody().stream().mapToInt(Point::getRow).min().orElseThrow(() -> new IllegalStateException("BLA"));
            int minCol = getSnakeBody().stream().mapToInt(Point::getCol).min().orElseThrow(() -> new IllegalStateException("BLA"));

            char[][] snakeMatrix = new char[snakeSize][snakeSize];

            for (Point bodyPart : snakeBody) {
                char symbol = '+';
                if (bodyPart.equals(snakeBody.get(0))) {
                    symbol = InputProcessor.getHeadSymbol(this.lastDirection);
                }
                snakeMatrix[bodyPart.getRow() - minRow][bodyPart.getCol() - minCol] = symbol;
            }

//            Point head = snakeBody.get(0);
//            snakeMatrix[head.getRow()][head.getCol()] = InputProcessor.getHeadSymbol(this.lastDirection);

            StringBuilder output = new StringBuilder();

            for (char[] row : snakeMatrix) {
                for (int col = 0; col < row.length; col++) {
                    if (row[col] == '\0') output.append(" ");
                    else output.append(row[col]);
                }
                output.append("\n");
            }

            output.append("**************************\n");

            return output.toString();
        }


    }

    private enum Direction {
        UP, DOWN, RIGHT, LEFT;
    }

    private static class Point {
        private final int row;
        private final int col;

        public Point(int row, int col) {
            this.row = row;
            this.col = col;
        }

        public int getRow() {
            return row;
        }

        public int getCol() {
            return col;
        }

        public Point move(Direction direction) {
            switch (direction) {
                case UP: return new Point(row - 1, col);
                case DOWN: return new Point(row + 1, col);
                case LEFT: return new Point(row, col - 1);
                case RIGHT: return new Point(row, col + 1);
                default: throw new IllegalArgumentException("BLA");
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Point point = (Point) o;
            return row == point.row &&
                    col == point.col;
        }

        @Override
        public int hashCode() {
            return Objects.hash(row, col);
        }
    }
}
