package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AE_PyraminxPuzzle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.test5();
    }

    public void test1() {
        char[] faceColors = {'R', 'G', 'Y', 'O'};
        String[] moves = {"B", "b'", "u'", "R"};

        char[][] output = pyraminxPuzzle(faceColors, moves);

        for (char[] face : output) {
            System.out.println(Arrays.toString(face));
        }
    }

    public void test5() {
        char[] faceColors = {'A', 'B', 'C', 'D'};
        String[] moves = {"l",
                "l'",
                "r'",
                "r",
                "u",
                "U",
                "u'",
                "R'",
                "L",
                "R",
                "L'",
                "B'",
                "U'",
                "b",
                "B",
                "b'"};

        char[][] output = pyraminxPuzzle(faceColors, moves);

        for (char[] face : output) {
            System.out.println(Arrays.toString(face));
        }
    }

    char[][] pyraminxPuzzle(char[] faceColors, String[] moves) {
        InputParser inputParser = new InputParser();
        MoveInverter moveInverter = new MoveInverter();

        Pyraminx pyraminx = new Pyraminx(inputParser.convertToColors(faceColors));

        List<MoveDescription> moveDescriptions = Arrays.stream(moves)
                .map(inputParser::convertToMoveDescription)
                .collect(Collectors.toList());

        List<MoveDescription> invertedMoveDescriptions = moveInverter.invert(moveDescriptions);

        System.out.println(pyraminx);

        for (MoveDescription invertedMoveDescription : invertedMoveDescriptions) {
            pyraminx.handleMove(invertedMoveDescription);

            System.out.println(invertedMoveDescription);
            System.out.println(pyraminx);
        }

        return pyraminx.getFacesConfigurations();
    }

    private class MoveInverter {
        public MoveDescription invert(MoveDescription moveDescription) {
            return new MoveDescription(moveDescription.getMove(),
                    moveDescription.getDirection() == Direction.CLOCKWISE ? Direction.COUNTERCLOCKWISE : Direction.CLOCKWISE
                    );
        }

        public List<MoveDescription> invert(List<MoveDescription> moveDescriptions) {
            List<MoveDescription> inverted = new ArrayList<>();
            for (int i = moveDescriptions.size() - 1; i >= 0; i--) {
                inverted.add(invert(moveDescriptions.get(i)));
            }
            return inverted;
        }
    }

    private class MoveDescription {
        private final Move move;
        private final Direction direction;

        public MoveDescription(Move move, Direction direction) {
            this.move = move;
            this.direction = direction;
        }

        public Move getMove() {
            return move;
        }

        public Direction getDirection() {
            return direction;
        }

        @Override
        public String toString() {
            return "MoveDescription{" +
                    "move=" + move +
                    ", direction=" + direction +
                    '}';
        }
    }

    private class InputParser {
        public MoveDescription convertToMoveDescription(String input) {
            return new MoveDescription(
                convertToMove(input),
                convertToDirection(input)
            );
        }

        public Move convertToMove(String input) {
            input = Character.toString(input.charAt(0));
            switch (input) {
                case "u": return Move.u;
                case "U": return Move.U;
                case "r": return Move.r;
                case "R": return Move.R;
                case "l": return Move.l;
                case "L": return Move.L;
                case "b": return Move.b;
                case "B": return Move.B;
                default: throw new IllegalArgumentException("Unsupported move: " + input);
            }
        }

        public Direction convertToDirection(String input) {
            if (input.contains("'")) return Direction.CLOCKWISE;
            return Direction.COUNTERCLOCKWISE;
        }

        public List<Color> convertToColors(char[] faceColors) {
            List<Color> colors = new ArrayList<>();

            for (char color : faceColors) {
                colors.add(new Color(color));
            }

            return colors;
        }
    }

    private class Pyraminx {
        private Face front;
        private Face left;
        private Face right;
        private Face bottom;
        private List<Face> U;
        private List<Face> L;
        private List<Face> R;
        private List<Face> B;
        private Map<Move, List<Face>> singleLayerFacesByMove;
        private Map<Move, List<Face>> doubleLayerFacesByMove;

        public Pyraminx(List<Color> faceColors) {
            this.front = new Face(faceColors.get(0), FacePosition.FRONT);
            this.bottom = new Face(faceColors.get(1), FacePosition.BOTTOM);
            this.left = new Face(faceColors.get(2), FacePosition.LEFT);
            this.right = new Face(faceColors.get(3), FacePosition.RIGHT);

            this.U = Arrays.asList(left, front, right);
            this.L = Arrays.asList(bottom, front, left);
            this.R = Arrays.asList(right, front, bottom);
            this.B = Arrays.asList(right, bottom, left);

            this.singleLayerFacesByMove = new HashMap<>();
            this.singleLayerFacesByMove.put(Move.U, U);
            this.singleLayerFacesByMove.put(Move.L, L);
            this.singleLayerFacesByMove.put(Move.R, R);
            this.singleLayerFacesByMove.put(Move.B, B);

            this.doubleLayerFacesByMove = new HashMap<>();
            this.doubleLayerFacesByMove.put(Move.u, U);
            this.doubleLayerFacesByMove.put(Move.l, L);
            this.doubleLayerFacesByMove.put(Move.r, R);
            this.doubleLayerFacesByMove.put(Move.b, B);
        }

        public void handleMove(MoveDescription moveDescription) {
            handleMove(moveDescription.getMove(), moveDescription.getDirection());
        }

        private FacePerspective createFacePerspective(Face face, Move move) {
            switch (move) {
                case U:
                case u: {
                    if (face.getFacePosition() == FacePosition.BOTTOM) throw new IllegalStateException("Bottom face is not related with U edge");
                    return new FacePerspective(Perspective.FRONT, face);
                }
                case L:
                case l: {
                    if (face.getFacePosition() == FacePosition.RIGHT) throw new IllegalStateException("Right face is not related with L edge");
                    switch (face.getFacePosition()) {
                        case FRONT: return new FacePerspective(Perspective.LEFT, face);
                        case LEFT: return new FacePerspective(Perspective.RIGHT, face);
                        case BOTTOM: return new FacePerspective(Perspective.RIGHT, face);

                    }
                    return new FacePerspective(Perspective.FRONT, face);
                }
                case R:
                case r: {
                    if (face.getFacePosition() == FacePosition.LEFT) throw new IllegalStateException("Left face is not related with R edge");
                    switch (face.getFacePosition()) {
                        case FRONT: return new FacePerspective(Perspective.RIGHT, face);
                        case RIGHT: return new FacePerspective(Perspective.LEFT, face);
                        case BOTTOM: return new FacePerspective(Perspective.LEFT, face);

                    }
                    return new FacePerspective(Perspective.FRONT, face);
                }
                case B:
                case b: {
                    if (face.getFacePosition() == FacePosition.FRONT) throw new IllegalStateException("Front face is not related with B edge");
                    switch (face.getFacePosition()) {
                        case BOTTOM: return new FacePerspective(Perspective.FRONT, face);
                        case RIGHT: return new FacePerspective(Perspective.RIGHT, face);
                        case LEFT: return new FacePerspective(Perspective.LEFT, face);
                    }
                    return new FacePerspective(Perspective.FRONT, face);
                }
                default: throw new IllegalArgumentException("Move direction is required");
            }
        }

        public void handleMove(Move move, Direction direction) {
            int layers;
            List<Face> faces;
            if (singleLayerFacesByMove.containsKey(move)) {
                faces = singleLayerFacesByMove.get(move);
                layers = 1;
            } else {
                faces = doubleLayerFacesByMove.get(move);
                layers = 2;
            }

            FacePerspective leftPerspective = createFacePerspective(faces.get(0), move);
            FacePerspective frontPerspective = createFacePerspective(faces.get(1), move);
            FacePerspective rightPerspective = createFacePerspective(faces.get(2), move);

            if (direction == Direction.CLOCKWISE) {
                leftPerspective.replaceTopRow(frontPerspective.getTopRow());
                frontPerspective.replaceTopRow(rightPerspective.getTopRow());
                rightPerspective.replaceTopRow(leftPerspective.getTopRow());
            } else {
                rightPerspective.replaceTopRow(frontPerspective.getTopRow());
                leftPerspective.replaceTopRow(rightPerspective.getTopRow());
                frontPerspective.replaceTopRow(leftPerspective.getTopRow());
            }

            if (layers == 2) {
                if (direction == Direction.CLOCKWISE) {
                    leftPerspective.replaceMiddleRow(frontPerspective.getMiddleRow());
                    frontPerspective.replaceMiddleRow(rightPerspective.getMiddleRow());
                    rightPerspective.replaceMiddleRow(leftPerspective.getMiddleRow());
                } else {
                    rightPerspective.replaceMiddleRow(frontPerspective.getMiddleRow());
                    leftPerspective.replaceMiddleRow(rightPerspective.getMiddleRow());
                    frontPerspective.replaceMiddleRow(leftPerspective.getMiddleRow());
                }
            }

            leftPerspective.applyNewColors();
            frontPerspective.applyNewColors();
            rightPerspective.applyNewColors();
        }

        public char[][] getFacesConfigurations() {
            char[][] output = {
                    getPerspectiveConfiguration(front, Perspective.FRONT),
                    getPerspectiveConfiguration(bottom, Perspective.FRONT),
                    getPerspectiveConfiguration(left, Perspective.RIGHT),
                    getPerspectiveConfiguration(right, Perspective.LEFT)
            };

            return output;
        }

        private char[] getPerspectiveConfiguration(Face face, Perspective perspective) {
            FacePerspective facePerspective = new FacePerspective(perspective, face);
            return getPerspectiveConfiguration(facePerspective);
        }

        private char[] getPerspectiveConfiguration(FacePerspective facePerspective) {
            List<Triangle> configuration = new ArrayList<>();
            configuration.addAll(facePerspective.getTopRow());
            configuration.addAll(facePerspective.getMiddleRow());
            configuration.addAll(facePerspective.getBottomRow());

            char[] output = new char[configuration.size()];
            for (int i = 0; i < configuration.size(); i++) {
                output[i] = configuration.get(i).getColor().getSymbol().charAt(0);
            }

            return output;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder()
                    .append(left)
                    .append(front)
                    .append(right)
                    .append(bottom);

            return sb.toString();
        }
    }

    private enum Move {
        L, l, U, u, R, r, B, b
    }

    private enum Direction {
        CLOCKWISE, COUNTERCLOCKWISE
    }

    private enum FacePosition {
        FRONT, LEFT, RIGHT, BOTTOM
    }

    private class Face {
        private List<Triangle> topRow;
        private List<Triangle> middleRow;
        private List<Triangle> bottomRow;
        private FacePosition facePosition;

        public Face(Color faceColor, FacePosition facePosition) {
            this.facePosition = facePosition;
            topRow = createRowOfColor(1, faceColor);
            middleRow = createRowOfColor(3, faceColor);
            bottomRow = createRowOfColor(5, faceColor);
        }

        private List<Triangle> createRowOfColor(int size, Color color) {
            List<Triangle> list = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                list.add(new Triangle(color));
            }
            return list;
        }

        public List<Triangle> getTopRow() {
            return topRow;
        }

        public List<Triangle> getMiddleRow() {
            return middleRow;
        }

        public List<Triangle> getBottomRow() {
            return bottomRow;
        }

        public FacePosition getFacePosition() {
            return facePosition;
        }

        public Triangle getLastFromRow(List<Triangle> triangles) {
            return triangles.get(triangles.size() - 1);
        }

        @Override
        public String toString() {
            char separator = '_';
            StringBuilder sb = new StringBuilder();
            sb.append(repeat(separator, 4)).append(topRow.get(0).getColor().getSymbol()).append(repeat(separator, 3)).append("\n");

            sb.append(repeat(separator, 2));
            for (Triangle triangle : middleRow) {
                sb.append(triangle.getColor().getSymbol()).append(repeat(separator, 1));
            }
            sb.append(repeat(separator, 1)).append("\n");

            sb.append(repeat(separator, 0));
            for (Triangle triangle : bottomRow) {
                sb.append(triangle.getColor().getSymbol()).append(repeat(separator, 1));
            }
            sb.append("\n");

            return sb.toString();
        }

        private String repeat(char c, int times) {
            return new String(new char[times]).replace('\0', c);
        }
    }

    /**
     * At the beginning all faces are seen as standing in front of them.
     * FacePerspective allows you to see the same face as if it was rotated.
     * For example, if you have the Front face, in the beginning the U edge will be Up and the R edge will be at the right.
     * If you create a RIGHT perspective of this face, the R edge will be Up and the U edge will be at the left.
     */
    private class FacePerspective {
        private Perspective perspective;
        private List<Triangle> topRow;
        private List<Triangle> middleRow;
        private List<Triangle> bottomRow;

        public FacePerspective(Perspective perspective, Face face) {
            this.perspective = perspective;

            if (perspective == Perspective.FRONT) {
                initializeFrontPerspective(face);
            } else if (perspective == Perspective.LEFT) {
                initializeLeftPerspective(face);
            } else if (perspective == Perspective.RIGHT) {
                initializeRightPerspective(face);
            } else {
                throw new IllegalArgumentException("Invalid perspective: " + perspective);
            }
        }

        private void initializeFrontPerspective(Face face) {
            this.topRow = face.getTopRow();
            this.middleRow = face.getMiddleRow();
            this.bottomRow = face.getBottomRow();
        }

        private void initializeRightPerspective(Face face) {
            this.topRow = new ArrayList<>();
            this.topRow.add(face.getLastFromRow(face.getBottomRow()));

            this.middleRow = new ArrayList<>();
            this.middleRow.add(face.getLastFromRow(face.getMiddleRow()));
            this.middleRow.add(face.getBottomRow().get(face.getBottomRow().size() - 2));
            this.middleRow.add(face.getBottomRow().get(face.getBottomRow().size() - 3));

            this.bottomRow = new ArrayList<>();
            this.bottomRow.add(face.getTopRow().get(0));
            this.bottomRow.add(face.getMiddleRow().get(face.getMiddleRow().size() - 2));
            this.bottomRow.add(face.getMiddleRow().get(face.getMiddleRow().size() - 3));
            this.bottomRow.add(face.getBottomRow().get(face.getBottomRow().size() - 4));
            this.bottomRow.add(face.getBottomRow().get(face.getBottomRow().size() - 5));
        }

        private void initializeLeftPerspective(Face face) {
            this.topRow = new ArrayList<>();
            this.topRow.add(face.getBottomRow().get(0));

            this.middleRow = new ArrayList<>();
            this.middleRow.add(face.getBottomRow().get(face.getBottomRow().size() - 3));
            this.middleRow.add(face.getBottomRow().get(face.getBottomRow().size() - 4));
            this.middleRow.add(face.getMiddleRow().get(0));

            this.bottomRow = new ArrayList<>();
            this.bottomRow.add(face.getBottomRow().get(face.getBottomRow().size() - 1));
            this.bottomRow.add(face.getBottomRow().get(face.getBottomRow().size() - 2));
            this.bottomRow.add(face.getMiddleRow().get(face.getMiddleRow().size() - 1));
            this.bottomRow.add(face.getMiddleRow().get(face.getMiddleRow().size() - 2));
            this.bottomRow.add(face.getTopRow().get(0));
        }

        public void replaceTopRow(List<Triangle> replacement) {
            doReplace(topRow, replacement);
        }

        public void replaceMiddleRow(List<Triangle> replacement) {
            doReplace(middleRow, replacement);
        }

        public void replaceBottomRow(List<Triangle> replacement) {
            doReplace(bottomRow, replacement);
        }

        public List<Triangle> getTopRow() {
            return topRow;
        }

        public List<Triangle> getMiddleRow() {
            return middleRow;
        }

        public List<Triangle> getBottomRow() {
            return bottomRow;
        }

        public Perspective getPerspective() {
            return perspective;
        }

        public void applyNewColors() {
            topRow.forEach(Triangle::applyTemp);
            middleRow.forEach(Triangle::applyTemp);
            bottomRow.forEach(Triangle::applyTemp);
        }

        private void doReplace(List<Triangle> target, List<Triangle> origin) {
            if (target.size() != origin.size()) {
                throw new IllegalArgumentException(String.format("Attempt to replace row of size %s with row of size %s", target.size(), origin.size()));
            }

            for (int i = 0; i < target.size(); i++) {
                target.get(i).setTempColor(origin.get(i).getColor());
            }
        }
    }

    private class Triangle {
        private Color color;
        private Color temp;

        public Triangle(Color color) {
            this.color = color;
        }

        public Color getColor() {
            return this.color;
        }

        public void setTempColor(Color color) {
            this.temp = color;
        }

        public void applyTemp() {
            if (this.temp == null) return;
            this.color = temp;
            this.temp = null;
        }
    }

    private enum Perspective {
        FRONT, RIGHT, LEFT
    }

    private class Color {
        private String symbol;

        public Color(char symbol) {
            this.symbol = Character.toString(symbol);
        }

        public String getSymbol() {
            return symbol;
        }
    }
}
