package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class FractalModelPrinter {
    public static String printModelRepresentation(FractalModel fractalModel) {
        return printModelRepresentation(fractalModel.getModel());
    }

    public static String printModelRepresentation(char[][] model) {
        StringBuilder sb = new StringBuilder();

        sb.append("X  ");
        for (int col = 0; col < model[0].length; col++) {
            sb.append(" ").append(String.format("%02d", col)).append(" ");
        }
        sb.append("\n");

        for (int row = 0; row < model.length; row++) {
            sb.append(String.format("%02d", row)).append(" ");
            for (int col = 0; col < model[row].length; col++) {
                sb.append("  ").append(model[row][col]).append(" ");
            }
            sb.append("\n");
        }

        return sb.toString();
    }

    public static String printBridgeInformationRepresentation(BridgeInformation bridgeInformation) {
        final FractalModelRelative firstFractalModelRelative = bridgeInformation.getFirstFractal().getFractalModelRelative();
        final FractalModelRelative secondFractalModelRelative = bridgeInformation.getSecondFractal().getFractalModelRelative();

        final Point firstFractalModelPosition = firstFractalModelRelative.getGlobalRelativePosition();
        final Point secondFractalModelPosition = secondFractalModelRelative.getGlobalRelativePosition();

        final int fractalRows = firstFractalModelRelative.getFractalModel().getRows();
        final int fractalCols = firstFractalModelRelative.getFractalModel().getCols();

        final int maxRow = Integer.max(firstFractalModelPosition.getRow(), secondFractalModelPosition.getRow()) + fractalRows;
        final int maxCol = Integer.max(firstFractalModelPosition.getCol(), secondFractalModelPosition.getCol()) + fractalCols;

        final List<FractalModelElement> firstFractalElements = firstFractalModelRelative.getAllFractalModelElementsWithRelativePosition();
        final List<FractalModelElement> secondFractalElements = secondFractalModelRelative.getAllFractalModelElementsWithRelativePosition();
        final List<FractalModelElement> allFractalElements = new ArrayList<>();
        allFractalElements.addAll(firstFractalElements);
        allFractalElements.addAll(secondFractalElements);

        final List<String> bridgesRepresentation = new ArrayList<>();

        for (Bridge bridge : bridgeInformation.getBridgesBetweenFractals()) {
            char[][] matrixRepresentation = new char[maxRow][maxCol];
            for (char[] row : matrixRepresentation) {
                Arrays.fill(row, CharManager.getModelEmpty());
            }

            for (FractalModelElement fractalModelElement : allFractalElements) {
                matrixRepresentation[fractalModelElement.getRow()][fractalModelElement.getCol()] = fractalModelElement.getSymbol();
            }

            for (FractalModelElement bridgeElement : bridge.getFractalModelElementList()) {
                matrixRepresentation[bridgeElement.getRow()][bridgeElement.getCol()] = bridgeElement.getSymbol();
            }

            String bridgeRepresentation = FractalModelPrinter.printModelRepresentation(matrixRepresentation);

            StringBuilder sb = new StringBuilder();
            sb.append(bridge.toString()).append("\n\n")
                    .append(bridgeRepresentation);

            bridgesRepresentation.add(sb.toString());
        }

        StringBuilder output = new StringBuilder();
        for (String bridgeRepresentation : bridgesRepresentation) {
            output.append(bridgeRepresentation);
            output.append(repeat('*', maxCol * 2));
            output.append("\n\n");
        }

        return output.toString();
    }

    private static String repeat(char c, int times) {
        return new String(new char[times]).replace('\0', c);
    }
}
