package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Bridge {
    private final char connector;
    private final UnionPointWithAnchorsRelative firstFractalUnionPoint;
    private final UnionPointWithAnchorsRelative secondFractalUnionPoint;
    private final AnchorRelative firstAnchor;
    private final AnchorRelative secondAnchor;
    private final List<FractalModelElement> fractalModelElementList;

    public Bridge(char connector, UnionPointWithAnchorsRelative firstFractalUnionPoint,
                  UnionPointWithAnchorsRelative secondFractalUnionPoint, AnchorRelative firstAnchor, AnchorRelative secondAnchor) {
        this.connector = connector;
        this.firstFractalUnionPoint = firstFractalUnionPoint;
        this.secondFractalUnionPoint = secondFractalUnionPoint;
        this.firstAnchor = firstAnchor;
        this.secondAnchor = secondAnchor;
        this.fractalModelElementList = createFractalModelElements(firstAnchor, secondAnchor, connector);
    }

    private List<FractalModelElement> createFractalModelElements(AnchorRelative firstAnchor, AnchorRelative secondAnchor, char connector) {
        final Point firstAnchorPosition = firstAnchor.getGlobalRelativePosition();
        final Point secondAnchorPosition = secondAnchor.getGlobalRelativePosition();

        if (firstAnchor.isVerticallyAligned(secondAnchor)) {
            final int minRow = Integer.min(firstAnchorPosition.getRow(), secondAnchorPosition.getRow());
            final int maxRow = Integer.max(firstAnchorPosition.getRow(), secondAnchorPosition.getRow());

            return IntStream.range(minRow, maxRow + 1)
                    .mapToObj(row -> new FractalModelElement(connector, new Point(firstAnchorPosition.getCol(), row)))
                    .collect(Collectors.toList());
        } else if (firstAnchor.isHorizontallyAligned(secondAnchor)) {
            final int minCol = Integer.min(firstAnchorPosition.getCol(), secondAnchorPosition.getCol());
            final int maxCol = Integer.max(firstAnchorPosition.getCol(), secondAnchorPosition.getCol());

            return IntStream.range(minCol, maxCol + 1)
                    .mapToObj(col -> new FractalModelElement(connector, new Point(col, firstAnchorPosition.getRow())))
                    .collect(Collectors.toList());
        } else {
            throw new IllegalStateException("Unable to generate bridge FractalModelElements");
        }
    }

    public char getConnector() {
        return connector;
    }

    public AnchorRelative getFirstAnchor() {
        return firstAnchor;
    }

    public AnchorRelative getSecondAnchor() {
        return secondAnchor;
    }

    public UnionPointWithAnchorsRelative getFirstFractalUnionPoint() {
        return firstFractalUnionPoint;
    }

    public UnionPointWithAnchorsRelative getSecondFractalUnionPoint() {
        return secondFractalUnionPoint;
    }

    public int getSize() {
        return getFractalModelElementList().size();
    }

    public List<FractalModelElement> getFractalModelElementList() {
        return fractalModelElementList;
    }

    @Override
    public String toString() {
        return String.format("Bridge{(%s) %s (%s)}", getFirstAnchor().getGlobalRelativePosition(), connector, getSecondAnchor().getGlobalRelativePosition());
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        Bridge that = (Bridge) other;

        if (!this.getFirstAnchor().getGlobalRelativePosition().equals(that.getFirstAnchor().getGlobalRelativePosition())) return false;
        if (!this.getSecondAnchor().getGlobalRelativePosition().equals(that.getSecondAnchor().getGlobalRelativePosition())) return false;
        if (this.getConnector() != that.getConnector()) return false;

        return true;
    }
}
