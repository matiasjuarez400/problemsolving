package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.List;
import java.util.stream.Collectors;

public class UnionPointWithAnchorsRelative implements RelativePositionable {
    private final UnionPointWithAnchors local;
    private final Point globalRelativePosition;
    private final List<AnchorRelative> anchorRelatives;

    public UnionPointWithAnchorsRelative(UnionPointWithAnchors local, Point globalRelativePosition) {
        this.local = local;
        this.globalRelativePosition = globalRelativePosition;
        this.anchorRelatives = createAnchorRelatives();
    }

    public Character getUnionSymbol() {
        return local.getUnionSymbol();
    }

    private List<AnchorRelative> createAnchorRelatives() {
        return local.getAnchors().stream()
                .map(anchor -> new AnchorRelative(anchor, this, this.globalRelativePosition))
                .collect(Collectors.toList());
    }

    public FractalModelElement convertToFractalModelElement() {
        return new FractalModelElement(local.getUnionSymbol(), globalRelativePosition.add(local.getUnionLocation()));
    }

    public List<AnchorRelative> getAnchorRelatives() {
        return anchorRelatives;
    }

    @Override
    public Point getGlobalRelativePosition() {
        return local.getUnionLocation().add(globalRelativePosition);
    }

    @Override
    public Point getLocalPosition() {
        return local.getUnionLocation();
    }
}
