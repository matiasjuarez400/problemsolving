package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.Comparator;
import java.util.List;

public class BridgeInformation {
    private final FractalUnionPointsInformationRelative firstFractal;
    private final FractalUnionPointsInformationRelative secondFractal;
    private final List<Bridge> bridgesBetweenFractals;
    private final Bridge shortestBridge;

    public BridgeInformation(FractalUnionPointsInformationRelative firstFractal, FractalUnionPointsInformationRelative secondFractal, List<Bridge> bridgesBetweenFractals) {
        this.firstFractal = firstFractal;
        this.secondFractal = secondFractal;
        this.bridgesBetweenFractals = bridgesBetweenFractals;

        shortestBridge = bridgesBetweenFractals.stream()
                .min(Comparator.comparing(Bridge::getSize)).orElse(null);
    }

    public FractalUnionPointsInformationRelative getFirstFractal() {
        return firstFractal;
    }

    public FractalUnionPointsInformationRelative getSecondFractal() {
        return secondFractal;
    }

    public List<Bridge> getBridgesBetweenFractals() {
        return bridgesBetweenFractals;
    }

    public Bridge getShortestBridge() {
        return shortestBridge;
    }

    public boolean bridgeExists() {
        return shortestBridge != null;
    }

    @Override
    public String toString() {
        return FractalModelPrinter.printBridgeInformationRepresentation(this);
    }
}
