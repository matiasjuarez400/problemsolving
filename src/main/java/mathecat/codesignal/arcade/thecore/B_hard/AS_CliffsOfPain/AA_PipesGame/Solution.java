package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AA_PipesGame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String[] s1 = {
                "a224C22300000",
                "0001643722B00",
                "0b27275100000",
                "00c7256500000",
                "0006A45000000"
        };

        String[] s2 = {
                "a000",
                "000A"
        };

        String[] s11 = {
                "3222222400000000",
                "1000032A40000000",
                "1000010110000000",
                "72q227277Q000000",
                "1000010110000000",
                "1000062a50000000",
                "6222222500000000"
        };

        String[] s8 = {
                "0002270003777z24",
                "3a40052001000101",
                "1064000001000101",
                "1006774001032501",
                "1000001001010001",
                "1010001001064035",
                "6227206A0622Z250"
        };

        String[] s9 = {
                "00p2400003777z24",
                "1a406P0001000101",
                "1064000001000101",
                "1006774001032501",
                "1000001001010001",
                "1000001001064035",
                "6227276A0622Z250"
        };

        String[] sourcesColliding = {
          "00a2220bCD"
        };

        System.out.println(solution.pipesGame(sourcesColliding));
    }

    int pipesGame(String[] state) {
        Game game = new Game(state);
        GamePrinter.printGameWithPipesShape(game);

        GameSolutionAnalyzer gameSolutionAnalyzer = new GameSolutionAnalyzer(game);

        GameSolutionAnalysis gameSolutionAnalysis = new GameSolutionAnalysis(gameSolutionAnalyzer);

        GamePrinter.printGameWaterStatus(game);

        System.out.println(gameSolutionAnalysis);

        Integer pipesWithWaterUntilFirstLeak = gameSolutionAnalysis.getPipesWithWaterUntilFirstLeak();
        Integer pipesWithWaterUntiLFirstWrongDestination = gameSolutionAnalysis.getPipesWithWaterUntilFirstWrongDestination();

        if (pipesWithWaterUntilFirstLeak != null && pipesWithWaterUntiLFirstWrongDestination != null) return -Integer.min(pipesWithWaterUntilFirstLeak, pipesWithWaterUntiLFirstWrongDestination);
        else if (pipesWithWaterUntilFirstLeak != null) return -pipesWithWaterUntilFirstLeak;
        else if (pipesWithWaterUntiLFirstWrongDestination != null) return -pipesWithWaterUntiLFirstWrongDestination;
        else return gameSolutionAnalysis.getPipesCorrectFlowsCount();
    }

    private static class GameSolutionAnalysis {
        private Map<String, List<GamePiece>> leakedFlows;
        private Integer pipesWithWaterUntilFirstLeak;

        private Map<String, List<GamePiece>> completedFlows;

        private Map<String, List<GamePiece>> correctFlows;
        private Integer pipesCorrectFlowsWithWaterCount;
        private Integer pipesCorrectFlowsCount;

        private Map<String, List<GamePiece>> flowsWithWrongDestination;
        private Integer pipesWithWaterUntilFirstWrongSink;

        private Map<String, List<GamePiece>> flowsOnlyWithPipes;

        public GameSolutionAnalysis(GameSolutionAnalyzer gameSolutionAnalyzer) {
            gameSolutionAnalyzer.analyze();

            this.correctFlows = gameSolutionAnalyzer.getCorrectFlows();
            Set<GamePiece> pipesCorrectFlow = correctFlows.values().stream()
                    .flatMap(Collection::stream)
                    .filter(gamePiece -> gamePiece.getPieceType().equals(GamePiece.PieceType.PIPE))
                    .collect(Collectors.toSet());

            this.pipesCorrectFlowsCount = pipesCorrectFlow.size();
            this.pipesCorrectFlowsWithWaterCount = Math.toIntExact(pipesCorrectFlow.stream().filter(GamePiece::isReceivesWater).count());

            this.completedFlows = gameSolutionAnalyzer.getCompletedWaterFlows();
            this.leakedFlows = gameSolutionAnalyzer.getLeakedFlows();
            this.flowsWithWrongDestination = gameSolutionAnalyzer.getFlowsWithWrongDestination();
            this.flowsOnlyWithPipes = gameSolutionAnalyzer.getAllFlowsOnlyWithPipes();
            this.pipesWithWaterUntilFirstLeak = calculatePipesWithWaterUsingLeakFlows();
            this.pipesWithWaterUntilFirstWrongSink = calculatePipesWithWaterUsingWrongSinkFlows();
        }

        private Integer calculatePipesWithWaterUsingLeakFlows() {
            return calculatePipesWithWater(leakedFlows);
        }

        private Integer calculatePipesWithWaterUsingWrongSinkFlows() {
            return calculatePipesWithWater(flowsWithWrongDestination);
        }

        private Integer calculatePipesWithWater(Map<String, List<GamePiece>> flowsWithWrongCondition) {
            if (flowsWithWrongCondition.isEmpty()) return null;

            int minFlowSize = Integer.MAX_VALUE;

            for (String sourceName : flowsWithWrongCondition.keySet()) {
                List<GamePiece> flow = flowsWithWrongCondition.get(sourceName).stream()
                        .filter(gamePiece -> gamePiece.getPieceType().equals(GamePiece.PieceType.PIPE))
                        .collect(Collectors.toList());
                if (flow.size() < minFlowSize) minFlowSize = flow.size();
            }

            Set<GamePiece> pipesWithWater = new HashSet<>();
            for (String key : flowsOnlyWithPipes.keySet()) {
                List<GamePiece> currentFlow = flowsOnlyWithPipes.get(key);
                for (int i = 0; i < Integer.min(minFlowSize, currentFlow.size()); i++) {
                    pipesWithWater.add(currentFlow.get(i));
                }
            }

            return pipesWithWater.size() == 0 ? null : pipesWithWater.size();
        }

        @Override
        public String toString() {
            return "GameSolutionAnalysis{" +
                    "pipesWithWaterUntilFirstLeak=" + pipesWithWaterUntilFirstLeak +
                    ", pipesCorrectFlowsWithWaterCount=" + pipesCorrectFlowsWithWaterCount +
                    ", pipesCorrectFlowsCount=" + pipesCorrectFlowsCount +
                    ", pipesWithWaterUntilFirstWrongSink=" + pipesWithWaterUntilFirstWrongSink +
                    '}';
        }

        public Map<String, List<GamePiece>> getLeakedFlows() {
            return leakedFlows;
        }

        public Map<String, List<GamePiece>> getCompletedFlows() {
            return completedFlows;
        }

        public Map<String, List<GamePiece>> getCorrectFlows() {
            return correctFlows;
        }

        public Integer getPipesCorrectFlowsWithWaterCount() {
            return pipesCorrectFlowsWithWaterCount;
        }

        public Integer getPipesCorrectFlowsCount() {
            return pipesCorrectFlowsCount;
        }

        public Map<String, List<GamePiece>> getFlowsWithWrongDestination() {
            return flowsWithWrongDestination;
        }

        public Integer getPipesWithWaterUntilFirstLeak() {
            return pipesWithWaterUntilFirstLeak;
        }

        public Integer getPipesWithWaterUntilFirstWrongDestination() {
            return pipesWithWaterUntilFirstWrongSink;
        }
    }

    private static class GameSolutionAnalyzer {
        private Game game;
        private Map<String, List<GamePiece>> connectedPiecesBySource;
        private static final List<GamePiece.PieceType> flowEndPieceTypes = Arrays.asList(
                GamePiece.PieceType.SINK, GamePiece.PieceType.SOURCE
        );

        public GameSolutionAnalyzer(Game game) {
            this.game = game;
        }

        public void analyze() {
            joinGamePieces();

            this.connectedPiecesBySource = new HashMap<>();

            List<Source> sources = game.getSources();

            for (Source source : sources) {
                List<GamePiece> currentFlow = new ArrayList<>();
                currentFlow.add(source);
                buildFlow(currentFlow);
            }
        }

        public Map<String, List<GamePiece>> getCompletedWaterFlows() {
            Map<String, List<GamePiece>> flows = new HashMap<>();

            this.connectedPiecesBySource.forEach((key, flow) -> {
                if (flow.size() > 1) {
                    GamePiece ending = flow.get(flow.size() - 1);

                    if (flowEndPieceTypes.contains(ending.getPieceType())) {
                        flows.put(key, flow);
                    }
                }
            });

            return flows;
        }

        public Map<String, List<GamePiece>> getCorrectFlows() {
            Map<String, List<GamePiece>> flows = new HashMap<>();

            this.connectedPiecesBySource.forEach((key, flow) -> {
                if (flow.size() > 1) {
                    GamePiece origin = flow.get(0);
                    GamePiece ending = flow.get(flow.size() - 1);

                    if (origin.getPieceType().equals(GamePiece.PieceType.SOURCE) && ending.getPieceType().equals(GamePiece.PieceType.SINK)) {
                        if (origin.getName().toLowerCase().equals(ending.getName().toLowerCase())) {
                            flows.put(key, flow);
                        }
                    }
                }
            });

            return flows;
        }

        public Map<String, List<GamePiece>> getLeakedFlows() {
            Map<String, List<GamePiece>> flows = new HashMap<>();

            this.connectedPiecesBySource.forEach((key, flow) -> {
                if (flow.size() > 1) {
                    GamePiece ending = flow.get(flow.size() - 1);

                    if (!flowEndPieceTypes.contains(ending.getPieceType())) {
                        flows.put(key, flow);
                    }
                }
            });

            return flows;
        }

        public Map<String, List<GamePiece>> getAllFlowsOnlyWithPipes() {
            Map<String, List<GamePiece>> result = new HashMap<>();

            for (String key : this.connectedPiecesBySource.keySet()) {
                result.put(key, this.connectedPiecesBySource.get(key).stream()
                        .filter(gamePiece -> gamePiece.getPieceType().equals(GamePiece.PieceType.PIPE))
                        .collect(Collectors.toList()));
            }

            return result;
        }

        public Map<String, List<GamePiece>> getFlowsWithWrongDestination() {
            Map<String, List<GamePiece>> flows = new HashMap<>();

            this.connectedPiecesBySource.forEach((key, flow) -> {
                if (flow.size() > 1) {
                    GamePiece origin = flow.get(0);
                    GamePiece ending = flow.get(flow.size() - 1);

                    if (origin.getPieceType().equals(GamePiece.PieceType.SOURCE)) {
                        if (ending.getPieceType().equals(GamePiece.PieceType.SINK)) {
                            if (!origin.getName().toLowerCase().equals(ending.getName().toLowerCase())) {
                                flows.put(key, flow);
                            }
                        } else if (ending.getPieceType().equals(GamePiece.PieceType.SOURCE)) {
                            // Seems that connecting two Sources doesn't count as an incorrect flow
//                            flows.put(key, flow);
                        }
                    }
                }
            });

            // filter reversed flows
//            Set<String> keysToRemove = new HashSet<>();
//            for (String key : flows.keySet()) {
//                List<GamePiece> currentFlow = flows.get(key);
//
//                GamePiece currentFirstPiece = currentFlow.get(0);
//                GamePiece currentLastPiece = currentFlow.get(currentFlow.size() - 1);
//
//                for (String key2 : flows.keySet()) {
//                    List<GamePiece> subFlow = flows.get(key2);
//                    GamePiece comparingFirstPiece = subFlow.get(0);
//                    GamePiece comparingLastPiece = subFlow.get(subFlow.size() - 1);
//
//                    if (currentFirstPiece.equals(comparingLastPiece) && currentLastPiece.equals(comparingFirstPiece) &&
//                            Stream.of(currentFirstPiece, currentLastPiece, comparingFirstPiece, comparingLastPiece).allMatch(gamePiece -> gamePiece.getPieceType().equals(GamePiece.PieceType.SOURCE))
//                    ) {
//                        keysToRemove.add(Stream.of(key, key2).sorted().collect(Collectors.joining(";")));
//                    }
//                }
//            }
//
//            for (String keyToRemove : keysToRemove) {
//                flows.remove(keyToRemove.split(";")[0]);
//            }

            return flows;
        }

        private void joinGamePieces() {
            GamePiece[][] matrix = game.getMatrix();

            for (GamePiece[] row : matrix) {
                for (GamePiece pieceUnderAnalysis : row) {
                    List<GamePiece> aroundPieces = new ArrayList<>();
                    if (pieceUnderAnalysis.row > 0) {
                        GamePiece upGamePiece = matrix[pieceUnderAnalysis.row - 1][pieceUnderAnalysis.col];
                        aroundPieces.add(upGamePiece);
                    }

                    if (pieceUnderAnalysis.row < matrix.length - 1) {
                        GamePiece downGamePiece = matrix[pieceUnderAnalysis.row + 1][pieceUnderAnalysis.col];
                        aroundPieces.add(downGamePiece);
                    }

                    if (pieceUnderAnalysis.col > 0) {
                        GamePiece leftGamePiece = matrix[pieceUnderAnalysis.row][pieceUnderAnalysis.col - 1];
                        aroundPieces.add(leftGamePiece);
                    }

                    if (pieceUnderAnalysis.col < matrix[0].length - 1) {
                        GamePiece rightGamePiece = matrix[pieceUnderAnalysis.row][pieceUnderAnalysis.col + 1];
                        aroundPieces.add(rightGamePiece);
                    }

                    aroundPieces.stream()
                            .filter(pieceUnderAnalysis::canJoin)
                            .forEach(pieceUnderAnalysis::addConnection);
                }
            }
        }

        private void buildFlow(List<GamePiece> currentFlow) {
            GamePiece currentPiece = currentFlow.get(currentFlow.size() - 1);
            Map<GamePiece.FlowDirection, GamePiece> connectionMap = currentPiece.getConnectionsMap();

            if (connectionMap.isEmpty()) return;

            if (connectionMap.size() == 1) {
                for (GamePiece.FlowDirection key : connectionMap.keySet()) {
                    GamePiece gamePiece = connectionMap.get(key);
                    if (currentFlow.contains(gamePiece)) {
                        // This means that the only connection we have is the previous piece that led us here and this piece is not connected to anything else
                        addFlowToMap(currentFlow);
                        return;
                    }
                }
            }

            final GamePiece previousPieceInFlow = currentFlow.size() > 1 ? currentFlow.get(currentFlow.size() - 2) : null;

            final boolean isPipeSeven = currentPiece.getPieceType().equals(GamePiece.PieceType.PIPE) && currentPiece.getName().equals("7");
            boolean pipeSevenHasNextConnection = false;

            for (GamePiece.FlowDirection flowDirection : connectionMap.keySet()) {
                GamePiece connectedPiece = connectionMap.get(flowDirection);

                boolean skipPiece = connectedPiece.equals(previousPieceInFlow);

                if (!skipPiece && isPipeSeven) {
                    skipPiece = !isInSameFlowForPipeTypeSeven(previousPieceInFlow, currentPiece, connectedPiece);
                }

                if (!skipPiece) {
                    pipeSevenHasNextConnection = isPipeSeven;

                    connectedPiece.setReceivesWater(true);
                    List<GamePiece> workingCopy = new ArrayList<>(currentFlow);
                    GamePiece.PieceType connectedPieceType = connectedPiece.getPieceType();

                    if (flowEndPieceTypes.contains(connectedPieceType)) {
                        workingCopy.add(connectedPiece);
                        addFlowToMap(workingCopy);
                    } else {
                        workingCopy.add(connectedPiece);
                        buildFlow(workingCopy);
                    }
                }
            }

            if (isPipeSeven && !pipeSevenHasNextConnection) {
                addFlowToMap(currentFlow);
            }
        }

        private boolean isInSameFlowForPipeTypeSeven(GamePiece previous, GamePiece current, GamePiece nextPiece) {
            GamePiece.FlowDirection workingFlowDirection = current.calculateGamePieceRelativePosition(previous);
            GamePiece.FlowDirection nextPieceFlowDirection = current.calculateGamePieceRelativePosition(nextPiece);

            List<GamePiece.FlowDirection> horizontalList = Arrays.asList(GamePiece.FlowDirection.LEFT, GamePiece.FlowDirection.RIGHT);
            List<GamePiece.FlowDirection> verticalList = Arrays.asList(GamePiece.FlowDirection.UP, GamePiece.FlowDirection.DOWN);

            return (horizontalList.contains(workingFlowDirection) && horizontalList.contains(nextPieceFlowDirection)) ||
                    verticalList.contains(workingFlowDirection) && verticalList.contains(nextPieceFlowDirection);
        }

        private void addFlowToMap(List<GamePiece> flow) {
            GamePiece rawSource = flow.get(0);
            if (rawSource.getPieceType() != GamePiece.PieceType.SOURCE) {
                throw new IllegalStateException("The first piece of the flow should always be a Source. Current type: " + rawSource.getPieceType());
            }

            Source source = (Source) rawSource;
            for (int i = 1; i < Integer.MAX_VALUE; i++) {
                String key = source.getName() + i;
                if (this.connectedPiecesBySource.containsKey(key)) continue;

                connectedPiecesBySource.put(key, flow);
                break;
            }
        }
    }

    private static class GamePrinter {
        public static void printGamePiecesLocation(Game game) {
            GamePiece[][] matrix = game.getMatrix();

            List<String> toPrint = new ArrayList<>();

            for (int r = 0; r < matrix.length; r++) {
                StringBuilder newLine = new StringBuilder();
                for (int c = 0; c < matrix[r].length; c++) {
                    GamePiece gamePiece = matrix[r][c];

                    switch (gamePiece.getPieceType()) {
                        case PIPE:
                            newLine.append("P");
                            break;
                        case SINK:
                            newLine.append("E");
                            break;
                        case SOURCE:
                            newLine.append("S");
                            break;
                        case EMPTY:
                            newLine.append("-");
                            break;
                    }

                    newLine.append(" | ");
                }

                toPrint.add(newLine.toString().substring(0, newLine.length() - 2));
            }

            doPrint(toPrint.toArray(new String[0]));
        }

        public static void printGameWaterStatus(Game game) {
            GamePiece[][] matrix = game.getMatrix();

            List<String> toPrint = new ArrayList<>();
            for (GamePiece[] row : matrix) {
                StringBuilder newLine = new StringBuilder();
                for (int c = 0; c < row.length; c++) {
                    GamePiece gamePiece = row[c];

                    switch (gamePiece.getPieceType()) {
                        case PIPE: {
                            if (gamePiece.isReceivesWater()) {
                                newLine.append("+");
                            } else {
                                newLine.append("-");
                            }
                            break;
                        }
                        case SINK:
                            newLine.append("E");
                            break;
                        case SOURCE:
                            newLine.append("S");
                            break;
                        case EMPTY:
                            newLine.append(" ");
                            break;
                    }

                    newLine.append(" | ");
                }

                toPrint.add(newLine.toString().substring(0, newLine.length() - 2));
            }

            doPrint(toPrint.toArray(new String[0]));
        }

        public static void printGameWithPipesShape(Game game) {
            Map<Integer, String> symbolByPipeType = new HashMap<>();
            symbolByPipeType.put(1, "║");
            symbolByPipeType.put(2, "═");
            symbolByPipeType.put(3, "╔");
            symbolByPipeType.put(4, "╗");
            symbolByPipeType.put(5, "╝");
            symbolByPipeType.put(6, "╚");
            symbolByPipeType.put(7, "╬");

            GamePiece[][] matrix = game.getMatrix();

            List<String> toPrint = new ArrayList<>();

            for (int r = 0; r < matrix.length; r++) {
                StringBuilder newLine = new StringBuilder();
                for (int c = 0; c < matrix[r].length; c++) {
                    GamePiece gamePiece = matrix[r][c];

                    switch (gamePiece.getPieceType()) {
                        case PIPE:
                            Pipe pipe = (Pipe) gamePiece;
                            newLine.append(symbolByPipeType.get(pipe.getType()));
                            break;
                        case SINK:
                        case SOURCE:
                            newLine.append(gamePiece.getName());
                            break;
                        case EMPTY:
                            newLine.append(" ");
                            break;
                    }

                    newLine.append(" | ");
                }

                toPrint.add(newLine.toString().substring(0, newLine.length() - 2));
            }

            doPrint(toPrint.toArray(new String[0]));
        }

        private static void doPrint(String[] linesToPrint) {
            String upDownLimit = new String(new char[linesToPrint[0].length() + 3]).replace('\0', '*');

            System.out.println(upDownLimit);
            for (String line : linesToPrint) {
                System.out.println("* " + line + "*");
            }
            System.out.println(upDownLimit);
        }
    }

    private static class Game {
        private GamePiece[][] matrix;

        public Game(String[] state) {
            this.matrix = new GamePiece[state.length][state[0].length()];

            for (int r = 0; r < state.length; r++) {
                for (int c = 0; c < state[r].length(); c++) {
                    char current = state[r].charAt(c);

                    if (current >= 'a' && current <= 'z') {
                        matrix[r][c] = new Source(r, c, current + "");
                    } else if (current >= 'A' && current <= 'Z') {
                        matrix[r][c] = new Sink(r, c, current + "");
                    } else if (current >= '1' && current <= '7') {
                        matrix[r][c] = Pipe.buildType(Character.getNumericValue(current), r, c);
                    } else if (current == '0') {
                        matrix[r][c] = new Empty(r, c);
                    } else {
                        throw new IllegalArgumentException("Received unexpected input: " + current);
                    }
                }
            }
        }

        public List<Source> getSources() {
            return getPiecesByType(GamePiece.PieceType.SOURCE, Source.class);
        }

        public List<Pipe> getPipes() {
            return getPiecesByType(GamePiece.PieceType.PIPE, Pipe.class);
        }

        public List<Sink> getSinks() {
            return getPiecesByType(GamePiece.PieceType.SINK, Sink.class);
        }

        private <T extends GamePiece> List<T> getPiecesByType(GamePiece.PieceType pieceType, Class<T> clazz) {
            return Stream.of(matrix).flatMap(Stream::of)
                    .filter(gamePiece -> gamePiece.getPieceType() == pieceType)
                    .map(gamePiece -> (T) gamePiece)
                    .collect(Collectors.toList());
        }

        public GamePiece[][] getMatrix() {
            return matrix;
        }
    }

    private static abstract class GamePiece {
        enum FlowDirection {
            UP, DOWN,
            LEFT, RIGHT
        }

        enum PieceType {
            PIPE, SINK, SOURCE, EMPTY
        }

        protected Map<FlowDirection, GamePiece> connectionsMap;
        protected List<FlowDirection> flowDirections;
        protected int row;
        protected int col;
        protected PieceType pieceType;
        protected boolean receivesWater;
        private static int NEXT_ID = 1;
        protected int id;
        protected String name;

        private GamePiece(int row, int col, PieceType pieceType) {
            this.connectionsMap = new HashMap<>();
            this.flowDirections = new ArrayList<>();
            this.row = row;
            this.col = col;
            this.pieceType = pieceType;
            this.id = NEXT_ID++;
        }

        boolean canJoin(GamePiece anotherGamePiece) {
            FlowDirection anotherPieceRelativePosition = calculateGamePieceRelativePosition(anotherGamePiece);
            for (FlowDirection thisFlow : this.flowDirections) {
                if (thisFlow == FlowDirection.UP) {
                    if (anotherPieceRelativePosition == FlowDirection.UP && anotherGamePiece.flowDirections.contains(FlowDirection.DOWN)) return true;
                } else if (thisFlow == FlowDirection.DOWN) {
                    if (anotherPieceRelativePosition == FlowDirection.DOWN && anotherGamePiece.flowDirections.contains(FlowDirection.UP)) return true;
                } else if (thisFlow == FlowDirection.LEFT) {
                    if (anotherPieceRelativePosition == FlowDirection.LEFT && anotherGamePiece.flowDirections.contains(FlowDirection.RIGHT)) return true;
                } else if (thisFlow == FlowDirection.RIGHT) {
                    if (anotherPieceRelativePosition == FlowDirection.RIGHT && anotherGamePiece.flowDirections.contains(FlowDirection.LEFT)) return true;
                }
            }

            return false;
        }

        public FlowDirection calculateGamePieceRelativePosition(GamePiece anotherGamePiece) {
            if (anotherGamePiece.row == this.row - 1 && anotherGamePiece.col == this.col) return FlowDirection.UP;
            else if (anotherGamePiece.row == this.row + 1 && anotherGamePiece.col == this.col) return FlowDirection.DOWN;
            else if (anotherGamePiece.col == this.col - 1 && anotherGamePiece.row == this.row) return FlowDirection.LEFT;
            else if (anotherGamePiece.col == this.col + 1 && anotherGamePiece.row == this.row) return FlowDirection.RIGHT;

            throw new IllegalArgumentException("Received pieces that are not adjacent");
        }

        public void addConnection(GamePiece anotherGamePiece) {
            FlowDirection relativeAnotherPiecePosition = calculateGamePieceRelativePosition(anotherGamePiece);
            this.connectionsMap.put(relativeAnotherPiecePosition, anotherGamePiece);
        }

        public Map<FlowDirection, GamePiece> getConnectionsMap() {
            return connectionsMap;
        }

        public int calculateNotConnectedFlows() {
            return this.flowDirections.size() - this.connectionsMap.size();
        }

        protected void addDirections(FlowDirection... newDirections) {
            this.flowDirections.addAll(Arrays.asList(newDirections));
        }

        public boolean isReceivesWater() {
            return receivesWater;
        }

        public void setReceivesWater(boolean receivesWater) {
            this.receivesWater = receivesWater;
        }

        public PieceType getPieceType() {
            return pieceType;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            GamePiece gamePiece = (GamePiece) o;
            return id == gamePiece.id;
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }
    }

    private static class Empty extends GamePiece {
        public Empty(int row, int col) {
            super(row, col, PieceType.EMPTY);
        }

        @Override
        public String toString() {
            return "Empty{" +
                    "row=" + row +
                    ", col=" + col +
                    '}';
        }
    }

    private static class Sink extends GamePiece {
        public Sink(int row, int col, String name) {
            super(row, col, PieceType.SINK);
            this.name = name;
            super.addDirections(FlowDirection.LEFT, FlowDirection.RIGHT, FlowDirection.UP, FlowDirection.DOWN);
        }

        @Override
        public String toString() {
            return "Sink{" +
                    "row=" + row +
                    ", col=" + col +
                    ", name='" + name + '\'' +
                    '}';
        }
    }

    private static class Source extends GamePiece {
        public Source(int row, int col, String name) {
            super(row, col, PieceType.SOURCE);
            this.name = name;
            super.addDirections(FlowDirection.LEFT, FlowDirection.RIGHT, FlowDirection.UP, FlowDirection.DOWN);
            super.receivesWater = true;
        }

        @Override
        public String toString() {
            return "Source{" +
                    "row=" + row +
                    ", col=" + col +
                    ", name='" + name + '\'' +
                    '}';
        }
    }

    private static class Pipe extends GamePiece {
        private int type;

        private Pipe(int row, int col, int type) {
            super(row, col, PieceType.PIPE);
            this.type = type;
            this.name = Integer.toString(type);
        }

        public static Pipe buildType(int type, int row, int col) {
            Pipe pipe = new Pipe(row, col, type);
            switch (type) {
                case 1: {
                    pipe.addDirections(FlowDirection.UP, FlowDirection.DOWN);
                    break;
                }
                case 2: {
                    pipe.addDirections(FlowDirection.LEFT, FlowDirection.RIGHT);
                    break;
                }
                case 3: {
                    pipe.addDirections(FlowDirection.DOWN, FlowDirection.RIGHT);
                    break;
                }
                case 4: {
                    pipe.addDirections(FlowDirection.LEFT, FlowDirection.DOWN);
                    break;
                }
                case 5: {
                    pipe.addDirections(FlowDirection.LEFT, FlowDirection.UP);
                    break;
                }
                case 6: {
                    pipe.addDirections(FlowDirection.UP, FlowDirection.RIGHT);
                    break;
                }
                case 7: {
                    pipe.addDirections(FlowDirection.LEFT, FlowDirection.RIGHT, FlowDirection.UP, FlowDirection.DOWN);
                    break;
                }
                default: {
                    throw new IllegalArgumentException(String.format("Received invalid type: %s", type));
                }
            }

            return pipe;
        }

        public int getType() {
            return type;
        }

        @Override
        public String toString() {
            return "Pipe{" +
                    "row=" + row +
                    ", col=" + col +
                    ", type=" + type +
                    '}';
        }
    }
}