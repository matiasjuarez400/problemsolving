package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class FractalModelElementWithSurroundings {
    private final FractalModelElement[][] fractalModelElements;
    private final FractalModelElement centralElement;

    public FractalModelElementWithSurroundings(FractalModel fractalModel, Point elementLocation) {
        fractalModelElements = new FractalModelElement[3][3];

        for (int row = -1; row <= 1; row++) {
            for (int col = -1; col <= 1; col++) {
                fractalModelElements[row + 1][col + 1] = fractalModel.getFractalModelElement(col + elementLocation.getCol(), row + elementLocation.getRow());
            }
        }

        this.centralElement = fractalModelElements[1][1];
    }

    public FractalModelElement[][] getFractalModelElements() {
        return fractalModelElements;
    }

    public FractalModelElement getCentralElement() {
        return centralElement;
    }

    public List<FractalModelElement> getTopRowElements() {
        return Arrays.stream(fractalModelElements[0]).filter(Objects::nonNull).collect(Collectors.toList());
    }

    public List<FractalModelElement> getMiddleRowElements() {
        List<FractalModelElement> elements = new ArrayList<>();
        for (int col = 0; col < fractalModelElements[1].length; col++) {
            if (col == 1) continue;
            elements.add(fractalModelElements[1][col]);
        }

        return filterNull(elements);
    }

    public List<FractalModelElement> getDownRowElements() {
        return Arrays.stream(fractalModelElements[2]).filter(Objects::nonNull).collect(Collectors.toList());
    }

    public List<FractalModelElement> getLeftColumnElements() {
        List<FractalModelElement> elements = new ArrayList<>();

        for (int row = 0; row < this.fractalModelElements.length; row++) {
            elements.add(this.fractalModelElements[row][0]);
        }

        return filterNull(elements);
    }

    public List<FractalModelElement> getMiddleColumnElements() {
        List<FractalModelElement> elements = new ArrayList<>();

        for (int row = 0; row < this.fractalModelElements.length; row++) {
            if (row == 1) continue;
            elements.add(this.fractalModelElements[row][1]);
        }

        return filterNull(elements);
    }

    public List<FractalModelElement> getRightColumnElements() {
        List<FractalModelElement> elements = new ArrayList<>();

        for (int row = 0; row < this.fractalModelElements.length; row++) {
            elements.add(this.fractalModelElements[row][2]);
        }

        return filterNull(elements);
    }

    private List<FractalModelElement> filterNull(List<FractalModelElement> elements) {
        return elements.stream().filter(Objects::nonNull).collect(Collectors.toList());
    }
}
