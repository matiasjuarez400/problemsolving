package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class FractalJoiner {
    private final ConnectionAnalyzer connectionAnalyzer;

    public FractalJoiner(ConnectionAnalyzer connectionAnalyzer) {
        this.connectionAnalyzer = connectionAnalyzer;
    }

    public BridgeInformation joinFractals(FractalUnionPointsInformationRelative first, FractalUnionPointsInformationRelative second) {
        final List<Bridge> bridges = createBridges(first, second);
        final List<Bridge> validBridges = bridges.stream()
                .filter(bridge -> isValidBridge(bridge, first, second))
                .collect(Collectors.toList());

        final BridgeInformation bridgeInformation = new BridgeInformation(first, second, validBridges);

        return bridgeInformation;
    }

    List<Bridge> createValidBridges(FractalUnionPointsInformationRelative first,
                               FractalUnionPointsInformationRelative second) {
        final List<Bridge> bridges = createBridges(first, second);

        return bridges.stream()
                .filter(bridge -> isValidBridge(bridge, first, second))
                .collect(Collectors.toList());
    }

    boolean isValidBridge(Bridge bridge, FractalUnionPointsInformationRelative first, FractalUnionPointsInformationRelative second) {
        final FractalModelRelative firstFractalModelRelative = first.getFractalModelRelative();
        final FractalModelRelative secondFractalModelRelative = second.getFractalModelRelative();

        final Point firstFractalModelPosition = firstFractalModelRelative.getGlobalRelativePosition();
        final Point secondFractalModelPosition = secondFractalModelRelative.getGlobalRelativePosition();

        final int fractalRows = firstFractalModelRelative.getFractalModel().getRows();
        final int fractalCols = firstFractalModelRelative.getFractalModel().getCols();

        final int maxRow = Integer.max(firstFractalModelPosition.getRow(), secondFractalModelPosition.getRow()) + fractalRows;
        final int maxCol = Integer.max(firstFractalModelPosition.getCol(), secondFractalModelPosition.getCol()) + fractalCols;

        for (FractalModelElement bridgeElement : bridge.getFractalModelElementList()) {
            if (bridgeElement.getCol() < 0 || bridgeElement.getRow() < 0) return false;
            if (bridgeElement.getCol() >= maxCol || bridgeElement.getRow() >= maxRow) return false;

            if (first.getFractalModelRelative().isConnectorInPosition(bridgeElement.getPosition()) ||
                second.getFractalModelRelative().isConnectorInPosition(bridgeElement.getPosition())
            ) return false;

            if (connectionAnalyzer.areBlocking(bridgeElement, bridge.getFirstFractalUnionPoint().convertToFractalModelElement()) ||
                connectionAnalyzer.areBlocking(bridgeElement, bridge.getSecondFractalUnionPoint().convertToFractalModelElement())
            ) return false;
        }

        return true;
    }

    List<Bridge> createBridges(FractalUnionPointsInformationRelative first,
                               FractalUnionPointsInformationRelative second) {
        List<UnionPointWithAnchorsRelative> firstUnionPoints = first.getUnionsWithRelativePosition();
        List<UnionPointWithAnchorsRelative> secondUnionPoints = second.getUnionsWithRelativePosition();

        List<Bridge> bridges = new ArrayList<>();

        for (UnionPointWithAnchorsRelative firstUnionPoint : firstUnionPoints) {
            for (UnionPointWithAnchorsRelative secondUnionPoint : secondUnionPoints) {
                bridges.addAll(createBridges(firstUnionPoint, secondUnionPoint));
            }
        }

        return bridges;
    }

    List<Bridge> createBridges(UnionPointWithAnchorsRelative firstUnion, UnionPointWithAnchorsRelative secondUnion) {
        List<Bridge> bridges = new ArrayList<>();

        for (AnchorRelative firstAnchor : firstUnion.getAnchorRelatives()) {
            for (AnchorRelative secondAnchor : secondUnion.getAnchorRelatives()) {
                if (firstAnchor.isInSameRelativePosition(secondAnchor)) {
                    for (Character connector : CharManager.getConnectionChars()) {
                        Bridge bridge = createBridgeIfCanConnect(connector, firstAnchor, secondAnchor, firstUnion, secondUnion);
                        if (bridge != null) bridges.add(bridge);
                    }
                } else if (firstAnchor.isVerticallyAligned(secondAnchor)) {
                    Bridge bridge = createBridgeIfCanConnect(CharManager.getModelVertical(), firstAnchor, secondAnchor, firstUnion, secondUnion);
                    if (bridge != null) bridges.add(bridge);
                } else if (firstAnchor.isHorizontallyAligned(secondAnchor)) {
                    Bridge bridge = createBridgeIfCanConnect(CharManager.getModelHorizontal(), firstAnchor, secondAnchor, firstUnion, secondUnion);
                    if (bridge != null) bridges.add(bridge);
                }
            }
        }

        return bridges;
    }

    private Bridge createBridgeIfCanConnect(char connector, AnchorRelative firstAnchor, AnchorRelative secondAnchor,
                                            UnionPointWithAnchorsRelative firstUnion, UnionPointWithAnchorsRelative secondUnion) {
        final FractalModelElement firstAnchorFractalModelElementConnector = new FractalModelElement(connector, firstAnchor.getGlobalRelativePosition());
        final FractalModelElement secondAnchorFractalModelElementConnector = new FractalModelElement(connector, secondAnchor.getGlobalRelativePosition());

        if (connectionAnalyzer.canConnectWithElement(firstAnchorFractalModelElementConnector, firstUnion.convertToFractalModelElement()) &&
                connectionAnalyzer.canConnectWithElement(secondAnchorFractalModelElementConnector, secondUnion.convertToFractalModelElement())
        ) {
            return new Bridge(connector, firstUnion, secondUnion, firstAnchor, secondAnchor);
        }

        return null;
    }
}
