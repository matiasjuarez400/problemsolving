package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.Arrays;

public class CombinationGenerator {
    private final int[] maxIndexes;
    private final int[] currentCombination;
    private boolean hasReachedTheEnd = false;

    public CombinationGenerator(int maxIndex, int amountOfElements) {
        this.maxIndexes = new int[amountOfElements];
        Arrays.fill(maxIndexes, maxIndex);
        this.currentCombination = new int[amountOfElements];
    }

    public CombinationGenerator(int[] maxIndexes) {
        this.maxIndexes = maxIndexes;
        this.currentCombination = new int[maxIndexes.length];
    }

    public int[] getCurrentCombination() {
        return currentCombination;
    }

    public int[] getAndGenerate() {
        int[] temp = Arrays.copyOf(currentCombination, currentCombination.length);
        generateNextCombination();
        return temp;
    }

    public int[] generateNextCombination() {
        if (!hasNext()) return null;

        for (int i = 0; i < currentCombination.length; i++) {
            if (currentCombination[i] == maxIndexes[i]) {
                if (i == currentCombination.length - 1) {
                    hasReachedTheEnd = true;
                    return currentCombination;
                }
                currentCombination[i] = 0;
            } else {
                currentCombination[i]++;
                break;
            }
        }

        return Arrays.copyOf(currentCombination, currentCombination.length);
    }

    public boolean hasNext() {
        return !hasReachedTheEnd;
    }
}
