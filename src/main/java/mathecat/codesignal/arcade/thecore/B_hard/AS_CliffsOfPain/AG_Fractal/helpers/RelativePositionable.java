package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

public interface RelativePositionable {
    Point getLocalPosition();
    Point getGlobalRelativePosition();
    default boolean isInSameLocalPosition(RelativePositionable that) {
        return this.getLocalPosition().equals(that.getLocalPosition());
    }
    default boolean isInSameRelativePosition(RelativePositionable that) {
        return this.getGlobalRelativePosition().equals(that.getGlobalRelativePosition());
    }
    default boolean isHorizontallyAligned(RelativePositionable that) {
        return this.getGlobalRelativePosition().getRow() == that.getGlobalRelativePosition().getRow();
    }
    default boolean isVerticallyAligned(RelativePositionable that) {
        return this.getGlobalRelativePosition().getCol() == that.getGlobalRelativePosition().getCol();
    }
}
