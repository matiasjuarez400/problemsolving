package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.List;

class FractalUnionPointsInformation {
    private final FractalModel fractalModel;
    private final boolean[][] unionPointsMatrix;
    private final List<UnionPointWithAnchors> unionPointsWithAnchors;

    public FractalUnionPointsInformation(FractalModel fractalModel, boolean[][] unionPointsMatrix, List<UnionPointWithAnchors> unionPointsWithAnchors) {
        this.fractalModel = fractalModel;
        this.unionPointsMatrix = unionPointsMatrix;
        this.unionPointsWithAnchors = unionPointsWithAnchors;
    }

    public FractalModel getFractalModel() {
        return fractalModel;
    }

    public boolean[][] getUnionPointsMatrix() {
        return unionPointsMatrix;
    }

    public List<UnionPointWithAnchors> getUnionPointsWithAnchors() {
        return unionPointsWithAnchors;
    }
}
