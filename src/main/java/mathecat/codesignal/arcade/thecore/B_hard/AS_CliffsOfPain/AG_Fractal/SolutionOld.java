package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal;

class SolutionOld {
    public static void main(String[] args) {
        SolutionOld solutionOld = new SolutionOld();

        char[][] matrix = {
                {'.', '_', '.'},
                {'.', '_', '|'}
        };

        FractalModel fractalModel = new FractalModel(matrix);

//        for (int i = 0; i < 4; i++) {
//            System.out.println(fractalModel.rotate(i));
//        }

        System.out.println(fractalModel);
        System.out.println(FractalModel.printModelRepresentation(fractalModel.get180Rotation()));
    }

    private static class FractalModel {
        private char[][] model;
        private static final char RAW_HORIZONTAL = '_';
        private static final char RAW_VERTICAL = '|';
        private static final char MODEL_HORIZONTAL = '_';
        private static final char MODEL_VERTICAL = '|';
        private static final char MODEL_EMPTY = '.';
        private static final int MODEL_EXPANSION = 3;

        public FractalModel(char[][] rawModel) {
            model = new char[rawModel.length * MODEL_EXPANSION][rawModel[0].length * MODEL_EXPANSION];

            for (int row = 0; row < rawModel.length; row++) {
                int modelRow = row * MODEL_EXPANSION;
                for (int col = 0; col < rawModel[row].length; col++) {
                    int modelCol = col * MODEL_EXPANSION;

                    char rawChar = rawModel[row][col];

                    for (int currentPieceRow = modelRow; currentPieceRow < modelRow + MODEL_EXPANSION; currentPieceRow++) {
                        for (int currentPieceCol = modelCol; currentPieceCol < modelCol + MODEL_EXPANSION; currentPieceCol++) {
                            if (rawChar == RAW_HORIZONTAL) {
                                if (currentPieceRow == modelRow + MODEL_EXPANSION - 1) {
                                    model[currentPieceRow][currentPieceCol] = MODEL_HORIZONTAL;
                                } else {
                                    model[currentPieceRow][currentPieceCol] = MODEL_EMPTY;
                                }
                            } else if (rawChar == RAW_VERTICAL) {
                                if (currentPieceCol == modelCol + MODEL_EXPANSION / 2) {
                                    model[currentPieceRow][currentPieceCol] = MODEL_VERTICAL;
                                } else {
                                    model[currentPieceRow][currentPieceCol] = MODEL_EMPTY;
                                }
                            } else {
                                model[currentPieceRow][currentPieceCol] = MODEL_EMPTY;
                            }
                        }
                    }
                }
            }
        }

        private void init1(char[][] rawModel) {
            model = new char[rawModel.length * MODEL_EXPANSION][rawModel[0].length * MODEL_EXPANSION];

            for (int row = 0; row < rawModel.length; row++) {
                int modelRow = row * MODEL_EXPANSION;
                for (int col = 0; col < rawModel[row].length; col++) {
                    int modelCol = col * MODEL_EXPANSION;

                    char rawChar = rawModel[row][col];

                    for (int currentPieceRow = modelRow; currentPieceRow < modelRow + MODEL_EXPANSION; currentPieceRow++) {
                        for (int currentPieceCol = modelCol; currentPieceCol < modelCol + MODEL_EXPANSION; currentPieceCol++) {
                            if (rawChar == RAW_HORIZONTAL) {
                                if (currentPieceRow == modelRow + MODEL_EXPANSION - 1) {
                                    model[currentPieceRow][currentPieceCol] = MODEL_HORIZONTAL;
                                } else {
                                    model[currentPieceRow][currentPieceCol] = MODEL_EMPTY;
                                }
                            } else if (rawChar == RAW_VERTICAL) {
                                if (currentPieceCol == modelCol + MODEL_EXPANSION / 2) {
                                    model[currentPieceRow][currentPieceCol] = MODEL_VERTICAL;
                                } else {
                                    model[currentPieceRow][currentPieceCol] = MODEL_EMPTY;
                                }
                            } else {
                                model[currentPieceRow][currentPieceCol] = MODEL_EMPTY;
                            }
                        }
                    }
                }
            }
        }

        private FractalModel() {}

        public char[][] getModel() {
            return model;
        }

        public char[][] get180Rotation() {
            char[][] output = new char[this.model.length][this.model[0].length];

            for (int row = 0; row < output.length; row++) {
                for (int col = 0; col < output[row].length; col++) {
                    output[row][output[row].length - 1 - col] = model[row][col];
                }
            }

            return output;
        }

        public FractalModel rotate(int times) {
            char[][] output = model;

            for (int i = 0; i < times; i++) {
                output = rotate(output);
            }

            FractalModel fractalModel = new FractalModel();
            fractalModel.model = output;

            return fractalModel;
        }

        private char[][] rotate(char[][] matrix) {
            char[][] output = new char[matrix[0].length][matrix.length];

            for (int row = 0; row < matrix.length; row++) {
                for (int col = 0; col < matrix[row].length; col++) {
                    char currentChar = matrix[row][col];
                    if (currentChar == MODEL_VERTICAL) currentChar = MODEL_HORIZONTAL;
                    else if (currentChar == MODEL_HORIZONTAL) currentChar = MODEL_VERTICAL;

                    output[matrix[row].length - 1 - col][row] = currentChar;
                }
            }

            return output;
        }

        @Override
        public String toString() {
            return printModelRepresentation(model);
        }

        public static String printModelRepresentation(char[][] model) {
            StringBuilder sb = new StringBuilder();

            for (int row = 0; row < model.length; row++) {
                for (int col = 0; col < model[row].length; col++) {
                    sb.append(model[row][col]).append(" ");
                }
                sb.append("\n");
            }

            return sb.toString();
        }
    }
}
