package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class FractalExpansionLogic {
    private final FractalUnionsFinder fractalUnionsFinder;
    private final NextLevelFractalJoiner nextLevelFractalJoiner;

    public FractalExpansionLogic(FractalUnionsFinder fractalUnionsFinder, NextLevelFractalJoiner nextLevelFractalJoiner) {
        this.fractalUnionsFinder = fractalUnionsFinder;
        this.nextLevelFractalJoiner = nextLevelFractalJoiner;
    }

    public FractalModel expand(FractalModel fractalModel) {
        final List<FractalModel> fractalRotations = fractalModel.getRotations();

        final List<FractalUnionPointsInformation> fractalUnionPointsInformationList = fractalRotations.stream()
                .map(fractalUnionsFinder::analyzeFractalModel)
                .collect(Collectors.toList());

        final CombinationGenerator fractalsInnerSpaceCombinator = new CombinationGenerator(1, 2);

        while (fractalsInnerSpaceCombinator.hasNext()) {
            final CombinationGenerator fractalsRotationCombinator = new CombinationGenerator(3, 3);

            final int[] fractalsInnerSpaceCurrentCombination = fractalsInnerSpaceCombinator.getAndGenerate();
            if (Arrays.equals(fractalsInnerSpaceCurrentCombination, new int[] {0, 0})) continue;

            while (fractalsRotationCombinator.hasNext()) {
                final int[] fractalRotationCurrentCombination = fractalsRotationCombinator.getAndGenerate();

                List<FractalUnionPointsInformationRelative> fractalUnionPointsInformationRelativeList =
                        getRelativeFractalUnionInformation(
                                fractalUnionPointsInformationList,
                                fractalsInnerSpaceCurrentCombination,
                                fractalRotationCurrentCombination,
                                fractalModel
                        );

                NextLevelFractalData nextLevelFractalData = nextLevelFractalJoiner.join(fractalUnionPointsInformationRelativeList);

                if (nextLevelFractalData != null) {
                    if (nextLevelFractalData.getTopRight().getFractal().getRotation() == 2 ||
                        nextLevelFractalData.getDownLeft().getFractal().getRotation() == 2
                    )
                    return generateNextLevel(nextLevelFractalData, fractalsInnerSpaceCurrentCombination, fractalModel);
                }
            }
        }

        throw new IllegalStateException("Unable to generate next fractal level");
    }



    private FractalModel generateNextLevel(NextLevelFractalData nextLevelFractalData,
                                           int[] fractalsInnerSpaceCurrentCombination,
                                           FractalModel original) {
        final int newModelCols = original.getCols() * 2 + fractalsInnerSpaceCurrentCombination[0];
        final int newModelRows = original.getRows() * 2 + fractalsInnerSpaceCurrentCombination[1];

        final char[][] matrixModel = new char[newModelRows][newModelCols];
        for (char[] row : matrixModel) {
            Arrays.fill(row, CharManager.getModelEmpty());
        }

        insertFractalUnionPointInformationRelative(matrixModel, nextLevelFractalData.getTopLeft());
        insertFractalUnionPointInformationRelative(matrixModel, nextLevelFractalData.getTopRight());
        insertFractalUnionPointInformationRelative(matrixModel, nextLevelFractalData.getDownLeft());
        insertFractalUnionPointInformationRelative(matrixModel, nextLevelFractalData.getDownRight());

        for (Bridge connector : nextLevelFractalData.getUsedBridges()) {
            insertConnector(matrixModel, connector);
        }

        return new FractalModel(matrixModel);
    }

    private void insertFractalUnionPointInformationRelative(char[][] matrix, FractalUnionPointsInformationRelative fractalUnionPointsInformationRelative) {
        FractalModel fractalModel = fractalUnionPointsInformationRelative.getFractal();
        Point relativePosition = fractalUnionPointsInformationRelative.getGlobalRelativePosition();

        for (int row = 0; row < fractalModel.getRows(); row++) {
            for (int col = 0; col < fractalModel.getCols(); col++) {
                matrix[row + relativePosition.getRow()][col + relativePosition.getCol()] = fractalModel.get(col, row);
            }
        }
    }

    private void insertConnector(char[][] matrix, Bridge bridge) {
        for (FractalModelElement fractalModelElement : bridge.getFractalModelElementList()) {
            Point position = fractalModelElement.getPosition();
            matrix[position.getRow()][position.getCol()] = fractalModelElement.getSymbol();
        }
    }

    private List<FractalUnionPointsInformationRelative> getRelativeFractalUnionInformation(
            List<FractalUnionPointsInformation> fractalUnionPointsInformationList,
            int[] fractalsInnerSpaceCurrentCombination,
            int[] fractalRotationCurrentCombination,
            FractalModel original) {

        final int rowOffset = fractalsInnerSpaceCurrentCombination[1] + original.getRows();
        final int colOffset = fractalsInnerSpaceCurrentCombination[0] + original.getCols();

        List<FractalUnionPointsInformationRelative> fractalUnionPointsInformationRelativeList = new ArrayList<>();
        fractalUnionPointsInformationRelativeList.add(new FractalUnionPointsInformationRelative(
                fractalUnionPointsInformationList.get(0), new Point(0, 0)
        ));

        fractalUnionPointsInformationRelativeList.add(new FractalUnionPointsInformationRelative(
                fractalUnionPointsInformationList.get(fractalRotationCurrentCombination[0]),
                new Point(0, rowOffset)
        ));

        fractalUnionPointsInformationRelativeList.add(new FractalUnionPointsInformationRelative(
                fractalUnionPointsInformationList.get(fractalRotationCurrentCombination[1]),
                new Point(colOffset, rowOffset)
        ));

        fractalUnionPointsInformationRelativeList.add(new FractalUnionPointsInformationRelative(
                fractalUnionPointsInformationList.get(fractalRotationCurrentCombination[2]),
                new Point(colOffset, 0)
        ));

        return fractalUnionPointsInformationRelativeList;
    }
}
