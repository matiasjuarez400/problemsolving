package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AF_LinesGame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        
        solution.test4();
    }

    public void test4() {
        char[][] field = {
                {'.', 'G', '.', '.', '.', '.', 'V', '.', '.'},
                {'.', '.', 'V', 'V', 'V', 'V', '.', 'V', 'V'},
                {'E', 'E', '.', 'E', 'E', '.', '.', 'E', 'E'},
                {'.', '.', '.', 'E', 'O', '.', 'V', '.', '.'},
                {'O', 'T', '.', 'E', '.', '.', 'V', '.', 'O'},
                {'E', 'S', '.', '.', '.', 'E', '.', '.', '.'},
                {'.', 'L', '.', 'R', 'O', '.', 'E', 'B', '.'},
                {'.', '.', '.', '.', '.', '.', '.', 'E', '.'},
                {'.', '.', 'C', 'R', '.', '.', '.', '.', '.'}
        };

        int[][] clicks = {{4, 8}, {5, 0}, {2, 6}};

        char[] newBalls = {'E', 'E', 'E'};

        int[][] newBallsCoordinates = {{2, 2}, {2, 5}, {0, 0}};

        System.out.println(linesGame(field, clicks, newBalls, newBallsCoordinates));
    }

    public void test3() {
        char[][] field = {
                {'.', 'G', '.', '.', '.', '.', 'V', '.', '.'},
                {'.', '.', 'V', 'V', 'V', 'V', '.', 'V', 'V'},
                {'E', 'E', '.', 'E', 'E', '.', 'V', 'O', '.'},
                {'.', '.', '.', 'E', 'O', '.', 'V', '.', '.'},
                {'O', 'T', '.', 'E', '.', '.', 'V', '.', 'O'},
                {'E', 'S', '.', '.', '.', 'E', '.', '.', '.'},
                {'.', 'L', '.', 'R', 'O', '.', 'E', 'B', '.'},
                {'.', '.', '.', '.', '.', '.', '.', 'E', '.'},
                {'.', '.', 'C', 'R', '.', '.', '.', '.', '.'}
        };

        int[][] clicks = {{4, 8}, {5, 0}, {5, 1}};

        char[] newBalls = {'E', 'E', 'B'};

        int[][] newBallsCoordinates = {{2, 2}, {4, 4}, {1, 7}};

        System.out.println(linesGame(field, clicks, newBalls, newBallsCoordinates));
    }

    public void test2() {
        char[][] field = {
                {'.', 'G', '.', '.', '.', '.', '.', '.', '.'},
                {'.', '.', '.', 'V', 'V', '.', '.', 'V', '.'},
                {'.', '2', '.', '.', '.', '.', '.', 'O', '.'},
                {'.', '.', '.', 'E', 'O', '.', 'V', '.', '.'},
                {'O', 'T', '.', 'E', '.', '.', 'V', '.', 'O'},
                {'F', 'S', '.', 'E', 'O', '.', '.', '.', '.'},
                {'.', 'L', '.', 'R', 'O', '.', 'B', 'B', '.'},
                {'.', '.', '.', '.', '.', '.', '.', 'R', '.'},
                {'.', '.', 'C', 'R', '.', '.', '.', '.', '.'}
        };

        int[][] clicks = {{4, 8}, {5, 0}, {4, 4}};

        char[] newBalls = {'R', 'V', 'C', 'G', 'Y', 'O'};

        int[][] newBallsCoordinates = {{1, 2}, {8, 5}, {8, 6}, {1, 1}, {1, 8}, {7, 4}};

        System.out.println(linesGame(field, clicks, newBalls, newBallsCoordinates));
    }

    public void test1() {
        char[][] field = {
                {'.', 'G', '.', '.', '.', '.', '.', '.', '.'},
                {'.', '.', '.', '.', '.', '.', '.', 'V', '.'},
                {'.', 'O', '.', '.', 'O', '.', '.', '.', '.'},
                {'.', '.', '.', '.', 'O', '.', '.', '.', '.'},
                {'.', '.', '.', '.', '.', '.', '.', '.', 'O'},
                {'.', '.', '.', '.', 'O', '.', '.', '.', '.'},
                {'.', '.', '.', '.', '.', '.', '.', '.', '.'},
                {'R', '.', '.', '.', '.', '.', '.', 'B', 'R'},
                {'.', '.', 'C', '.', '.', '.', '.', 'Y', 'O'}
        };

        int[][] clicks = {{4, 8}, {2, 1}, {4, 4}, {6, 4}, {4, 8}, {1, 2}, {1, 4}, {4, 8}, {6, 4}};

        char[] newBalls = {'R', 'V', 'C', 'G', 'Y', 'O'};

        int[][] newBallsCoordinates = {{1, 2}, {8, 5}, {8, 6}, {1, 1}, {1, 8}, {7, 4}};

        System.out.println(linesGame(field, clicks, newBalls, newBallsCoordinates));
    }

    int linesGame(char[][] fieldRaw, int[][] clicksRaw, char[] newBallsRaw, int[][] newBallsCoordinatesRaw) {
        Logger logger = Logger.getInstance();

        Field field = new Field(fieldRaw);

        logger.log(field);

        List<Click> clicks = Click.createClicks(clicksRaw);
        List<Ball> newBalls = Ball.createNewBalls(newBallsRaw, newBallsCoordinatesRaw);

        PathFinder pathFinder = new PathFinder(field);
        InteractionAnalyzer interactionAnalyzer = new InteractionAnalyzer(field, pathFinder);

        LineFormationAnalyzer lineFormationAnalyzer = new LineFormationAnalyzer(field);

        PointsCalculator pointsCalculator = new PointsCalculator();
        SuccessfullLinesPostProcessor successfullLinesPostProcessor = new SuccessfullLinesPostProcessor(field);

        List<Click> validInteractions;
        while (!(validInteractions = interactionAnalyzer.getNextValidInteraction(clicks)).isEmpty()) {
            Click first = validInteractions.get(0);
            Click second = validInteractions.get(1);

            LineFormationResult lineFormationResult = lineFormationAnalyzer.moveAndAnalyze(first.getRow(), first.getCol(), second.getRow(), second.getCol());

            if (lineFormationResult.isSuccessful()) {
                pointsCalculator.processLineFormationResult(lineFormationResult);
                successfullLinesPostProcessor.processLineFormationResult(lineFormationResult);
            } else {

                List<Ball> ballsToPlace = new ArrayList<>();
                int loopLimit = Integer.min(3, newBalls.size());
                for (int i = 0; i < loopLimit; i++) {
                    ballsToPlace.add(newBalls.remove(0));
                }

                LineFormationResult newBallsResult = new LineFormationResult();
                for (Ball ball : ballsToPlace) {
                    LineFormationResult newBallResult = lineFormationAnalyzer.placeAndAnalyze(ball);
                    newBallsResult.mix(newBallResult);
                }

                if (newBallsResult.isSuccessful()) {
                    pointsCalculator.processLineFormationResult(newBallsResult);
                    successfullLinesPostProcessor.processLineFormationResult(newBallsResult);
                }
            }
        }

        logger.log(field);

        return pointsCalculator.getPoints();
    }

    private interface LineFormationResultProcessor {
        void processLineFormationResult(LineFormationResult lineFormationResult);
        default void processLineFormationResults(List<LineFormationResult> lineFormationResults) {
            lineFormationResults.forEach(this::processLineFormationResult);
        }
    }

    private static class SuccessfullLinesPostProcessor implements LineFormationResultProcessor {
        private Field field;

        public SuccessfullLinesPostProcessor(Field field) {
            this.field = field;
        }

        @Override
        public void processLineFormationResult(LineFormationResult lineFormationResult) {
            lineFormationResult.getDifferentSucessfullBalls().forEach(field::removeBall);
        }
    }

    private static class PointsCalculator implements LineFormationResultProcessor{
        private int points = 0;

        @Override
        public void processLineFormationResult(LineFormationResult lineFormationResult) {
            if (lineFormationResult.isSuccessful()) {
                List<List<Ball>> differentLines = getOnlyDifferentLines(lineFormationResult.getSuccessfulLines());

                int A = differentLines.size();

                int B = differentLines.stream().mapToInt(List::size).sum();

                points += A + B - 1;
            }
        }

        private List<List<Ball>> getOnlyDifferentLines(List<List<Ball>> lines) {
            List<List<Ball>> differentLines = new ArrayList<>();

            for (List<Ball> nextLine : lines) {
                if (differentLines.isEmpty()) {
                    differentLines.add(nextLine);
                } else {
                    boolean addLine = true;
                    for (int i = 0; i < differentLines.size(); i++) {
                        List<Ball> differentLine = differentLines.get(i);

                        if (!isDifferent(nextLine, differentLine)) {
                            addLine = false;
                            break;
                        } else if (contains(nextLine, differentLine)) {
                            addLine = false;
                            differentLines.set(i, nextLine);
                            break;
                        }
                    }

                    if (addLine) {
                        differentLines.add(nextLine);
                    }
                }
            }

            return differentLines;
        }

        private boolean contains(List<Ball> origin, List<Ball> destination) {
            List<Point> originPoints = convertToPoints(origin);
            List<Point> destinationPoints = convertToPoints(destination);

            return originPoints.containsAll(destinationPoints);
        }

        private boolean isDifferent(List<Ball> origin, List<Ball> destination) {
            if (origin.size() != destination.size()) return true;

            List<Point> originPoints = convertToPoints(origin);
            List<Point> destinationPoints = convertToPoints(destination);

            return !originPoints.containsAll(destinationPoints);
        }

        private List<Point> convertToPoints(List<Ball> balls) {
            return balls.stream().map(ball -> new Point(ball.getRow(), ball.getCol())).collect(Collectors.toList());
        }

        public int getPoints() {
            return points;
        }
    }

    private static class LineFormationAnalyzer {
        private Field field;

        public LineFormationAnalyzer(Field field) {
            this.field = field;
        }

        public LineFormationResult placeAndAnalyze(Ball ball) {
            field.placeBall(ball);

            LineFormationResult lineFormationResult = new LineFormationResult();

            List<Ball> upLeftDownRightDiagonal = searchInLine(ball, -1, -1, 1, 1);
            List<Ball> downLeftUpRightDiagonal = searchInLine(ball, 1, -1, -1, 1);
            List<Ball> horizontal = searchInLine(ball, 0, -1, 0, 1);
            List<Ball> vertical = searchInLine(ball, -1, 0, 1, 0);

            fillLineFormationResult(lineFormationResult, upLeftDownRightDiagonal);
            fillLineFormationResult(lineFormationResult, downLeftUpRightDiagonal);
            fillLineFormationResult(lineFormationResult, horizontal);
            fillLineFormationResult(lineFormationResult, vertical);

            return lineFormationResult;
        }

        public LineFormationResult moveAndAnalyze(int rowOrigin, int colOrigin, int rowDest, int colDest) {
            Ball ball = field.moveBall(rowOrigin, colOrigin, rowDest, colDest);

            LineFormationResult lineFormationResult = new LineFormationResult();

            List<Ball> upLeftDownRightDiagonal = searchInLine(ball, -1, -1, 1, 1);
            List<Ball> downLeftUpRightDiagonal = searchInLine(ball, 1, -1, -1, 1);
            List<Ball> horizontal = searchInLine(ball, 0, -1, 0, 1);
            List<Ball> vertical = searchInLine(ball, -1, 0, 1, 0);

            fillLineFormationResult(lineFormationResult, upLeftDownRightDiagonal);
            fillLineFormationResult(lineFormationResult, downLeftUpRightDiagonal);
            fillLineFormationResult(lineFormationResult, horizontal);
            fillLineFormationResult(lineFormationResult, vertical);

            return lineFormationResult;
        }

        private void fillLineFormationResult(LineFormationResult lineFormationResult, List<Ball> balls) {
            if (!balls.isEmpty()) {
                lineFormationResult.addLine(balls);

                Set<Ball> uniqueBalls = new HashSet<>(balls);
                if (uniqueBalls.size() >= 5) {
                    lineFormationResult.addSuccessfulLine(new ArrayList<>(uniqueBalls));
                }
            }
        }

        private List<Ball> searchInLine(Ball origin, int firstRowStep, int firstColStep, int secondRowStep, int secondColStep) {
            List<Ball> foundBalls = new ArrayList<>();
            foundBalls.addAll(search(origin, firstRowStep, firstColStep));
            foundBalls.addAll(search(origin, secondRowStep, secondColStep));

            return foundBalls;
        }

        private List<Ball> search(Ball origin, int rowStep, int colStep) {
            List<Ball> ballsFound = new ArrayList<>();

            int currentRow = origin.getRow();
            int currentCol = origin.getCol();

            while (currentRow >= 0 && currentRow < field.getHeight() && currentCol >= 0 && currentCol < field.getLength()) {
                Ball currentBall = field.getBall(currentRow, currentCol);

                if (currentBall == null || !currentBall.getColor().equals(origin.getColor())) break;

                ballsFound.add(currentBall);
                currentRow += rowStep;
                currentCol += colStep;
            }

            return ballsFound;
        }
    }

    private static class LineFormationResult {
        private List<List<Ball>> formedLines;
        private List<List<Ball>> successfulLines;
        private Set<Ball> differentSucessfullBalls;

        public LineFormationResult() {
            this.formedLines = new ArrayList<>();
            this.successfulLines = new ArrayList<>();
            this.differentSucessfullBalls = new HashSet<>();
        }

        public void mix(LineFormationResult lineFormationResult) {
            this.formedLines.addAll(lineFormationResult.getFormedLines());
            this.successfulLines.addAll(lineFormationResult.getSuccessfulLines());
            this.differentSucessfullBalls.addAll(lineFormationResult.getDifferentSucessfullBalls());
        }

        public boolean isSuccessful() {
            return successfulLines.size() > 0;
        }

        public List<List<Ball>> getFormedLines() {
            return formedLines;
        }

        public List<List<Ball>> getSuccessfulLines() {
            return successfulLines;
        }

        public Set<Ball> getDifferentSucessfullBalls() {
            return differentSucessfullBalls;
        }

        public void addLine(List<Ball> balls) {
            this.formedLines.add(balls);
        }

        public void addSuccessfulLine(List<Ball> balls) {
            this.successfulLines.add(balls);
            this.differentSucessfullBalls.addAll(balls);
        }
    }

    private static class PathFinder {
        private Field field;

        public PathFinder(Field field) {
            this.field = field;
        }

        public List<Point> findPathBetweenPoints(Point origin, Point destination) {
            return findPath(
                    origin,
                    destination,
                    new ArrayList<>(),
                    new HashSet<>()
            );
        }

        public boolean clearPathExistBetweenPoints(Point origin, Point destination) {
            List<Point> path = findPathBetweenPoints(origin, destination);

            return path != null && !path.isEmpty();
        }

        private List<Point> findPath(Point origin, Point destination, List<Point> path, Set<Point> visitedPoints) {
            if (origin.equals(destination)) return path;
            visitedPoints.add(origin);

            if (origin.getCol() > 0) {
                List<Point> result = processPointInPath(origin.left(), destination, path, visitedPoints);
                if (result != null) return result;
            }

            if (origin.getCol() < field.getLength() - 1) {
                List<Point> result = processPointInPath(origin.right(), destination, path, visitedPoints);
                if (result != null) return result;
            }

            if (origin.getRow() > 0) {
                List<Point> result = processPointInPath(origin.up(), destination, path, visitedPoints);
                if (result != null) return result;
            }

            if (origin.getRow() < field.getHeight() - 1) {
                List<Point> result = processPointInPath(origin.down(), destination, path, visitedPoints);
                if (result != null) return result;
            }

            return null;
        }

        private List<Point> processPointInPath(Point newPoint, Point destination, List<Point> pointsInPath, Set<Point> visitedPoints) {
            if (field.hasBall(newPoint) || pointsInPath.contains(newPoint) || visitedPoints.contains(newPoint)) return null;

            List<Point> visitedPointsCopy = new ArrayList<>(pointsInPath);
            visitedPointsCopy.add(newPoint);
            return findPath(newPoint, destination, visitedPointsCopy, visitedPoints);
        }
    }

    private static class InteractionAnalyzer {
        private Field field;
        private PathFinder pathFinder;

        public InteractionAnalyzer(Field field, PathFinder pathFinder) {
            this.field = field;
            this.pathFinder = pathFinder;
        }

        public List<Click> getNextValidInteraction(List<Click> clicks) {
            if (clicks.size() > 1) {
                Click first = clicks.get(0);
                Click second = clicks.get(1);

                if (!clickHasBall(first)) {
                    clicks.remove(0);
                    return getNextValidInteraction(clicks);
                } else if (bothClicksHaveBalls(first, second)) {
                    clicks.remove(0);
                    return getNextValidInteraction(clicks);
                } else if (!pathFinder.clearPathExistBetweenPoints(first.toPoint(), second.toPoint())) {
                    clicks.remove(0);
                    clicks.remove(0);
                    return getNextValidInteraction(clicks);
                } else {
                    clicks.remove(0);
                    clicks.remove(0);

                    if (field.hasBall(first.toPoint())) {
                        return Arrays.asList(first, second);
                    } else {
                        return Arrays.asList(second, first);
                    }
                }
            }

            return new ArrayList<>();
        }

        public boolean bothClicksHaveBalls(Click first, Click second) {
            return clickHasBall(first) && clickHasBall(second);
        }

        private boolean clickHasBall(Click click) {
            return this.field.hasBall(click.getRow(), click.getCol());
        }

        private boolean isClickInsideField(Click click) {
            return click.getCol() >= 0 && click.getCol() < field.getLength() &&
                    click.getRow() >= 0 && click.getRow() < field.getHeight();
        }
    }

    private static class Point {
        private final int row;
        private final int col;

        public Point(int row, int col) {
            this.row = row;
            this.col = col;
        }

        public int getRow() {
            return row;
        }

        public int getCol() {
            return col;
        }

        public Point left() {
            return new Point(row, col - 1);
        }

        public Point right() {
            return new Point(row, col + 1);
        }

        public Point up() {
            return new Point(row - 1, col);
        }

        public Point down() {
            return new Point(row + 1, col);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Point point = (Point) o;
            return row == point.row &&
                    col == point.col;
        }

        @Override
        public int hashCode() {
            return Objects.hash(row, col);
        }

        @Override
        public String toString() {
            return "Point{" +
                    "row=" + row +
                    ", col=" + col +
                    '}';
        }
    }

    private static class Field {
        private Ball[][] matrix;
        private Logger logger = Logger.getInstance();

        public Field(char[][] inputField) {
            matrix = new Ball[inputField.length][inputField[0].length];

            for (int row = 0; row < inputField.length; row++) {
                for (int col = 0; col < inputField[row].length; col++) {
                    if (inputField[row][col] != '.') {
                        matrix[row][col] = new Ball(row, col, new Color(inputField[row][col]));
                    }
                }
            }
        }

        public void placeBall(Ball ball) {
//            if (hasBall(ball.getRow(), ball.getCol()))
//                throw new IllegalArgumentException(String.format("There is already a ball placed in row %s - col %s", ball.getRow(), ball.getCol()));
            matrix[ball.getRow()][ball.getCol()] = new Ball(ball);

            logger.log("Placing ball: " + ball);
            logger.log(this);
        }

        public boolean hasBall(Point point) {
            return hasBall(point.getRow(), point.getCol());
        }

        public boolean hasBall(int row, int col) {
            return getBall(row, col) != null;
        }

        public Ball getBall(int row, int col) {
            return matrix[row][col];
        }

        public Ball moveBall(int rowOrigin, int colOrigin, int rowDest, int colDest) {
            if (!hasBall(rowOrigin, colOrigin)) throw new IllegalArgumentException(String.format("Ball doesn't exist in %s - %s", rowOrigin, colOrigin));

            Ball currentBall = getBall(rowOrigin, colOrigin);
            matrix[rowOrigin][colOrigin] = null;
            Ball afterBall = new Ball(rowDest, colDest, currentBall.getColor());
            matrix[rowDest][colDest] = afterBall;

            logger.log(String.format("Moving ball from [%s, %s] to [%s, %s] - %s", rowOrigin, colOrigin, rowDest, colDest, currentBall));
            logger.log(this);

            return afterBall;
        }

        public boolean ballExists(Ball ball) {
            Ball storedBall = getBall(ball.getRow(), ball.getCol());

            return storedBall != null && storedBall.equals(ball);
        }

        public void removeBall(Ball ball) {
            if (!ballExists(ball)) throw new IllegalArgumentException(String.format("Ball doesn't exist in field [%s]", ball));
            matrix[ball.getRow()][ball.getCol()] = null;
        }

        public int getHeight() {
            return this.matrix.length;
        }

        public int getLength() {
            return this.matrix[0].length;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();

            sb.append("- |");
            for (int col = 0; col < matrix[0].length; col++) {
                sb.append(" ").append(col).append(" |");
            }
            sb.append("\n");

            for (int row = 0; row < matrix.length; row++) {
                sb.append(row).append(" | ");
                for (int col = 0; col < matrix[0].length; col++) {
                    Ball ball = matrix[row][col];
                    sb.append(ball == null ? " " : ball.getColor().getSymbol()).append(" - ");
                }
                sb.append("\n");
            }

            sb.append("******************************************");

            return sb.toString();
        }
    }

    private static class Click {
        private int row;
        private int col;

        public Click(int[] click) {
            this.row = click[0];
            this.col = click[1];
        }

        public static List<Click> createClicks(int[][] clicks) {
            return Arrays.stream(clicks).map(Click::new).collect(Collectors.toList());
        }

        public int getRow() {
            return row;
        }

        public int getCol() {
            return col;
        }

        public Point toPoint() {
            return new Point(row, col);
        }

        @Override
        public String toString() {
            return "Click{" +
                    "row=" + row +
                    ", col=" + col +
                    '}';
        }
    }

    private static class Ball {
        private final int row;
        private final int col;
        private final Color color;

        public Ball(int row, int col, Color color) {
            this.row = row;
            this.col = col;
            this.color = color;
        }

        public Ball(Ball ball) {
            this(ball.row, ball.col, new Color(ball.getColor().getSymbol()));
        }

        public static List<Ball> createNewBalls(char[] ballsColors, int[][] coordinates) {
            List<Ball> balls = new ArrayList<>();

            for (int i = 0; i < ballsColors.length; i++) {
                int[] currentCoordinates = coordinates[i];
                balls.add(new Ball(currentCoordinates[0], currentCoordinates[1], new Color(ballsColors[i])));
            }

            return balls;
        }

        public int getRow() {
            return row;
        }

        public int getCol() {
            return col;
        }

        public Color getColor() {
            return color;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Ball ball = (Ball) o;
            return row == ball.row &&
                    col == ball.col &&
                    Objects.equals(color, ball.color);
        }

        @Override
        public int hashCode() {
            return Objects.hash(row, col, color);
        }

        @Override
        public String toString() {
            return "Ball{" +
                    "row=" + row +
                    ", col=" + col +
                    ", color=" + color +
                    '}';
        }
    }

    private static class Color {
        private char symbol;

        public Color(char symbol) {
            this.symbol = symbol;
        }

        public char getSymbol() {
            return symbol;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Color color = (Color) o;
            return symbol == color.symbol;
        }

        @Override
        public int hashCode() {
            return Objects.hash(symbol);
        }

        @Override
        public String toString() {
            return "Color{" +
                    "symbol=" + symbol +
                    '}';
        }
    }

    private static class Logger {
        private boolean enabled;
        private static Logger logger;

        public void log(String s) {
            if (enabled) {
                System.out.println(s);
            }
        }

        public void log(Object o) {
            log(o.toString());
        }

        private Logger() {
            this.enabled = true;
        }

        public static Logger getInstance() {
            if (logger == null) {
                logger = new Logger();
            }
            return logger;
        }
    }
}
