package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AD_TetrisGame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.runtTest4());

//        Piece piece = PieceBuilder.build(PieceBuilder.Type.T);
//
//        for (int i = 0; i < 4; i++) {
//            System.out.println(new Piece(piece).rotate(i));
//        }
    }

    int tetrisGame(char[][][] pieces) {
        TetrisGame tetrisGame = new TetrisGame(new TetrisBoard());
        PlayingAlgorithm playingAlgorithm = new PlayingAlgorithm(tetrisGame);

        List<Piece> pieceList = new ArrayList<>();
        for (char[][] piece : pieces) {
            pieceList.add(new Piece(piece));
        }

        pieceList.forEach(System.out::println);

        playingAlgorithm.handlePieces(pieceList.toArray(new Piece[0]));

        return tetrisGame.getPoints();
    }

    int runTest1() {
        char[][][] pieces = {
                {
                        {'.','#','.'},
                        {'#','#','#'}
                }, 
                {
                         {'#','.','.'},
                         {'#','#','#'}
                }, 
                 {
                         {'#','#','.'},
                         {'.','#','#'}
                 },
                 {
                     {'#','#','#','#'}
                 },
                 {
                     {'#','#','#','#'}
                 },
                 {
                         {'#','#'},
                         {'#','#'}
                 }
        };

        return tetrisGame(pieces);
    }
    
    int runtTest4() {
        char[][][] pieces = {{{'.','#','#'},{'#','#','.'}}, 
                 {{'.','#','.'},{'#','#','#'}}, 
                 {{'#','#','.'},{'.','#','#'}}, 
                 {{'.','#','.'},{'#','#','#'}}, 
                 {{'#','#','#','#'}}, 
                 {{'#','.','.'},{'#','#','#'}}, 
                 {{'#','#'},{'#','#'}}, 
                 {{'#','#','#'},{'.','.','#'}}, 
                 {{'.','#','#'},{'#','#','.'}}, 
                 {{'.','#','.'},{'#','#','#'}}, 
                 {{'#','#','.'},{'.','#','#'}}, 
                 {{'.','#','.'},{'#','#','#'}}, 
                 {{'#','#','#','#'}}, 
                 {{'#','.','.'},{'#','#','#'}}, 
                 {{'#','#'},{'#','#'}}, 
                 {{'#','#','#'},{'.','.','#'}}};

        return tetrisGame(pieces);
    }

    void runSandbox() {
        TetrisGame tetrisGame = new TetrisGame(new TetrisBoard());
        PlayingAlgorithm playingAlgorithm = new PlayingAlgorithm(tetrisGame);

        playingAlgorithm.handlePieces(
                PieceBuilder.build(PieceBuilder.Type.L),
                PieceBuilder.build(PieceBuilder.Type.L).rotate(2),
                PieceBuilder.build(PieceBuilder.Type.SQUARE),
                PieceBuilder.build(PieceBuilder.Type.L).rotate(3),
                PieceBuilder.build(PieceBuilder.Type.S),
                PieceBuilder.build(PieceBuilder.Type.SQUARE),
                PieceBuilder.build(PieceBuilder.Type.S).rotate(1),
                PieceBuilder.build(PieceBuilder.Type.I).rotate(0),
                PieceBuilder.build(PieceBuilder.Type.SQUARE),
                PieceBuilder.build(PieceBuilder.Type.SQUARE),
                PieceBuilder.build(PieceBuilder.Type.SQUARE),
                PieceBuilder.build(PieceBuilder.Type.I).rotate(0)
        );

        System.out.println(tetrisGame.getPoints());
    }

    private static class TetrisGame {
        private TetrisBoard tetrisBoard;
        private int points = 0;

        public TetrisGame(TetrisBoard tetrisBoard) {
            this.tetrisBoard = tetrisBoard;
        }

        public boolean processPiece(Piece piece, int col) {
            BoardPiece boardPiece = this.tetrisBoard.receivePiece(piece, col);

            if (boardPiece == null) return false;

            List<Integer> completedRows = this.tetrisBoard.removeFilledRows();
            points += completedRows.size();

            System.out.println(tetrisBoard);

            return true;
        }

        public int getPoints() {
            return points;
        }

        public TetrisBoard getTetrisBoard() {
            return new TetrisBoard(tetrisBoard);
        }
    }

    private static class PlayingAlgorithm {
        private TetrisGame tetrisGame;

        public PlayingAlgorithm(TetrisGame tetrisGame) {
            this.tetrisGame = tetrisGame;
        }

        public void handlePieces(Piece... pieces) {
            for (Piece piece : pieces) {
                handlePiece(piece);
                int a = 0;
            }
        }

        public void handlePiece(Piece piece) {
            TetrisBoard tetrisBoard = tetrisGame.getTetrisBoard();

            final List<BoardPieceStatistics> boardPieceStatisticsList = generateStatisticsForPiece(piece, tetrisBoard);

            final List<BoardPieceStatistics> maximizedBlocks = getBoardStatisticsWithMaxAmountOfBlocksFilled(boardPieceStatisticsList);

            final List<BoardPieceStatistics> minRotationsBlocks = getBoardStatisticsWithMinAmountOfRotations(maximizedBlocks);

            final List<BoardPieceStatistics> leftMostBlocks = getLeftMostBoardStatistics(minRotationsBlocks);

            if (leftMostBlocks.size() > 1) {
                throw new IllegalStateException("Not sure but maybe there should be only one guy here");
            }

            BoardPieceStatistics boardPieceStatistics = leftMostBlocks.get(0);
            Piece finalPiece = piece.rotate(boardPieceStatistics.getRotation());
            int finalCol = boardPieceStatistics.getCol();

            tetrisGame.processPiece(finalPiece, finalCol);
        }

        private List<BoardPieceStatistics> getBoardStatisticsWithMaxAmountOfBlocksFilled(List<BoardPieceStatistics> boardPieceStatisticsList) {
            final Map<Integer, List<BoardPieceStatistics>> statisticsByTotalBlocksFilleds = boardPieceStatisticsList.stream()
                    .collect(Collectors.groupingBy(BoardPieceStatistics::getTotalBlocks));

            final List<BoardPieceStatistics> maximizedBlocks = statisticsByTotalBlocksFilleds.get(
                    statisticsByTotalBlocksFilleds.keySet().stream()
                            .mapToInt(i -> i).max().orElse(-1)
            );

            if (maximizedBlocks == null || maximizedBlocks.isEmpty()) {
                throw new IllegalStateException("There should be at least one row of blocks occupied by this piece");
            }

            return maximizedBlocks;
        }

        private List<BoardPieceStatistics> getBoardStatisticsWithMinAmountOfRotations(List<BoardPieceStatistics> boardPieceStatisticsList) {
            final Map<Integer, List<BoardPieceStatistics>> statisticsByNumberOfRotations = boardPieceStatisticsList.stream()
                    .collect(Collectors.groupingBy(BoardPieceStatistics::getRotation));

            final List<BoardPieceStatistics> minRotationsList = statisticsByNumberOfRotations.get(
                    statisticsByNumberOfRotations.keySet().stream()
                            .mapToInt(i -> i).min().orElse(-1)
            );

            if (minRotationsList == null || minRotationsList.isEmpty()) {
                throw new IllegalStateException("There should be at least one piece here rotated in any way");
            }

            return minRotationsList;
        }

        private List<BoardPieceStatistics> getLeftMostBoardStatistics(List<BoardPieceStatistics> boardPieceStatisticsList) {
            final Map<Integer, List<BoardPieceStatistics>> statisticsByColumnNumber = boardPieceStatisticsList.stream()
                    .collect(Collectors.groupingBy(BoardPieceStatistics::getCol));

            final List<BoardPieceStatistics> leftMostList = statisticsByColumnNumber.get(
                    statisticsByColumnNumber.keySet().stream()
                            .mapToInt(i -> i).min().orElse(-1)
            );

            if (leftMostList == null || leftMostList.isEmpty()) {
                throw new IllegalStateException("There should be at least one piece here rotated in any way");
            }

            return leftMostList;
        }

        private List<BoardPieceStatistics> generateStatisticsForPiece(Piece piece, TetrisBoard tetrisBoard) {
            List<BoardPieceStatistics> boardPieceStatisticsList = new ArrayList<>();

            for (int rotation = 0; rotation < 4; rotation++) {
                Piece rotatedPiece = new Piece(piece).rotate(rotation);
                for (int col = 0; col <= tetrisBoard.getBoardLength() - rotatedPiece.getLength(); col++) {
                    BoardPiece boardPiece = tetrisBoard.receivePiece(rotatedPiece, col);
                    BoardPieceStatistics boardPieceStatistics = new BoardPieceStatistics(boardPiece);
                    boardPieceStatisticsList.add(boardPieceStatistics);

                    for (int prow = boardPiece.getRow(); prow < boardPiece.getRow() + boardPiece.getPiece().getHeight(); prow++) {
                        int rowBlocksCount = 0;
                        for (int countCol = 0; countCol < tetrisBoard.getBoardLength(); countCol++) {
                            if (tetrisBoard.isFilled(prow, countCol)) rowBlocksCount++;
                        }
                        boardPieceStatistics.addRowBlocks(rowBlocksCount);
                    }

                    tetrisBoard.removeLastPiece();
                }
            }

            return boardPieceStatisticsList;
        }
    }

    private static class BoardPieceStatistics {
        private int row;
        private int col;
        private List<Integer> blocksByRow;
        private int rotation;

        public BoardPieceStatistics(BoardPiece boardPiece) {
            this(boardPiece.getRow(), boardPiece.getCol(), boardPiece.getPiece().getRotation());
        }

        public BoardPieceStatistics(int row, int col, int rotation) {
            this.row = row;
            this.col = col;
            this.rotation = rotation;
            this.blocksByRow = new ArrayList<>();
        }

        public int getTotalBlocks() {
            return blocksByRow.stream().mapToInt(i -> i).sum();
        }

        public void addRowBlocks(int blocks) {
            this.blocksByRow.add(blocks);
        }

        public int getRow() {
            return row;
        }

        public int getCol() {
            return col;
        }

        public List<Integer> getBlocksByRow() {
            return blocksByRow;
        }

        public int getRotation() {
            return rotation;
        }
    }

    private static class PieceBuilder {
        public enum Type {
            SQUARE, L, I_L, S, I_S, I, T
        }

        public static Piece build(Type type) {
            switch (type) {
                case SQUARE: {
                    return new Piece(new char[][] {
                            {'#', '#'},
                            {'#', '#'}
                    });
                }
                case L: {
                    return new Piece(new char[][] {
                            {'.', '#'},
                            {'.', '#'},
                            {'#', '#'}
                    });
                }
                case I_L: {
                    return new Piece(new char[][] {
                            {'#', '.'},
                            {'#', '.'},
                            {'#', '#'}
                    });
                }
                case S: {
                    return new Piece(new char[][] {
                            {'.', '#'},
                            {'#', '#'},
                            {'#', '.'}
                    });
                }
                case I_S: {
                    return new Piece(new char[][] {
                            {'#', '.'},
                            {'#', '#'},
                            {'.', '#'}
                    });
                }
                case I: {
                    return new Piece(new char[][] {
                            {'#'},
                            {'#'},
                            {'#'},
                            {'#'}
                    });
                }
                case T: {
                    return new Piece(new char[][] {
                            {'.', '#', '.'},
                            {'#', '#', '#'},
                    });
                }
                default: throw new IllegalArgumentException("BLA");
            }
        }
    }

    private static class TetrisBoard {
        private Character[][] board = new Character[20][10];
        private List<BoardPiece> pieceList = new ArrayList<>();
        private char nextPieceIdentifier = 'A';

        public TetrisBoard() {}

        public TetrisBoard(TetrisBoard origin) {
            for (int row = 0; row < origin.board.length; row++) {
                for (int col = 0; col < origin.board[row].length; col++) {
                    this.board[row][col] = origin.board[row][col];
                }
            }

            this.pieceList = origin.pieceList.stream()
                    .peek(BoardPiece::new)
                    .collect(Collectors.toList());

            this.nextPieceIdentifier = origin.nextPieceIdentifier;
        }

        public BoardPiece receivePiece(Piece piece, int column) {
            return receivePiece(piece, 0, column);
        }

        private BoardPiece receivePiece(Piece piece, int row, int column) {
            BoardPiece boardPiece = new BoardPiece(piece, row, column, nextPieceIdentifier++);

            return receivePiece(boardPiece);
        }

        private BoardPiece receivePiece(BoardPiece pieceBoard) {
            int column = pieceBoard.getCol();
            int row = pieceBoard.getRow();
            Piece piece = pieceBoard.getPiece();
            char identifier = pieceBoard.getIdentifier();

            if (column < 0 || column + piece.getLength() > this.board[0].length) return null;

            Integer lastWorkingRow = null;
            boolean continueSearch = true;
            for (; row < board.length - piece.getHeight() + 1 && continueSearch; row++) {
                boolean workingRow = true;
                for (int prow = 0; prow < piece.getHeight() && workingRow; prow++) {
                    for (int pcol = 0; pcol < piece.getLength() && workingRow; pcol++) {
                        if (piece.getStructure()[prow][pcol] && board[row + prow][column + pcol] != null) {
                            workingRow = false;
                            if (lastWorkingRow == null) {
                                throw new IllegalStateException("Can not allocate next piece in column " + column);
                            }
                        }
                    }
                }

                if (workingRow) {
                    lastWorkingRow = row;
                } else {
                    continueSearch = false;
                }
            }

            if (lastWorkingRow == null) {
                throw new IllegalStateException("lastWorkingRow should not be null at this point");
            }

            BoardPiece boardPiece = new BoardPiece(piece, lastWorkingRow, column, identifier);
            addPieceToBoard(boardPiece);

            return boardPiece;
        }

        public boolean isFilled(int row, int col) {
            return this.board[row][col] != null;
        }

        public int getBoardLength() {
            return this.board[0].length;
        }

        private void addPieceToBoard(BoardPiece boardPiece) {
            this.pieceList.add(boardPiece);
            calculateBoardStatus();
        }

        public List<Integer> getFilledRows() {
            List<Integer> rowList = new ArrayList<>();

            for (int rowIndex = 0; rowIndex < this.board.length; rowIndex++) {
                Character[] row = this.board[rowIndex];
                if (Arrays.stream(row).allMatch(Objects::nonNull)) {
                    rowList.add(rowIndex);
                }
            }

            return rowList;
        }

        public List<Integer> removeFilledRows() {
            List<Integer> filledRows = getFilledRows();
            filledRows.forEach(this::deleteRow);
            return filledRows;
        }

        public void removeLastPiece() {
            if (this.pieceList.isEmpty()) return;
            removePiece(this.pieceList.size() - 1);
            this.nextPieceIdentifier--;
        }

        public void deleteRow(int row) {
            List<BoardPiece> temp = this.pieceList.stream()
                    .peek(boardPiece -> boardPiece.processRowRemoval(row))
                    .filter(boardPiece -> !boardPiece.isEmpty())
                    .collect(Collectors.toList());

            removeAllPieces();

            temp.forEach(this::receivePiece);
        }

        private void removePiece(int pieceIndex) {
            this.pieceList.remove(pieceIndex);
            calculateBoardStatus();
        }

        private void removeAllPieces() {
            this.pieceList.clear();
            calculateBoardStatus();
        }

        private void calculateBoardStatus() {
            this.board = new Character[20][10];

            this.pieceList.forEach(boardPiece -> {
                for (int prow = 0; prow < boardPiece.getPiece().getHeight(); prow++) {
                    for (int pcol = 0; pcol < boardPiece.getPiece().getLength(); pcol++) {
                        if (boardPiece.getPiece().getStructure()[prow][pcol]) {
                            this.board[boardPiece.getRow() + prow][boardPiece.getCol() + pcol] = boardPiece.getIdentifier();
                        }
                    }
                }
            });
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();

            for (int row = 0; row < board.length; row++) {
                sb.append(String.format("%2d", row)).append(" | ");
                for (int col = 0; col < board[row].length; col++) {
                    Character value = board[row][col];

                    if (value == null) {
                        sb.append('-');
                    } else {
                        sb.append(board[row][col]);
                    }
                    sb.append(" ");
                }
                sb.append("\n");
            }

            return sb.toString();
        }
    }

    private static class BoardPiece {
        private Piece piece;
        private int row;
        private int col;
        private char identifier;

        public BoardPiece(Piece piece, int row, int col, char identifier) {
            this.piece = piece;
            this.row = row;
            this.col = col;
            this.identifier = identifier;
        }

        public BoardPiece(BoardPiece origin) {
            this(new Piece(origin.piece), origin.row, origin.col, origin.identifier);
        }

        public void processRowRemoval(int row) {
            if (row >= this.row && row < this.row + piece.getHeight()) {
                piece.removeRow(row - this.row);
                this.row++;
            }
        }

        public boolean isEmpty() {
            return this.piece.isEmpty();
        }

        public Piece getPiece() {
            return piece;
        }

        public int getRow() {
            return row;
        }

        public int getCol() {
            return col;
        }

        public char getIdentifier() {
            return identifier;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            BoardPiece that = (BoardPiece) o;
            return identifier == that.identifier;
        }

        @Override
        public int hashCode() {
            return Objects.hash(identifier);
        }
    }

    private static class Piece {
        private boolean[][] structure;
        private int rotation = 0;

        public Piece(char[][] inputRepresentation) {
            structure = new boolean[inputRepresentation.length][inputRepresentation[0].length];

            for (int row = 0; row < inputRepresentation.length; row++) {
                for (int col = 0; col < inputRepresentation[row].length; col++) {
                    char current = inputRepresentation[row][col];
                    if (current == '#') {
                        structure[row][col] = true;
                    }
                }
            }
        }

        public Piece(Piece origin) {
            this.structure = new boolean[origin.getStructure().length][origin.getStructure()[0].length];
            for (int row = 0; row < origin.getStructure().length; row++) {
                for (int col = 0; col < origin.getStructure()[row].length; col++) {
                    this.structure[row][col] = origin.getStructure()[row][col];
                }
            }

            this.rotation = origin.getRotation();
        }

        public Piece rotate() {
            boolean[][] newStructure = new boolean[structure[0].length][structure.length];

//            for (int row = 0; row < structure.length; row++) {
//                for (int col = 0; col < structure[row].length; col++) {
//                    newStructure[structure[0].length - 1 - col][row] = structure[row][col];
//                }
//            }

            for (int row = 0; row < structure.length; row++) {
                for (int col = 0; col < structure[row].length; col++) {
                    newStructure[col][structure.length - 1 - row] = structure[row][col];
                }
            }

            this.structure = newStructure;
            this.rotation = (this.rotation + 1) % 4;

            return this;
        }

        public Piece rotate(int times) {
            int fixedTimes = times % 4;
            for (int i = 0; i < fixedTimes; i++) rotate();
            return this;
        }

        public void removeRow(int row) {
            if (row < 0 || row >= getHeight()) {
                throw new IllegalArgumentException("Unable to delete piece row. This piece doesn't have that amount of rows!");
            }

            this.structure = IntStream.range(0, getHeight()).filter(current -> current != row)
                    .mapToObj(current -> this.structure[current])
                    .toArray(boolean[][]::new);
        }

        public int getLength() {
            return this.structure[0].length;
        }

        public int getHeight() {
            return this.structure.length;
        }

        public boolean[][] getStructure() {
            return structure;
        }

        public int getRotation() {
            return rotation;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            for (boolean[] row : this.structure) {
                for (int col = 0; col < row.length; col++) {
                    if (row[col]) sb.append("#");
                    else sb.append(" ");
                }
                sb.append("\n");
            }

            return sb.toString();
        }

        public boolean isEmpty() {
            if (this.structure.length == 0) return true;
            for (boolean[] row : this.structure) {
                for (boolean b : row) {
                    if (b) return false;
                }
            }
            return true;
        }
    }
}
