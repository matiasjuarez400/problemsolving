package mathecat.codesignal.arcade.thecore.notsortedyet._11._97;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String[] names = {"doc", "doc", "image", "doc(1)", "doc"};

        System.out.println(Arrays.toString(solution.fileNaming(names)));
    }

    String[] fileNaming(String[] names) {
        Map<String, Integer> counterByName = new HashMap<>();

        String[] result = new String[names.length];
        for (int i = 0; i < names.length; i++) {
            String name = names[i];
            Integer counter = counterByName.get(name);
            if (counter == null) {
                result[i] = name;
                counterByName.put(name, 1);
            } else {
                while(true) {
                     String newName = String.format("%s(%s)", name, counter);
                     if (counterByName.containsKey(newName)) counter++;
                     else {
                         result[i] = newName;
                         counterByName.put(name, counter + 1);
                         counterByName.put(result[i], 1);
                         break;
                     }
                }
            }
        }

        return result;
    }
}
