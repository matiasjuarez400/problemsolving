package mathecat.codesignal.arcade.thecore.notsortedyet._15._127;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.chessTriangle(2, 3));
    }

    int chessTriangle(int n, int m) {
        Board board = new Board(n, m);
        Knight knight = new Knight(0, 0, board);
        Bishop bishop = new Bishop(0, 0, board);
        Rock rock = new Rock(0, 0, board);

        int triangleConfigurations = 0;
        for (int r = 0; r < n; r++) {
            for (int c = 0; c < m; c++) {
                knight.setPosition(r, c);
                List<Cell> knightMoves = knight.getPossibleMoves();

                for (Cell knightMove : knightMoves) {
                    bishop.setPosition(knightMove);

                    List<Cell> bishopMoves = bishop.getPossibleMoves();
                    for (Cell bishopMove : bishopMoves) {
                        rock.setPosition(bishopMove);

                        if (rock.canCapture(knight)) {
                            triangleConfigurations++;
                        }
                    }

                    rock.setPosition(knightMove);

                    List<Cell> rockMoves = rock.getPossibleMoves();
                    for (Cell rockMove : rockMoves) {
                        bishop.setPosition(rockMove);

                        if (bishop.canCapture(knight)) {
                            triangleConfigurations++;
                        }
                    }
                }
            }
        }

        return triangleConfigurations;
    }

    private static class Cell {
        private int row;
        private int col;

        public Cell(int row, int col) {
            this.row = row;
            this.col = col;
        }

        public int getRow() {
            return row;
        }

        public int getCol() {
            return col;
        }

        public void setRow(int row) {
            this.row = row;
        }

        public void setCol(int col) {
            this.col = col;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Cell cell = (Cell) o;
            return row == cell.row &&
                    col == cell.col;
        }

        @Override
        public int hashCode() {
            return Objects.hash(row, col);
        }
    }

    private abstract static class Piece {
        protected Cell position;
        protected Board board;

        public Piece(int row, int col, Board board) {
            this.position = new Cell(row, col);
            this.board = board;
        }

        public void setPosition(int row, int col) {
            setCol(col);
            setRow(row);
        }

        public void setPosition(Cell position) {
            setPosition(position.getRow(), position.getCol());
        }

        public void setRow(int row) {
            if (row < 0 || row >= board.getRows()) throw new IllegalArgumentException("The specified ROW is out of bounds");
            this.position.setRow(row);
        }

        public void setCol(int col) {
            if (col < 0 || col >= board.getCols()) throw new IllegalArgumentException("The specified COL is out of bounds");
            this.position.setCol(col);
        }

        public Cell getPosition() {
            return position;
        }

        public Board getBoard() {
            return board;
        }

        public abstract List<Cell> getPossibleMoves();

        public boolean canCapture(Piece piece) {
            List<Cell> moves = getPossibleMoves();

            return moves.stream().anyMatch(cell -> piece.getPosition().equals(cell));
        }
    }

    private static class Board {
        private int rows;
        private int cols;

        public Board(int rows, int cols) {
            this.rows = rows;
            this.cols = cols;
        }

        public int getRows() {
            return rows;
        }

        public int getCols() {
            return cols;
        }
    }

    private static class Knight extends Piece {

        public Knight(int row, int col, Board board) {
            super(row, col, board);
        }

        @Override
        public List<Cell> getPossibleMoves() {
            int row = getPosition().getRow();
            int col = getPosition().getCol();

            List<Cell> candidates = Arrays.asList(
                new Cell(row - 1, col - 2),
                new Cell(row - 1, col + 2),
                new Cell(row + 1, col - 2),
                new Cell(row + 1, col + 2),
                new Cell(row - 2, col - 1),
                new Cell(row - 2, col + 1),
                new Cell(row + 2, col - 1),
                new Cell(row + 2, col + 1)
            );

            return candidates.stream().filter(cell -> {
                int r = cell.getRow();
                int c = cell.getCol();

                return r >= 0 && r < getBoard().getRows() && c >= 0 && c < getBoard().getCols();
            }).collect(Collectors.toList());
        }
    }

    private static class Bishop extends Piece {

        public Bishop(int row, int col, Board board) {
            super(row, col, board);
        }

        @Override
        public List<Cell> getPossibleMoves() {
            List<Cell> moves = new ArrayList<>();

            Cell workingCell = this.getPosition();
            while (workingCell.getCol() >= 0 && workingCell.getRow() >= 0) {
                moves.add(workingCell);
                workingCell = new Cell(workingCell.getRow() - 1, workingCell.getCol() - 1);
            }

            workingCell = this.getPosition();
            while (workingCell.getCol() < getBoard().getCols() && workingCell.getRow() >= 0) {
                moves.add(workingCell);
                workingCell = new Cell(workingCell.getRow() - 1, workingCell.getCol() + 1);
            }

            workingCell = this.getPosition();
            while (workingCell.getCol() < getBoard().getCols() && workingCell.getRow() < getBoard().getRows()) {
                moves.add(workingCell);
                workingCell = new Cell(workingCell.getRow() + 1, workingCell.getCol() + 1);
            }

            workingCell = this.getPosition();
            while (workingCell.getCol() >= 0 && workingCell.getRow() < getBoard().getRows()) {
                moves.add(workingCell);
                workingCell = new Cell(workingCell.getRow() + 1, workingCell.getCol() - 1);
            }

            moves.remove(this.getPosition());

            return moves;
        }

        @Override
        public boolean canCapture(Piece piece) {
            return Math.abs(piece.getPosition().getRow() - this.getPosition().getRow()) == Math.abs(piece.getPosition().getCol() - this.getPosition().getCol());
        }
    }

    private static class Rock extends Piece {

        public Rock(int row, int col, Board board) {
            super(row, col, board);
        }

        @Override
        public List<Cell> getPossibleMoves() {
            List<Cell> moves = new ArrayList<>();

            for (int r = 0; r < getBoard().getRows(); r++) {
                moves.add(new Cell(r, getPosition().getCol()));
            }

            for (int c = 0; c < getBoard().getCols(); c++) {
                moves.add(new Cell(getPosition().getRow(), c));
            }

            moves.remove(getPosition());

            return moves;
        }

        @Override
        public boolean canCapture(Piece piece) {
            return piece.getPosition().getRow() == this.getPosition().getRow() || piece.getPosition().getCol() == this.getPosition().getCol();
        }
    }
}
