package mathecat.codesignal.arcade.thecore.notsortedyet._10laboftransformation;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class _87AlphanumericLess {
    public static void main(String[] args) {
        List<Token> tokens = getTokens("ab01c004");

        System.out.println(tokens);
    }

    static boolean alphanumericLess(String s1, String s2) {
        List<Token> ts1 = getTokens(s1);
        List<Token> ts2 = getTokens(s2);
        int limit = Integer.min(ts1.size(), ts2.size());

        Integer trailingZerosDiff = null;
        for (int i = 0; i < limit; i++) {
            Token t1 = ts1.get(i);
            Token t2 = ts2.get(i);

            if (trailingZerosDiff == null && t1.isNumeric() && t2.isNumeric()) {
                trailingZerosDiff = t1.getTrailingZeros() - t2.getTrailingZeros();
            }

            int comparison = t1.compareTo(t2);

            if (comparison == 0) continue;
            return comparison < 0;
        }

        if (ts1.size() != ts2.size()) return ts1.size() - ts2.size() < 0;

        if (trailingZerosDiff != null) return trailingZerosDiff > 0;

        return false;
    }

    static List<Token> getTokens(String s) {
        List<Token> tokens = new ArrayList<>();

        int i = 0;
        while (i < s.length()) {
            char current = s.charAt(i);
            if (Character.isLetter(current)) {
                tokens.add(new Token(current + "", TokenType.CHAR));
                i++;
            }
            else {
                String token = "";

                char nextChar;
                while (i < s.length() && Character.isDigit(nextChar = s.charAt(i))) {
                    token += nextChar;
                    i++;
                }

                tokens.add(new Token(token, TokenType.NUMBER));
            }
        }

        return tokens;
    }

    private static class Token {
        String value;
        TokenType tokenType;

        public Token(String value, TokenType tokenType) {
            this.value = value;
            this.tokenType = tokenType;
        }

        public String getValue() {
            return value;
        }

        public TokenType getTokenType() {
            return tokenType;
        }

        public boolean isNumeric() {
            return this.tokenType == TokenType.NUMBER;
        }

        int getTrailingZeros() {
            if (tokenType == TokenType.NUMBER) {
                int acc = 0;
                for (char c : value.toCharArray()) {
                    if (c == '0') acc++;
                    else break;
                }
                return acc;
            } else return 0;
        }

        int compareTo(Token anotherToken) {
            if (this.tokenType == anotherToken.tokenType) {
                if (this.tokenType == TokenType.NUMBER) {
                    return new BigInteger(this.value).compareTo(new BigInteger(anotherToken.value));
                } else {
                    return value.compareTo(anotherToken.value);
                }
            } else {
                if (this.tokenType == TokenType.NUMBER) return -1;
                else return 1;
            }
        }
    }

    private enum TokenType {
        NUMBER, CHAR
    }
}
