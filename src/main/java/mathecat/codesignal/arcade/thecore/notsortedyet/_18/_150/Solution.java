package mathecat.codesignal.arcade.thecore.notsortedyet._18._150;

import java.util.Arrays;
import java.util.stream.IntStream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String[] table =
                {"+----+--+-----+----+",
                "|abcd|56|!@#$%|qwer|",
                "|1234|78|^&=()|tyui|",
                "+----+--+-----+----+",
                "|zxcv|90|77777|stop|",
                "+----+--+-----+----+",
                "|asdf|~~|ghjkl|100$|",
                "+----+--+-----+----+"};

        int[][] coords = {{2, 0}, {1, 1}};
        String[] solved = solution.cellsJoining(table, coords);

        for (String row : solved) System.out.println(row);
    }

    String[] cellsJoining(String[] table, int[][] coords) {
        int upRow = coords[1][0];
        int rightCol = coords[1][1];
        int downRow = coords[0][0];
        int leftCol = coords[0][1];

        char[][] matrix = convertToMatrix(table);

        int[] colSeparatorIndexes = IntStream.range(0, matrix[0].length)
                .filter(i -> matrix[0][i] == '+')
                .toArray();

        int[] rowSeparatorIndexes = IntStream.range(0, matrix.length)
                .filter(i -> matrix[i][0] == '+')
                .toArray();

        int leftSepCol = colSeparatorIndexes[leftCol];
        int rightSepCol = colSeparatorIndexes[rightCol + 1];
        int upSepRow = rowSeparatorIndexes[upRow];
        int downSepRow = rowSeparatorIndexes[downRow + 1];

        for (int sepRow = upRow; sepRow <= downRow + 1 ; sepRow++) {
            int row = rowSeparatorIndexes[sepRow];
            char[] currentRow = matrix[row];
            for (int col = leftSepCol + 1; col < rightSepCol; col++) {
                if (currentRow[col] == '+') {
                    if (row == 0 || row == matrix.length - 1) {
                        currentRow[col] = '-';
                    } else if (row > upSepRow && row < downSepRow) {
                        currentRow[col] = ' ';
                    }
                } else if (currentRow[col] == '-') {
                    if (row > upSepRow && row < downSepRow) {
                        currentRow[col] = ' ';
                    }
                }
            }
        }

        for (int sepCol = leftCol; sepCol <= rightCol + 1 ; sepCol++) {
            int col = colSeparatorIndexes[sepCol];
            for (int row = upSepRow + 1; row < downSepRow; row++) {
                char currentChar = matrix[row][col];
                if (currentChar == '+') {
                    if (col == 0 || col == matrix[0].length - 1) {
                        matrix[row][col] = '|';
                    } else if (col > leftSepCol && col < rightSepCol) {
                        matrix[row][col] = ' ';
                    }
                } else if (matrix[row][col] == '|') {
                    if (col > leftSepCol && col < rightSepCol) {
                        matrix[row][col] = ' ';
                    }
                }
            }
        }

        return colapseMatrix(matrix);
    }

    String[] colapseMatrix(char[][] matrix) {
        return Arrays.stream(matrix).map(String::new).toArray(String[]::new);
    }

    char[][] convertToMatrix(String[] table) {
        return Arrays.stream(table).map(String::toCharArray).toArray(char[][]::new);
    }
}
