package mathecat.codesignal.arcade.thecore.notsortedyet._11._95;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] startPosition = {1, 4, 2};
        int[] speed = {27, 18, 24};

        System.out.println(solution.runnersMeetings(startPosition, speed));
    }

    int runnersMeetings(int[] startPosition, int[] speed) {
        List<Runner2> runner2List = IntStream.range(0, startPosition.length).boxed()
                .map(i -> new Runner2(startPosition[i], speed[i]))
                .collect(Collectors.toList());

        int maxMeetingCardinality = -1;
        for (int i = 0; i < runner2List.size(); i++) {
            Runner2 r1 = runner2List.get(i);
            for (int j = i + 1; j < runner2List.size(); j++) {
                Runner2 r2 = runner2List.get(j);

                Double meetingTime = findMeetingTime(r1, r2);

                if (meetingTime == null) continue;

                Double meetingPosition = findPositionForTime(r1, meetingTime);

                int meetingCardinality = 2;

                for (int k = j + 1; k < runner2List.size(); k++) {
                    Runner2 r3 = runner2List.get(k);
                    Double positionForMeetingTime = findPositionForTime(r3, meetingTime);

                    if (meetingPosition.equals(positionForMeetingTime)) meetingCardinality++;
                }

                if (meetingCardinality > maxMeetingCardinality) maxMeetingCardinality = meetingCardinality;
            }
        }

        return maxMeetingCardinality;
    }

    private Double findPositionForTime(Runner2 r, Double time) {
        return r.getSpeed() * time + r.getStartPosition();
    }

    private Double findMeetingTime(Runner2 r1, Runner2 r2) {
        double speedDifference = r1.getSpeed() - r2.getSpeed();
        if (speedDifference == 0) return null;

        double startPositionDifference = r2.getStartPosition() - r1.getStartPosition();
        double meetingPoint = startPositionDifference / speedDifference;
        return meetingPoint;
    }

    // This didn't work because the problem is supposed to handle continuous time (This wasn't specified in the description)
    int runnersMeetings2(int[] startPosition, int[] speed) {
        List<Runner> runnersSortedBySpeed = IntStream.range(0, startPosition.length).boxed()
                .map(i -> new Runner(startPosition[i], speed[i]))
                .sorted(Comparator.comparing(Runner::getSpeed).thenComparingInt(Runner::getId))
                .collect(Collectors.toList());

        Comparator<Runner> comparatorByPosition = Comparator.comparing(Runner::getCurrentPosition).thenComparingInt(Runner::getId);
        List<Runner> runnersSortedByPosition = new ArrayList<>(runnersSortedBySpeed);
        runnersSortedByPosition.sort(comparatorByPosition);

        int maxCardinality = -1;
        boolean listsMatch = false;
        while (!listsMatch) {
            Map<String, List<Runner>> runnersByPosition = runnersSortedByPosition.stream()
                    .collect(Collectors.groupingBy(runner -> runner.getCurrentPosition().toString()));
            for (String positionKey : runnersByPosition.keySet()) {
                int meetingSize = runnersByPosition.get(positionKey).size();
                if (meetingSize > 1 && meetingSize > maxCardinality) {
                    maxCardinality = meetingSize;
                }
            }

            listsMatch = true;
            for (int i = 0; i < runnersSortedBySpeed.size(); i++) {
                if (runnersSortedBySpeed.get(i).getId() != runnersSortedByPosition.get(i).getId()) {
                    listsMatch = false;
                    break;
                }
            }

            if (!listsMatch) {
                runnersSortedByPosition.forEach(Runner::run);
                runnersSortedByPosition.sort(comparatorByPosition);
            }
        }

        return maxCardinality;
    }

    private static class Runner2 {
        private static int nextId = 1;
        private int id;
        private int startPosition;
        private int speed;

        public Runner2(int startPosition, int speed) {
            this.startPosition = startPosition;
            this.speed = speed;
            this.id = nextId++;
        }

        public int getStartPosition() {
            return startPosition;
        }

        public int getSpeed() {
            return speed;
        }
    }

    private static class Runner {
        private static int nextId = 1;
        private int id;
        private BigDecimal startPosition;
        private BigDecimal speed;
        private BigDecimal currentPosition;

        public Runner(int startPosition, int speed) {
            this.startPosition = BigDecimal.valueOf(startPosition);
            this.speed = BigDecimal.valueOf(speed);
            this.currentPosition = BigDecimal.valueOf(startPosition);
            this.id = nextId++;
        }

        public BigDecimal run() {
            this.currentPosition = currentPosition.add(speed.divide(BigDecimal.valueOf(3333), 10, BigDecimal.ROUND_HALF_EVEN)).setScale(10, BigDecimal.ROUND_HALF_EVEN);
            System.out.println("Runner " + id + " - " + currentPosition.toString());
            return currentPosition;
        }

        public int getId() {
            return id;
        }

        public BigDecimal getStartPosition() {
            return startPosition;
        }

        public BigDecimal getSpeed() {
            return speed;
        }

        public BigDecimal getCurrentPosition() {
            return currentPosition;
        }
    }
}
