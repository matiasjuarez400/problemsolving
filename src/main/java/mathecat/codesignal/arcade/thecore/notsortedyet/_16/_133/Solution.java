package mathecat.codesignal.arcade.thecore.notsortedyet._16._133;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

class Solution {
    public static void main(String[] args) throws ParseException {
        Solution solution = new Solution();

        String someTime = "2015-01-14 09:12";
        String leavingTime = "2015-11-04 17:36";

        System.out.println(solution.curiousClock(someTime, leavingTime));
    }

    String curiousClock(String someTime, String leavingTime) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        Date startTime = sdf.parse(someTime);
        Date endTime = sdf.parse(leavingTime);

        long timeDistance = endTime.getTime() - startTime.getTime();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(startTime.getTime() - timeDistance);
        Date clockTIme = calendar.getTime();

        return sdf.format(clockTIme);
    }
}
