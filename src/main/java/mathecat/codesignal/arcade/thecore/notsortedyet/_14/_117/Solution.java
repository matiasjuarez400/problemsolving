package mathecat.codesignal.arcade.thecore.notsortedyet._14._117;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.test13();
    }

    void test2() {
        int[] length = {1, 1};
        int[] width = {1, 1};
        int[] height = {1, 1};

        System.out.println(boxesPacking(length, width, height));
    }
    
    void test5() {
         int[] length = {5, 7, 4, 1, 2};
         int[] width = {4, 10, 3, 1, 4};
         int[] height = {6, 5, 5, 1, 2};

        System.out.println(boxesPacking(length, width, height));
    }
    
    void test13() {
         int[] length = {9980, 9984, 9981};
         int[] width = {9980, 9984, 9983};
         int[] height = {9981, 9984, 9982};

        System.out.println(boxesPacking(length, width, height));
    }

    
    boolean boxesPacking(int[] length, int[] width, int[] height) {
        List<Box> boxes = IntStream.range(0, length.length).boxed()
                .map(i -> new Box(length[i], width[i], height[i]))
                .sorted(Comparator.comparing(Box::getVolume))
                .collect(Collectors.toList());

        for (int i = 0; i < boxes.size() - 1; i++) {
            if (!boxes.get(i + 1).fitsInside(boxes.get(i))) return false;
        }

        return true;
    }

    private static class Box {
        private static int nextId;
        private int length;
        private int width;
        private int height;
        private long volume;
        private int id;

        public Box(int length, int width, int height) {
            this.length = length;
            this.width = width;
            this.height = height;
            this.volume = (long) length * width * height;
            this.id = nextId++;
        }

        public boolean fitsInside(Box anotherBox) {
            int[] thisBoxDim = {this.length, this.width, this.height};
            int[] anotherBoxDim = {anotherBox.length, anotherBox.width, anotherBox.height};

            int[] startingCombination = {0, 1, 2};
            int[] currentCombination = Arrays.copyOf(startingCombination, startingCombination.length);

            if (fittingCombination(thisBoxDim, anotherBoxDim, currentCombination)) return true;

            currentCombination = getNextCombination(currentCombination);
            while (!Arrays.equals(startingCombination, currentCombination) && currentCombination != null) {
                if (fittingCombination(thisBoxDim, anotherBoxDim, currentCombination)) return true;
                currentCombination = getNextCombination(currentCombination);
            }

            return false;
        }

        private boolean fittingCombination(int[] thisDims, int[] anotherDims, int[] combination) {
            return thisDims[0] > anotherDims[combination[0]] &&
                    thisDims[1] > anotherDims[combination[1]] &&
                    thisDims[2] > anotherDims[combination[2]];
        }

        private int[] getNextCombination(int[] input) {
            return getNextCombination(input, input.length - 1);
        }

        private int[] getNextCombination(int[] input, int workingIndex) {
            if (workingIndex < 0) return null;
            if (input[workingIndex] == input.length - 1) {
                input[workingIndex] = 0;
                return getNextCombination(input, workingIndex - 1);
            } else {
                input[workingIndex]++;
                if (Arrays.stream(input).distinct().count() == input.length) return input;
                return getNextCombination(input, input.length - 1);
            }
        }

        public long getVolume() {
            return volume;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Box box = (Box) o;
            return id == box.id;
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }
    }
}
