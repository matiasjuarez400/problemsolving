package mathecat.codesignal.arcade.thecore.notsortedyet._13._108;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        int[][] image = {{7, 4, 0, 1}, 
                         {5, 6, 2, 2},
                         {6, 10, 7, 8},
                         {1, 4, 2, 0}};

        Solution solution = new Solution();

        System.out.println(Arrays.deepToString(solution.boxBlur(image)));
    }

    int[][] boxBlur(int[][] image) {
        int[][] result = new int[image.length - 2][image[0].length - 2];

        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                for (int ic = i; ic <= i + 2; ic++) {
                    for (int jc = j; jc <= j + 2; jc++) {
                        result[i][j] += image[ic][jc];
                    }
                }
                result[i][j] /= 9;
            }
        }

        return result;
    }
}
