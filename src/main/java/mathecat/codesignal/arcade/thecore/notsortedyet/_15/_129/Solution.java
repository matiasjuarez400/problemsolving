package mathecat.codesignal.arcade.thecore.notsortedyet._15._129;

import java.util.stream.IntStream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String white = "f2";
        String black = "h6";
        char toMove = 'w';

        System.out.println(solution.pawnRace(white, black, toMove));
    }

    String pawnRace(String white, String black, char toMove) {
        final char WHITE_MOVE = 'w';
        final char BLACK_MOVE = 'b';
        final String WHITE_WINS = "white";
        final String BLACK_WINS = "black";
        final String DRAW = "draw";

        Pawn w = new Pawn(white, Pawn.Color.WHITE);
        Pawn b = new Pawn(black, Pawn.Color.BLACK);

        int rowDistance = Math.abs(b.getRow() - w.getRow());
        int colDistance = Math.abs(w.getCol() - b.getCol());
        int whiteMovesToGoal = w.movesToGoal();
        int blackMovesToGoal = b.movesToGoal();

        if (w.getRow() <= b.getRow()) {
            if (b.movesToGoal() < w.movesToGoal()) return BLACK_WINS;
            else if (b.movesToGoal() > w.movesToGoal()) return WHITE_WINS;
            else if (b.movesToGoal() == w.movesToGoal()) return WHITE_MOVE == toMove ? WHITE_WINS : BLACK_WINS;
        }
        else if (colDistance > 1) {
            if (b.movesToGoal() < w.movesToGoal()) return BLACK_WINS;
            else if (b.movesToGoal() > w.movesToGoal()) return WHITE_WINS;
            else if (b.movesToGoal() == w.movesToGoal()) return WHITE_MOVE == toMove ? WHITE_WINS : BLACK_WINS;
        }
        else if (colDistance == 0) return DRAW;
        else if (colDistance == 1) {
            if (b.canCapture(w)) {
                return WHITE_MOVE == toMove ? WHITE_WINS : BLACK_WINS;
            }
            else if (rowDistance == 2) {
                return WHITE_MOVE == toMove ? BLACK_WINS : WHITE_WINS;
            }
            else if (rowDistance == 3 && (w.canMoveTwoSquares() || b.canMoveTwoSquares())) {
                if (w.canMoveTwoSquares()) {
                    return WHITE_MOVE == toMove ? WHITE_WINS : BLACK_WINS;
                } else if (b.canMoveTwoSquares()){
                    return BLACK_MOVE == toMove ? BLACK_WINS : WHITE_WINS;
                }
            }
            else if (b.canMoveTwoSquares() && w.canMoveTwoSquares()) {
                return WHITE_MOVE == toMove ? BLACK_WINS : WHITE_WINS;
            }
            else if (b.canMoveTwoSquares()) return BLACK_WINS;
            else if (w.canMoveTwoSquares()) return WHITE_WINS;
            else {
                int pawnDistance = Math.abs(b.getRow() - w.getRow());

                if (pawnDistance % 2 == 0) {
                    return WHITE_MOVE == toMove ? BLACK_WINS : WHITE_WINS;
                } else {
                    return WHITE_MOVE == toMove ? WHITE_WINS : BLACK_WINS;
                }
            }
        }

        throw new IllegalStateException("Could not decide who is going to win");
    }

    private static class Pawn {
        enum Color {
            WHITE, BLACK
        }

        private String position;
        private Color color;
        private int row;
        private int col;

        public Pawn(String position, Color color) {
            this.position = position;
            this.color = color;
            this.col = position.charAt(0) - 'a';
            this.row = '8' - position.charAt(1);
        }

        public boolean canMoveTwoSquares() {
            return this.color == Color.WHITE && row == 6 || this.color == Color.BLACK && row == 1;
        }

        public boolean canCapture(Pawn prey) {
            if (this.color == Color.WHITE) {
                if (prey.getRow() == this.row - 1) {
                    return IntStream.range(this.col - 1, this.col + 2).anyMatch(c -> c == prey.col);
                }
            } else {
                if (prey.getRow() == this.row + 1) {
                    return IntStream.range(this.col - 1, this.col + 2).anyMatch(c -> c == prey.col);
                }
            }

            return false;
        }

        public int movesToGoal() {
            if (this.color == Color.WHITE) {
                return canMoveTwoSquares() ? this.row - 1 : this.row;
            } else {
                int basicDistance = 7 - this.row;
                return canMoveTwoSquares() ? basicDistance - 1 : basicDistance;
            }
        }

        public int getRow() {
            return row;
        }

        public int getCol() {
            return col;
        }
    }
}
