package mathecat.codesignal.arcade.thecore.notsortedyet._16._132;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

class Solution {
    public static void main(String[] args) throws ParseException {
        Solution solution = new Solution();

        String birthday = "02-29-2072";

        System.out.println(solution.dayOfWeek(birthday));
    }

    int dayOfWeek(String birthdayDate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        Date birthday = sdf.parse(birthdayDate);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(birthday);
        int luckyDay = calendar.get(Calendar.DAY_OF_WEEK);
        int initialYear = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        int yearStep = (month == Calendar.FEBRUARY && day == 29) ? 4 : 1;
        int workingYear = initialYear + yearStep;
        while (true) {
            Calendar workingCalendar = Calendar.getInstance();
            workingCalendar.set(Calendar.YEAR, workingYear);
            workingCalendar.set(Calendar.MONTH, month);
            workingCalendar.set(Calendar.DAY_OF_MONTH, day);

            workingYear += yearStep;

            if (workingCalendar.get(Calendar.DAY_OF_WEEK) == luckyDay) return workingCalendar.get(Calendar.YEAR) - initialYear;
        }
    }

    boolean isLeapYear(int year) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        return cal.getActualMaximum(Calendar.DAY_OF_YEAR) > 365;
    }
}
