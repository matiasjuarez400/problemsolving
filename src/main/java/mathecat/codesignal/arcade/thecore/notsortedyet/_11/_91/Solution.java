package mathecat.codesignal.arcade.thecore.notsortedyet._11._91;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String comb1 = "*..*";
        String comb2 = "*.*";

        System.out.println(solution.combs(comb1, comb2));
        System.out.println(solution.combs(comb2, comb1));

        comb1 = "*...*";
        comb2 = "*.*";

        System.out.println(solution.combs(comb1, comb2));
        System.out.println(solution.combs(comb2, comb1));
    }

//    int combs2(String comb1, String comb2) {
//        int offset = 0;
//        int arraySize = comb1.length() + comb2.length() * 2;
//        int limit = comb1.length() + comb2.length();
//        int bestSoFar = Integer.MAX_VALUE;
//
//        boolean[] comb1Rep = new boolean[arraySize];
//        for (int i = comb2.length(); i < comb1.length() + comb2.length(); i++) {
//            comb1Rep[i] = '*' == comb1.charAt(i - comb2.length());
//        }
//
//
//        while (offset <= limit) {
//            boolean[] comb2Rep = new boolean[arraySize];
//            for (int i = offset; i < offset + comb2Rep.length; i++) {
//                comb2Rep[i] = '*' == comb2.charAt(i - offset);
//            }
//
//            boolean valid = true;
//            for (int i = 0; i < comb1Rep.length; i++) {
//                if (!comb1Rep[i] ^ comb2Rep[i]) {
//                    valid = false;
//                    break;
//                }
//            }
//
//            if (valid) {
//                int count =
//            }
//
//            offset++;
//        }
//    }

    int combs(String comb1, String comb2) {
        int offset = 0;
        int length1 = comb1.length();
        int length2 = comb2.length();
        int idealCase = Integer.max(length1, length2);
        int bestSoFar = Integer.MAX_VALUE;
        int limit = length1 + length2 - 1;

        while (offset < limit) {
            drawSituation(comb1, comb2, offset);
            boolean offsetWorks = true;
            for (int i = 0; i <= Integer.min(offset, length1 - 1); i++) {
                char comb1Char = comb1.charAt(i);

                int comb2Index = length2 - offset - 1 + i;
                if (comb2Index < 0) continue;
                char comb2Char = comb2.charAt(comb2Index);
                if (comb1Char == comb2Char && comb1Char == '*') {
                    offsetWorks = false;
                    break;
                }
            }

            if (offsetWorks) {
                int currentSize;
                if (offset < Integer.min(length1, length2) - 1) {
                    currentSize = length1 + length2 - offset - 1;
                } else if (offset < Integer.max(length1, length2)) {
                    currentSize = idealCase;
                } else {
                    currentSize = Integer.max(length1, length2) + offset - Integer.max(length1, length2) + 1;
                }

                if (currentSize == idealCase) return idealCase;
                if (currentSize < bestSoFar) bestSoFar = currentSize;
            }

            offset++;
        }

        return bestSoFar == Integer.MAX_VALUE ? length1 + length2 : bestSoFar;
    }

    void drawSituation(String comb1, String comb2, int offset) {
        if (true) return;
        System.out.println(repeat('/', 30));
        System.out.println(offset + repeat('_', comb2.length() - 1 - 1) + comb1);
        System.out.println(repeat('_', offset) + comb2);
        System.out.println(repeat('\\', 30));
    }

    String repeat(char c, int times) {
        return new String(new char[times]).replace('\0', c);
    }
}
