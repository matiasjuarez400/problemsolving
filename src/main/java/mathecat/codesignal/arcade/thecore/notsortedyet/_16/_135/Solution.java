package mathecat.codesignal.arcade.thecore.notsortedyet._16._135;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

class Solution {
    public static void main(String[] args) throws ParseException {
        Solution solution = new Solution();

        System.out.println(solution.regularMonths("02-2016"));
    }

    String regularMonths(String currMonth) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
        Date date = sdf.parse(currMonth);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, 1);

        while(true) {
            calendar.set(Calendar.DAY_OF_MONTH, 1);

            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

            if (dayOfWeek == Calendar.MONDAY) return sdf.format(calendar.getTime());

            calendar.add(Calendar.MONTH, 1);
        }
    }
}
