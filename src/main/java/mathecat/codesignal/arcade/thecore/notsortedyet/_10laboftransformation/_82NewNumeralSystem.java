package mathecat.codesignal.arcade.thecore.notsortedyet._10laboftransformation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class _82NewNumeralSystem {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(newNumeralSystem('G')));
    }

    static String[] newNumeralSystem(char number) {
        String outputModel = "%s + %s";
        List<String> output = new ArrayList<>();

        char limit = (char) (((number - 'A') / 2) + 'A');
        for (char i = 'A'; i <= limit; i++) {
            output.add(String.format(outputModel, i, (char) (number - i + 'A')));
        }

        return output.toArray(new String[0]);
    }
}
