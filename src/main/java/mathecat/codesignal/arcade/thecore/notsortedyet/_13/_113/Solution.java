package mathecat.codesignal.arcade.thecore.notsortedyet._13._113;

import java.util.ArrayList;
import java.util.List;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.test2();
    }

    private void test1() {
        String[][] nonogramField = {
                {"-", "-", "-", "-", "-", "-", "-", "-"},
                {"-", "-", "-", "2", "2", "1", "-", "1"},
                {"-", "-", "-", "2", "1", "1", "3", "3"},
                {"-", "3", "1", "#", "#", "#", ".", "#"},
                {"-", "-", "2", "#", "#", ".", ".", "."},
                {"-", "-", "2", ".", ".", ".", "#", "#"},
                {"-", "1", "2", "#", ".", ".", "#", "#"},
                {"-", "-", "5", "#", "#", "#", "#", "#"}
        };

        System.out.println(correctNonogram(5, nonogramField));
    }

    private void test2() {
        String[][] nonogramField = {
                {"-","-","-","-","-","-","-","-","-","-","-","-","-","-"},
                {"-","-","-","-","-","-","-","-","-","-","-","-","-","-"},
                {"-","-","-","-","-","-","-","-","-","-","-","-","-","-"},
                {"-","-","-","-","-","-","-","4","3","-","-","-","-","-"},
                {"-","-","-","-","-","2","2","2","1","9","3","4","2","2"},
                {"-","-","-","-","1",".",".",".",".","#",".",".",".","."},
                {"-","-","-","-","5",".",".","#","#","#","#","#",".","."},
                {"-","-","-","-","7",".","#","#","#","#","#","#","#","."},
                {"-","-","-","-","9","#","#","#","#","#","#","#","#","#"},
                {"1","1","1","1","1","#",".","#",".","#",".","#",".","#"},
                {"-","-","-","-","1",".",".",".",".","#",".",".",".","."},
                {"-","-","-","-","1",".",".","#",".","#",".",".",".","."},
                {"-","-","-","1","1",".",".","#",".","#",".",".",".","."},
                {"-","-","-","-","3",".",".","#","#","#",".",".",".","."}
        };

        System.out.println(correctNonogram(9, nonogramField));
    }

    boolean correctNonogram(int size, String[][] nonogramField) {
        final char BLACK = '#';

        int initialNonogramPoint = (size + 1) / 2;

        // ROWS
        for (int r = initialNonogramPoint; r < initialNonogramPoint + size; r++) {
            List<Integer> rowRules = new ArrayList<>();
            for (int c1 = 0; c1 < initialNonogramPoint; c1++) {
                String current = nonogramField[r][c1];
                if (current.equals("-")) continue;
                rowRules.add(Integer.parseInt(current));
            }

            int rowBlackCount = 0;
            int nextRunIndex = 0;
            for (int c2 = initialNonogramPoint; c2 < initialNonogramPoint + size; c2++) {
                if (nonogramField[r][c2].charAt(0) == BLACK) rowBlackCount++;
                else if (rowBlackCount != 0){
                    if (nextRunIndex >= rowRules.size()) return false;
                    if (rowBlackCount != rowRules.get(nextRunIndex)) return false;

                    nextRunIndex++;
                    rowBlackCount = 0;
                }
            }

            if (rowBlackCount != 0){
                if (rowBlackCount != rowRules.get(nextRunIndex)) return false;

                nextRunIndex++;
            }

            if (nextRunIndex != rowRules.size()) return false;
        }

        // COLS
        for (int c = initialNonogramPoint; c < initialNonogramPoint + size; c++) {
            List<Integer> colRules = new ArrayList<>();
            for (int r1 = 0; r1 < initialNonogramPoint; r1++) {
                String current = nonogramField[r1][c];
                if (current.equals("-")) continue;
                colRules.add(Integer.parseInt(current));
            }

            int colBlackCount = 0;
            int nextRunIndex = 0;
            for (int r2 = initialNonogramPoint; r2 < initialNonogramPoint + size; r2++) {
                if (nonogramField[r2][c].charAt(0) == BLACK) colBlackCount++;
                else if (colBlackCount != 0){
                    if (nextRunIndex >= colRules.size()) return false;
                    if (colBlackCount != colRules.get(nextRunIndex)) return false;

                    nextRunIndex++;
                    colBlackCount = 0;
                }
            }

            if (colBlackCount != 0){
                if (colBlackCount != colRules.get(nextRunIndex)) return false;

                nextRunIndex++;
            }

            if (nextRunIndex != colRules.size()) return false;
        }

        return true;
    }
}
