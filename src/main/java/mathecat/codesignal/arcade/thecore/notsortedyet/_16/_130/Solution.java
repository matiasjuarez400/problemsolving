package mathecat.codesignal.arcade.thecore.notsortedyet._16._130;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.validTime("13:58"));
        System.out.println(solution.validTime("02:76"));
        System.out.println(solution.validTime("25:51"));
    }

    boolean validTime(String time) {
        String[] splitted = time.split(":");
        if (splitted.length < 2) return false;

        int hours = Integer.parseInt(splitted[0]);
        int minutes = Integer.parseInt(splitted[1]);

        return hours >= 0 && hours <= 23 && minutes >= 0 && minutes <= 59;
//        return time.matches("^((2[0-3])|([0-1][0-9])):[0-5][0-9]$");
    }
}
