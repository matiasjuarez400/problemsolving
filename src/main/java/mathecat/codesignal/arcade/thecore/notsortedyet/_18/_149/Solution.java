package mathecat.codesignal.arcade.thecore.notsortedyet._18._149;

import java.util.Arrays;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String input = "rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR";

        System.out.println(solution.chessNotation(input));
    }

    String chessNotation(String notation) {
        String expandedNotation = expandNotation(notation);
        char[][] matrix = convertToMatrix(expandedNotation);
        char[][] rotated = rotateMatrix(matrix);
        return collapseMatrix(rotated);
    }

    String collapseMatrix(char[][] matrix) {
        return Arrays.stream(matrix)
                .map(this::collapseRow)
                .collect(Collectors.joining("/"));
    }

    String collapseRow(char[] row) {
        StringBuilder sb = new StringBuilder();

        int emptyAcc = 0;
        for (char c : row) {
            if (c == 'X') emptyAcc++;
            else {
                if (emptyAcc != 0) {
                    sb.append(emptyAcc);
                    emptyAcc = 0;
                }
                sb.append(c);
            }
        }

        if (emptyAcc != 0) sb.append(emptyAcc);

        return sb.toString();
    }

    char[][] rotateMatrix(char[][] matrix) {
        char[][] output = new char[matrix.length][matrix[0].length];

        for (int r = 0; r < matrix.length; r++) {
            for (int c = 0; c < matrix[r].length; c++) {
                output[c][output.length - 1 - r] = matrix[r][c];
            }
        }

        return output;
    }

    char[][] convertToMatrix(String notation) {
        String[] rows = notation.split("/");
        return Arrays.stream(rows).map(String::toCharArray).toArray(char[][]::new);
    }

    String expandNotation(String input) {
        StringBuilder sb = new StringBuilder();
        for (char c : input.toCharArray()) {
            if (Character.isDigit(c)) {
                sb.append(new String(new char[Character.getNumericValue(c)]).replace('\0', 'X'));
            } else {
                sb.append(c);
            }
        }

        return sb.toString();
    }
}
