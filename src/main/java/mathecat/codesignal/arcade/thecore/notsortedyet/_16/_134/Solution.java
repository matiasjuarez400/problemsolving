package mathecat.codesignal.arcade.thecore.notsortedyet._16._134;

import java.util.Calendar;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String takeOffTime = "00:00";
        int[] minutes = {32, 64, 87, 99, 120, 456};

        System.out.println(solution.newYearCelebrations(takeOffTime, minutes));
    }

    int newYearCelebrations(String takeOffTime, int[] minutes) {
        for (int i = minutes.length - 1; i > 0; i--) {
            minutes[i] = minutes[i] - minutes[i - 1];
        }

        String[] splittedInput = takeOffTime.split(":");

        Calendar currentTime = Calendar.getInstance();
        currentTime.set(Calendar.HOUR_OF_DAY, Integer.parseInt(splittedInput[0]));
        currentTime.set(Calendar.MINUTE, Integer.parseInt(splittedInput[1]));
        currentTime.set(Calendar.SECOND, 0);
        currentTime.set(Calendar.MILLISECOND, 0);

        int initialYear = currentTime.get(Calendar.YEAR);
        int initialMonth = currentTime.get(Calendar.MONTH);
        int initialDay = currentTime.get(Calendar.DATE);
        int initialHourOfDay = currentTime.get(Calendar.HOUR_OF_DAY);
        int initialMinutes = currentTime.get(Calendar.MINUTE);

        boolean happensNextDay = initialHourOfDay != 0 || initialMinutes != 0;

        int celebrations = 0;
        for (int i = 0; i < minutes.length; i++) {
            System.out.println(currentTime.getTime());

            int minutesToNextTimeZone = minutes[i];

            if (canCelebrate(currentTime, minutesToNextTimeZone, initialYear, initialMonth, initialDay, happensNextDay)) {
                celebrations++;
            }

            currentTime.add(Calendar.MINUTE, minutesToNextTimeZone);
            currentTime.add(Calendar.HOUR_OF_DAY, -1);
        }

        System.out.println(currentTime.getTime());
        if (canCelebrate(currentTime, Integer.MAX_VALUE, initialYear, initialMonth, initialDay, happensNextDay)) {
            celebrations++;
        }

        return celebrations;
    }

    boolean canCelebrate(Calendar currentTime, int minutesToNextTimeZone, int initialYear, int initialMonth, int initialDay, boolean happensNextDay) {
        if (currentTime.get(Calendar.YEAR) == initialYear &&
                currentTime.get(Calendar.MONTH) == initialMonth) {
            int currentHour = currentTime.get(Calendar.HOUR_OF_DAY);
            int currentMinutes = currentTime.get(Calendar.MINUTE);
            int currentDate = currentTime.get(Calendar.DATE);

            if (currentHour == 0 && currentMinutes == 0 && currentDate == (happensNextDay ? initialDay + 1 : initialDay)) {
                return true;
            } else if (currentDate == initialDay && happensNextDay || currentDate == initialDay - 1 && !happensNextDay) {
                int minutesToMidnight = (23 - currentTime.get(Calendar.HOUR_OF_DAY)) * 60 + 60 - currentTime.get(Calendar.MINUTE);

                if (minutesToMidnight <= minutesToNextTimeZone) return true;
            }
        }

        return false;
    }
}
