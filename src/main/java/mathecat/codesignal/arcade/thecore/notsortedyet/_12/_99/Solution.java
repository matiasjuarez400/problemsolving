package mathecat.codesignal.arcade.thecore.notsortedyet._12._99;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[][] array1 = {{1, 1, 1},
                        {0, 0}};

        int[][] array2 = {{2, 1, 1},
                          {2, 1}};

        System.out.println(solution.areIsomorphic(array1, array2));
    }

    boolean areIsomorphic(int[][] array1, int[][] array2) {
        if (array1.length != array2.length) return false;

        for (int i = 0; i < array1.length; i++) {
            if (array1[i].length != array2[i].length) return false;
        }

        return true;
    }
}
