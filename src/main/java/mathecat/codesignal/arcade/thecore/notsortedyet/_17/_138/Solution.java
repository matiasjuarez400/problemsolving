package mathecat.codesignal.arcade.thecore.notsortedyet._17._138;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.isSentenceCorrect("This is an example of *correct* sentence."));
    }

    boolean isSentenceCorrect(String sentence) {
        String regex = "[A-Z][^\\.\\?\\!]*[\\.\\?\\!]$";
        return sentence.matches(regex);
    }
}
