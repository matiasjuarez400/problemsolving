package mathecat.codesignal.arcade.thecore.notsortedyet._12._100;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[][] matrix = {{1, 2, 3},
                          {4, 5, 6},
                          {7, 8, 9}};

        System.out.println(Arrays.deepToString(solution.reverseOnDiagonals(matrix)));
    }

    int[][] reverseOnDiagonals(int[][] matrix) {
        int l = matrix.length;
        int limit = l / 2;
        for (int i = 0; i < limit; i++) {
            int upRow = i;
            int downRow = l - 1 - i;
            int leftCol = i;
            int rightCol = l - 1 - i;

            int temp1 = matrix[upRow][leftCol];
            matrix[upRow][leftCol] = matrix[downRow][rightCol];
            matrix[downRow][rightCol] = temp1;

            int temp2 = matrix[downRow][leftCol];
            matrix[downRow][leftCol] = matrix[upRow][rightCol];
            matrix[upRow][rightCol] = temp2;
        }

        return matrix;
    }
}
