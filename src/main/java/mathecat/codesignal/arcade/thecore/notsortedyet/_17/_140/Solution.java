package mathecat.codesignal.arcade.thecore.notsortedyet._17._140;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.swapAdjacentWords("are How today you guys"));
    }

    String swapAdjacentWords(String s) {
        return s.replaceAll("(\\w+) (\\w+)" , "$2 $1");
    }
}
