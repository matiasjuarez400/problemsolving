package mathecat.codesignal.arcade.thecore.notsortedyet._17._139;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.replaceAllDigitsRegExp("There are 12 points"));
    }

    String replaceAllDigitsRegExp(String input) {
        return input.replaceAll("\\d", "#");
    }
}
