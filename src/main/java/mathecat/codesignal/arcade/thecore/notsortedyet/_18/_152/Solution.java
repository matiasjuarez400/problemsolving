package mathecat.codesignal.arcade.thecore.notsortedyet._18._152;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String in1 = "[ \"one\", 2, \"three\" ]";
        String in2 = "[true,\"false\", true, [2], false, false]";

        System.out.println(solution.countElements(in2));
    }

    int countElements(String inputString) {
        Pattern pattern = Pattern.compile("\".*?\"|\\d+|true|false");

        Matcher matcher = pattern.matcher(inputString);

        int acc = 0;
        while (matcher.find()) acc++;
        return acc;
    }
}
