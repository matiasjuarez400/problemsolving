package mathecat.codesignal.arcade.thecore.notsortedyet._14._118;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.test2();
    }

    void test1() {
        int[][] q = {{1, 3}, {1, 4}, {0, 2}};
        int[] a = {9, 7, 2, 4, 4};

        System.out.println(maximumSum(a, q));
    }

    void test2() {
        int[][] q = {{0,1}};
        int[] a = {2, 1, 2};

        System.out.println(maximumSum(a, q));
    }

    int maximumSum(int[] a, int[][] q) {
        Map<Integer, Pair> counterByIndex = new HashMap<>();
        for (int i = 0; i < a.length; i++) {
            counterByIndex.put(i, new Pair(i, 0));
        }

        for (int i = 0; i < q.length; i++) {
            int[] query = q[i];
            for (int j = query[0]; j <= query[1]; j++) {
                counterByIndex.computeIfPresent(j, (k, p) -> new Pair(k, p.getRight() + 1));
            }
        }

        List<Pair> countersSortedByCounter = counterByIndex.values().stream()
                .sorted(Comparator.comparing(Pair::getRight)).collect(Collectors.toList());

        int[] aCopy = Arrays.copyOf(a, a.length);
        Arrays.sort(aCopy);

        for (int i = 0; i < aCopy.length; i++) {
            a[countersSortedByCounter.get(i).getLeft()] = aCopy[i];
        }

        int acc = 0;
        for (int i = 0; i < q.length; i++) {
            int[] query = q[i];

            for (int j = query[0]; j <= query[1]; j++) {
                acc += a[j];
            }
        }
        return acc;
    }

    private class Pair {
        private int left;
        private int right;

        public Pair(int left, int right) {
            this.left = left;
            this.right = right;
        }

        public int getLeft() {
            return left;
        }

        public int getRight() {
            return right;
        }
    }
}
