package mathecat.codesignal.arcade.thecore.notsortedyet._13._112;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        
        int[][] evidences = {{ 0, 1, 0, 1}, 
                             {-1, 1, 0, 0},
                             {-1, 0, 0, 1}};

        System.out.println(solution.isInformationConsistent(evidences));
    }

    boolean isInformationConsistent(int[][] evidences) {
        int[] infoFound = new int[evidences[0].length];

        for (int i = 0; i < evidences.length; i++) {
            for (int j = 0; j < evidences[i].length; j++) {
                int currentEntry = evidences[i][j];
                int currentInfo = infoFound[j];

                if (currentInfo == 0) {
                    infoFound[j] = currentEntry;
                } else if (currentInfo * currentEntry < 0){
                    return false;
                }
            }
        }

        return true;
    }
}
