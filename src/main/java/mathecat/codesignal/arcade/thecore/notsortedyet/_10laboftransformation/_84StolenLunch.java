package mathecat.codesignal.arcade.thecore.notsortedyet._10laboftransformation;

public class _84StolenLunch {
    public static void main(String[] args) {
        System.out.println(stolenLunch("you'll n4v4r 6u4ss 8t: cdja"));
    }

    static String stolenLunch(String note) {
        char[] output = note.toCharArray();

        for (int i = 0; i < output.length; i++) {
            char c = output[i];
            if (c >= 'a' && c <= 'j') output[i] = (char) (c - 'a' + '0');
            else if (c >= '0' && c <= '9') output[i] = (char) (c - '0' + 'a');
        }

        return new String(output);
    }
}
