package mathecat.codesignal.arcade.thecore.notsortedyet._10laboftransformation;

public class _83Cipher26 {
    public static void main(String[] args) {
        System.out.println(cipher26("taiaiaertkixquxjnfxxdh"));
    }

    static String cipher26(String message) {
        char[] output = new char[message.length()];

        int acc = 0;
        for (int i = 0; i < output.length; i++) {
            int cypheredLetterValue = message.charAt(i) - 'a';
            int j = 0;
            while ((acc + j) % 26 != cypheredLetterValue) {
                j++;
            }

            char decypheredChar = (char) (j + 'a');

            output[i] = decypheredChar;
            acc += decypheredChar - 'a';
        }

        return new String(output);
    }
}
