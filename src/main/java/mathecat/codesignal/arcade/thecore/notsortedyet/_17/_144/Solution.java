package mathecat.codesignal.arcade.thecore.notsortedyet._17._144;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String function =
                "function add($n, m) {\t" +
                "return n + $m;\t" +
                "}";

        System.out.println(function);
        String[] functionArgs = {"n", "m"};

        System.out.println(solution.programTranslation(function, functionArgs));
    }

    String programTranslation(String solution, String[] args) {
        String argumentVariants = String.join("|", args);
        String pattern = "([^\\w$])(" + argumentVariants + ")(?=\\W)";
        String sub = "$1\\$$2";
        return solution.replaceAll(pattern, sub);
    }
}
