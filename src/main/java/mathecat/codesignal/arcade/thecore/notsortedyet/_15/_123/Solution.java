package mathecat.codesignal.arcade.thecore.notsortedyet._15._123;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.chessKnightMoves("d5"));
        System.out.println(solution.chessKnightMoves("a1"));
        System.out.println(solution.chessKnightMoves("c2"));
    }

    int chessKnightMoves(String cell) {
        Knight knight = new Knight(cell);

        List<String> knightMoves = knight.getPossibleMoves();

        return knightMoves.size();
    }

    private static class Knight {
        private char col;
        private char row;

        Knight(String cell) {
            this.col = cell.charAt(0);
            this.row = cell.charAt(1);
        }

        List<String> getPossibleMoves() {
            return Stream.of(
                    getNewValidCell(1, -2),
                    getNewValidCell(1, 2),
                    getNewValidCell(-1, -2),
                    getNewValidCell(-1, 2),
                    getNewValidCell(2, -1),
                    getNewValidCell(2, 1),
                    getNewValidCell(-2, -1),
                    getNewValidCell(-2, 1)
            ).filter(Objects::nonNull).collect(Collectors.toList());
        }

        private String getNewValidCell(int rowOffset, int colOffset) {
            char newCol = (char) (this.col + colOffset);
            char newRow = (char) (this.row + rowOffset);

            if (newCol < 'a' || newCol > 'h' || newRow < '1' || newRow > '8') return null;

            return newCol + "" + newRow;
        }
    }
}
