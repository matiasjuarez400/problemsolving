package mathecat.codesignal.arcade.thecore.notsortedyet._13._110;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        boolean[][] matrix = {{false, true,  true },
          {true,  true,  false},
          {true,  false, false}};

        System.out.println(solution.polygonPerimeter(matrix));
    }

    int polygonPerimeter(boolean[][] matrix) {
        int count = 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j]) {
                    if (i + 1 >= matrix.length || !matrix[i + 1][j]) count++;
                    if (i - 1 < 0 || !matrix[i - 1][j]) count++;
                    if (j + 1 >= matrix[i].length || !matrix[i][j + 1]) count++;
                    if (j - 1 < 0 || !matrix[i][j - 1]) count++;
                }
            }
        }

        return count;
    }
}
