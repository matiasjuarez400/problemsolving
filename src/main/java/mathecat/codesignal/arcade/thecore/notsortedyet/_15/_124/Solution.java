package mathecat.codesignal.arcade.thecore.notsortedyet._15._124;

import java.util.Arrays;
import java.util.stream.Stream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(Arrays.toString(solution.bishopDiagonal("d7", "f5")));
    }

    String[] bishopDiagonal(String bishop1, String bishop2) {
        Bishop b1 = new Bishop(bishop1);
        Bishop b2 = new Bishop(bishop2);

        if (b1.canReachCell(b2.getCell())) {
            Cell b1NewPosition = moveBishopToEdge(b1, b2);
            Cell b2NewPosition = moveBishopToEdge(b2, b1);

            b1.setPosition(b1NewPosition);
            b2.setPosition(b2NewPosition);
        }

        return Stream.of(b1.getCell().toString(), b2.getCell().toString())
                .sorted(String::compareTo).toArray(String[]::new);
    }

    private Cell moveBishopToEdge(Bishop b1, Bishop b2) {
        int b1RowDistance = b1.getCell().getRowDistance(b2.getCell());
        int b1ColDistance = b1.getCell().getColDistance(b2.getCell());

        int b1RowOffset = b1RowDistance > 0 ? 8 - b1.getCell().getRowAsInt() : 1 - b1.getCell().getRowAsInt();
        int b1ColOffset = b1ColDistance > 0 ? 8 - b1.getCell().getColAsInt() : 1 - b1.getCell().getColAsInt();

        int b1MinOffset = Integer.min(Math.abs(b1RowOffset), Math.abs(b1ColOffset));

        Cell newPosition = b1.getCell().move((int) Math.copySign(b1MinOffset, b1RowOffset), (int) Math.copySign(b1MinOffset, b1ColOffset));
        return newPosition;
    }

    private static class Bishop {
        enum MovementDirection {
            UP_LEFT, UP_RIGHT, DOWN_LEFT, DOWN_RIGHT
        }

        private Cell cell;

        public Bishop(String cell) {
            this.cell = new Cell(cell);
        }

        public boolean canReachCell(String cell) {
            return canReachCell(new Cell(cell));
        }

        public boolean canReachCell(Cell cell) {
            return Math.abs(this.cell.getCol() - cell.getCol()) == Math.abs(this.cell.getRow() - cell.getRow());
        }

        public void move(int offset, MovementDirection movementDirection) {
            switch (movementDirection) {
                case UP_LEFT: this.cell = this.cell.move(offset, -offset); break;
                case UP_RIGHT: this.cell = this.cell.move(offset, offset); break;
                case DOWN_LEFT: this.cell = this.cell.move(-offset, -offset); break;
                case DOWN_RIGHT: this.cell = this.cell.move(-offset, offset); break;
            }
        }

        public void move(int rowOffset, int colOffset) {
            if (Math.abs(rowOffset) != Math.abs(colOffset)) {
                throw new IllegalArgumentException("Bishops should move only diagonally");
            }

            if (rowOffset > 0) {
                if (colOffset > 0) {
                    move(Math.abs(rowOffset), MovementDirection.UP_RIGHT);
                } else {
                    move(Math.abs(rowOffset), MovementDirection.UP_LEFT);
                }
            } else {
                if (colOffset > 0) {
                    move(Math.abs(rowOffset), MovementDirection.DOWN_RIGHT);
                } else {
                    move(Math.abs(rowOffset), MovementDirection.DOWN_LEFT);
                }
            }
        }

        public void setPosition(Cell cell) {
            if (Math.abs(this.cell.getCol() - cell.getCol()) != Math.abs(this.cell.getRow() - cell.getRow())) {
                throw new IllegalArgumentException("You are trying to move this bishop in a way that's not diagonal");
            }

            this.cell = cell;
        }

        public Cell getCell() {
            return cell;
        }
    }

    private static class Cell {
        private final char row;
        private final char col;

        public Cell(String cell) {
            this.row = cell.charAt(1);
            this.col = cell.charAt(0);
        }

        public Cell move(int rowOffset, int colOffset) {
            char newRow = (char) (this.row + rowOffset);
            char newCol = (char) (this.col + colOffset);

            if (newRow < '1' || newRow > '8' || newCol < 'a' || newCol > 'h') {
                throw new IllegalArgumentException("Can't move the cell by the specified offsets");
            }

            return new Cell(newCol + "" + newRow);
        }

        public int calculateAbsoluteDistance(char colOrRow) {
            if (colOrRow >= 'a' && colOrRow <= 'h') {
                return Math.abs(this.col - colOrRow);
            } else if (colOrRow >= '1' && colOrRow <= '8') {
                return Math.abs(this.row - colOrRow);
            } else {
                throw new IllegalArgumentException("The specified input can not be associated to a column or a row: " + colOrRow);
            }
        }

        public int getRowDistance(Cell cell) {
            return this.row - cell.row;
        }

        public int getColDistance(Cell cell) {
            return this.col - cell.col;
        }

        public char getRow() {
            return row;
        }

        public int getRowAsInt() {
            return row - '1' + 1;
        }

        public char getCol() {
            return col;
        }

        public int getColAsInt() {
            return col - 'a' + 1;
        }

        @Override
        public String toString() {
            return col + "" + row;
        }
    }
}
