package mathecat.codesignal.arcade.thecore.notsortedyet._13._109;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[][] matrix = solution.contoursShifting(test3());

        solution.printMatrix(matrix);
    }

    private static int[][] test1() {
        return new int[][] {{ 1,  2,  3,  4},
                { 5,  6,  7,  8},
                { 9, 10, 11, 12},
                {13, 14, 15, 16},
                {17, 18, 19, 20}};
    }

    private static int[][] test2() {
        return new int[][]{{238,239,240,241,242,243,244,245}};
    }
    
    private static int[][] test3() {
        return new int[][] {{1,2,3,4,5}, 
                             {6,7,8,9,10}, 
                             {11,12,13,14,15}};
    }

    int[][] contoursShifting(int[][] matrix) {
        int level = 0;
        ContourPointsExtractor extractor = new ContourPointsExtractor(level, matrix);
        List<Point> levelPoints = extractor.extract();

        while (!levelPoints.isEmpty()) {
            List<Integer> matrixValues = new ArrayList<>();
            for (Point point : levelPoints) {
                matrixValues.add(matrix[point.i][point.j]);
            }

            if (level % 2 == 0) {
                int lastValue = matrixValues.remove(matrixValues.size() - 1);
                matrixValues.add(0, lastValue);
            } else {
                int firstValue = matrixValues.remove(0);
                matrixValues.add(firstValue);
            }

            for (int i = 0; i < levelPoints.size(); i++) {
                Point current = levelPoints.get(i);
                Integer value = matrixValues.get(i);

                matrix[current.i][current.j] = value;
            }

            level++;
            levelPoints = new ContourPointsExtractor(level, matrix).extract();
        }

        return matrix;
    }

    private class ContourPointsExtractor {
        private int level;
        private int[][] matrix;

        public ContourPointsExtractor(int level, int[][] matrix) {
            this.level = level;
            this.matrix = matrix;
        }

        public List<Point> extract() {
            List<Point> points = new ArrayList<>();

            int up = level;
            int down = matrix.length - level - 1;
            int left = level;
            int right = matrix[0].length - level - 1;

            if (up > down || left > right) return points;

            for (int j = left; j <= right; j++) {
                points.add(new Point(up, j));
            }

            for (int i = up; i <= down; i++) {
                points.add(new Point(i, right));
            }

            for (int j = right; j >= left; j--) {
                points.add(new Point(down, j));
            }

            for (int i = down; i >= up; i--) {
                points.add(new Point(i, left));
            }

            return points.stream().distinct().collect(Collectors.toList());
        }
    }

    private class Point {
        int i;
        int j;

        public Point(int i, int j) {
            this.i = i;
            this.j = j;
        }

        public int getI() {
            return i;
        }

        public int getJ() {
            return j;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Point point = (Point) o;
            return i == point.i &&
                    j == point.j;
        }

        @Override
        public int hashCode() {
            return Objects.hash(i, j);
        }
    }

    private void printMatrix(int[][] matrix) {
        System.out.println(Arrays.deepToString(matrix).replace("], ", "]\n"));
    }
}
