package mathecat.codesignal.arcade.thecore.notsortedyet._13._106;

import java.util.Arrays;
import java.util.stream.IntStream;

class Solution {
    public static void main(String[] args) {
        int[][] grid = {{1, 3, 2, 5, 4, 6, 9, 8, 7},
        {4, 6, 5, 8, 7, 9, 3, 2, 1},
        {7, 9, 8, 2, 1, 3, 6, 5, 4},
        {9, 2, 1, 4, 3, 5, 8, 7, 6},
        {3, 5, 4, 7, 6, 8, 2, 1, 9},
        {6, 8, 7, 1, 9, 2, 5, 4, 3},
        {5, 7, 6, 9, 8, 1, 4, 3, 2},
        {2, 4, 3, 6, 5, 7, 1, 9, 8},
        {8, 1, 9, 3, 2, 4, 7, 6, 5}};

        Solution solution = new Solution();

        System.out.println(solution.sudoku(grid));
    }

    boolean sudoku(int[][] grid) {
        int[] rowCheck = new int[9];
        int[] columnCheck = new int[9];
        int[][] regionCheck = new int[3][3];

        for (int r = 0; r < 9; r++) {
            for (int c = 0; c < 9; c++) {
                rowCheck[r] += checkValueCalculation(grid[r][c]);
                columnCheck[c] += checkValueCalculation(grid[r][c]);
                regionCheck[r / 3][c / 3] += checkValueCalculation(grid[r][c]);
            }
        }

        int checkValue = IntStream.rangeClosed(1, 9).reduce(0, (a, b) -> a + checkValueCalculation(b));

        return Arrays.stream(rowCheck).allMatch(value -> value == checkValue) &&
                Arrays.stream(columnCheck).allMatch(value -> value == checkValue) &&
                Arrays.stream(regionCheck).allMatch(regionRow -> Arrays.stream(regionRow).allMatch(value -> value == checkValue));
    }

    private int checkValueCalculation(int baseValue) {
        return baseValue * baseValue;
    }
}
