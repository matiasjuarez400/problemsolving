package mathecat.codesignal.arcade.thecore.notsortedyet._15._125;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String input = "b1;g1;b8;g8";

        System.out.println(solution.whoseTurn(input));
    }

    boolean whoseTurn(String p) {
        String[] knightPositions = p.split(";");

        int acc = 0;
        for (String knightPosition : knightPositions) {
            if (isWhiteSquare(knightPosition)) acc++;
        }

        return acc % 2 == 0;
    }

    boolean isWhiteSquare(String cell) {
        return ((cell.charAt(0) - 'a') + ('8' - cell.charAt(1))) % 2 == 0;
    }
}
