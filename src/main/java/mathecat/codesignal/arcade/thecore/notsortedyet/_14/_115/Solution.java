package mathecat.codesignal.arcade.thecore.notsortedyet._14._115;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] a = {-1, 150, 190, 170, -1, -1, 160, 180};

        System.out.println(Arrays.toString(solution.sortByHeight(a)));
    }

    int[] sortByHeight(int[] a) {
        List<Integer> people = Arrays.stream(a).filter(i -> i != -1).sorted().boxed().collect(Collectors.toList());
        for (int i = 0, p = 0; i < a.length; i++) {
            if (a[i] == -1) continue;
            a[i] = people.get(p++);
        }
        return a;
    }
}
