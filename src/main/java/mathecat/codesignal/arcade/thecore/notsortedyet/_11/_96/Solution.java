package mathecat.codesignal.arcade.thecore.notsortedyet._11._96;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int levelNum = 2;
        int levelHeight = 4;

        String[] result = solution.christmasTree(levelNum, levelHeight);

        for (String s : result) {
            System.out.println(s);
        }
    }

    String[] christmasTree(int levelNum, int levelHeight) {
        Tree tree = new Tree(levelNum, levelHeight);

        List<String> drawResult = tree.draw();

        return drawResult.toArray(new String[0]);
    }

    private static String repeat(char c, int times) {
        return new String(new char[times]).replace('\0', c);
    }

    private static class Tree {
        private List<Drawable> treeParts;
        private int offsetToMiddle;

        public Tree(int levelNum, int levelHeight) {
            this.treeParts = new ArrayList<>();

            treeParts.add(new TreeCrown());
            List<TreeLevel> treeLevels = initializeLevels(levelNum, levelHeight);
            treeParts.addAll(treeLevels);
            treeParts.add(new TreeFoot(levelNum, levelHeight));

            this.offsetToMiddle = calculateOffsetToMiddle(treeLevels);
        }

        public List<String> draw() {
            List<String> result = new ArrayList<>();

            for (Drawable treePart : this.treeParts) {
                result.addAll(treePart.draw(offsetToMiddle));
            }

            return result;
        }

        private List<TreeLevel> initializeLevels(int levelNum, int levelHeight) {
            List<TreeLevel> treeLevels = new ArrayList<>();
            for (int i = 0; i < levelNum; i++) {
                treeLevels.add(new TreeLevel(i, levelHeight));
            }

            return treeLevels;
        }

        private int calculateOffsetToMiddle(List<TreeLevel> treeLevels) {
            return (treeLevels.get(treeLevels.size() - 1).calculateBottomLineSize() - 1) / 2;
        }
    }

    private interface Drawable {
        List<String> draw(int offsetToMiddle);
    }

    private static class TreeCrown implements Drawable {
        @Override
        public List<String> draw(int offsetToMiddle) {
            List<String> crown = Arrays.asList(
                    repeat(' ', offsetToMiddle) + "*",
                    repeat(' ', offsetToMiddle) + "*",
                    repeat(' ', offsetToMiddle - 1) + repeat('*', 3)
            );

            return crown;
        }
    }

    private static class TreeLevel implements Drawable {
        private int level;
        private int levelHight;

        public TreeLevel(int level, int levelHight) {
            this.level = level;
            this.levelHight = levelHight;
        }

        @Override
        public List<String> draw(int offsetToMiddle) {
            List<String> result = new ArrayList<>();

            for (int i = 0; i < levelHight; i++) {
                result.add(drawLine(i, offsetToMiddle));
            }

            return result;
        }

        public int calculateBottomLineSize() {
            return calculateAsterisksForLine(levelHight - 1);
        }

        private String drawLine(int line, int offsetToMiddle) {
            int whitespaces = offsetToMiddle - calculateAsterisksToSides(line);
            return repeat(' ', whitespaces) + repeat('*', calculateAsterisksForLine(line));
        }

        private int calculateAsterisksForLine(int line) {
            return (5 + this.level * 2) + line * 2;
        }

        private int calculateAsterisksToSides(int line) {
            return (calculateAsterisksForLine(line) - 1) / 2;
        }
    }

    private static class TreeFoot implements Drawable {
        private int levelHeight;
        private int levelNum;

        public TreeFoot(int levelNum, int levelHeight) {
            this.levelHeight = levelHeight;
            this.levelNum = levelNum;
        }

        @Override
        public List<String> draw(int offsetToMiddle) {
            List<String> foot = new ArrayList<>();

            int asterisks = levelHeight % 2 == 0 ? levelHeight + 1 : levelHeight;
            String asterisksString = repeat('*', asterisks);
            int whitespaces = offsetToMiddle - (asterisks - 1) / 2;
            String whitespacesString = repeat(' ', whitespaces);

            String footString = whitespacesString + asterisksString;

            for (int i = 0; i < levelNum; i++) {
                foot.add(footString);
            }

            return foot;
        }
    }
}
