package mathecat.codesignal.arcade.thecore.notsortedyet._14._121;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] a = {2, 8, 121, 42, 222, 23};

        System.out.println(solution.uniqueDigitProducts(a));
    }

    int uniqueDigitProducts(int[] a) {
        return (int) Arrays.stream(a).mapToObj(Integer::toString)
                .mapToInt(s -> {
                    int k = 1;
                    for (char c : s.toCharArray()) k *= Character.getNumericValue(c);
                    return k;
                }).distinct().count();
    }
}
