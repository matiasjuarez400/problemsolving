package mathecat.codesignal.arcade.thecore.notsortedyet._11._93;

import java.util.Collections;
import java.util.List;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String input = "cabca";

        System.out.println(solution.cyclicString(input));
    }

    int cyclicString2(String s) {
        int c = 1;
        while (true) {
            String currentSubstring = s.substring(0,c);
            List<String> ncopies = Collections.nCopies(s.length(),s.substring(0,c));
            String concatenated = String.join("", Collections.nCopies(s.length(),s.substring(0,c)));
            String substringOfConcatenated = String.join("", Collections.nCopies(s.length(),s.substring(0,c))).substring(0,s.length());

            if (substringOfConcatenated.equals(s)) return c;

            c++;
        }
    }

    int cyclicString(String s) {
        char firstChar = s.charAt(0);

        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i) == firstChar) {
                String testString = s.substring(0, i);

                boolean worksFine = true;
                for (int j = testString.length(); j < s.length(); j++) {
                    char schar = s.charAt(j);
                    int currentTestingCharIndex = j % testString.length();
                    char currentTestingChar = testString.charAt(currentTestingCharIndex);
                    if (schar != currentTestingChar) {
                        worksFine = false;
                        break;
                    }
                }

                if (worksFine) {
//                    System.out.println("Solution: " + testString);
                    return testString.length();
                }
            }
        }

        return s.length();
    }
}
