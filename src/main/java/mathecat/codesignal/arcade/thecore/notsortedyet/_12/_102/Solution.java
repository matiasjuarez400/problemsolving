package mathecat.codesignal.arcade.thecore.notsortedyet._12._102;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[][] matrix = {{1, 1, 1, 1}, 
                          {2, 2, 2, 2}, 
                          {3, 3, 3, 3}};

        System.out.println(solution.crossingSum(matrix, 1, 3));
    }

    int crossingSum(int[][] matrix, int a, int b) {
        int acc = 0;
        for (int col = 0; col < matrix[a].length; col++) {
            acc += matrix[a][col];
        }

        for (int row = 0; row < matrix.length; row++) {
            if (row != a) acc += matrix[row][b];
        }

        return acc;
    }
}
