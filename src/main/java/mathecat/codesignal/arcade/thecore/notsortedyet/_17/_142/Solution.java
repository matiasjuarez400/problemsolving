package mathecat.codesignal.arcade.thecore.notsortedyet._17._142;

import java.util.Arrays;
import java.util.regex.Pattern;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String t = "he sd.f dsk e8.sd??l**23, 23,f.s++83+";
        String s = "h  8.?*3+";

        System.out.println(solution.isSubsequence(t, s));
    }

    boolean isSubsequence(String t, String s) {
        String pattern = "";
        for (int i = 0; i < s.length(); i++) {
            System.out.println(s.charAt(i));
            pattern += (Arrays.asList('*', '+', '?', '.').contains(s.charAt(i)) ? "\\" : "") + (s.charAt(i) + "") + ".*";
        }
        System.out.println(pattern);
        Pattern regex = Pattern.compile(pattern);
        return regex.matcher(t).find();
    }
}
