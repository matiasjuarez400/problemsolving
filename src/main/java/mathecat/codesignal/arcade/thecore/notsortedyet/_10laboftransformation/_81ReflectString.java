package mathecat.codesignal.arcade.thecore.notsortedyet._10laboftransformation;

public class _81ReflectString {
    public static void main(String[] args) {
        System.out.println(reflectString("name"));
    }

    static String reflectString(String inputString) {
        char[] input = inputString.toCharArray();
        for (int i = 0; i < input.length; i++) {
            input[i] = (char) ('z' - input[i] + 'a');
        }

        return new String(input);
    }
}
