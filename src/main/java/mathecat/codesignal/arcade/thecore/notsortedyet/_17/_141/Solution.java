package mathecat.codesignal.arcade.thecore.notsortedyet._17._141;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.nthNumber("8one 003number 201numbers li-000233le number444", 4));
    }

    String nthNumber(String s, int n) {
        Pattern pattern = Pattern.compile(
                "(?:.*?[0-9]+){" + (n - 1) + "}.*?([1-9][0-9]*).*"
        );

        Matcher matcher = pattern.matcher(s);
        matcher.matches();
        return matcher.group(1);
    }
}
