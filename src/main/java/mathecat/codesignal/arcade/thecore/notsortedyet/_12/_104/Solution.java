package mathecat.codesignal.arcade.thecore.notsortedyet._12._104;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Solution {
    public static void main(String[] args) {
        String[][] formation = {{"empty",   "Player5", "empty"},
                                 {"Player4", "empty",   "Player2"},
                                 {"empty",   "Player3", "empty"},
                                 {"Player6", "empty",   "Player1"}};

        int k = 2;

        Solution solution = new Solution();

        System.out.println(Arrays.deepToString(solution.volleyballPositions(formation, k)));
    }

    String[][] volleyballPositions(String[][] formation, int k) {
        int[][] positions = {
                {3, 2},
                {1, 2},
                {0, 1},
                {1, 0},
                {3, 0},
                {2, 1}
        };

        int length = positions.length;

        List<String> players = new ArrayList<>();
        for (int[] position : positions) {
            players.add(formation[position[0]][position[1]]);
        }

//        for (int i = 0; i < k % positions.length; i++) {
//            String lastPlayer = players.remove(players.size() - 1);
//            players.add(0, lastPlayer);
//        }
        String[] initialPositions = new String[players.size()];
        for (int i = 0; i < initialPositions.length; i++) {
            int newIndex = ((length - k % length) + i) % length;

            initialPositions[i] = players.get(newIndex);
        }

//        for (int i = 0; i < positions.length; i++) {
//            int[] position = positions[i];
//            formation[position[0]][position[1]] = players.get(i);
//        }

        for (int i = 0; i < positions.length; i++) {
            int[] position = positions[i];
            formation[position[0]][position[1]] = initialPositions[i];
        }

        return formation;
    }
}
