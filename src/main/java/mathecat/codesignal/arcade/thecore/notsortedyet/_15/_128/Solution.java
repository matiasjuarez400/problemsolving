package mathecat.codesignal.arcade.thecore.notsortedyet._15._128;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(Arrays.toString(solution.amazonCheckmate("f3", "f2")));
    }

    int[] amazonCheckmate(String king, String amazon) {
        Board board = new Board(8, 8);

        King whiteKing = new King(king, board);
        Amazon whiteAmazon = new Amazon(amazon, board);

        board.addPiece(whiteKing);
        board.addPiece(whiteAmazon);

        List<Cell> whiteKingMoves = whiteKing.getPossibleMoves();

        Set<Cell> whiteCells = new HashSet<>();
        whiteCells.addAll(whiteKingMoves);
        whiteCells.addAll(whiteAmazon.getPossibleMoves());

        King blackKing = new King(0, 0, board);

        int[] result = new int[4];

        Map<String, List<Cell>> cellsBySituation = new HashMap<>();

        for (int r = 0; r < board.getRows(); r++) {
            for (int c = 0; c < board.getCols(); c++) {
                blackKing.setPosition(r, c);

                if (blackKing.getPosition().equals(whiteKing.getPosition()) ||
                        blackKing.getPosition().equals(whiteAmazon.getPosition()) ||
                        whiteKingMoves.contains(blackKing.getPosition())
                ) continue;

                List<Cell> blackKingMoves = blackKing.getPossibleMoves();

                int notUnderWhiteControl = (int) blackKingMoves.stream()
                        .filter(cell -> !whiteCells.contains(cell)).count();

                boolean isUnderAttack = whiteCells.contains(blackKing.getPosition());

                if (isUnderAttack) {
                     if (notUnderWhiteControl == 0) {
                         result[0]++;
                         cellsBySituation.computeIfAbsent("MATE", k -> new ArrayList<>()).add(blackKing.getPosition());
                     }
                     else {
                         result[1]++;
                         cellsBySituation.computeIfAbsent("CHECK", k -> new ArrayList<>()).add(blackKing.getPosition());
                     }
                } else {
                    if (notUnderWhiteControl == 0) {
                        result[2]++;
                        cellsBySituation.computeIfAbsent("STALE", k -> new ArrayList<>()).add(blackKing.getPosition());
                    }
                    else {
                        result[3]++;
                        cellsBySituation.computeIfAbsent("FREE", k -> new ArrayList<>()).add(blackKing.getPosition());
                    }
                }
            }
        }

        for (String key : cellsBySituation.keySet()) {
            List<Cell> cells = cellsBySituation.get(key);
            for (Cell cell : cells) {
                System.out.println(key + "_" + cell);
            }
        }

        return result;
    }

    private static class Cell {
        private int row;
        private int col;

        public Cell(int row, int col) {
            this.row = row;
            this.col = col;
        }

        public int getRow() {
            return row;
        }

        public int getCol() {
            return col;
        }

        public Cell setRow(int row) {
            return new Cell(row, this.col);
        }

        public Cell setCol(int col) {
            return new Cell(this.row, col);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Cell cell = (Cell) o;
            return row == cell.row &&
                    col == cell.col;
        }

        @Override
        public int hashCode() {
            return Objects.hash(row, col);
        }

        @Override
        public String toString() {
            return "Cell{" +
                    "row=" + row +
                    ", col=" + col +
                    '}';
        }
    }

    private abstract static class Piece {
        protected Cell position;
        protected Board board;

        public Piece(int row, int col, Board board) {
            this.board = board;
            this.position = new Cell(row, col);
        }

        public Piece(String position, Board board) {
            this('8' - position.charAt(1), position.charAt(0) - 'a', board);
        }

        public void setPosition(int row, int col) {
            setCol(col);
            setRow(row);
        }

        public void setPosition(Cell position) {
            setPosition(position.getRow(), position.getCol());
        }

        public void setRow(int row) {
            if (row < 0 || row >= board.getRows()) throw new IllegalArgumentException("The specified ROW is out of bounds");
            this.position = this.position.setRow(row);
        }

        public void setCol(int col) {
            if (col < 0 || col >= board.getCols()) throw new IllegalArgumentException("The specified COL is out of bounds");
            this.position = this.position.setCol(col);
        }

        public Cell getPosition() {
            return this.position;
        }

        public Board getBoard() {
            return board;
        }

        public abstract List<Cell> getPossibleMoves();

        public boolean canCapture(Piece piece) {
            List<Cell> moves = getPossibleMoves();

            return moves.stream().anyMatch(cell -> piece.getPosition().equals(cell));
        }
    }

    private static class Board {
        private int rows;
        private int cols;
        private List<Piece> pieces = new ArrayList<>();

        public Board(int rows, int cols) {
            this.rows = rows;
            this.cols = cols;
        }

        public int getRows() {
            return rows;
        }

        public int getCols() {
            return cols;
        }

        public void addPiece(Piece piece) {
            if (getPiece(piece.getPosition()) != null) throw new IllegalArgumentException("There is already a piece at position: " + piece.getPosition().toString());
            this.pieces.add(piece);
        }

        public List<Piece> getPieces() {
            return pieces;
        }

        public Piece getPiece(int row, int col) {
            return getPiece(new Cell(row, col));
        }

        public Piece getPiece(Cell cell) {
            for (Piece piece : pieces) {
                if (piece.getPosition().equals(cell)) return piece;
            }
            return null;
        }

        public boolean isFilled(int row, int col) {
            return getPiece(row, col) != null;
        }

        public boolean isFilled(Cell cell) {
            return getPiece(cell) != null;
        }
    }

    private static class King extends Piece {

        public King(int row, int col, Board board) {
            super(row, col, board);
        }

        public King(String position, Board board) {
            super(position, board);
        }

        @Override
        public List<Cell> getPossibleMoves() {
            List<Cell> candidates = new ArrayList<>();

            for (int r = getPosition().getRow() - 1; r <= getPosition().getRow() + 1; r++) {
                for (int c = getPosition().getCol() - 1; c <= getPosition().getCol() + 1; c++) {
                    candidates.add(new Cell(r, c));
                }
            }

            candidates.remove(getPosition());

            return candidates.stream().filter(cell -> {
                int row = cell.getRow();
                int col = cell.getCol();

                return  (row >= 0 && row < getBoard().getRows() && col >= 0 && col < getBoard().getCols());
            }).collect(Collectors.toList());
        }
    }

    private static class Amazon extends Piece {
        private Queen queen;
        private Knight knight;

        public Amazon(int row, int col, Board board) {
            super(row, col, board);
            queen = new Queen(row, col, board);
            knight = new Knight(row, col, board);
        }

        public Amazon(String position, Board board) {
            super(position, board);
            queen = new Queen(getPosition().getRow(), getPosition().getCol(), board);
            knight = new Knight(getPosition().getRow(), getPosition().getCol(), board);
        }

        @Override
        public List<Cell> getPossibleMoves() {
            Set<Cell> moves = new HashSet<>();
            moves.addAll(queen.getPossibleMoves());
            moves.addAll(knight.getPossibleMoves());

            return new ArrayList<>(moves);
        }

        @Override
        public void setRow(int row) {
            if (row < 0 || row >= board.getRows()) throw new IllegalArgumentException("The specified ROW is out of bounds");
            this.position = this.position.setRow(row);
            this.queen.setRow(row);
            this.knight.setRow(row);
        }

        @Override
        public void setCol(int col) {
            if (col < 0 || col >= board.getCols()) throw new IllegalArgumentException("The specified COL is out of bounds");
            this.position = this.position.setCol(col);
            this.queen.setRow(col);
            this.knight.setRow(col);
        }

        public boolean canCapture(Piece piece) {
            return queen.canCapture(piece) || knight.canCapture(piece);
        }
    }

    private static class Queen extends Piece {
        private Rock rock;
        private Bishop bishop;

        public Queen(int row, int col, Board board) {
            super(row, col, board);
            rock = new Rock(row, col, board);
            bishop = new Bishop(row, col, board);
        }

        @Override
        public List<Cell> getPossibleMoves() {
            Set<Cell> moves = new HashSet<>();
            moves.addAll(rock.getPossibleMoves());
            moves.addAll(bishop.getPossibleMoves());

            return new ArrayList<>(moves);
        }

        @Override
        public void setRow(int row) {
            if (row < 0 || row >= board.getRows()) throw new IllegalArgumentException("The specified ROW is out of bounds");
            this.position = this.position.setRow(row);
            this.bishop.setRow(row);
            this.rock.setRow(row);
        }

        @Override
        public void setCol(int col) {
            if (col < 0 || col >= board.getCols()) throw new IllegalArgumentException("The specified COL is out of bounds");
            this.position = this.position.setCol(col);
            this.bishop.setRow(col);
            this.rock.setRow(col);
        }

        public boolean canCapture(Piece piece) {
            return rock.canCapture(piece) || bishop.canCapture(piece);
        }
    }

    private static class Knight extends Piece {

        public Knight(int row, int col, Board board) {
            super(row, col, board);
        }

        @Override
        public List<Cell> getPossibleMoves() {
            int row = getPosition().getRow();
            int col = getPosition().getCol();

            List<Cell> candidates = Arrays.asList(
                    new Cell(row - 1, col - 2),
                    new Cell(row - 1, col + 2),
                    new Cell(row + 1, col - 2),
                    new Cell(row + 1, col + 2),
                    new Cell(row - 2, col - 1),
                    new Cell(row - 2, col + 1),
                    new Cell(row + 2, col - 1),
                    new Cell(row + 2, col + 1)
            );

            return candidates.stream().filter(cell -> {
                int r = cell.getRow();
                int c = cell.getCol();

                return r >= 0 && r < getBoard().getRows() && c >= 0 && c < getBoard().getCols();
            }).collect(Collectors.toList());
        }
    }

    private static class Bishop extends Piece {

        public Bishop(int row, int col, Board board) {
            super(row, col, board);
        }

        @Override
        public List<Cell> getPossibleMoves() {
            List<Cell> moves = new ArrayList<>();
            moves.addAll(findMovesInDirection(-1, -1));
            moves.addAll(findMovesInDirection(-1, 1));
            moves.addAll(findMovesInDirection(1, -1));
            moves.addAll(findMovesInDirection(1, 1));

            moves.remove(this.getPosition());

            return moves;
        }

        private List<Cell> findMovesInDirection(int rowDir, int colDir) {
            List<Cell> moves = new ArrayList<>();

            Cell workingCell = this.getPosition();
            workingCell = new Cell(workingCell.getRow() + rowDir, workingCell.getCol() + colDir);
            while (workingCell.getCol() >= 0 && workingCell.getRow() >= 0 && workingCell.getCol() < getBoard().getCols() && workingCell.getRow() < getBoard().getRows()) {
                moves.add(workingCell);
                if (board.getPiece(workingCell) != null) break;
                workingCell = new Cell(workingCell.getRow() + rowDir, workingCell.getCol() + colDir);
            }

            return moves;
        }

//        @Override
//        public boolean canCapture(Piece piece) {
//            return Math.abs(piece.getPosition().getRow() - this.getPosition().getRow()) == Math.abs(piece.getPosition().getCol() - this.getPosition().getCol());
//        }
    }

    private static class Rock extends Piece {

        public Rock(int row, int col, Board board) {
            super(row, col, board);
        }

        @Override
        public List<Cell> getPossibleMoves() {
            List<Cell> moves = new ArrayList<>();

            moves.addAll(findMovesInDirection(1, 0));
            moves.addAll(findMovesInDirection(-1, 0));
            moves.addAll(findMovesInDirection(0, 1));
            moves.addAll(findMovesInDirection(0, -1));

            moves.remove(getPosition());

            return moves;
        }

        private List<Cell> findMovesInDirection(int rowDir, int colDir) {
            List<Cell> moves = new ArrayList<>();

            Cell workingCell = this.getPosition();
            workingCell = new Cell(workingCell.getRow() + rowDir, workingCell.getCol() + colDir);
            while (workingCell.getCol() >= 0 && workingCell.getRow() >= 0 && workingCell.getCol() < getBoard().getCols() && workingCell.getRow() < getBoard().getRows()) {
                moves.add(workingCell);
                if (board.getPiece(workingCell) != null) break;
                workingCell = new Cell(workingCell.getRow() + rowDir, workingCell.getCol() + colDir);
            }

            return moves;
        }

//        @Override
//        public boolean canCapture(Piece piece) {
//            return piece.getPosition().getRow() == this.getPosition().getRow() || piece.getPosition().getCol() == this.getPosition().getCol();
//        }
    }
}
