package mathecat.codesignal.arcade.thecore.notsortedyet._14._114;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.test1();
    }

    void test1() {
        int[] shuffled = {1, 12, 3, 6, 2};
        System.out.println(Arrays.toString(shuffledArray(shuffled)));
    }

    int[] shuffledArray(int[] shuffled) {
        for (int i = 0; i < shuffled.length; i++) {
            int current = shuffled[i];
            int sum = 0;
            for (int j = 0; j < shuffled.length; j++) {
                if (j == i) continue;
                sum += shuffled[j];
            }

            if (sum == current) {
                int[] result = new int[shuffled.length - 1];
                for (int k = 0, l = 0; k < shuffled.length; k++) {
                    if (k == i) continue;
                    result[l++] = shuffled[k];
                }
                Arrays.sort(result);
                return result;
            }
        }

        return null;
    }
}
