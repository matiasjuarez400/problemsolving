package mathecat.codesignal.arcade.thecore.notsortedyet._10laboftransformation;

class _80CharacterParity {
    public static void main(String[] args) {
        System.out.println(characterParity('5'));
    }

    static String characterParity(char symbol) {
        if (symbol < '0' || symbol > '9') return "not a digit";
        return Character.getNumericValue(symbol) % 2 == 0 ? "even" : "odd";
    }
}
