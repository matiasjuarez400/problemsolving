package mathecat.codesignal.arcade.thecore.notsortedyet._18._153;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String tree = "(2 (7 (2 () ()) (6 (5 () ()) (11 () ()))) (5 () (9 (4 () ()) ())))";

        System.out.println(Arrays.toString(solution.treeBottom(tree)));
    }

    int[] treeBottom(String tree) {
        List<String> maxDepthNumbers = new ArrayList<>();
        int maxDepth = 0;
        int currentDepth = 0;
        for (int i = 0; i < tree.length(); i++) {
            char c = tree.charAt(i);
            if (c == '(') {
                currentDepth++;
            } else if (c == ')') {
                currentDepth--;
            } else if (Character.isDigit(c)) {
                if (currentDepth >= maxDepth) {
                    if (currentDepth > maxDepth) {
                        maxDepth = currentDepth;
                        maxDepthNumbers.clear();
                    }
                    StringBuilder currentNumber = new StringBuilder();
                    while (i < tree.length() && Character.isDigit(tree.charAt(i))) {
                        currentNumber.append(tree.charAt(i));
                        i++;
                    }
                    maxDepthNumbers.add(currentNumber.toString());
                }
            }
        }

        return maxDepthNumbers.stream().mapToInt(Integer::parseInt).toArray();
    }
}
