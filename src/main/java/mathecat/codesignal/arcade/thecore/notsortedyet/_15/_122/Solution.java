package mathecat.codesignal.arcade.thecore.notsortedyet._15._122;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.bishopAndPawn("a1", "c3"));
    }

    boolean bishopAndPawn(String bishop, String pawn) {
        return Math.abs(bishop.charAt(0) - pawn.charAt(0)) == Math.abs(bishop.charAt(1) - pawn.charAt(1));
    }
}
