package mathecat.codesignal.arcade.thecore.notsortedyet._10laboftransformation;

public class _86Decipher {
    public static void main(String[] args) {
        System.out.println(decipher("10197115121"));
    }

    static String decipher(String cipher) {
        String current = "";
        String deciphered = "";
        int i = 0;
        while (i < cipher.length()) {
            current += cipher.charAt(i);
            int currentInteger = Integer.parseInt(current);
            if (currentInteger >= 'a' && currentInteger <= 'z') {
                deciphered += (char) currentInteger;
                current = "";
            }

            i++;
        }

        return deciphered;
    }
}
