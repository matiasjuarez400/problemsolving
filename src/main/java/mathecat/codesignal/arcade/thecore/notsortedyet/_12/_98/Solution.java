package mathecat.codesignal.arcade.thecore.notsortedyet._12._98;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        int[][]matrix = {{1, 1, 1, 2}, 
                          {0, 5, 0, 4}, 
                          {2, 1, 3, 6}};

        System.out.println(Arrays.toString(solution.extractMatrixColumn(matrix, 2)));
    }

    int[] extractMatrixColumn(int[][] matrix, int column) {
        int[] result = new int[matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            result[i] = matrix[i][column];
        }

        return result;
    }
}
