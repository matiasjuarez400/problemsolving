package mathecat.codesignal.arcade.thecore.notsortedyet._17._146;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String rules = "Roll d6-3 and 4d4+3 to pick a weapon, and finish the boss with 3d7!";

        System.out.println(solution.bugsAndBugfixes(rules));
    }

    int bugsAndBugfixes(String rules) {
        Pattern pattern = Pattern.compile("(\\d*)d(\\d+)((?:-|\\+)\\d+)*");
        Matcher m = pattern.matcher(rules);

        int res = 0;
        while (m.find()) {
            int rolls = m.group(1).isEmpty() ? 1 : Integer.parseInt(m.group(1));
            int dieType = Integer.parseInt(m.group(2));
            int formulaMax = rolls * dieType;

            if (m.group(3) != null && !m.group(3).isEmpty()) {
                if (m.group(3).charAt(0) == '-') {
                    formulaMax -= Integer.parseInt(m.group(3).substring(1));
                }
                else {
                    formulaMax += Integer.parseInt(m.group(3).substring(1));
                }
            }

            res += formulaMax;
        }

        return res;
    }
}
