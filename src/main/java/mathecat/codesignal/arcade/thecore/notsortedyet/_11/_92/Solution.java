package mathecat.codesignal.arcade.thecore.notsortedyet._11._92;

import java.util.ArrayList;
import java.util.List;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String[] inputArray = {"bab", "aaa", "aba", "abc"};
        String result = "bbb";

        System.out.println(solution.stringsCrossover(inputArray, result));
    }

    int stringsCrossover(String[] inputArray, String result) {
        List<String> usedpairs = new ArrayList<>();

        for (int i = 0; i < inputArray.length - 1; i++) {
            String a = inputArray[i];
            for (int k = i + 1; k < inputArray.length; k++) {
                String b = inputArray[k];

                boolean worksFine = true;
                for (int j = 0; j < result.length(); j++) {
                    char expectedChar = result.charAt(j);
                    if (expectedChar != a.charAt(j) && expectedChar != b.charAt(j)) {
                        worksFine = false;
                        break;
                    }
                }

                if (worksFine) {
                    usedpairs.add(a + b);
                }
            }
        }
        return usedpairs.size();
    }
}
