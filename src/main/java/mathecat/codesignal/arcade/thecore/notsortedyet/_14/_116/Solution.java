package mathecat.codesignal.arcade.thecore.notsortedyet._14._116;

import java.util.Arrays;
import java.util.Comparator;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String[] inputArray = {"abc",
                "",
                "aaa",
                "a",
                "zz"};

        System.out.println(Arrays.deepToString(solution.sortByLength(inputArray)));
    }

    String[] sortByLength(String[] inputArray) {
        return Arrays.stream(inputArray).sorted(Comparator.comparing(String::length)).toArray(String[]::new);
    }
}
