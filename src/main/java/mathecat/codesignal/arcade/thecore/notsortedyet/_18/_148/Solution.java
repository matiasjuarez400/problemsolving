package mathecat.codesignal.arcade.thecore.notsortedyet._18._148;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String table1 = "<table><tr><td>1</td><td>TWO</td></tr></table>";
        String table2 = "<table><tr><td>mhPuzp82Mm</td><td>dQijA9O</td><td>x</td><td>2p1GX2lTrM</td></tr><tr><td>4hcQ</td><td>a046</td><td>8bQ7</td><td>Nmdt</td></tr><tr><td>jjC</td><td>zJ5SY05n</td><td>XQxJ</td><td>4yIXC8</td></tr></table>";
        String table3 = "<table><tr><td>jQu9ABs8l</td><td>9alQS</td><td>6j</td><td>x0C</td><td>VJwINu0wjE</td></tr><tr><td>52K</td><td>w5P</td><td>K0HTHBB</td><td>76H</td><td>2Up4kl</td></tr><tr><td>d7J9bn7lx</td><td>unJT</td><td>mdICgjl</td><td>v0</td><td>LKvS1LbYBo</td></tr><tr><td>eld9</td><td>O</td><td>Yqe184E9</td><td>b45QX0313A</td><td>4M02</td></tr><tr><td>6XKiOf96</td><td>wb7</td><td>HW5535kri</td><td>81U</td><td>V64O2502a</td></tr><tr><td>o8</td><td>col7G7g</td><td>y92s3R</td><td>q1</td><td>zl0LizILrm</td></tr></table>";

        System.out.println(solution.htmlTable(table1, 1, 0));
        System.out.println(solution.htmlTable(table2, 0, 0));
        System.out.println(solution.htmlTable(table3, 5, 4));
    }

    String htmlTable(String table, int row, int column) {
        String infoExtractorRegex = String.format(
                "^<table>(?:<tr>.*?</tr>){%s}<tr>(?:<td>[\\w]*</td>){%s}<td>(.*?)</td>", row, column);
        Pattern pattern = Pattern.compile(infoExtractorRegex);
        Matcher matcher = pattern.matcher(table);
        return matcher.find() ? matcher.group(1) : "No such cell";
    }
}
