package mathecat.codesignal.arcade.thecore.notsortedyet._11._88;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] input = {1, 2, 3, 4, 5, 6, 7, 8};

        System.out.println(solution.arrayConversion(input));
    }

    int arrayConversion(int[] inputArray) {
        int iteration = 1;
        int[] output = inputArray;

        while (output.length > 1) {
            int[] iterationOutput = new int[output.length / 2 + output.length % 2];
            for (int i = 0; i < output.length; i += 2) {
                if (iteration % 2 == 0) {
                    iterationOutput[i / 2] = output[i] * output[i + 1];
                } else {
                    iterationOutput[i / 2] = output[i] + output[i + 1];
                }
            }
            output = iterationOutput;
            iteration++;
        }

        return output[0];
    }
}
