package mathecat.codesignal.arcade.thecore.notsortedyet._11._94;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String input = "Look at this example of a correct text";
        int l = 5;
        int r = 15;

        System.out.println(solution.beautifulText(input, l, r));
    }

    boolean beautifulText(String inputString, int l, int r) {
        int[] lengths = Arrays.stream(inputString.split(" ")).mapToInt(String::length).toArray();

        int currentLineLength = 0;
        for (int i = 0; i < lengths.length; i++) {
            currentLineLength = i;
            for (int j = 0; j <= i; j++) {
                currentLineLength += lengths[j];
            }

            int k = i + 1;
            boolean success = true;
            while (k < lengths.length) {
                int lineLength = -1;
                boolean foundNextLine = false;
                for (int h = k; h < lengths.length; h++) {
                    lineLength += 1 + lengths[h];
                    if (currentLineLength == lineLength) {
                        foundNextLine = true;
                        k = h + 1;
                        break;
                    } else if (lineLength > currentLineLength) break;
                }

                if (!foundNextLine) {
                    success = false;
                    break;
                }
            }

            if (success && currentLineLength >= l && currentLineLength <= r) {
                System.out.println("Current size: " + currentLineLength);
                return true;
            }
        }

        return currentLineLength >= l && currentLineLength <= r;
    }
}
