package mathecat.codesignal.arcade.thecore.notsortedyet._17._145;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String letter = "Hi, hi Jane! I'm so. So glad to to finally be able to write - WRITE!! - to you!";

        System.out.println(solution.repetitionEncryption(letter));
    }

    int repetitionEncryption(String letter) {
        Pattern pattern = Pattern.compile("(\\w+)[^a-zA-Z]+\\1(?!\\w)", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(letter);

        int res = 0;
        while (matcher.find()) {
            res++;
        }
        return res;
    }
}