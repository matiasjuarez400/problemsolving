package mathecat.codesignal.arcade.thecore.notsortedyet._13._111;

import java.util.Arrays;
import java.util.stream.IntStream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String[] rows = {"#..##",
                ".##.#",
                ".#.##",
                "....."};

        System.out.println(Arrays.toString(solution.gravitation(rows)));
    }

    int[] gravitation(String[] rows) {
        boolean[][] matrix = convertToMatrix(rows);
        int[] effectiveSpaces = new int[rows[0].length()];

        for (int j = 0; j < matrix[0].length; j++) {
            int currentEmptySpaces = 0;
            for (int i = matrix.length - 1; i >= 0; i--) {
                if (matrix[i][j]) {
                    effectiveSpaces[j] += currentEmptySpaces;
                    currentEmptySpaces = 0;
                } else {
                    currentEmptySpaces++;
                }
            }
        }

        int min = Arrays.stream(effectiveSpaces).min().orElse(-1);

        return IntStream.range(0, effectiveSpaces.length).filter(i -> effectiveSpaces[i] == min).toArray();
    }

    boolean[][] convertToMatrix(String[] rows) {
        boolean[][] matrix = new boolean[rows.length][rows[0].length()];
        final char STONE = '#';

        for (int i = 0; i < rows.length; i++) {
            for (int j = 0; j < rows[i].length(); j++) {
                matrix[i][j] = rows[i].charAt(j) == STONE;
            }
        }

        return matrix;
    }
}
