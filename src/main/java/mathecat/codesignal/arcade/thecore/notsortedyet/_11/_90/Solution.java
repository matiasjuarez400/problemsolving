package mathecat.codesignal.arcade.thecore.notsortedyet._11._90;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        
        int[][] shoes = {{0, 21}, 
                         {1, 23},
                         {1, 21},
                         {0, 23}};

        System.out.println(solution.pairOfShoes(shoes));
    }

    boolean pairOfShoes(int[][] shoes) {
        Map<Integer, List<Integer>> shoesBySize = new HashMap<>();

        for (int[] shoe : shoes) {
            shoesBySize.computeIfAbsent(shoe[1], k -> new ArrayList<>());
            shoesBySize.get(shoe[1]).add(shoe[0]);
        }

        for (Integer size : shoesBySize.keySet()) {
            List<Integer> leftRightList = shoesBySize.get(size);
            int left = 0;
            int right = 0;

            for (int leftRight : leftRightList) {
                if (leftRight == 0) left++;
                else right++;
            }

            if (left != right) return false;
        }

        return true;
    }
}
