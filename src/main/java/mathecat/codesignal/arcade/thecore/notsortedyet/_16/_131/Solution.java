package mathecat.codesignal.arcade.thecore.notsortedyet._16._131;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String part = "02:20:00", total = "07:00:00";

        System.out.println(Arrays.toString(solution.videoPart(part, total)));
    }

    int[] videoPart(String part, String total) {
        int partSeconds = convertToSeconds(part);
        int totalSeconds = convertToSeconds(total);

        int nextStep = 2;
        int partPortion = partSeconds;
        int totalPortion = totalSeconds;

        while (nextStep <= partSeconds) {
            if (partPortion % nextStep == 0 && totalPortion % nextStep == 0) {
                partPortion /= nextStep;
                totalPortion /= nextStep;
            } else {
                nextStep++;
            }
        }

        return new int[] {partPortion, totalPortion};
    }

    int convertToSeconds(String timeString) {
        String[] splitted = timeString.split(":");

        int seconds = 0;
        seconds += Integer.parseInt(splitted[2]);
        seconds += Integer.parseInt(splitted[1]) * 60;
        seconds += Integer.parseInt(splitted[0]) * 3600;

        return seconds;
    }
}
