package mathecat.codesignal.arcade.thecore.notsortedyet._18._154;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println((int) '0');
        String[] p1 = {
                "               v",
                "v  ,,,,,\"Hello\"<",
                ">48*,          v",
                "\"!dlroW\",,,,,,v>",
                "25*,@         > "
        };

        String[] p2 = {"1+:::*.9`#@_"};

        String[] p3 = {
                ">25*\"!dlrow ,olleH\":v ",
                "                 v:,_@",
                "                 >  ^ "
        };

        String[] p4 = {
                "\"^a&EPm=kY}t/qYC+i9wHye$m N@~x+\"v",
                "\"|DsY<\"-\"z6n<[Yo2x|UP5VD:\">:#v_@>",
                "-:19+/\"0\"+,19+%\"0\"+,      ^  >39*"
        };

        String[] p7 = {
                "\">:#,_66*2-,@This prints itself out backwards......  but it has to be 80x1 cell"
        };

        String result = solution.befunge93(p4);
        System.out.println();
        System.out.println("RESULT: " + result);
    }

    String befunge93(String[] program) {
        int[] printedChars = new int[1];
        int rowLength = program[0].length();

        int rowStep = 0;
        int colStep = 1;

        int row = 0;
        int col = 0;

        Stack stack = new Stack();

        boolean stringMode = false;
        boolean skipNext = false;

        StringBuilder output = new StringBuilder();

        int instructions = 0;

        while (instructions <= 100000 && output.length() < 100) {
            char current = program[row].charAt(col);

            String top = stack.peek();

            printChar(current, printedChars, rowLength);

            if (skipNext) {
              skipNext = false;
            } else if (stringMode) {
                if (current == '"') stringMode = false;
                else {
                    if (Character.isDigit(current)) {
                        stack.push("0" + current);
                    } else {
                        stack.push(current);
                    }
                }
            } else if (current == '@') {
                break;
            } else if (current == '"') {
                stringMode = true;
            } else if (current == 'v') {
                rowStep = 1;
                colStep = 0;
            } else if (current == '<') {
                rowStep = 0;
                colStep = -1;
            } else if (current == '>') {
                rowStep = 0;
                colStep = 1;
            } else if (current == '^') {
                rowStep = -1;
                colStep = 0;
            } else if (current == ',') {
                String poped = stack.pop();
//                output.append(poped);
                if (poped.matches("-?\\d+")) {
                    int value = Integer.parseInt(poped);
                    if (value < 10) output.append(value);
                    else output.append((char) value);
                } else {
                    output.append(poped);
                }
            } else if (current == '.') {
                String poped = stack.pop();

                output.append(poped).append(" ");
            } else if (Character.isDigit(current)) {
                stack.push(current);
            } else if (Arrays.asList('*', '/', '+', '-', '%', '`').contains(current)) {
                String rightRaw = stack.pop();
                String leftRaw = stack.pop();

                String notStartingWithZeroRegex = "(?:^0$)|(?:^-?[1-9]\\d*$)";

                int right;
                if (rightRaw.matches(notStartingWithZeroRegex)) {
                    right = Integer.parseInt(rightRaw);
                } else if (rightRaw.startsWith("0")) {
                    right = (int) rightRaw.charAt(1);
                } else {
                    right = (int) rightRaw.charAt(0);
                }

                int left;
                if (leftRaw.matches(notStartingWithZeroRegex)) {
                    left = Integer.parseInt(leftRaw);
                } else if (leftRaw.startsWith("0")) {
                    left = (int) leftRaw.charAt(1);
                } else {
                    left = (int) leftRaw.charAt(0);
                }
                
                int result;
                if (current == '*') result = left * right;
                else if (current == '/') result = left / right;
                else if (current == '+') result = left + right;
                else if (current == '-') result = left - right;
                else if (current == '%') result = left % right;
                else if (current == '`') result = left > right ? 1 : 0;
                else throw new IllegalStateException("Shouldn't be here");

                stack.push(result);
            } else if (current == '#') {
                skipNext = true;
            } else if (current == '_') {
                String poped = stack.pop();
//                col = poped.equals("0") ? col + 1 : col - 1;
                colStep = poped.equals("0") ? 1 : -1;
                rowStep = 0;
            } else if (current == '|') {
                String poped = stack.pop();
//                row = poped.equals("0") ? row + 1 : row - 1;
                rowStep = poped.equals("0") ? 1 : -1;
                colStep = 0;
            } else if (current == '!') {
                String poped = stack.pop();

                if (poped.equals("0")) stack.push(1);
                else stack.push(0);
            } else if (current == ':') {
                String poped = stack.pop();
                stack.push(poped);
                stack.push(poped);
            } else if (current == '\\') {
                String first = stack.pop();
                String second = stack.pop();

                stack.push(first);
                stack.push(second);
            } else if (current == '$') {
                stack.pop();
            }

            row += rowStep;
            col += colStep;

            if (row >= program.length) row = 0;
            else if (row < 0) row = program.length -1;
            else if (col >= program[row].length()) col = 0;
            else if (col < 0) col = program[row].length() - 1;

            instructions++;
        }

        return output.toString();
    }

    private class Stack {
        private List<String> storage;

        public Stack() {
            this.storage = new ArrayList<>();
        }

        public String pop() {
            if (storage.isEmpty()) return "0";
            return storage.remove(storage.size() - 1);
        }

        public void push(String s) {
            this.storage.add(s);
        }

        public void push(char c) {
            push(c + "");
        }

        public void push(int i) {
            push(Integer.toString(i));
        }

        public String peek() {
            if (storage.isEmpty()) return "";
            return storage.get(storage.size() - 1);
        }
    }

    private void printChar(char c, int[] printInfo, int rowLength) {
        if (1 == 1) return;
        if (c == ' ') c = '¬';

        if (printInfo[0] == rowLength - 1) {
            System.out.println(" " + c);
            printInfo[0] = 1;
        } else {
            System.out.print(" " + c);
            printInfo[0]++;
        }
    }
}
