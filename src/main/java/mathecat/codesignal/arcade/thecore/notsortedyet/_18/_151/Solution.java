package mathecat.codesignal.arcade.thecore.notsortedyet._18._151;

import java.util.Arrays;
import java.util.stream.IntStream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String expr = "((2 + 2) * 2) * 3 + (2 + (2 * 2))";

        System.out.println(solution.firstOperationCharacter(expr));
    }

    int firstOperationCharacter(String expr) {
        Info[] infos = IntStream.range(0, expr.length())
                .filter(i -> Arrays.asList('(', ')', '+', '*').contains(expr.charAt(i)))
                .mapToObj(i -> new Info(i, expr.charAt(i)))
                .toArray(Info[]::new);

        int maxDepth = 0;
        int depth = 0;
        for (Info info : infos) {
            char operator = info.getOperator();
            if (operator == '(') {
                depth++;
                if (depth > maxDepth) maxDepth = depth;
            }
            else if (operator == '+' || operator == '*') info.setDepth(depth);
            else if (operator == ')') depth--;
        }

        final int maxDepthFinal = maxDepth;
        Info[] maxDepthInfos = Arrays.stream(infos)
                .filter(info -> info.getDepth() == maxDepthFinal)
                .toArray(Info[]::new);

        for (Info info : maxDepthInfos) {
            if (info.getOperator() == '*') return info.getIndex();
        }

        return maxDepthInfos[0].getIndex();
    }

    private class Info {
        private int index;
        private char operator;
        private int depth;

        public Info(int index, char operator) {
            this.index = index;
            this.operator = operator;
        }

        public int getIndex() {
            return index;
        }

        public char getOperator() {
            return operator;
        }

        public int getDepth() {
            return depth;
        }

        public void setDepth(int depth) {
            this.depth = depth;
        }
    }
}
