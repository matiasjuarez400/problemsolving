package mathecat.codesignal.arcade.thecore.notsortedyet._14._119;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[][] matrix =
                {
                  {2, 7, 1},
                  {0, 2, 0},
                  {1, 3, 1}
                };

        System.out.println(solution.rowsRearranging(matrix));
    }

    boolean rowsRearranging(int[][] matrix) {
        Comparator<int[]> comparator = (o1, o2) -> {
            int result = 0;
            for (int i = 0; i < o1.length; i++) {
                result = o1[i] - o2[i];
                if (result != 0) break;
            }

            return result;
        };

        List<int[]> rows = Arrays.stream(matrix)
                .sorted(comparator)
                .collect(Collectors.toList());

        for (int i = 0; i < matrix[0].length; i++) {
            for (int j = 0; j < rows.size() - 1; j++) {
                if (rows.get(j)[i] >= rows.get(j + 1)[i]) return false;
            }
        }

        return true;
    }
}
