package mathecat.codesignal.arcade.thecore.notsortedyet._11._89;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] input = {3, 5, 2, 4, 5};

        System.out.println(Arrays.toString(solution.arrayPreviousLess(input)));
    }

    int[] arrayPreviousLess(int[] items) {
        int[] output = new int[items.length];

        for (int i = 0; i < items.length; i++) {
            int current = items[i];
            output[i] = -1;
            for (int j = i - 1; j >= 0; j--) {
                if (items[j] < current) {
                    output[i] = items[j];
                    break;
                }
            }
        }

        return output;
    }
}
