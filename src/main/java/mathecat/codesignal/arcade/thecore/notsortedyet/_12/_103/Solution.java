package mathecat.codesignal.arcade.thecore.notsortedyet._12._103;

import java.util.Arrays;

public class Solution {
    public static void main(String[] args) {
        char[][] canvas = {{'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a'},
                          {'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a'},
                          {'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a'},
                          {'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b'},
                          {'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b'}};

        int[] rectangle = {1, 1, 4, 3};

        Solution solution = new Solution();

        System.out.println(Arrays.deepToString(solution.drawRectangle(canvas, rectangle)));
    }

    char[][] drawRectangle(char[][] canvas, int[] rectangle) {
        int x1 = rectangle[0];
        int y1 = rectangle[1];
        int x2 = rectangle[2];
        int y2 = rectangle[3];

        canvas[y1][x1] = '*';
        canvas[y2][x1] = '*';
        canvas[y1][x2] = '*';
        canvas[y2][x2] = '*';

        for (int row = y1 + 1; row < y2; row++) {
            canvas[row][x1] = '|';
            canvas[row][x2] = '|';
        }

        for (int col = x1 + 1; col < x2; col++) {
            canvas[y1][col] = '-';
            canvas[y2][col] = '-';
        }

        return canvas;
    }
}
