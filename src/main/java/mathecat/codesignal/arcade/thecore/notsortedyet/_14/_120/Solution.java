package mathecat.codesignal.arcade.thecore.notsortedyet._14._120;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] a = {152, 23, 7, 887, 243};

        System.out.println(Arrays.toString(solution.digitDifferenceSort(a)));
    }

    int[] digitDifferenceSort(int[] a) {
        List<Entry> entries = IntStream.range(0, a.length)
                .mapToObj(i -> new Entry(i, a[i]))
                .sorted(Comparator.comparing(Entry::getMaxMinDiff).thenComparing(Comparator.comparing(Entry::getIndex).reversed()))
                .collect(Collectors.toList());

        return entries.stream().mapToInt(Entry::getValue).toArray();
    }

    private class Entry {
        private int index;
        private int value;
        private int maxMinDiff;

        public Entry(int index, int value) {
            this.index = index;
            this.value = value;

            int[] maxMin = getMaxAndMinDigit(value);
            this.maxMinDiff = maxMin[1] - maxMin[0];
        }

        public int getIndex() {
            return index;
        }

        public int getValue() {
            return value;
        }

        public int getMaxMinDiff() {
            return maxMinDiff;
        }

        private int[] getMaxAndMinDigit(int number) {
            int[] result = {Integer.MAX_VALUE, Integer.MIN_VALUE};

            Integer.toString(number).chars().map(Character::getNumericValue)
                    .forEach(i -> {
                        if (i < result[0]) result[0] = i;
                        if (i > result[1]) result[1] = i;
                    });

            return result;
        }
    }
}
