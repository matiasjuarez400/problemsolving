package mathecat.codesignal.arcade.thecore.notsortedyet._13._107;

import java.util.Arrays;

class Solution {
    public static void main(String[] args) {
        boolean[][] matrix = {{true, false, false},
                              {false, true, false},
                              {false, false, false}};

        Solution solution = new Solution();

        System.out.println(Arrays.deepToString(solution.minesweeper(matrix)));
    }

    int[][] minesweeper(boolean[][] matrix) {
        int[][] result = new int[matrix.length][matrix[0].length];

        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                if (matrix[i][j]) {
                    for (int im = Integer.max(0, i - 1); im < Integer.min(result.length, i + 2); im++) {
                        for (int jm = Integer.max(0, j - 1); jm < Integer.min(result[im].length, j + 2); jm++) {
                            if (im == i && j == jm) continue;
                            result[im][jm]++;
                        }
                    }
                }
            }
        }

        return result;
    }
}
