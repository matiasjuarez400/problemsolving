package mathecat.codesignal.arcade.thecore.notsortedyet._12._105;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.solve2();
    }

    private void solve1() {
        int[][] matrix = {{1, 0, 0, 2, 0, 0, 3},
                {0, 1, 0, 2, 0, 3, 0},
                {0, 0, 1, 2, 3, 0, 0},
                {8, 8, 8, 9, 4, 4, 4},
                {0, 0, 7, 6, 5, 0, 0},
                {0, 7, 0, 6, 0, 5, 0},
                {7, 0, 0, 6, 0, 0, 5}};

        int width = 7;
        int[] center = {3, 3};
        int t = 1;

        int[][] result = starRotation(matrix, width, center, t);

        System.out.println(Arrays.deepToString(result));
    }

    private void solve2() {
        int[][] matrix = {{1, 0, 0, 2, 0, 0, 3},
                          {0, 1, 0, 2, 0, 3, 0},
                          {0, 0, 1, 2, 3, 0, 0},
                          {8, 8, 8, 9, 4, 4, 4},
                          {0, 0, 7, 6, 5, 0, 0}};

        int width = 3;
        int[] center = {1, 5};
        int t = 81;

        int[][] result = starRotation(matrix, width, center, t);

        System.out.println(Arrays.deepToString(result));
    }

    int[][] starRotation(int[][] matrix, int width, int[] center, int t) {
        int halfSide = width / 2;
        int rowStart = center[0] - halfSide;
        int rowEnd = center[0] + halfSide;
        int colStart = center[1] - halfSide;
        int colEnd = center[1] + halfSide;

        List<Integer> middleCol = new ArrayList<>();
        for (int i = 0; i < width; i++) {
            middleCol.add(matrix[i + rowStart][center[1]]);
        }

        List<Integer> upDownDiagonal = new ArrayList<>();
        for (int i = 0; i < width; i++) {
            upDownDiagonal.add(matrix[i + rowStart][i + colStart]);
        }

        List<Integer> middleRow = new ArrayList<>();
        for (int i = 0; i < width; i++) {
            middleRow.add(matrix[center[0]][i + colStart]);
        }

        List<Integer> downUpDiagonal = new ArrayList<>();
        for (int i = 0; i < width; i++) {
            downUpDiagonal.add(matrix[rowEnd - i][colStart + i]);
        }

        List<List<Integer>> starElements = new ArrayList<>();
        starElements.add(downUpDiagonal);
        starElements.add(middleRow);
        starElements.add(upDownDiagonal);
        starElements.add(middleCol);

        rotate(t, starElements);

        for (int i = 0; i < width; i++) {
            matrix[i + rowStart][center[1]] = starElements.get(3).get(i);
        }

        for (int i = 0; i < width; i++) {
            matrix[i + rowStart][i + colStart] = starElements.get(2).get(i);
        }

        for (int i = 0; i < width; i++) {
            matrix[center[0]][i + colStart] = starElements.get(1).get(i);
        }

        for (int i = 0; i < width; i++) {
            matrix[rowEnd - i][colStart + i] = starElements.get(0).get(i);
        }

        return matrix;
    }

    private void rotate(int t, List<List<Integer>> starElements) {
        int effectiveRotations = t % 8;

        for (int i = 0; i < effectiveRotations; i++) {
            List<Integer> lastElement = starElements.remove(starElements.size() - 1);
            Collections.reverse(lastElement);
            starElements.add(0, lastElement);
        }
    }
}
