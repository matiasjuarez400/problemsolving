package mathecat.codesignal.arcade.thecore.notsortedyet._18._147;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String[] lrcLyrics = {"[00:09.01]",
                "[00:10.01] Sweet dreams are made of this",
                "[00:13.26] Who am I to disagree?",
                "[00:17.01] Travel the world and the seven seas",
                "[00:20.95] Everybodys looking for something",
                "[00:24.57]",
                "[00:24.82] Some of them want to use you",
                "[00:28.64] Some of them want to get used by you",
                "[00:32.45] Some of them want to abuse you",
                "[00:36.32] Some of them want to be abused",
                "[00:40.32]",
                "[00:52.00] Sweet dreams are made of this",
                "[00:55.37] Who am I to disagree?",
                "[00:59.18] Travel the world and the seven seas",
                "[01:03.00] Everybodys looking for something",
                "[01:48.34] Some of them want to use you",
                "[01:52.16] Some of them want to get used by you",
                "[01:55.97] Some of them want to abuse you",
                "[01:59.72] Some of them want to be abused",
                "[02:03.58]",
                "[01:18.17]",
                "[01:29.17] Hold your head up, movin on",
                "[01:19.18] Hold your head up",
                "[01:31.11] Keep your head up",
                "[01:19.92] Keep your head up, movin on",
                "[01:21.86] Hold your head up, movin on",
                "[01:23.74] Keep your head up, movin on",
                "[01:25.67] Hold your head up, movin on",
                "[01:27.55] Keep your head up, movin on"
        };

        String songLength = "00:09:32";

        String[] subRipLyrics = solution.lrc2subRip(lrcLyrics, songLength);

        for (String subRipLyric : subRipLyrics) {
            System.out.println(subRipLyric);
        }
    }

    String[] lrc2subRip(String[] lrcLyrics, String songLength) {
        List<LrcInfoProcessor> lrcInfoProcessors = Arrays.stream(lrcLyrics)
                .map(LrcInfoProcessor::new)
                .collect(Collectors.toList());

        List<String> result = new ArrayList<>();
        for (int i = 1; i < lrcInfoProcessors.size(); i++) {
            result.addAll(LrcToSubRipConverter.convert(lrcInfoProcessors.get(i - 1), lrcInfoProcessors.get(i), i));
        }

        result.addAll(LrcToSubRipConverter.convert(lrcInfoProcessors.get(lrcInfoProcessors.size() - 1), songLength, lrcInfoProcessors.size()));

        return result.toArray(new String[0]);
    }

    private static class LrcToSubRipConverter {
        private static final String SINGLE_TIME_FORMAT = "%02d:%02d:%02d,%03d";
        private static final String WHOLE_TIME_FORMAT = "%s --> %s";
        private static final Pattern songLengthPattern = Pattern.compile("(\\d{2}):(\\d{2}):(\\d{2})");

        public static List<String> convert(LrcInfoProcessor main, LrcInfoProcessor next, int lrcInfoIndex) {
            List<String> result = new ArrayList<>();

            result.add(Integer.toString(lrcInfoIndex));

            String initialTime = transformTime(main);
            String endTime = transformTime(next);

            result.add(String.format(WHOLE_TIME_FORMAT, initialTime, endTime));

            result.add(main.getContent());

            result.add("");

            return result;
        }

        public static List<String> convert(LrcInfoProcessor main, String songLength, int lrcInfoIndex) {
            List<String> result = new ArrayList<>();

            result.add(Integer.toString(lrcInfoIndex));

            String initialTime = transformTime(main);
            String endTime = transformTime(songLength);

            result.add(String.format(WHOLE_TIME_FORMAT, initialTime, endTime));
            result.add(main.getContent());

            return result;
        }

        private static String transformTime(String songLength) {
            Matcher songLengthMatcher = songLengthPattern.matcher(songLength);

            if (!songLengthMatcher.matches()) {
                throw new IllegalArgumentException("Received unparseable input: " + songLengthMatcher);
            }

            return transformTime(
                    Integer.parseInt(songLengthMatcher.group(1)),
                    Integer.parseInt(songLengthMatcher.group(2)),
                    Integer.parseInt(songLengthMatcher.group(3)),
                    0
            );
        }

        private static String transformTime(LrcInfoProcessor lrcInfoProcessor) {
            return transformTime(0, lrcInfoProcessor.getMinutes(), lrcInfoProcessor.getSeconds(), lrcInfoProcessor.getMillis());
        }

        private static String transformTime(int hours, int minutes, int seconds, int millis) {
            if (minutes >= 60) {
                int extraHours = minutes / 60;
                hours += extraHours;
                minutes -= 60 * extraHours;
            }

            return String.format(SINGLE_TIME_FORMAT, hours, minutes, seconds, millis * 10);
        }
    }

    private static class LrcInfoProcessor {
        private int minutes;
        private int seconds;
        private int millis;
        private String content;
        private static final Pattern linePattern = Pattern.compile("\\[(\\d{2}):(\\d{2})\\.(\\d{2})\\]\\s*(.*)");

        public LrcInfoProcessor(String lrcLine) {
            Matcher lineMatcher = linePattern.matcher(lrcLine);

            if (lineMatcher.matches()) {
                this.minutes = Integer.parseInt(lineMatcher.group(1));
                this.seconds = Integer.parseInt(lineMatcher.group(2));
                this.millis = Integer.parseInt(lineMatcher.group(3));
                this.content = lineMatcher.group(4);
            } else {
                throw new IllegalArgumentException("Received unparseable input: " + lrcLine);
            }
        }

        public int getMinutes() {
            return minutes;
        }

        public int getSeconds() {
            return seconds;
        }

        public int getMillis() {
            return millis;
        }

        public String getContent() {
            return content;
        }
    }
}
