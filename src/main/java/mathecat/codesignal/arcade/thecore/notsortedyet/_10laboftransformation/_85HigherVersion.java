package mathecat.codesignal.arcade.thecore.notsortedyet._10laboftransformation;

public class _85HigherVersion {
    public static void main(String[] args) {

    }

    boolean higherVersion(String ver1, String ver2) {
        String[] ver1s = ver1.split("\\.");
        String[] ver2s = ver2.split("\\.");

        for (int i = 0; i < ver1s.length; i++) {
            if (ver1s[i].equals(ver2s[i])) continue;
            return Integer.parseInt(ver1s[i]) - (Integer.parseInt(ver2s[i])) > 0;
        }

        return false;
    }
}
