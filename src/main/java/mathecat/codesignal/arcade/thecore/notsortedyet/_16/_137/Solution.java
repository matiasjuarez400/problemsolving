package mathecat.codesignal.arcade.thecore.notsortedyet._16._137;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.holiday(3, "Wednesday", "November", 2016));
        System.out.println(solution.holiday(5, "Thursday", "November", 2016));
        System.out.println(solution.holiday(3, "Thursday", "January", 2101));
    }

    int holiday(int x, String weekDay, String month, int yearNumber) {
        final List<String> monthNames = Arrays.asList("January", "February", "March",
                "April", "May", "June", "July", "August", "September",
                "October", "November", "December");

        final List<String> dayNames = Arrays.asList("Sunday", "Monday", "Tuesday", "Wednesday",
                "Thursday", "Friday", "Saturday");

        Calendar calendar = Calendar.getInstance();
//        calendar.setFirstDayOfWeek(Calendar.SATURDAY);
        calendar.set(Calendar.YEAR, yearNumber);
        calendar.set(Calendar.MONTH, monthNames.indexOf(month));
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        int searchedDayOfWeek = dayNames.indexOf(weekDay) + 1;

        for (int i = 0; i < 7; i++) {
            if (calendar.get(Calendar.DAY_OF_WEEK) == searchedDayOfWeek) break;
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        for (int currentDaySeqNumber = 1; currentDaySeqNumber < x; currentDaySeqNumber++) {
            calendar.add(Calendar.DAY_OF_MONTH, 7);
        }

        String calendarMonth = monthNames.get(calendar.get(Calendar.MONTH));

        if (!calendarMonth.equals(month)) return -1;

        return calendar.get(Calendar.DAY_OF_MONTH);
    }
}
