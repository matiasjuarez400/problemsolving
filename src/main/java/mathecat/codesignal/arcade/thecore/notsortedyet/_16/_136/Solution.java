package mathecat.codesignal.arcade.thecore.notsortedyet._16._136;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Solution {
    public static void main(String[] args) {
        System.out.println(new Solution().missedClasses(2014, new int[]{2, 3}, new String[]{"11-04", "02-22", "02-23", "03-07", "03-08", "05-09" }));
    }

    int missedClasses(int year, int[] daysOfTheWeek, String[] holidays) {
        int[] daysOfTheWeekModified = Arrays.stream(daysOfTheWeek).map(oldDay -> oldDay != 7 ? oldDay + 1 : 1).toArray();
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
        List<Calendar> calList = Stream.of(holidays).map(holiday -> {
            try {
                Date date = sdf.parse(holiday);
                Calendar cal = new GregorianCalendar() {
                    @Override
                    public String toString() {
                        return this.getTime().toString();
                    }
                };
                cal.setTime(date);

                cal.set(Calendar.YEAR, cal.get(Calendar.MONTH) >= Calendar.SEPTEMBER ? year : year + 1);
                System.out.println(cal.getTime());
                return cal;
            } catch (ParseException e) {
                throw new IllegalArgumentException(e);
            }
        }).collect(Collectors.toList());
        return (int) calList.stream().filter(cal -> Arrays.stream(daysOfTheWeekModified).anyMatch(day -> cal.get(Calendar.DAY_OF_WEEK) == day)).count();
    }

}

