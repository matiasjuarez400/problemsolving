package mathecat.codesignal.arcade.thecore.notsortedyet._17._143;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        String pairOfLines1 = "cough\tbough";

        System.out.println(solution.eyeRhyme(pairOfLines1));
    }

    Boolean eyeRhyme(String pairOfLines) {
        Pattern pattern = Pattern.compile(".*(...)\\t.*(...)");
        Matcher matcher = pattern.matcher(pairOfLines);
        matcher.matches();
        return matcher.group(1).equals(matcher.group(2));
    }
}
