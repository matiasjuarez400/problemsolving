package mathecat.codesignal.common;

// Singly-linked lists are already defined with this interface:
public class ListNode<T> {
    public ListNode(T x) {
        value = x;
    }

    public T value;
    public ListNode<T> next;

    public static <T> ListNode<T> createFromArray(T... a) {
        final ListNode<T> returnNode = new ListNode<>(a[0]);
        ListNode<T> current = returnNode;
        for (int i = 1; i < a.length; i++) {
            current.next = new ListNode<>(a[i]);
            current = current.next;
        }

        return returnNode;
    }
}



