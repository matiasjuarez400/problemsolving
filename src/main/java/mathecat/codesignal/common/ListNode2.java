package mathecat.codesignal.common;

import java.util.HashSet;
import java.util.Set;

//     Singly-linked lists are already defined with this interface:
public class ListNode2<T> {
    public ListNode2(T x) {
        value = x;
    }
    public T value;
    public ListNode2<T> next;

    @Override
    public String toString() {
        Set<ListNode2<T>> visited = new HashSet<>();

        StringBuilder sb = new StringBuilder();
        ListNode2<T> current = this;
        boolean isCircularList = false;
        do {
            visited.add(current);
            sb.append(current.value).append("-");
            current = current.next;

            if (visited.contains(current)) {
                isCircularList = true;
                break;
            }
        } while (current != null);

        if (isCircularList) {
            return "CIRCULAR LIST: " + sb.toString();
        }

        return sb.toString();
    }

    public static <T> ListNode2<T> createFromArray(T... a) {
        final ListNode2<T> returnNode = new ListNode2<>(a[0]);
        ListNode2<T> current = returnNode;
        for (int i = 1; i < a.length; i++) {
            current.next = new ListNode2<>(a[i]);
            current = current.next;
        }

        return returnNode;
    }

    public ListNode2<T> reverse() {
        ListNode2<T> pivot = this;
        ListNode2<T> first = this;

        while (pivot.next != null) {
            ListNode2<T> second = pivot.next;
            ListNode2<T> third = second.next;

            second.next = first;
            first = second;
            pivot.next = third;
        }

        return first;
    }

    public ListNode2(ListNode2<T> that) {
        this.value = that.value;

        ListNode2<T> returnNodeHead = this;

        ListNode2<T> currentThat = that.next;
        while (currentThat != null) {
            returnNodeHead.next = new ListNode2<>(currentThat.value);
            returnNodeHead = returnNodeHead.next;
            currentThat = currentThat.next;
        }
    }

    public ListNode2(ListNode<T> that) {
        this.value = that.value;

        ListNode2<T> returnNodeHead = this;

        ListNode<T> currentThat = that.next;
        while (currentThat != null) {
            returnNodeHead.next = new ListNode2<>(currentThat.value);
            returnNodeHead = returnNodeHead.next;
            currentThat = currentThat.next;
        }
    }
}
