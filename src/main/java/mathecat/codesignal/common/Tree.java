package mathecat.codesignal.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Tree<T> {
    public Tree(T x) {
        value = x;
    }

    public Tree() {
    }

    public T value;
    public Tree<T> left;
    public Tree<T> right;

    public static <T> Tree<T> parse(String input, Class<T> clazz) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(input, Tree.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
