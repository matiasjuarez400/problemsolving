package mathecat.codesignal.tournaments.AAA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.stringsConstruction("abc", "abccba");
    }







    int pagesNumbering(int n) {

        int digits = 0;
        for (int i = 1; i <= n; i++) {
            String is = Integer.toString(i);
            digits += is.length();
        }

        return digits;
    }

    boolean charactersRearrangement(String string1, String string2) {
        List<Character> list1 = convertToList(string1);
        List<Character> list2 = convertToList(string2);

        Iterator<Character> it = list1.iterator();

        while (it.hasNext()) {
            Character next = it.next();
            if (list2.contains(next)) {
                list2.remove(next);
                it.remove();
            }
        }

        return list1.size() == 0 && list2.size() == 0;
    }

    String myConcat(String[] strings, String separator) {
        return Arrays.stream(strings).collect(Collectors.joining("-"));
    }

    List<Character> convertToList(String s) {
        List<Character> s1l = new ArrayList<>();
        for (char c : s.toCharArray()) {
            s1l.add(c);
        }
        return s1l;
    }

    int stringsConstruction(String a, String b) {
        List<Character> listb = convertToList(b);
        List<Character> lista = convertToList(a);

        int count = 0;
        while (listb.size() > 0) {
            for (int i = 0; i < listb.size(); i++) {
                char c = listb.get(i);
                if (lista.contains(c)) {
                    listb.remove((Character) c);
                    if (i == lista.size() - 1) count++;
                } else {
                    listb.clear();
                    break;
                }
            }
        }

        return count;
    }

    int telephoneGame(String[] messages) {
        for (int i = 1; i < messages.length; i++) {
            if (messages[0].equals(messages[i])) continue;

            return i;
        }

        return -1;
    }

    int nthElementFromTheEnd(ListNode<Integer> l, int n) {
        List<Integer> list = new ArrayList<>();
        list.add(l.value);

        while (l.next != null) {
            l = l.next;
            list.add(l.value);
        }

        if (list.size() < n) return -1;
        return list.get(list.size() - n);
    }

     class ListNode<T> {
   ListNode(T x) {
     value = x;
   }
   T value;
   ListNode<T> next;
 }
}
