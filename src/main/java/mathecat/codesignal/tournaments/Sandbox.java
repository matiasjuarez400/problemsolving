package mathecat.codesignal.tournaments;

import mathecat.codesignal.common.ListNode2;
import mathecat.codesignal.common.Tree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class Sandbox {
    public static void main(String[] args) {
        Sandbox sandbox = new Sandbox();


        Integer[] n = {1, 2, 2, 3};
        ListNode2<Integer> l = ListNode2.createFromArray(n);

        System.out.println(sandbox.isListPalindrome(l));
    }

    ArrayList<Double> liquidMixing(List<Integer> densities) {
        ArrayList<Double> result = new ArrayList<>();
        result.add((double)densities.get(densities.size() / 2));
        for (int i = 1; i < densities.size(); i++) {
            for (int j = 0; j <= i; j++) {
                if (densities.get(i) <= densities.get(j)) {
                    int tmp = densities.get(i);
                    for (int k = i; k > j; k--) {
                        densities.set(k, densities.get(k - 1));
                    }
                    densities.set(j, tmp);
                    if (i % 2 == 1) {
                        result.add((densities.get((i + 1) / 2) +
                                densities.get(i / 2)) / 2.0);
                    } else {
                        result.add((double)densities.get(i / 2));
                    }
                    break;
                }
            }
        }
        return result;
    }

    boolean isListPalindrome(ListNode2<Integer> l) {
        List<Integer> list = new ArrayList<>();

        while (l != null) {
            list.add(l.value);
            l = l.next;
        }

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) != list.size() - 1 - i) return false;
        }
        return true;
    }


    int[] extractEachKth(int[] inputArray, int k) {
        return Arrays.stream(inputArray)
                .filter(i -> i % k != 0)
                .toArray();
    }

    boolean bishopAndPawn(String bishop, String pawn) {
        return Math.abs(bishop.charAt(0) - pawn.charAt(0)) == Math.abs(bishop.charAt(1) - pawn.charAt(1));
    }

    int numberOfEvenDigits(int n) {
        String s = Integer.toString(n);
        int acc = 0;
        for (char c : s.toCharArray()) {
            int digit = Character.getNumericValue(c);
            if (digit % 2 == 0) acc++;
        }
        return acc;
    }

    String[] newNumeralSystem(char number) {
        List<String> pairs = new ArrayList<>();
        for (int i = 0; i < 26; i++) {
            for (int j = 0; j < 26; j++) {
                char left = (char) ('A' + i);
                char right = (char) ('A' + j);
                if (left + right - 'A' * 2 == number) {
                    pairs.add(left + " + " + right);
                }
            }
        }
        return pairs.stream().toArray(String[]::new);
    }

    int countIncreasingSequences(int n, int k) {

        /*
         * vector dp (short for dynamic programming)
         * is used for storing the interim values.
         */
        int[] line = new int[k + 1];
        int[][] dp = new int[n + 1][line.length];
        dp[0][0] = 1;

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                for (int q = 0; q < j; q++) {
                    dp[i][j] += dp[i - 1][q];
                }
            }
        }

        int sum = 0;
        for (int j = 1; j <= k; j++) {
            sum += dp[n][j];
        }

        return sum;
    }

    boolean isSuspiciousRespondent(boolean ans1, boolean ans2, boolean ans3) {
        return ans1 && ans2 && ans3 || !ans1 && !ans2 && !ans3;
    }

    boolean isInRange(int a, int b, int c) {
        return a < b && b < c;
    }

    int sequenceElement(int[] a, int n) {
        while (a.length <= n) {
            a = generateNextStep(a);
        }

        return a[a.length - 1];
    }

    int[] generateNextStep(int[] a) {
        int[] next = new int[a.length + 1];

        for (int i = 0; i < a.length; i++) {
            next[i] = a[i];
        }

        int acc = 0;
        for (int i = a.length - 5; i < a.length; i++) {
            acc += a[i];
        }
        next[next.length - 1] = acc % 10;
        return next;
    }

    int[] primeFactors2(int n) {
        int currentFactor = 2;
        Set<Integer> primes = new HashSet<>();
        while (n > 1) {
            if (n % currentFactor == 0) {
                n /= currentFactor;
                primes.add(currentFactor);
            } else {
                if (currentFactor != 2) {
                    currentFactor += 2;
                } else {
                    currentFactor = 3;
                }
            }
        }

        return primes.stream()
                .mapToInt(i -> i)
                .sorted()
                .toArray();
    }

    int higherVersion2(String ver1, String ver2) {
        String[] v1 = ver1.split("\\.");
        String[] v2 = ver2.split("\\.");

        for (int i = 0; i < v1.length; i++) {
            Integer v1i = Integer.parseInt(v1[i]);
            Integer v2i = Integer.parseInt(v2[i]);

            if (v1i > v2i) return 1;
            if (v2i > v1i) return -1;
        }
        return 0;
    }

    String lookAndSaySequenceNextElement(String element) {
        String result = "";

        char current = ' ';
        char acc = 0;
        for (int i = 0; i < element.length(); i++) {
            char next = element.charAt(i);
            if (current != next) {
                if (current != ' ') {
                    result += acc + current + "";
                }
                current = next;
                acc = 1;
            } else {
                acc++;
            }
        }

        return result;
    }

    int circleOfNumbers(int n, int firstNumber) {
        int movement = n / 2;
        if (firstNumber + movement > n) return firstNumber - movement;
        return firstNumber + movement;
    }

    int[][] setsOfFactors(int n) {
        List<List<Integer>> validSets = new ArrayList<>();
        for (int i = 1; i < n; i++) {
            List<Integer> set = new ArrayList<>();
            int acc = 1;
            boolean isValid = true;
            for (int j = i; j < n; j++) {
                if (acc == n) break;
                if (acc > n) {
                    isValid = false;
                    break;
                }
                acc *= j;
                set.add(j);
            }

            if (isValid) {
                validSets.add(set);
                Collections.reverse(set);
            }
        }

        Collections.reverse(validSets);

        int[][] output = new int[validSets.size()][];
        for (int i = 0; i < output.length; i++) {
            output[i] = validSets.get(i).stream().mapToInt(in -> in).toArray();
        }

        return output;
    }

    boolean checkFactorial(int n) {
        for (int i = 1; i <= n; i++) {
            int acc = 1;
            for(int j = 1; j <= i; j++) {
                acc *= j;
            }

            if (acc == n) return true;
            if (acc > n) return false;
        }
        return false;
    }

    int depositProfit(int deposit, int rate, int threshold) {
        double acc = deposit;
        int counter = 0;
        while (acc < threshold) {
            counter++;
            acc = acc * (1 + rate / 100.0);
        }
        return counter;
    }

    String caseUnification(String inputString) {
        int lowerAcc = 0;
        int upperAcc = 0;
        String upper = inputString.toUpperCase();
        String lower = inputString.toLowerCase();

        for (int i = 0; i < inputString.length(); i++) {
            if (inputString.charAt(i) == upper.charAt(i)) {
                upperAcc++;
            } else {
                lowerAcc++;
            }
        }

        return lowerAcc < upperAcc ? upper : lower;
    }

    int sumBelowBound(int bound) {
        int acc = 0;
        for (int i = 0; i < bound; i++) {
            acc += i;
            if (acc > bound) return i - 1;
        }
        return -1;
    }

    int squareDigitsSequence(int a0) {
        Set<Integer> sawNumbers = new HashSet<>();

        int currentResult = squareOfDigits(a0);

        while (!sawNumbers.contains(currentResult)) {
            currentResult = squareOfDigits(a0);
            sawNumbers.add(currentResult);
            a0 = currentResult;

        }

        return sawNumbers.size();
    }

    private int squareOfDigits(int n) {
        String ns = Integer.toString(n);

        int result = 0;
        for (char c : ns.toCharArray()) {
            int currentNum = Character.getNumericValue(c);
            result += currentNum * currentNum;
        }
        return result;
    }

    boolean insideCircle(int[] point, int[] center, int radius) {
        return Math.sqrt(Math.pow(point[0] - center[0], 2) + Math.pow(point[1] - center[1], 2)) <= radius;
    }

    int[] easyAssignmentProblem(int[][] skills) {
        int[] e1 = skills[0];
        int[] e2 = skills[1];

        if (e1[0] > e2[0]) return new int[] {1, 2};
        else if (e1[0] < e2[0]) return new int[] {2, 1};
        else return new int[] {1, 2};
    }

    String swapCase(String text) {
        String lower = text.toLowerCase();
        String upper = text.toUpperCase();

        String result = "";
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (c == lower.charAt(i)) result += upper.charAt(i);
            else result += lower.charAt(i);
        }

        return result;
    }

    boolean areIsomorphic(int[][] array1, int[][] array2) {
        if (array1 == null && array2 == null) return true;
        if (array1 == null ^ array2 == null) return false;
        if (array1.length != array2.length) return false;

        for (int i = 0; i < array1.length; i++) {
            if (array1[i].length != array2[i].length) return false;
        }
        return true;
    }

    int surpasserCount(int[] a) {
        long acc = 0;
        for (int i = 0; i < a.length; i++) {
            for (int j = i + 1; j < a.length; j++) {
                if (a[i] < a[j]) acc++;
            }
        }

        return (int) (acc % (1000000000 + 7));
    }

    int differentValuesInMultiplicationTable2(int n, int m) {
        Set<Integer> set = new HashSet<>();
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                set.add(i * j);
            }
        }
        return set.size();
    }

    int leftLeavesSum(Tree<Integer> t) {
        if (t == null) return 0;
        return sumLeft(t, false);
    }

    private int sumLeft(Tree<Integer> t, boolean sum) {
        int acc = sum ? t.value : 0;
        if (t.left != null) acc += sumLeft(t.left, true);
        if (t.right != null) acc += sumLeft(t.right, false);
        return acc;
    }

    int arrayMaximalAdjacentDifference(int[] inputArray) {
        int max = Integer.MIN_VALUE;

        for (int i = 1; i < inputArray.length; i++) {
            int diff = inputArray[i - 1] - inputArray[i];
            diff = Math.abs(diff);
            if (diff > max) max = diff;
        }

        return max;
    }

    private static boolean toBoolean(String binaryString) {
        return binaryString.charAt(binaryString.length() - 1) == '1';
    }

    private static void showBooleanResult(boolean A, boolean B, boolean C, boolean D) {
        System.out.println(String.format(
                "(  A && B  ||  C && D  ) == (  %s && %s  ||  %s && %s  ) == %s", A, B, C, D, (A && B || C && D)
        ));

        System.out.println(String.format(
                "( (A && B) || (C && D) ) == ( (%s && %s) || (%s && %s) ) == %s", A, B, C, D, ( (A && B) || (C && D) )
        ));

        System.out.println(new String(new char[80]).replace('\0', '*'));
    }
}
