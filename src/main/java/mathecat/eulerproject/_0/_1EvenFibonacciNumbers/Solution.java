package mathecat.eulerproject._0._1EvenFibonacciNumbers;

import mathecat.eulerproject.commons.generators.FibonacciSequenceGenerator;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        final Set<Integer> evenElements = new HashSet<>();

        final Consumer<Integer> processor = fibonacciElement -> {
            if (fibonacciElement % 2 == 0) evenElements.add(fibonacciElement);
        };

        final int sequenceLimit = 4_000_000;

        solution.generateFibonacciSequenceAndProcess(sequenceLimit, processor);

        System.out.println(evenElements.stream().reduce(0, Integer::sum));
    }

    private void generateFibonacciSequenceAndProcess(int sequenceValueLimit, Consumer<Integer> processor) {
        FibonacciSequenceGenerator generator = new FibonacciSequenceGenerator();

        int nextElement;
        while ((nextElement = generator.next()) < sequenceValueLimit) {
            processor.accept(nextElement);
        }
    }
}
