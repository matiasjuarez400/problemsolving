package mathecat.eulerproject._0._2LargestPrimeFactor;

import mathecat.eulerproject.commons.primes.PrimeUtils;

class Solution {
    public static void main(String[] args) {
        final long baseNumber = 600851475143L;

        long largestPrimeFactor = PrimeUtils.findMaxPrimeFactor(baseNumber);

        System.out.println(largestPrimeFactor);
    }
}
