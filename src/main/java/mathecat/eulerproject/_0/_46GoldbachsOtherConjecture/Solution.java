package mathecat.eulerproject._0._46GoldbachsOtherConjecture;

import mathecat.eulerproject.commons.primes.PrimeUtils;

import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        long answer = solution.solve();

        System.out.println("Solution found: " + answer);
    }

    public long solve() {
        List<Long> primes = new ArrayList<>();
        // We want to ignore number 2 because subtracting it to any odd number will cause another odd number.
        long primeLowerLimit = 3;

        long currentOddNumber = 9;
        while (true) {
            if (!PrimeUtils.isPrime(currentOddNumber)) {
                primes.addAll(PrimeUtils.findPrimesInRange(primeLowerLimit, currentOddNumber));
                primeLowerLimit = currentOddNumber + 1;

                if (!complainsWithGoldbach(currentOddNumber, primes)) break;
            }

            currentOddNumber += 2;
        }

        return currentOddNumber;
    }

    private boolean complainsWithGoldbach(long oddNumber, List<Long> primes) {
        for (Long prime : primes) {
            long target = (oddNumber - prime) / 2L;
            double baseNeeded = Math.sqrt(target);
            if (baseNeeded % 1L == 0) {
                System.out.printf("%s = %s + 2 * %s ^ 2\n", oddNumber, prime, baseNeeded);
                return true;
            }
        }

        return false;
    }
}
