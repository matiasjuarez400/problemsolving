package mathecat.eulerproject._0._49PrimePermutations;

import mathecat.eulerproject.commons.primes.PrimeUtilsWithMemory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class Solution {
    private final PrimeUtilsWithMemory primeUtilsWithMemory;

    public static void main(String[] args) {
        Solution solution = new Solution();

        List<String> answers = solution.solve();

        System.out.println("Found the following answers: ");
        answers.forEach(System.out::println);
    }

    public Solution() {
        this.primeUtilsWithMemory = new PrimeUtilsWithMemory(9999);
    }

    public List<String> solve() {
        List<Long> fourDigitPrimes = primeUtilsWithMemory.getPrimesInRange(1000, 9999);

        Map<String, List<Long>> permutationFamilies = getPermutationFamilies(fourDigitPrimes);

        return permutationFamilies.values().stream()
                .map(this::findWorkingCombination)
                .filter(result -> result.length() > 0)
                .collect(Collectors.toList());
    }

    /**
     * Find that combination (a, b, c) such that abs(a - b) == abs(b - c)
     * @param permutationFamily
     * @return
     */
    private String findWorkingCombination(List<Long> permutationFamily) {
        Collections.sort(permutationFamily);

        List<Long> combinationFound = doFindWorkingCombination(permutationFamily, new ArrayList<>());

        return combinationFound.stream()
                .map(Objects::toString)
                .collect(Collectors.joining());
    }

    private List<Long> doFindWorkingCombination(List<Long> permutationFamily, List<Long> currentCombination) {
        if (currentCombination.size() == 3) {
            if (checkIfCombinationSatisfiesDistances(currentCombination)) return currentCombination;
            return Collections.emptyList();
        }

        for (Long permutationFamilyElement : permutationFamily) {
            if (!currentCombination.isEmpty() &&
                    permutationFamilyElement <= currentCombination.get(currentCombination.size() - 1)) continue;

            currentCombination.add(permutationFamilyElement);
            List<Long> result = doFindWorkingCombination(permutationFamily, currentCombination);

            if (!result.isEmpty()) return result;

            currentCombination.remove(permutationFamilyElement);
        }

        return Collections.emptyList();
    }

    private boolean checkIfCombinationSatisfiesDistances(List<Long> combination) {
        return Math.abs(combination.get(0) - combination.get(1)) == Math.abs(combination.get(1) - combination.get(2));
    }

    private Map<String, List<Long>> getPermutationFamilies(List<Long> fourDigitPrimes) {
        Map<String, List<Long>> permutationFamilies = fourDigitPrimes.stream()
                .collect(Collectors.groupingBy(number -> {
                    String numberString = Long.toString(number);
                    char[] numberChars = numberString.toCharArray();
                    Arrays.sort(numberChars);
                    return new String(numberChars);
                }));

        permutationFamilies.keySet().stream()
                .filter(key -> permutationFamilies.get(key).size() < 3)
                .collect(Collectors.toSet())
                .forEach(permutationFamilies::remove);

        return permutationFamilies;
    }
}