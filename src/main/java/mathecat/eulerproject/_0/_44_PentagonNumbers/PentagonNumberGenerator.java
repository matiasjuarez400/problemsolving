package mathecat.eulerproject._0._44_PentagonNumbers;

public class PentagonNumberGenerator {
    /**
     * 0-based version of the formula n * (3 * n - 1) / 2
     * @param position - the pentagon n.
     * @return the pentagon number at position 'position'
     */
    public long generate(int position) {
        return ((3L * position * position + 5L * position) / 2L + 1L);
    }

    /**
     * Returns how much we have to subtract to the value at currentPosition 'currentPosition' to get to the previous pentagon number.
     * Position is 0 based:
     * Value | Difference | Position
     * 1     | 1          | 0
     * 5     | 4          | 1
     * 12    | 7          | 2
     * 22    | 10         | 3
     * @param currentPosition
     * @return
     */
    public long distanceToPreviousPentagon(int currentPosition) {
        return currentPosition * 3 + 1;
    }
}
