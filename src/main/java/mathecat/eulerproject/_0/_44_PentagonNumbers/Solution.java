package mathecat.eulerproject._0._44_PentagonNumbers;

public class Solution {
    private final GeneratorWrapper generator;

    public static void main(String[] args) {
        Solution solution = new Solution();
        String diffFound = solution.solve();
    }

    public Solution() {
        generator = new GeneratorWrapper();
    }

    public String solve() {
        int leftPosition = 0;
        int leftPositionLimit = Integer.MAX_VALUE;

        while (leftPosition < leftPositionLimit) {
            long leftPentagon = generator.generate(leftPosition);

            int rightPosition = 1;
            while (shouldAnalyzePosition(leftPentagon, rightPosition)) {
                long rightPentagon = generator.generate(rightPosition);

                long pentagonDiff = rightPentagon - leftPentagon;

                if (generator.isPentagonInMemory(pentagonDiff)) {
                    System.out.printf("Found pentagon diff %s for [%s, %s]\n", pentagonDiff, leftPentagon, rightPentagon);

                    long pentagonAddition = rightPentagon + leftPentagon;

                    long searchPentagon = searchForPentagonExistence(rightPosition + 1, pentagonAddition);

                    if (searchPentagon > 0) {
                        // Solution found
                        System.out.printf("Found pentagon addition %s for [%s, %s]\n", pentagonAddition, leftPentagon, rightPentagon);
                        return Long.toString(pentagonDiff);
                    }

                }

                rightPosition++;
            }

            leftPosition++;
        }

        return "";
    }

    private long searchForPentagonExistence(int startPosition, long target) {
        int currentPosition = startPosition;
        long currentPentagon = generator.generate(currentPosition);

        while (currentPentagon <= target) {
            if (currentPentagon == target) return currentPentagon;

            currentPosition++;
            currentPentagon = generator.generate(currentPosition);
        }

        return -1;
    }

    public boolean shouldAnalyzePosition(long pentagonPivot, int position) {
        long differenceToNextPentagon = generator.distanceToPreviousPentagon(position);
        return differenceToNextPentagon <= pentagonPivot;
    }
}
