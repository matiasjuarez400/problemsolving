package mathecat.eulerproject._0._44_PentagonNumbers;

import java.util.HashSet;
import java.util.Set;

public class GeneratorWrapper {
    private final PentagonNumberGenerator generator;
    private final Set<Long> generatedPentagons;

    public GeneratorWrapper() {
        generator = new PentagonNumberGenerator();
        generatedPentagons = new HashSet<>();
    }

    public long generate(int position) {
        long pentagon = generator.generate(position);

        if (pentagon < 0) {
            long previous = generate(position - 1);
            String message = String.format("Generation failed for position [%s]. " +
                    "The value at previous position is: %s." +
                    "The difference between previous position and limit is %s",
                    position, previous, Long.MAX_VALUE - previous);

            throw new IllegalStateException(message);
        }

        generatedPentagons.add(pentagon);

        return pentagon;
    }

    public long distanceToPreviousPentagon(int currentPosition) {
        return generator.distanceToPreviousPentagon(currentPosition);
    }

    public boolean isPentagonInMemory(long number) {
        return generatedPentagons.contains(number);
    }
}
