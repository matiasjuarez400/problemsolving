package mathecat.eulerproject._0._9SummationOfPrimes;

import mathecat.eulerproject.commons.primes.PrimeUtils;

import java.util.List;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.calculateSum(10);
        solution.calculateSum(2_000_000);
    }

    private void calculateSum(int upperBound) {
        final List<Long> primes = PrimeUtils.findPrimesLessThan(upperBound);

        System.out.printf("Sum of primes below %s: [%s]%n", upperBound, primes.stream().reduce(Long::sum).orElse(-1L));
    }
}
