package mathecat.eulerproject._0._11HighlyDivisibleTriangularNumber;

class TriangularSequenceGenerator {
    private long currentElement = 1;
    private long accumulated;

    public long next() {
        accumulated += currentElement;
        currentElement++;

        return accumulated;
    }
}
