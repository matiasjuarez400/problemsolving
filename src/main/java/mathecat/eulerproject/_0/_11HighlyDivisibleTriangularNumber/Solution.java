package mathecat.eulerproject._0._11HighlyDivisibleTriangularNumber;

import mathecat.eulerproject.commons.factorizartion.Factorizer;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        long firstTriangleNumber = solution.findFirstTriangleNumberWithMoreThanDivisors(500, 250, 200, 0, 20);

        System.out.println(firstTriangleNumber);
    }

    /**
     * This method works but only for evaluation steps that are very close to 1 and cautionThresholds close the the value of desiredDivisors.
     * I was expecting the number of divisors to consistently increase with the absolute value of the number you are evaluating, but the number
     * of divisors doesn't seem to follow a pattern.
     * @param desiredDivisors - how many divisors you want
     * @param evaluationStep - how many numbers you will skip before evaluating the number of divisors
     * @param cautionThreshold - how far from the desiredDivisors you should be to start paying attention to the numbers that you skipped throw evaluationStep
     * @param lowerBoundToEvaluate - how big should the triangle number be before start evaluating the number of divisors
     * @return A triangle number that has more divisors that 'desiredDivisors'. Can't guarantee it will be the first one though LOL!
     */
    private long findFirstTriangleNumberWithMoreThanDivisors(int desiredDivisors, int evaluationStep, int cautionThreshold, int lowerBoundToEvaluate, int backtrackCheckSize) {
        if (backtrackCheckSize > evaluationStep) backtrackCheckSize = evaluationStep;

        final List<Long> skippedNumbers = new ArrayList<>();
        final TriangularSequenceGenerator triangularSequenceGenerator = new TriangularSequenceGenerator();

        boolean foundNumber = false;

        long lowestNumberWithDesiredDivisors = Long.MAX_VALUE;

        while (!foundNumber) {
            long nextTriangleNumber = triangularSequenceGenerator.next();
            if (nextTriangleNumber < lowerBoundToEvaluate) continue;

            skippedNumbers.add(nextTriangleNumber);

            if (skippedNumbers.size() == evaluationStep) {
                for (int backCheckIndex = 0; backCheckIndex < backtrackCheckSize; backCheckIndex++) {
                    long backCheckSkippedNumber = skippedNumbers.get(skippedNumbers.size() - 1 - backCheckIndex);
                    Set<Long> divisors = Factorizer.findAllDivisorOfNumber(backCheckSkippedNumber);

                    if (divisors.size() >= desiredDivisors - cautionThreshold) {
                        for (Long skippedNumber : skippedNumbers) {
                            divisors = Factorizer.findAllDivisorOfNumber(skippedNumber);

                            if (divisors.size() > desiredDivisors) {
                                lowestNumberWithDesiredDivisors = skippedNumber;
                                foundNumber = true;
                                break;
                            }
                        }
                    }
                }

                skippedNumbers.clear();
            }
        }

        return lowestNumberWithDesiredDivisors;
    }
}
