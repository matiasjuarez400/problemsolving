package mathecat.eulerproject._0._45_TraingularPentagonalHexagonal;

public class HexagonalGenerator extends Generator {
    @Override
    protected long generate(int n) {
        return n * (2L * n - 1L);
    }
}
