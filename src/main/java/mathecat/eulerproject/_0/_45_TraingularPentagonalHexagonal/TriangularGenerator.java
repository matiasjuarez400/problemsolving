package mathecat.eulerproject._0._45_TraingularPentagonalHexagonal;

class TriangularGenerator extends Generator {
    @Override
    protected long generate(int n) {
        return (n * (n + 1L) ) / 2L;
    }
}
