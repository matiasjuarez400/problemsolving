package mathecat.eulerproject._0._45_TraingularPentagonalHexagonal;

class PentagonalGenerator extends Generator {
    @Override
    protected long generate(int n) {
        return n * (3L * n - 1L) / 2L;
    }
}
