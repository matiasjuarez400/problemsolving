package mathecat.eulerproject._0._45_TraingularPentagonalHexagonal;

abstract class Generator {
    protected int n;

    public long next() {
        long generated = generate(++n);

        if (generated < 0) {
            throw new IllegalStateException(String.format("Negative generation for n [%s]. Generator: %s", n, this.getClass().getSimpleName()));
        }

        return generated;
    }

    public long current() {
        return generate(n);
    }

    public void setN(int n) {
        this.n = n;
    }

    public long generateUpTo(long number) {
        long current = current();

        while(current < number) {
            current = next();
        }

        return current;
    }

    protected abstract long generate(int n);
}
