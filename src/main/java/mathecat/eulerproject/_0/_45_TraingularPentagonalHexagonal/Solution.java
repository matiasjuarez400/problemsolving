package mathecat.eulerproject._0._45_TraingularPentagonalHexagonal;

public class Solution {
    private final Generator hexagonalGenerator;
    private final Generator pentagonalGenerator;
    private final Generator triangularGenerator;

    public static void main(String[] args) {
        Solution solution = new Solution();
        long result = solution.solve(40756);
    }

    public Solution() {
        this.hexagonalGenerator = new HexagonalGenerator();
        this.pentagonalGenerator = new PentagonalGenerator();
        this.triangularGenerator = new TriangularGenerator();
    }

    public long solve(long generationLowerLimit) {
        long generationUpperLimit = generationLowerLimit;

        boolean solved = false;
        while (!solved) {
            long triangularNumber = triangularGenerator.generateUpTo(generationUpperLimit);
            long hexagonalNumber = hexagonalGenerator.generateUpTo(generationUpperLimit);

            generationUpperLimit = Long.max(triangularNumber, hexagonalNumber);

            if (triangularNumber == hexagonalNumber) {
                System.out.printf("Found triangular and hexagonal friends: %s\n", triangularNumber);

                long pentagonalNumber = pentagonalGenerator.generateUpTo(generationUpperLimit);

                if (pentagonalNumber == triangularNumber) {
                    System.out.printf("Found triplet at %s", triangularNumber);
                    solved = true;
                } else {
                    generationUpperLimit++;
                }
            }
        }

        return generationUpperLimit;
    }
}
