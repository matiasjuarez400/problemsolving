package mathecat.eulerproject._0._0MultiplesOf3And5;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        final int[] desiredNumbers = {3, 5};
        final int limit = 1000;

        final Set<Integer> multiples = new HashSet<>();

        Arrays.stream(desiredNumbers)
                .forEach(divisor -> multiples.addAll(solution.getMultiplesForDivisorBelowLimit(divisor, limit)));

        int sum = multiples.stream()
                .reduce(0, Integer::sum);

        System.out.println(sum);
    }

    private Set<Integer> getMultiplesForDivisorBelowLimit(int divisor, int limit) {
        final Set<Integer> output = new HashSet<>();

        for (int next = divisor; next < limit; next += divisor) {
            output.add(next);
        }

        return output;
    }
}
