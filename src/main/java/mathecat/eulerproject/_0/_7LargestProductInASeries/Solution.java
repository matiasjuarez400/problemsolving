package mathecat.eulerproject._0._7LargestProductInASeries;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Solution {
    /**
     * 73167176531330624919225119674426574742355349194934
     * 96983520312774506326239578318016984801869478851843
     * 85861560789112949495459501737958331952853208805511
     * 12540698747158523863050715693290963295227443043557
     * 66896648950445244523161731856403098711121722383113
     * 62229893423380308135336276614282806444486645238749
     * 30358907296290491560440772390713810515859307960866
     * 70172427121883998797908792274921901699720888093776
     * 65727333001053367881220235421809751254540594752243
     * 52584907711670556013604839586446706324415722155397
     * 53697817977846174064955149290862569321978468622482
     * 83972241375657056057490261407972968652414535100474
     * 82166370484403199890008895243450658541227588666881
     * 16427171479924442928230863465674813919123162824586
     * 17866458359124566529476545682848912883142607690042
     * 24219022671055626321111109370544217506941658960408
     * 07198403850962455444362981230987879927244284909188
     * 84580156166097919133875499200524063689912560717606
     * 05886116467109405077541002256983155200055935729725
     * 71636269561882670428252483600823257530420752963450
     * @param args
     */
    public static void main(String[] args) {
        Solution solution = new Solution();
        
        final String input = "73167176531330624919225119674426574742355349194934" +
                "96983520312774506326239578318016984801869478851843" +
                "85861560789112949495459501737958331952853208805511" +
                "12540698747158523863050715693290963295227443043557" +
                "66896648950445244523161731856403098711121722383113" +
                "62229893423380308135336276614282806444486645238749" +
                "30358907296290491560440772390713810515859307960866" +
                "70172427121883998797908792274921901699720888093776" +
                "65727333001053367881220235421809751254540594752243" +
                "52584907711670556013604839586446706324415722155397" +
                "53697817977846174064955149290862569321978468622482" +
                "83972241375657056057490261407972968652414535100474" +
                "82166370484403199890008895243450658541227588666881" +
                "16427171479924442928230863465674813919123162824586" +
                "17866458359124566529476545682848912883142607690042" +
                "24219022671055626321111109370544217506941658960408" +
                "07198403850962455444362981230987879927244284909188" +
                "84580156166097919133875499200524063689912560717606" +
                "05886116467109405077541002256983155200055935729725" +
                "71636269561882670428252483600823257530420752963450";

        solution.execute(4, input);
        solution.execute(13, input);
    }

    private void execute(int groupLength, String rawNumber) {
        final String digits = findAdjacentDigitsWithGreatestProduct(groupLength, rawNumber);
        System.out.printf("Digits: %s%n", digits);

        System.out.printf("Product: %s%n%n", convertAdjacentDigitsToProduct(digits));
    }

    private long convertAdjacentDigitsToProduct(String digits) {
        long product = 1;
        for (char c : digits.toCharArray()) {
            product *= Character.getNumericValue(c);
        }

        return product;
    }

    private String findAdjacentDigitsWithGreatestProduct(int groupLength, String rawNumber) {
        final int[] convertedNumber = new int[rawNumber.length()];

        for (int i = 0; i < rawNumber.length(); i++) {
            convertedNumber[i] = Character.getNumericValue(rawNumber.charAt(i));
        }

        long maxProduct = Long.MIN_VALUE;
        long maxProductLowIndex = Long.MIN_VALUE;

        int currentIndex = 0;
        long currentGroupProduct = Long.MIN_VALUE;
        final List<Integer> group = new ArrayList<>();

        while (currentIndex < convertedNumber.length) {
            int currentDigit = convertedNumber[currentIndex];

            if (currentDigit == 0) {
                currentIndex++;
                group.clear();
                currentGroupProduct = Long.MIN_VALUE;
                continue;
            }

            if (group.size() == groupLength) {
                int firstElement = group.remove(0);
                currentGroupProduct /= firstElement;
                currentGroupProduct *= currentDigit;
                group.add(currentDigit);
            } else {
                group.add(currentDigit);
                if (group.size() == 1) {
                    currentGroupProduct = currentDigit;
                } else {
                    currentGroupProduct *= currentDigit;
                }
            }

            if (group.size() == groupLength) {
                if (currentGroupProduct > maxProduct) {
                    maxProduct = currentGroupProduct;
                    maxProductLowIndex = currentIndex - groupLength + 1;
                }
            }

            currentIndex++;
        }

        return Arrays.stream(convertedNumber, (int) maxProductLowIndex, (int) maxProductLowIndex + groupLength)
                .boxed()
                .map(digit -> Integer.toString(digit))
                .collect(Collectors.joining());
    }
}
