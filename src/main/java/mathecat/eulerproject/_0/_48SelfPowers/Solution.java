package mathecat.eulerproject._0._48SelfPowers;

import java.math.BigInteger;

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.solve());
    }

    public String solve() {
        BigInteger rawResult = powerUpTo(1000);
        String numberString = rawResult.toString();

        return numberString.substring(numberString.length() - 10);
    }

    private BigInteger powerUpTo(int limit) {
        BigInteger current = BigInteger.ZERO;

        for (int i = 1; i <= limit; i++) {
            current = current.add(BigInteger.valueOf(i).pow(i));
        }

        return current;
    }
}
