package mathecat.eulerproject._0._5SumSquareDifference;

import mathecat.eulerproject.commons.BasicMath;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.calculateSolutionForUpperBoundLimit(10);
        solution.calculateSolutionForUpperBoundLimit(100);
    }

    private void calculateSolutionForUpperBoundLimit(long upperBound) {
        final List<Long> tenNaturalNumbers = LongStream.rangeClosed(1, upperBound)
                .boxed()
                .collect(Collectors.toList());

        System.out.println(calculateDifferenceBetweenSumOfSquaresAndSquareOfSum(tenNaturalNumbers));
    }

    private long calculateDifferenceBetweenSumOfSquaresAndSquareOfSum(List<Long> numbers) {
        return calculateSquareOfSum(numbers) - calculateTheSumOfTheSquares(numbers);
    }

    private long calculateTheSumOfTheSquares(List<Long> numbers) {
        long acc = 0;

        for (Long number : numbers) {
            acc += BasicMath.pow(number, 2);
        }

        return acc;
    }

    private long calculateSquareOfSum(List<Long> numbers) {
        long sum = numbers.stream().mapToLong(l -> l).sum();

        return BasicMath.pow(sum, 2);
    }
}
