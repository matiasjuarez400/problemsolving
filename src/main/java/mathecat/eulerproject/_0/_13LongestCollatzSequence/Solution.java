package mathecat.eulerproject._0._13LongestCollatzSequence;

import mathecat.eulerproject.commons.generators.CollatzSequenceGenerator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;

class Solution {
    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(40);

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Solution solution = new Solution();

        int samples = 1;

        showTaskMeasurementAvg("UsingAtomicLong", () -> {
            Map<Long, AtomicLong> sequenceLengthByStartingPoint = new HashMap<>();

            for (int startingPoint = 1; startingPoint < 1_000_000; startingPoint++) {
                solution.calculateSequenceLengthForCurrentStartPoint(startingPoint, sequenceLengthByStartingPoint);
            }

            Map.Entry<Long, AtomicLong> maxSequenceLength = sequenceLengthByStartingPoint.entrySet().stream()
                    .max(Comparator.comparing(entry -> entry.getValue().get())).orElse(null);

            System.out.println(maxSequenceLength);
        }, samples);

        showTaskMeasurementAvg("UsingLong", () -> {
            Map<Long, Long> sequenceLengthByStartingPoint = new HashMap<>();

            for (int startingPoint = 1; startingPoint < 1_000_000; startingPoint++) {
                solution.calculateSequenceLengthForCurrentStartPointUsingLong(startingPoint, sequenceLengthByStartingPoint);
            }

            Map.Entry<Long, Long> maxSequenceLength = sequenceLengthByStartingPoint.entrySet().stream()
                    .max(Map.Entry.comparingByValue()).orElse(null);

            System.out.println(maxSequenceLength);
        }, samples);
    }

    public static double measureAvg(Runnable runnable, int samples) throws ExecutionException, InterruptedException {
        AtomicLong acc = new AtomicLong(0);

        List<Future> futures = new ArrayList<>();
        for (int i = 0; i < samples; i++) {
            futures.add(EXECUTOR_SERVICE.submit(() -> {
                acc.addAndGet(measureTime(runnable));
            }));
        }

        for (Future future : futures) {
            future.get();
        }

        return acc.get() * 1.0 / samples;
    }

    public static long measureTime(Runnable runnable) {
        long startTime = System.currentTimeMillis();

        runnable.run();

        long endTime = System.currentTimeMillis();

        return endTime - startTime;
    }

    public static void showTaskMeasurementAvg(String taskName, Runnable runnable, int samples) throws ExecutionException, InterruptedException {
        System.out.printf("Task %s avg: %s ms%n", taskName, measureAvg(runnable, samples));
    }

    public static void showTaskMeasurement(String taskName, Runnable runnable) {
        System.out.printf("Task %s ended in %s ms%n", taskName, measureTime(runnable));
    }

    public void calculateSequenceLengthForCurrentStartPointUsingLong(long startPoint, Map<Long, Long> exploredStartingPoints) {
        CollatzSequenceGenerator generator = new CollatzSequenceGenerator(startPoint);

        Map<Long, Long> inUseCountersForLengths = new HashMap<>();

        while (generator.hasNext()) {
            long nextElement = generator.next();

            Long exploredStartingPointCount = exploredStartingPoints.get(nextElement);

            if (exploredStartingPointCount != null) {
                final long valueToAddToEveryone = exploredStartingPointCount;
                inUseCountersForLengths.forEach((k, v) -> inUseCountersForLengths.put(k, v + valueToAddToEveryone));
                break;
            } else {
                inUseCountersForLengths.put(nextElement, 0L);
                inUseCountersForLengths.forEach((k, v) -> inUseCountersForLengths.put(k, v + 1));
            }
        }

        exploredStartingPoints.putAll(inUseCountersForLengths);
    }

    public void calculateSequenceLengthForCurrentStartPoint(long startPoint, Map<Long, AtomicLong> exploredStartingPoints) {
        CollatzSequenceGenerator generator = new CollatzSequenceGenerator(startPoint);

        List<AtomicLong> inUseCountersForLengths = new ArrayList<>();

        while (generator.hasNext()) {
            long nextElement = generator.next();

            AtomicLong exploredStartingPoint = exploredStartingPoints.get(nextElement);

            if (exploredStartingPoint != null) {
                final long valueToAddToEveryone = exploredStartingPoint.get();
                inUseCountersForLengths.forEach(atomicLong -> atomicLong.set(atomicLong.get() + valueToAddToEveryone));
                break;
            } else {
                exploredStartingPoint = new AtomicLong(0);
                exploredStartingPoints.put(nextElement, exploredStartingPoint);
                inUseCountersForLengths.add(exploredStartingPoint);
                inUseCountersForLengths.forEach(AtomicLong::incrementAndGet);
            }
        }
    }
}
