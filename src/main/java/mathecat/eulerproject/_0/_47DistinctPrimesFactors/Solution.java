package mathecat.eulerproject._0._47DistinctPrimesFactors;

import mathecat.eulerproject.commons.factorizartion.Factorizer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Solution {
    private final PrimeGenerator generator;
    private final Factorizer factorizer;

    public static void main(String[] args) {
        findSolution(2, 2);
        findSolution(3, 3);
        findSolution(4, 4);
    }

    private static void findSolution(int desiredFactors, int desiredConsecutive) {
        Solution solution = new Solution();

        long answer = solution.solve(desiredFactors, desiredConsecutive);

        System.out.printf("Found solution [%s] for factor size [%s] and consecutive amount [%s]\n", answer, desiredFactors, desiredConsecutive);
    }

    public Solution() {
        this.generator = new PrimeGenerator();
        this.factorizer = new Factorizer();
    }

    public long solve(int desiredFactors, int desiredConsecutive) {
        long currentNumber = 2;
        List<Long> primes;

        List<Long> consecutiveNumbersFound = new ArrayList<>();

        while (true) {
            primes = generator.getPrimesUpToLimit(currentNumber);

            Map<Long, Integer> currentNumberFactors = factorizer.factorizeNumberFromPrimeList(currentNumber, primes);

            if (currentNumberFactors.size() == desiredFactors) {
                consecutiveNumbersFound.add(currentNumber);
                if (consecutiveNumbersFound.size() == desiredConsecutive) {
                    return consecutiveNumbersFound.get(0);
                }
            } else {
                consecutiveNumbersFound.clear();
            }

            currentNumber++;
        }
    }

}
