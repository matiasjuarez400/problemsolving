package mathecat.eulerproject._0._47DistinctPrimesFactors;

import mathecat.eulerproject.commons.primes.PrimeUtils;

import java.util.ArrayList;
import java.util.List;

class PrimeGenerator {
    private List<Long> storedPrimes;
    private long generatedRangeUpperLimit;

    public PrimeGenerator() {
        this.storedPrimes = new ArrayList<>();
        generatedRangeUpperLimit = 0;
    }

    public List<Long> getPrimesUpToLimit(long limit) {
        if (limit <= generatedRangeUpperLimit) return storedPrimes;

        storedPrimes.addAll(PrimeUtils.findPrimesInRange(generatedRangeUpperLimit + 1, limit));
        generatedRangeUpperLimit = limit;

        return storedPrimes;
    }
}
