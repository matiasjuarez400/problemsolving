package mathecat.eulerproject._0._8SpecialPythagoreanTriplet;

import mathecat.eulerproject.commons.BasicMath;

class Solution {
    public static void main(String[] args) {
        final int target = 1000;

        for (int a = 1; a < target; a++) {
            for (int b = a; b < target; b++) {
                long product = BasicMath.square(a) + BasicMath.square(b);

                double c = Math.sqrt(product);

                if (c % 1 == 0) {
                    if (a + b + (int) c == target) {
                        System.out.printf("Found triple: %s%s%s%n", a, b, (int) c);
                        System.out.printf("Product: %s", a * b * (int) c);
                    }
                }
            }
        }
    }
}
