package mathecat.eulerproject._0._4SmallestMultiple;

import mathecat.eulerproject.commons.factorizartion.Factorizer;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

class Solution {
    public static void main(String[] args) {
        final int start = 1;
        final int end = 20;

        final List<Long> numbers = LongStream.rangeClosed(start, end)
                .boxed().collect(Collectors.toList());


        System.out.println(Factorizer.findLeastCommonMultiple(numbers));
    }
}
