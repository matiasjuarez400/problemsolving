package mathecat.eulerproject._0._3LargestPalindromeProduct;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.IntStream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        final List<SearchInformation> searchInformationList = solution.findPalindromes(3);

        final SearchInformation largestPalindrome = searchInformationList.stream()
                .max(Comparator.comparing(SearchInformation::getProduct))
                .orElseThrow(() -> new IllegalStateException("No palindrome found"));

        System.out.println(largestPalindrome);
    }

    /**
     * This method return a lot more information than what the problem needs.
     * But I'm doing it just for fun.
     */
    private List<SearchInformation> findPalindromes(int baseNumbersLength) {
        final int lowerBound = (int) Math.pow(10, baseNumbersLength - 1);
        final int upperBound = (int) Math.pow(10, baseNumbersLength);

        final List<SearchInformation> searchInformationList = new ArrayList<>();

        IntStream.range(lowerBound, upperBound).forEach(multiplier -> {
            IntStream.range(lowerBound, upperBound).forEach(multiplicand -> {
                int product = multiplier * multiplicand;

                if (isPalindrome(product)) {
                    searchInformationList.add(new SearchInformation(multiplier, multiplicand));
                }
            });
        });

        return searchInformationList;
    }

    private boolean isPalindrome(int number) {
        String numberString = Integer.toString(number);
        return new StringBuilder(numberString).reverse().toString().equals(numberString);
    }

    private static class SearchInformation {
        private final int multiplier;
        private final int multiplicand;
        private final int product;

        public SearchInformation(int multiplier, int multiplicand) {
            this.multiplier = multiplier;
            this.multiplicand = multiplicand;
            this.product = multiplicand * multiplier;
        }

        public int getMultiplier() {
            return multiplier;
        }

        public int getMultiplicand() {
            return multiplicand;
        }

        public int getProduct() {
            return product;
        }

        @Override
        public String toString() {
            return "SearchInformation{" +
                    "multiplier=" + multiplier +
                    ", multiplicand=" + multiplicand +
                    ", product=" + product +
                    '}';
        }
    }
}
