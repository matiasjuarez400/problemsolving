package mathecat.eulerproject._0._6_10001stPrime;

import mathecat.eulerproject.commons.primes.PrimeUtils;

class Solution {
    public static void main(String[] args) {
        System.out.println(PrimeUtils.findNthPrime(6));
        System.out.println(PrimeUtils.findNthPrime(10001));
        System.out.println(PrimeUtils.findNthPrime(20001));
    }
}
