package mathecat.eulerproject.commons.memory;

import java.util.Collection;
import java.util.Collections;

public abstract class Memory<T> {
    public abstract int size();
    public abstract boolean contains(T element);
    public abstract boolean add(T element);
    public abstract boolean remove(T element);
    public abstract T get(int index);
    public abstract Collection<T> get(int start, int end);
    protected abstract Collection<T> doGetElements();

    public Collection<T> getElements() {
        return Collections.unmodifiableCollection(doGetElements());
    }

    public void addAll(Collection<T> elements) {
        for (T element : elements) add(element);
    }

    public boolean isEmpty() {
        return size() == 0;
    }
}
