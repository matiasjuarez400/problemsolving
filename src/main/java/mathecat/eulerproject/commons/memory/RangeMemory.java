package mathecat.eulerproject.commons.memory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RangeMemory extends Memory<Long> {
    private final long rangeSize;
    private final Map<Long, List<Long>> elementsByRange;
    private List<Long> ranges;
    private int elementCount = 0;

    public RangeMemory(long rangeSize) {
        if (rangeSize < 1) {
            throw new IllegalArgumentException("RangeSize must be a positive number");
        }

        this.rangeSize = rangeSize;
        this.elementsByRange = new HashMap<>();
        this.ranges = new ArrayList<>();
    }

    private long getRangeForValue(long value) {
        return value / rangeSize;
    }

    @Override
    public int size() {
        return elementCount;
    }

    @Override
    public boolean contains(Long element) {
        long range = getRangeForValue(element);

        List<Long> elementsInRange = this.elementsByRange.get(range);

        if (elementsInRange == null) return false;

        return elementsInRange.contains(element);
    }

    @Override
    public boolean add(Long element) {
        long range = getRangeForValue(element);
        List<Long> elementsForRange = this.elementsByRange.computeIfAbsent(range, k -> new ArrayList<>());

        elementCount++;

        elementsForRange.add(element);

        Collections.sort(elementsForRange);

        if (ranges.size() < elementsByRange.size()) {
            ranges.add(range);
            Collections.sort(ranges);
        }

        return true;
    }

    @Override
    public boolean remove(Long element) {
        long range = getRangeForValue(element);

        List<Long> elementsInRange = this.elementsByRange.get(range);

        if (elementsInRange == null) return false;

        if (elementsInRange.remove(element)) {
            elementCount--;
            return true;
        }

        return false;
    }

    @Override
    public Long get(int index) {
        if (index >= size()) {
            throw new IndexOutOfBoundsException(String.format("Element count: %s. Asked for index: %s", size(), index));
        }

        int accumulatedCount = 0;
        for (Long range : this.ranges) {
            List<Long> elementsForRange = this.elementsByRange.get(range);

            int newAccumulatedCount = accumulatedCount + elementsForRange.size();
            if (newAccumulatedCount <= index) {
                accumulatedCount = newAccumulatedCount;
            } else {
                return elementsForRange.get(index - accumulatedCount);
            }
        }

        throw new IllegalStateException("At this point an element should have been found");
    }

    @Override
    public Collection<Long> get(int start, int end) {
        if (start == end) return Collections.emptyList();

        if (end < start) {
            throw new IllegalArgumentException(String.format("end[%s] must be greater than start[%s]", end, start));
        }

        if (start > size() || end > size()) {
            throw new IndexOutOfBoundsException(String.format("Element count: %s. Asked for indexes: %s to %s", size(), start, end));
        }

        List<Long> returnElements = new ArrayList<>();
        int desiredElements = end - start;
        int accumulatedCount = 0;
        boolean foundFirstElement = false;

        for (Long range : this.ranges) {
            List<Long> elementsForRange = this.elementsByRange.get(range);

            if (foundFirstElement) {
                if (returnElements.size() == desiredElements) break;

                for (Long elementForRange : elementsForRange) {
                    if (returnElements.size() < desiredElements) {
                        returnElements.add(elementForRange);
                    } else {
                        break;
                    }
                }
            } else {
                int newAccumulatedCount = accumulatedCount + elementsByRange.size();
                if (newAccumulatedCount <= start) {
                    accumulatedCount = newAccumulatedCount;
                } else {
                    int currentListStartIndex = start - accumulatedCount;
                    foundFirstElement = true;
                    for (int i = currentListStartIndex; i < elementsForRange.size(); i++) {
                        if (returnElements.size() < desiredElements) {
                            returnElements.add(elementsForRange.get(i));
                        } else {
                            break;
                        }
                    }
                }
            }
        }

        return Collections.unmodifiableCollection(returnElements);
    }

    @Override
    protected Collection<Long> doGetElements() {
        List<Long> elements = new ArrayList<>();
        for (List<Long> storedElements : this.elementsByRange.values()) {
            elements.addAll(storedElements);
        }

        return elements;
    }

    public Collection<Long> getElementsForRange(Long rangeNumber) {
        List<Long> elementsInRange = this.elementsByRange.get(rangeNumber);
        if (elementsInRange == null) return Collections.emptyList();

        return Collections.unmodifiableCollection(elementsInRange);
    }
}
