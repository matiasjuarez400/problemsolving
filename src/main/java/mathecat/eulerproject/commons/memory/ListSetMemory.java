package mathecat.eulerproject.commons.memory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ListSetMemory<T> extends Memory<T> {
    private List<T> elementsByInsertionOrder;
    private Set<T> elementsSet;

    public ListSetMemory() {
        this.elementsByInsertionOrder = new ArrayList<>();
        this.elementsSet = new HashSet<>();
    }

    @Override
    public int size() {
        return elementsSet.size();
    }

    @Override
    public boolean contains(T element) {
        return elementsSet.contains(element);
    }

    @Override
    public boolean add(T element) {
        if (contains(element)) return false;
        elementsByInsertionOrder.add(element);
        elementsSet.add(element);

        return true;
    }

    @Override
    public boolean remove(T element) {
        if(!elementsSet.remove(element)) return false;
        return elementsByInsertionOrder.remove(element);
    }

    @Override
    public T get(int index) {
        return elementsByInsertionOrder.get(index);
    }

    @Override
    public Collection<T> get(int start, int end) {
        int realEnd = Integer.min(end, elementsByInsertionOrder.size());

        return elementsByInsertionOrder.subList(start, end);
    }

    @Override
    protected Collection<T> doGetElements() {
        return elementsByInsertionOrder;
    }
}
