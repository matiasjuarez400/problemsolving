package mathecat.eulerproject.commons.fileUtils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class SimpleFileWriter {
    private final Path file;

    public SimpleFileWriter(String filePath) throws IOException {
        this(Paths.get(filePath));
    }

    public SimpleFileWriter(Path file) {
        this.file = file;
    }

    public void writeLines(List<String> lines) {
        writeLines(lines, false);
    }

    public void writeLines(List<String> lines, boolean append) {
        try (BufferedWriter writer = getWriter(append)) {
            for (String line : lines) {
                writer.write(line);
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private BufferedWriter getWriter(boolean append) throws IOException {
        if (append) {
            return Files.newBufferedWriter(file, StandardCharsets.UTF_8, StandardOpenOption.APPEND);
        } else {
            return Files.newBufferedWriter(file, StandardCharsets.UTF_8);
        }
    }
}
