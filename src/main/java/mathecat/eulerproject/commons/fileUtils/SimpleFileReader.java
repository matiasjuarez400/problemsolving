package mathecat.eulerproject.commons.fileUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class SimpleFileReader implements Iterator<String> {
    private final Path file;
    private final Scanner scanner;

    public SimpleFileReader(String filePath) {
        this(Paths.get(filePath));
    }

    public SimpleFileReader(Path path) {
        this.file = path;

        try {
            this.scanner = new Scanner(file);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean hasNext() {
        return this.scanner.hasNextLine();
    }

    @Override
    public String next() {
        return this.scanner.nextLine();
    }

    public List<String> readAllLines() {
        List<String> lines = new ArrayList<>();
        while (hasNext()) {
            lines.add(next());
        }

        return lines;
    }

    public Path getFile() {
        return file;
    }

    public String getFirstLine() {
        try (BufferedReader reader = Files.newBufferedReader(file, StandardCharsets.UTF_8)) {
            return reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        }
    }
}
