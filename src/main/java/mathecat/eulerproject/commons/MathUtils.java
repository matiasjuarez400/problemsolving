package mathecat.eulerproject.commons;

import java.math.BigInteger;

public class MathUtils {
    private static final BigInteger SQUARE_BINARY_SEARCH_STEP = BigInteger.valueOf(50);

    public BigInteger findBase(BigInteger inputValue, int power) {
        BigInteger lowLimit = BigInteger.ONE;
        BigInteger highLimit = BigInteger.ONE;
        BigInteger bigTwo = BigInteger.valueOf(2);

        while (highLimit.pow(power).compareTo(inputValue) < 0) {
            highLimit = highLimit.add(SQUARE_BINARY_SEARCH_STEP);
        }

        while (highLimit.subtract(lowLimit).compareTo(BigInteger.ONE) > 0) {
            BigInteger nextBase = (highLimit.add(lowLimit)).divide(bigTwo);
            BigInteger nextBaseSquare = nextBase.pow(power);

            int comparison = nextBaseSquare.compareTo(inputValue);

            if (comparison == 0) {
                return nextBase;
            } else if (comparison < 0) {
                lowLimit = nextBase;
            } else {
                highLimit = nextBase;
            }
        }

        return null;
    }
}
