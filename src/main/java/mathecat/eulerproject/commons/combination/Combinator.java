package mathecat.eulerproject.commons.combination;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Combinator<T> {
    public List<List<T>> findPermutations(List<T> input) {
        boolean[] free = new boolean[input.size()];
        Arrays.fill(free, true);

        List<List<T>> permutations = new ArrayList<>();

        doFindPermutations(input, free, new ArrayList<>(), permutations);

        return permutations;
    }

    private List<T> doFindPermutations(List<T> input, boolean[] free, List<T> currentPermutation, List<List<T>> permutations) {
        if (currentPermutation.size() == input.size()) return new ArrayList<>(currentPermutation);

        for (int i = 0; i < free.length; i++) {
            if (free[i]) {
                free[i] = false;
                currentPermutation.add(input.get(i));

                List<T> newPermutation = doFindPermutations(input, free, currentPermutation, permutations);

                if (newPermutation.size() == input.size()) {
                    permutations.add(newPermutation);
                }

                free[i] = true;
                currentPermutation.remove(currentPermutation.size() - 1);
            }
        }

        return Collections.emptyList();
    }
}
