package mathecat.eulerproject.commons.combination;

import java.util.Arrays;

public class CombinationGenerator {
    private final boolean[] currentCombination;
    private boolean hasNext = true;

    public CombinationGenerator(int combinationSize) {
        currentCombination = new boolean[combinationSize];
    }

    public boolean[] next() {
        boolean[] temp = Arrays.copyOf(currentCombination, currentCombination.length);

        boolean isNextCombinationAvailable = false;
        for (int i = 0; i < currentCombination.length; i++) {
            if (!currentCombination[i]) {
                currentCombination[i] = true;
                isNextCombinationAvailable = true;
                break;
            } else {
                currentCombination[i] = false;
            }
        }

        if (!isNextCombinationAvailable) {
            hasNext = false;
        }

        return temp;
    }

    public boolean hasNext() {
        return hasNext;
    }
}
