package mathecat.eulerproject.commons.combination;

import mathecat.eulerproject.commons.BasicMath;

import java.math.BigInteger;

public class CombinationUtils {
    /**
     * The order of the elements does not matter
     * @param totalElements
     * @param elementsPerCombination
     * @return
     */
    public static BigInteger combinatorics(int totalElements, int elementsPerCombination) {
        if (totalElements < elementsPerCombination) {
            throw new IllegalArgumentException("totalElements must be greater than elementsPerCombination");
        }

        BigInteger nf = BasicMath.factorial(totalElements);
        BigInteger rf = BasicMath.factorial(elementsPerCombination);
        BigInteger nrf = BasicMath.factorial(totalElements - elementsPerCombination);

        return nf.divide(rf.multiply(nrf));
    }

    public static BigInteger maxCombinatorics(int totalElements) {
        if (totalElements == 1) return BigInteger.ONE;

        return combinatorics(totalElements, totalElements / 2);
    }
}
