package mathecat.eulerproject.commons.factorizartion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FactorizedNumber {
    private final long number;
    private final Map<Long, Factor> factors;

    public FactorizedNumber(long number) {
        this.number = number;
        this.factors = new HashMap<>();
    }

    public long getNumber() {
        return number;
    }

    public Map<Long, Factor> getFactors() {
        return factors;
    }

    public void addFactor(long factor, long occurrences) {
        if (factors.containsKey(factor)) {
            throw new IllegalArgumentException(String.format("Factor %s already exists", factor));
        }

        this.factors.put(factor, new Factor(factor, occurrences));
    }

    public List<Long> getFactorsAsList() {
        List<Long> factorList = new ArrayList<>();
        factors.forEach((k, v) -> {
            for (int i = 0; i < v.getOccurrences(); i++) {
                factorList.add(v.getFactor());
            }
        });

        return factorList;
    }
}
