package mathecat.eulerproject.commons.factorizartion;

import java.math.BigInteger;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BigFactorizedNumber {
    private final BigInteger number;
    private Map<BigInteger, BigFactor> factors;

    public BigFactorizedNumber(BigInteger number) {
        this.number = number;
        this.factors = new HashMap<>();
    }

    public BigInteger getNumber() {
        return number;
    }

    public Map<BigInteger, BigFactor> getFactors() {
        return factors;
    }

    public void addFactor(BigInteger factor, long occurrences) {
        if (factors.containsKey(factor)) {
            throw new IllegalArgumentException(String.format("Factor %s already exists", factor));
        }

        this.factors.put(factor, new BigFactor(factor, occurrences));
    }

    public List<BigFactor> getFactorsAsList() {
        return factors.values().stream()
                .sorted(Comparator.comparing(BigFactor::getFactor))
                .collect(Collectors.toList());
    }

    /**
     * Ensure no further modifications are applicable
     */
    public void close() {
        this.factors = Collections.unmodifiableMap(factors);
    }
}
