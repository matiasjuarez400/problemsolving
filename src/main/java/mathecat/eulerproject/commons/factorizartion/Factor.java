package mathecat.eulerproject.commons.factorizartion;

public class Factor {
    private final long factor;
    private final long occurrences;

    public Factor(long factor, long occurrences) {
        this.factor = factor;
        this.occurrences = occurrences;
    }

    public long getFactor() {
        return factor;
    }

    public long getOccurrences() {
        return occurrences;
    }
}
