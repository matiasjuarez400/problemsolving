package mathecat.eulerproject.commons.factorizartion;

import mathecat.eulerproject.commons.primes.PrimeUtilsWithMemory;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Factorizer2 {
    private static final PrimeUtilsWithMemory PRIME_UTILS = new PrimeUtilsWithMemory();

    public static BigFactorizedNumber factorize(BigInteger number) {
        final BigFactorizedNumber factorizedNumber = new BigFactorizedNumber(number);

        if (number.equals(BigInteger.ONE)) {
            factorizedNumber.addFactor(BigInteger.ONE, 1);
            return factorizedNumber;
        }

        long startRange = 0;
        long endRange = 1_000_000;

        BigInteger currentValue = number;

        while (currentValue.compareTo(BigInteger.ONE) > 0) {
            for (long prime : PRIME_UTILS.getPrimesInRange(startRange, endRange)) {
                BigInteger currentDivisor = BigInteger.valueOf(prime);

                if (currentDivisor.compareTo(currentValue) > 0) break;

                long currentFactor = 0;
                while (true) {
                    BigInteger[] divisionAndReminder = currentValue.divideAndRemainder(currentDivisor);
                    if (!divisionAndReminder[1].equals(BigInteger.ZERO)) break;
                    currentValue = divisionAndReminder[0];
                    currentFactor++;
                }

                if (currentFactor > 0) {
                    factorizedNumber.addFactor(currentDivisor, currentFactor);
                }
            }

            startRange = endRange + 1;
            endRange += 1_000_000;
        }

        return factorizedNumber;
    }

    public static BigInteger findGreatestCommonDivisor(BigInteger a, BigInteger b) {
        a = a.abs();
        b = b.abs();

        BigInteger r = a.remainder(b);

        while (r.compareTo(BigInteger.ZERO) > 0) {
            a = b;
            b = r;
            r = a.remainder(b);
        }

        return b;
    }

    public static long findGreatestCommonDivisor(long a, long b) {
        return findGreatestCommonDivisor(BigInteger.valueOf(a), BigInteger.valueOf(b)).longValue();
    }

    public static BigInteger findLeastCommonMultiple(BigInteger... numbers) {
        final Map<BigInteger, BigFactorizedNumber> factorizedNumberByNumber = Stream.of(numbers)
                .collect(Collectors.toMap(
                        Function.identity(),
                        Factorizer2::factorize,
                        (f1, f2) -> f1
                ));

        final Map<BigInteger, Long> occurrencesByFactor = new HashMap<>();
        factorizedNumberByNumber.forEach((number, factorized) -> {
            factorized.getFactors().forEach((factor, primeFactor) -> {
                occurrencesByFactor.compute(factor, (k, v) -> {
                    if (v == null) return primeFactor.getOccurrences();
                    return Long.max(primeFactor.getOccurrences(), v);
                });
            });
        });

        BigInteger acc = BigInteger.ONE;
        for (Map.Entry<BigInteger, Long> entry : occurrencesByFactor.entrySet()) {
            acc = acc.multiply(entry.getKey().pow(entry.getValue().intValue()));
        }

        return acc;
    }
}
