package mathecat.eulerproject.commons.factorizartion;

import java.math.BigInteger;

public class BigFactor {
    private final BigInteger factor;
    private final long occurrences;

    public BigFactor(BigInteger factor, long occurrences) {
        this.factor = factor;
        this.occurrences = occurrences;
    }

    public BigInteger getFactor() {
        return factor;
    }

    public long getOccurrences() {
        return occurrences;
    }
}
