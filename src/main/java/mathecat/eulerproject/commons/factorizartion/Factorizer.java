package mathecat.eulerproject.commons.factorizartion;

import mathecat.eulerproject.commons.combination.CombinationGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Factorizer {
    public static FactorizedNumber factorize(long number) {
        final FactorizedNumber factorizedNumber = new FactorizedNumber(number);
        if (number == 1) {
            factorizedNumber.addFactor(1, 1);
            return factorizedNumber;
        }

        long nextDivisor = 2;
        long currentValue = number;
        long currentDivisorOccurrences = 0;

        while (nextDivisor <= currentValue) {
            if (currentValue % nextDivisor == 0) {
                currentDivisorOccurrences++;
                currentValue /= nextDivisor;
            } else {
                if (currentDivisorOccurrences != 0) {
                    factorizedNumber.addFactor(nextDivisor, currentDivisorOccurrences);
                }

                if (nextDivisor == 2) nextDivisor = 3;
                else nextDivisor += 2;
                currentDivisorOccurrences = 0;
            }
        }

        if (currentDivisorOccurrences != 0) {
            factorizedNumber.addFactor(nextDivisor, currentDivisorOccurrences);
        }

        return factorizedNumber;
    }

    public static long findLeastCommonMultiple(List<Long> numbers) {
        final Map<Long, FactorizedNumber> factorizedNumberByNumber = numbers.stream()
                .collect(Collectors.toMap(
                        Function.identity(),
                        Factorizer::factorize,
                        (f1, f2) -> f1
                        ));

        final Map<Long, Long> occurrencesByFactor = new HashMap<>();
        factorizedNumberByNumber.forEach((number, factorized) -> {
            factorized.getFactors().forEach((factor, primeFactor) -> {
                occurrencesByFactor.compute(factor, (k, v) -> {
                    if (v == null) return primeFactor.getOccurrences();
                    return Long.max(primeFactor.getOccurrences(), v);
                });
            });
        });

        long acc = 1;
        for (Map.Entry<Long, Long> entry : occurrencesByFactor.entrySet()) {
            acc *= Math.pow(entry.getKey(), entry.getValue());
        }

        return acc;
    }

    public static long findLeastCommonMultiple(long... numbers) {
        List<Long> longs = new ArrayList<>();
        for (long n : numbers) longs.add(n);

        return findLeastCommonMultiple(longs);
    }

    public static Set<Long> findAllDivisorOfNumber(long number) {
        FactorizedNumber factorizedNumber = factorize(number);
        List<Long> factors = factorizedNumber.getFactorsAsList();

        CombinationGenerator combinationGenerator = new CombinationGenerator(factors.size());

        Set<Long> divisors = new HashSet<>();
        divisors.add(1L);

        while (combinationGenerator.hasNext()) {
            boolean[] nextCombination = combinationGenerator.next();

            long divisor = 1;

            for (int i = 0; i < nextCombination.length; i++) {
                if (nextCombination[i]) {
                    divisor *= factors.get(i);
                }
            }

            divisors.add(divisor);
        }

        return divisors;
    }

    public Map<Long, Integer> factorizeNumberFromPrimeList(long number, List<Long> primes) {
        Map<Long, Integer> powersByFactor = new HashMap<>();

        long remaining = number;

        for (Long prime : primes) {
            while (remaining % prime == 0) {
                remaining /= prime;
                powersByFactor.compute(prime, (k, v) -> v == null ? 1 : v + 1);
            }

            if (remaining == 1) break;
        }

        if (remaining != 1) {
            throw new IllegalStateException(String.format("Remaining should be 1. Failed for %s", number));
        }

        return powersByFactor;
    }
}
