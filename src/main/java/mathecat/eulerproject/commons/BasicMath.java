package mathecat.eulerproject.commons;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;

public class BasicMath {
    public static long pow(long base, long exponent) {
        if (exponent < 0) throw new IllegalArgumentException("Unsupported exponent: " + exponent);

        long result = 1;
        for (int i = 0; i < exponent; i++) {
            result *= base;
        }

        return result;
    }

    public static long square(long base) {
        return pow(base, 2);
    }

    public static BigInteger factorial(int n) {
        BigInteger factorial = BigInteger.ONE;

        for (int i = 2; i <= n; i++) {
            factorial = factorial.multiply(BigInteger.valueOf(i));
        }

        return factorial;
    }

    public static BigDecimal root(BigDecimal value, long iterations, int whichRoot, int precision) {
        // Specify a math context with 40 digits of precision.

        MathContext mc = new MathContext(precision);

        BigDecimal x = new BigDecimal("1", mc);

        // Search for the cube root via the Newton-Raphson loop. Output each // successive iteration's value.

        for (int i = 0; i < iterations; i++) {
            x = x.subtract(
                    x.pow(whichRoot, mc)
                            .subtract(value, mc)
                            .divide(new BigDecimal(whichRoot, mc).multiply(
                                    x.pow(2, mc), mc), mc), mc);
        }
        return x;
    }

    public static BigDecimal root(BigDecimal value, int whichRoot) {
        return root(value, 100, whichRoot);
    }

    public static BigDecimal root(BigInteger value, int whichRoot) {
        return root(new BigDecimal(value), whichRoot);
    }

    public static BigDecimal root(BigDecimal value, long iterations, int whichRoot) {
        return root(value, iterations, whichRoot, 100);
    }
}
