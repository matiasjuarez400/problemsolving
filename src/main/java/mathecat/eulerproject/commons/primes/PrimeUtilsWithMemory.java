package mathecat.eulerproject.commons.primes;

import mathecat.eulerproject.commons.concepts.Range;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class PrimeUtilsWithMemory {
    private final Map<Range, List<Long>> primesByRange;
    private final Set<Long> knownPrimes;

    public PrimeUtilsWithMemory() {
        this.primesByRange = new HashMap<>();
        this.knownPrimes = new HashSet<>();
    }

    public PrimeUtilsWithMemory(long initialKnownRange) {
        this();
        storeRange(new Range(0, initialKnownRange), PrimeUtils.findPrimesInRange(0, initialKnownRange));
    }

    private Range storeRange(Range newRange, List<Long> primes) {
        List<Range> joinableRanges = primesByRange.keySet().stream()
                .filter(storedRange -> storedRange.canMerge(newRange))
                .collect(Collectors.toList());

        Set<Long> primesJoinedInNewRange = new HashSet<>(primes);
        joinableRanges.forEach(range -> {
            primesJoinedInNewRange.addAll(primesByRange.get(range));
            primesByRange.remove(range);
        });

        Range finalNewRange = newRange.merge(joinableRanges);

        List<Long> primesToStore = new ArrayList<>(primesJoinedInNewRange);
        Collections.sort(primesToStore);

        primesByRange.put(finalNewRange, primesToStore);
        knownPrimes.addAll(primes);

        return finalNewRange;
    }

    private Range getRangeForNumber(long number) {
        return primesByRange.keySet().stream()
                .filter(range -> range.includes(number))
                .findFirst()
                .orElse(null);
    }

    public boolean isPrime(long number) {
        if (number < 0) {
            throw new IllegalArgumentException("Negative numbers are not welcome here");
        }

        Range associatedRange = getRangeForNumber(number);

        if (associatedRange == null) {
            associatedRange = new Range(number, number);
            List<Long> associatedPrimeList = new ArrayList<>();
            if (PrimeUtils.isPrime(number)) {
                associatedPrimeList.add(number);
            }

            storeRange(associatedRange, associatedPrimeList);
        }

        return knownPrimes.contains(number);
    }

    public List<Long> getPrimesInRange(long start, long end) {
        if (start > end) {
            throw new IllegalArgumentException("start must be greater than end");
        }

        Range filterRange = new Range(start, end);

        Range startRange = getRangeForNumber(start);
        Range endRange = getRangeForNumber(end);

        List<Long> primesToReturn;

        // There is a gap in the middle
        if (startRange != null && endRange != null) {
            if (startRange.equals(endRange)) {
                primesToReturn = primesByRange.get(startRange);
            } else {
                Range maxRange = Range.createMaxRange(startRange, endRange);
                Range gapRange = startRange.gap(endRange);

                return searchAndStorePrimes(gapRange, maxRange, filterRange);
            }
        } else if (startRange == null && endRange == null) {
            Range maxRange = new Range(start, end);

            return searchAndStorePrimes(maxRange, maxRange, filterRange);
        } else if (startRange == null) {
            Range maxRange = new Range(start, endRange.getEnd());
            Range gapRange = new Range(start, endRange.getStart() - 1);

            return searchAndStorePrimes(gapRange, maxRange, filterRange);
        } else {
            Range maxRange = new Range(startRange.getStart(), end);
            Range gapRange = new Range(startRange.getEnd() + 1, end);

            return searchAndStorePrimes(gapRange, maxRange, filterRange);
        }

        return primesToReturn.stream()
                .filter(prime -> start <= prime && prime <= end)
                .collect(Collectors.toList());
    }

    private List<Long> searchAndStorePrimes(final Range searchRange, final Range storeRange, final Range filterRange) {
        List<Long> newPrimes = PrimeUtils.findPrimesInRange(searchRange);

        Range finalRange = storeRange(storeRange, newPrimes);

        return primesByRange.get(finalRange).stream()
                .filter(filterRange::includes)
                .collect(Collectors.toList());
    }
}
