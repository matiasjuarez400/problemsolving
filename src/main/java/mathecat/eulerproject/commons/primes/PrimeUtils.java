package mathecat.eulerproject.commons.primes;

import mathecat.eulerproject.commons.concepts.Range;

import java.util.ArrayList;
import java.util.List;

public class PrimeUtils {
    public static long findMaxPrimeFactor(long number) {
        long nextDivisor = 2;
        long currentValue = number;

        while (nextDivisor <= currentValue) {
            if (currentValue % nextDivisor == 0) {
                currentValue /= nextDivisor;
            } else {
                if (nextDivisor == 2) nextDivisor = 3;
                else nextDivisor += 2;
            }
        }

        return nextDivisor;
    }

    public static long findNthPrime(int primeIndex) {
        final long timeStart = System.currentTimeMillis();

        if (primeIndex == 1) return 2;

        final List<Long> foundPrimes = new ArrayList<>();
        foundPrimes.add(2L);

        int currentIndex = 1;
        long primeCandidate = 3;

        while (currentIndex < primeIndex) {
            boolean isPrime = true;

            for (Long prime : foundPrimes) {
                if (primeCandidate % prime == 0) {
                    isPrime = false;
                    break;
                }
            }

            if (isPrime) {
                foundPrimes.add(primeCandidate);
                currentIndex++;
            }

            primeCandidate += 2;
        }

        final long timeEnd = System.currentTimeMillis();

        System.out.printf("findNthPrime(%s) - Time: %s\n", primeIndex, timeEnd - timeStart);

        return foundPrimes.get(foundPrimes.size() - 1);
    }

    public static List<Long> findPrimesLessThan(final long upperLimit) {
        final long startTime = System.currentTimeMillis();

        if (upperLimit < 2) throw new IllegalArgumentException("Invalid upper limit: " + upperLimit);

        final List<Long> foundPrimes = new ArrayList<>();

        foundPrimes.add(2L);

        for (long primeCandidate = 3; primeCandidate < upperLimit; primeCandidate += 2) {
            if (isPrime(primeCandidate)) {
                foundPrimes.add(primeCandidate);
            }
        }

        long endTime = System.currentTimeMillis();

        System.out.printf("findPrimesLessThan(%s) - Time elapsed: %s%n", upperLimit, endTime - startTime);

        return foundPrimes;
    }

    public static boolean isPrime(long number) {
        if (number == 2) return true;
        if (number % 2 == 0) return false;
        if (number < 2) return false;

        double checkLimit = Math.sqrt(number);

        long currentPrime = 3;

        while (currentPrime <= checkLimit) {
            if (number % currentPrime == 0) {
                return false;
            }

            currentPrime += 2;
        }

        return true;
    }

    public static List<Long> findPrimesInRange(long start, long end) {
        List<Long> primes = new ArrayList<>();

        if (start < 2) start = 2;
        if (start == 2) primes.add(2L);

        long current = start % 2 == 0 ? start + 1 : start;

        while(current <= end) {
            if (isPrime(current)) primes.add(current);
            current += 2;
        }

        return primes;
    }

    public static List<Long> findPrimesInRange(Range range) {
        return findPrimesInRange(range.getStart(), range.getEnd());
    }

    public static long findNextPrime(final long start) {
        long current = start;

        if (isPrime(current)) return current;

        if (current % 2 == 0) current++;

        while (!isPrime(current)) {
            current += 2;
        }

        return current;
    }
}
