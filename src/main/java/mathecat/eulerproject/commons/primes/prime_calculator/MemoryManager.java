package mathecat.eulerproject.commons.primes.prime_calculator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MemoryManager {
    private final String memoryPath;

    public MemoryManager(String memoryPath) {
        this.memoryPath = memoryPath;
    }

    public MemoryManager(File file) {
        this.memoryPath = file.getAbsolutePath();
    }

    public List<Long> loadPrimes() {
        validateActiveMemory();

        Path memory = Paths.get(memoryPath);

        List<Long> loadedPrimes = new ArrayList<>();

        try (BufferedReader reader = Files.newBufferedReader(memory, StandardCharsets.UTF_8)) {
            String line = reader.readLine();

            if (line == null || line.length() == 0) return Collections.emptyList();

            long expectedLines = Long.parseLong(line);

            while ((line = reader.readLine()) != null) {
                loadedPrimes.add(Long.parseLong(line));
            }

            if (expectedLines != loadedPrimes.size()) {
                throw new IllegalStateException(String.format("Expected %s numbers, but got %s", expectedLines, loadedPrimes.size()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return loadedPrimes;
    }

    public long getLineCount() {
        validateActiveMemory();

        Path memory = Paths.get(memoryPath);

        try (BufferedReader reader = Files.newBufferedReader(memory, StandardCharsets.UTF_8)) {
            String line = reader.readLine();

            return line == null ? 0 : Long.parseLong(line);
        } catch (IOException e) {
            e.printStackTrace();
        }

        throw new IllegalStateException("Unable to read number of lines from file");
    }

    public void savePrimes(List<Long> primes) {
        validateActiveMemory();

        Path memory = Paths.get(memoryPath);

        try (BufferedWriter writer = Files.newBufferedWriter(memory, StandardCharsets.UTF_8)) {
            writer.write(Long.toString(primes.size()));
            writer.newLine();
            for (Long prime : primes) {
                writer.write(prime.toString());
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isActive() {
        return this.memoryPath != null && this.memoryPath.length() > 0;
    }

    public boolean isUsingPath(String path) {
        if (!isActive()) return false;
        return this.memoryPath.equals(path);
    }

    private void validateActiveMemory() {
        if (!isActive()) throw new IllegalStateException("The memory is inactive");
    }
}
