package mathecat.eulerproject.commons.primes.prime_calculator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PrimeCalculator {
    private long lastExploredNumber;
    private final List<Long> orderedPrimes;
    private final Set<Long> knownPrimes;
    private final MemoryManager memoryManager;
    private static final String DEFAULT_MEMORY_PATH = "src/main/java/mathecat/eulerproject/commons/primes/prime_calculator/prime_memory.txt";

    public PrimeCalculator() {
        this(false);
    }

    public PrimeCalculator(boolean useMemory) {
        this(useMemory ? DEFAULT_MEMORY_PATH : "");
    }

    public PrimeCalculator(String memoryPath) {
        this.orderedPrimes = new ArrayList<>();
        this.knownPrimes = new HashSet<>();
        this.memoryManager = new MemoryManager(memoryPath);

        if (memoryManager.isActive()) {
            for (Long prime : this.memoryManager.loadPrimes()) {
                addPrime(prime);
            }
        }

        if (this.orderedPrimes.isEmpty()) {
            addPrime(2);
            addPrime(3);
        }
    }

    public void savePrimes(String memoryPathString) {
        MemoryManager memoryManager = this.memoryManager.isUsingPath(memoryPathString) ? this.memoryManager : new MemoryManager(memoryPathString);

        doSavePrimesToMemory(memoryManager);
    }

    public void savePrimes() {
        doSavePrimesToMemory(memoryManager);
    }

    private void doSavePrimesToMemory(MemoryManager memoryManager) {
        long storedPrimes = memoryManager.getLineCount();

        if (storedPrimes < this.orderedPrimes.size()) {
            memoryManager.savePrimes(this.orderedPrimes);
        }
    }

    private void addPrime(long prime) {
        if (knownPrimes.contains(prime)) {
            throw new IllegalStateException("The prime already exists in memory");
        }

        orderedPrimes.add(prime);
        knownPrimes.add(prime);

        setLastExploredNumber(prime);
    }

    private boolean isPrime(long number, boolean isInternalCall) {
        double checkLimit = Math.sqrt(number);

        for (Long knownPrime : orderedPrimes) {
            if (knownPrime > checkLimit) break;
            if (number % knownPrime == 0) return false;
        }

        // We explore the odd numbers in order when doing the isPrime call from inside this class code
        if (isInternalCall) return true;

        long nextNumberCheck = getBiggestKnownPrime() + 2;

        while (nextNumberCheck <= checkLimit) {
            if (isPrime(nextNumberCheck, true)) {
                addPrime(nextNumberCheck);

                if (number % nextNumberCheck == 0) return false;
            }

            nextNumberCheck += 2;
        }

        setLastExploredNumber(nextNumberCheck - 2);

        return true;
    }

    private void setLastExploredNumber(long number) {
        if (number > lastExploredNumber) lastExploredNumber = number;
    }

    private long getBiggestKnownPrime() {
        if (this.orderedPrimes.isEmpty()) return -1;

        return orderedPrimes.get(orderedPrimes.size() - 1);
    }

    public List<Long> getKnownPrimes() {
        return new ArrayList<>(this.orderedPrimes);
    }

    public boolean isPrime(long number) {
        if (knownPrimes.contains(number)) return true;
        if (number < lastExploredNumber) return false;

        return isPrime(number, false);
    }

    public long nextPrime(long startValue) {
        if (startValue <= 1) return orderedPrimes.get(0);

        if (startValue < getBiggestKnownPrime()) {
            int leftIndex = 0;
            int rightIndex = orderedPrimes.size() - 1;

            while (leftIndex < rightIndex && Math.abs(rightIndex - leftIndex) != 1) {
                int middleIndex = (rightIndex + leftIndex) / 2;

                long currentPrime = orderedPrimes.get(middleIndex);
                if (startValue == currentPrime) {
                    return orderedPrimes.get(middleIndex + 1);
                } else if (startValue < currentPrime) {
                    rightIndex = middleIndex;
                } else {
                    leftIndex = middleIndex;
                }
            }

            if (leftIndex < rightIndex) {
                long leftPrime = orderedPrimes.get(leftIndex);
                long rightPrime = orderedPrimes.get(rightIndex);

                if (leftPrime <= startValue && startValue < rightPrime) return rightPrime;

                throw new IllegalStateException(String.format("%s should be between %s and %s", startValue, leftPrime, rightPrime));
            } else if (leftIndex == rightIndex) {
                return orderedPrimes.get(rightIndex);
            } else {
                throw new IllegalStateException("Unexpected. Right index is less than Left index");
            }
        } else {
            long nextPrimeSubject = startValue % 2 == 0 ? startValue + 1 : startValue + 2;

            while (!isPrime(nextPrimeSubject)) {
                nextPrimeSubject += 2;
            }

            return nextPrimeSubject;
        }
    }
}
