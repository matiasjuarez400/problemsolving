package mathecat.eulerproject.commons.generators;

public class CollatzSequenceGenerator {
    private long current;
    private boolean hasEnded = false;

    public CollatzSequenceGenerator(long startPoint) {
        if (startPoint <= 0) {
            throw new IllegalArgumentException("startPoint point should be positive");
        }

        this.current = startPoint;
    }

    public long next() {
        if (current == 1) {
            hasEnded = true;
            return 1;
        }

        long temp = current;

        if (current % 2 == 0) {
            current /= 2;
        } else {
            current = current * 3 + 1;
        }

        return temp;
    }

    public boolean hasNext() {
        return !hasEnded;
    }
}
