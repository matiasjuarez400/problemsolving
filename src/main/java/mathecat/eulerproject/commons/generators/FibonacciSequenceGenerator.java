package mathecat.eulerproject.commons.generators;

public class FibonacciSequenceGenerator {
    private int first = 0;
    private int second = 1;
    private int nextElementIndex = 1;

    public int next() {
        int output = first + second;
        first = second;
        second = output;

        nextElementIndex++;

        return output;
    }

    public int getNextElementIndex() {
        return nextElementIndex;
    }
}
