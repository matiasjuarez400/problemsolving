package mathecat.eulerproject.commons.continuedFractions;

import mathecat.eulerproject.commons.BasicMath;
import mathecat.eulerproject.commons.concepts.BigFraction;

import java.math.BigDecimal;
import java.math.BigInteger;

public class ContinuedFractionExpander {
    public static long[] expand(BigFraction inputValue, int iterations, int precision) {
        return expand(inputValue.toDecimal(precision), iterations);
    }

    public static long[] expand(BigDecimal inputValue, int iterations) {
        long[] expansionIndexes = new long[iterations];

        BigFraction currentValue = BigFraction.of(inputValue);

        for (int i = 0; i < iterations; i++) {
            BigFraction[] divideAndRemainder = currentValue.divideAndRemainder();
            BigInteger expansionIndex = divideAndRemainder[0].getNumerator();
            currentValue = divideAndRemainder[1].invert();

            expansionIndexes[i] = expansionIndex.longValue();
        }

        return expansionIndexes;
    }

    public static long[] expandSqrt(long base, int rootIterations, int expansionIterations, int decimalPrecision) {
        BigDecimal sqrtValue = BasicMath.root(BigDecimal.valueOf(base), rootIterations, 2, decimalPrecision);

        return expand(sqrtValue, expansionIterations);
    }
}
