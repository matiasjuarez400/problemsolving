package mathecat.eulerproject.commons.continuedFractions;

import mathecat.eulerproject.commons.concepts.BigFraction;

import java.util.List;

public class ConvergentCalculator {
    public static BigFraction findConvergent(long base, long[] indexes) {
        BigFraction currentValue = BigFraction.of(1, indexes[indexes.length - 1]);

        for (int i = indexes.length - 2; i >= 0; i--) {
            long currentPeriodMember = indexes[i];

            currentValue = currentValue.add(BigFraction.of(currentPeriodMember));

            currentValue = BigFraction.ONE.divide(currentValue);
        }

        return currentValue.add(BigFraction.of(base));
    }

    public static BigFraction findConvergent(long base, List<Long> indexes) {
        return findConvergent(base, indexes.stream().mapToLong(l -> l).toArray());
    }
}
