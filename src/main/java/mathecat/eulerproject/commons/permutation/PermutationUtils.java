package mathecat.eulerproject.commons.permutation;

import mathecat.eulerproject.commons.BasicMath;

import java.math.BigInteger;

public class PermutationUtils {
    public static BigInteger getTotalPermutations(int totalElements, int permutationSize, boolean repetitionAllowed) {
        if (repetitionAllowed) {
            return BigInteger.valueOf(totalElements).pow(permutationSize);
        } else {
            BigInteger nf = BasicMath.factorial(totalElements);
            BigInteger nrf = BasicMath.factorial(totalElements - permutationSize);

            return nf.divide(nrf);
        }
    }
}
