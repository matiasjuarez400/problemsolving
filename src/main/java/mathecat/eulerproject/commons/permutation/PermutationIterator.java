package mathecat.eulerproject.commons.permutation;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class PermutationIterator<T> implements Iterator<List<T>> {
    private final List<T> values;
    private BigInteger usedPermutations;
    private final BigInteger totalPermutationsWithRepetition;
    private final BigInteger totalPermutationsNoRepetition;
    private final int[] currentPermutationIndexes;
    private final int lastIndex;
    private final boolean allowRepeated;

    public PermutationIterator(List<T> values, int permutationSize) {
        this(values, permutationSize, true);
    }

    public PermutationIterator(List<T> values, int permutationSize, boolean allowRepeated) {
        if (values == null || values.isEmpty()) {
            throw new IllegalArgumentException("The values must be present");
        }

        if (permutationSize <= 0) {
            throw new IllegalArgumentException("permutationSize must be at least 1");
        }

        if (!allowRepeated) {
            if (values.size() < permutationSize) {
                throw new IllegalArgumentException("Permutations with no repetition must have a size of at most the total elements");
            }
        }

        this.values = values;
        this.currentPermutationIndexes = new int[permutationSize];
        this.usedPermutations = BigInteger.ZERO;
        this.totalPermutationsWithRepetition = PermutationUtils.getTotalPermutations(values.size(), permutationSize, true);
        this.totalPermutationsNoRepetition = PermutationUtils.getTotalPermutations(values.size(), permutationSize, false);
        this.lastIndex = values.size() - 1;
        this.allowRepeated = allowRepeated;
    }

    @Override
    public boolean hasNext() {
        if (allowRepeated) {
            return usedPermutations.compareTo(totalPermutationsWithRepetition) < 0;
        } else {
            return usedPermutations.compareTo(totalPermutationsNoRepetition) < 0;
        }
    }

    @Override
    public List<T> next() {
        if (allowRepeated) {
            return iterateWithRepetitions();
        } else {
            return iterateNoRepetitions();
        }
    }

    private List<T> iterateNoRepetitions() {
        if (!hasNext()) return Collections.emptyList();

        Set<Integer> usedIndexes = new HashSet<>();

        while (usedIndexes.size() < currentPermutationIndexes.length) {
            usedIndexes.clear();
            for (int i = 0; i < currentPermutationIndexes.length; i++) {
                int currentIndex = currentPermutationIndexes[i];

                if (currentIndex == lastIndex) {
                    currentPermutationIndexes[i] = 0;
                } else {
                    currentPermutationIndexes[i] = currentIndex + 1;
                    break;
                }
            }

            for (int currentPermutationIndex : currentPermutationIndexes) {
                usedIndexes.add(currentPermutationIndex);
            }
        }

        usedPermutations = usedPermutations.add(BigInteger.ONE);

        List<T> permutations = new ArrayList<>();
        for (int index : currentPermutationIndexes) {
            permutations.add(values.get(index));
        }

        return permutations;
    }

    private List<T> iterateWithRepetitions() {
        if (!hasNext()) return Collections.emptyList();

        for (int i = 0; i < currentPermutationIndexes.length; i++) {
            int currentIndex = currentPermutationIndexes[i];

            if (currentIndex == lastIndex) {
                currentPermutationIndexes[i] = 0;
            } else {
                currentPermutationIndexes[i] = currentIndex + 1;
                break;
            }
        }

        usedPermutations = usedPermutations.add(BigInteger.ONE);

        List<T> permutations = new ArrayList<>();
        for (int index : currentPermutationIndexes) {
            permutations.add(values.get(index));
        }

        return permutations;
    }

    public BigInteger totalPermutations() {
        return totalPermutationsWithRepetition;
    }
}
