package mathecat.eulerproject.commons.concepts;

import mathecat.eulerproject.commons.factorizartion.Factorizer2;

public class Fraction {
    private final long numerator;
    private final long denominator;

    public Fraction(long numerator, long denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public Fraction(long numerator) {
        this(numerator, 1);
    }

    public static Fraction of(long numerator, long denominator) {
        return new Fraction(numerator, denominator);
    }

    public static Fraction of(long numerator) {
        return new Fraction(numerator);
    }

    public Fraction add(Fraction that) {
        long tempDenominator = this.denominator * that.denominator;

        long tempNumerator = (tempDenominator / this.denominator) * this.numerator + (tempDenominator / that.denominator) * that.numerator;

        return new Fraction(tempNumerator, tempDenominator).simplify();
    }

    public Fraction diff(Fraction that) {
        return this.add(that.negative());
    }

    public Fraction times(Fraction that) {
        return Fraction.of(this.numerator * that.numerator, this.denominator * that.denominator).simplify();
    }

    public Fraction divide(Fraction that) {
        return Fraction.of(this.numerator * that.denominator, this.denominator * that.numerator).simplify();
    }

    public Fraction simplify() {
        long greatestCommonDivisor = Factorizer2.findGreatestCommonDivisor(this.numerator, this.denominator);

        return Fraction.of(this.numerator / greatestCommonDivisor, this.denominator / greatestCommonDivisor);
    }

    public Fraction negative() {
        return Fraction.of(this.numerator * -1, this.denominator);
    }

    public long getNumerator() {
        return numerator;
    }

    public long getDenominator() {
        return denominator;
    }

    @Override
    public String toString() {
        return String.format("%s/%s", numerator, denominator);
    }
}
