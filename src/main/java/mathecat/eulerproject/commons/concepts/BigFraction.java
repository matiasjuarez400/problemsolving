package mathecat.eulerproject.commons.concepts;

import mathecat.eulerproject.commons.factorizartion.Factorizer2;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Objects;

public class BigFraction {
    private final BigInteger numerator;
    private final BigInteger denominator;

    public static final BigFraction ONE = BigFraction.of(1L);
    public static final BigFraction ZERO = BigFraction.of(0L);

    public static BigFraction of(double decimal) {
        return of(BigDecimal.valueOf(decimal));
    }

    public static BigFraction of(BigDecimal decimal) {
        String decimalString = decimal.toString();

        int indexOfSeparator = decimalString.indexOf('.');
        if (indexOfSeparator == -1) {
            indexOfSeparator = decimalString.indexOf(',');
        }

        BigInteger numerator;
        BigInteger denominator;

        if (indexOfSeparator == -1) {
            numerator = new BigInteger(decimalString);
            denominator = BigInteger.valueOf(1);
        } else {
            numerator = new BigInteger(decimalString.substring(0, indexOfSeparator) + decimalString.substring(indexOfSeparator + 1));
            denominator = BigInteger.valueOf(10).pow(decimalString.length() - indexOfSeparator - 1);
        }

        return new BigFraction(numerator, denominator).simplify();
    }

    public BigFraction(BigInteger numerator, BigInteger denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
//        this.showToString = numerator.toString().length() < 1000 && denominator.toString().length() < 1000;
    }

    public BigFraction(long numerator, long denominator) {
        this(BigInteger.valueOf(numerator), BigInteger.valueOf(denominator));
    }

    public BigFraction(BigInteger numerator) {
        this(numerator, BigInteger.ONE);
    }

    public static BigFraction of(BigInteger numerator, BigInteger denominator) {
        return new BigFraction(numerator, denominator);
    }

    public static BigFraction of(BigInteger numerator) {
        return new BigFraction(numerator);
    }

    public static BigFraction of(long numerator) {
        return new BigFraction(BigInteger.valueOf(numerator));
    }

    public static BigFraction of(long numerator, long denominator) {
        return new BigFraction(BigInteger.valueOf(numerator), BigInteger.valueOf(denominator));
    }

    public BigFraction simplify() {
        BigInteger greatestCommonDivisor = Factorizer2.findGreatestCommonDivisor(this.numerator, this.denominator);

        return BigFraction.of(this.numerator.divide(greatestCommonDivisor), this.denominator.divide(greatestCommonDivisor));
    }

    public BigFraction add(BigFraction that) {
        BigInteger tempDenominator = this.denominator.multiply(that.denominator);

        BigInteger tempNumerator = tempDenominator.divide(this.denominator).multiply(this.numerator)
                .add(tempDenominator.divide(that.denominator).multiply(that.numerator));

        return new BigFraction(tempNumerator, tempDenominator).simplify();
    }

    public BigFraction diff(BigFraction that) {
        return this.add(that.negative());
    }

    public BigFraction times(BigFraction that) {
        return BigFraction.of(this.numerator.multiply(that.numerator), this.denominator.multiply(that.denominator)).simplify();
    }

    public BigFraction divide(BigFraction that) {
        return BigFraction.of(this.numerator.multiply(that.denominator), this.denominator.multiply(that.numerator)).simplify();
    }

    public BigFraction divide(BigInteger divisor) {
        return divide(BigFraction.of(divisor));
    }

    public BigFraction pow(int power) {
        return pow(power, false);
    }

    public BigFraction pow(int power, boolean simplify) {
        BigFraction result = BigFraction.of(
                this.numerator.pow(power),
                this.denominator.pow(power)
        );

        if (simplify) {
            result = result.simplify();
        }

        return result;
    }

    public BigFraction negative() {
        return BigFraction.of(this.numerator.multiply(BigInteger.valueOf(-1)), this.denominator);
    }

    public BigInteger getNumerator() {
        return numerator;
    }

    public BigInteger getDenominator() {
        return denominator;
    }

    public BigDecimal toDecimal(int precision) {
        return new BigDecimal(numerator).divide(new BigDecimal(denominator), precision, RoundingMode.HALF_EVEN);
    }

    public BigInteger toBigInteger() {
        return numerator.divide(denominator);
    }

    public BigFraction[] divideAndRemainder() {
        BigFraction[] output = new BigFraction[2];
        output[0] = BigFraction.of(toBigInteger());
        output[1] = diff(output[0]);

        return output;
    }

    public BigFraction invert() {
        if (this.numerator.equals(BigInteger.ZERO)) return this;

        return BigFraction.of(denominator, numerator).simplify();
    }

    public BigDecimal toDecimal() {
        return toDecimal(100);
    }

    public boolean isZero() {
        return this.numerator.equals(BigInteger.ZERO);
    }

    @Override
    public String toString() {
        return String.format("%s/%s", numerator, denominator);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BigFraction that = (BigFraction) o;
        return numerator.equals(that.numerator) &&
                denominator.equals(that.denominator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numerator, denominator);
    }
}
