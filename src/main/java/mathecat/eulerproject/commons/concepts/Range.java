package mathecat.eulerproject.commons.concepts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Range {
    private final long start;
    private final long end;

    public Range(long start, long end) {
        if (start > end) {
            throw new IllegalArgumentException("start must be less than end");
        }

        this.start = start;
        this.end = end;
    }

    public boolean isOverlapping(Range that) {
        return checkIfRangeLimitsAreContained(this, that) || checkIfRangeLimitsAreContained(that, this);
    }

    private boolean checkIfRangeLimitsAreContained(Range left, Range right) {
        return (left.start >= right.start && left.start <= right.end) || (left.end >= right.start && left.end <= right.end);
    }

    public boolean isAdjoined(Range that) {
        return this.end + 1 == that.start || this.start - 1 == that.end;
    }

    public boolean canMerge(Range that) {
        return isOverlapping(that) || isAdjoined(that);
    }

    public Range merge(Range that) {
        if (!canMerge(that)) {
            throw new IllegalArgumentException(
                    String.format("Can not merge ranges. Reason: they don't overlap nor are adjoined. [%s | %s]", this, that)
            );
        }

        return new Range(Long.min(this.start, that.start), Long.max(this.end, that.end));
    }

    public Range merge(Range... ranges) {
        return merge(Arrays.asList(ranges));
    }

    public Range merge(List<Range> ranges) {
        if (ranges.isEmpty()) return this;

        Range newRange = this;
        List<Range> remainingRanges = new ArrayList<>(ranges);

        while (!remainingRanges.isEmpty()) {
            List<Range> mergedRanges = new ArrayList<>();
            for (Range range : remainingRanges) {
                if (range.canMerge(newRange)) {
                    mergedRanges.add(range);
                }
            }

            if (mergedRanges.isEmpty()) {
                String sb = "The maximum ranged that was created is: " + newRange + ".\n" +
                        "Could not merge these ranges: [" +
                        remainingRanges.stream()
                                .sorted(Range.comparator())
                                .map(Objects::toString)
                                .collect(Collectors.joining(", ")) +
                        "]";
                throw new IllegalArgumentException(sb);
            }

            for (Range range : mergedRanges) {
                newRange = newRange.merge(range);
            }

            remainingRanges.removeAll(mergedRanges);
        }

        return newRange;
    }

    public Range gap(Range that) {
        if (isAdjoined(that) || isOverlapping(that)) return null;

        return new Range(Long.min(this.end, that.end) + 1, Long.max(this.start, that.start) - 1);
    }

    public static Range createMaxRange(Range... ranges) {
        if (ranges == null || ranges.length == 0) return null;

        long minStart = ranges[0].start;
        long maxEnd = ranges[0].end;

        for (Range range : ranges) {
            if (range.start < minStart) minStart = range.start;
            if (range.end > maxEnd) maxEnd = range.end;
        }

        return new Range(minStart, maxEnd);
    }

    public long size() {
        return end - start + 1;
    }

    public boolean includes(long value) {
        return start <= value && value <= end;
    }

    public boolean includes(Range that) {
        return this.start <= that.start && that.end <= this.end;
    }

    public long getStart() {
        return start;
    }

    public long getEnd() {
        return end;
    }

    public static Comparator<Range> comparator() {
        return Comparator.comparingLong(Range::getStart).thenComparingLong(Range::getEnd);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Range range = (Range) o;
        return start == range.start &&
                end == range.end;
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, end);
    }

    @Override
    public String toString() {
        return "Range{" + start + ", " + end + "}";
    }
}
