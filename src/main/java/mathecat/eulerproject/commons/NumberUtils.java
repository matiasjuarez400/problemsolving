package mathecat.eulerproject.commons;

import java.math.BigInteger;

public class NumberUtils {
    public static int size(long number) {
        if (number == 0) return 1;

        return (int) (Math.log10(Math.abs(number)) + 1);
    }

    public static int size(BigInteger number) {
        return number.toString().length();
    }

    public static int getDigit(long number, int position) {
        if (position < 0) {
            throw new IllegalArgumentException("Position can not be negative");
        }

        if (position >= size(number)) {
            throw new IllegalArgumentException(String.format("Position %s is out of range for number %s", position, number));
        }

        return (int) ((number % BasicMath.pow(10, position + 1)) / BasicMath.pow(10, position));
    }

    public static long nineComplement(long number) {
        int size = size(number);
        // The 9999 number for which we want to get the complement
        long goal = BasicMath.pow(10, size) - 1;

        return goal - number;
    }

    public static int[] toArray(long number) {
        int size = size(number);

        int[] array = new int[size];

        toArray(number, array);

        return array;
    }

    public static void toArray(long number, int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[array.length - 1 - i] = getDigit(number, i);
        }
    }

    public static long toNumber(int[] array) {
        long result = 0;

        for (int i = 0; i < array.length; i++) {
            result += BasicMath.pow(10, i) * array[array.length - 1 - i];
        }

        return result;
    }

    public static BigInteger reverse(BigInteger bigInteger) {
        return new BigInteger(new StringBuilder(bigInteger.toString()).reverse().toString());
    }

    public static long reverse(long n) {
        return reverse(BigInteger.valueOf(n)).longValue();
    }

    public static boolean isPalindrome(BigInteger number) {
        String n = number.toString();

        int limit = n.length() / 2;
        int last = n.length() - 1;
        for (int i = 0; i < limit; i++) {
            if (n.charAt(i) != n.charAt(last - i)) return false;
        }

        return true;
    }

    public static boolean isPalindrome(long number) {
        return isPalindrome(BigInteger.valueOf(number));
    }

    public static BigInteger sumDigits(BigInteger number) {
        String numberString = number.toString();

        BigInteger acc = BigInteger.ZERO;
        for (char c : numberString.toCharArray()) {
            acc = acc.add(BigInteger.valueOf(Character.digit(c, 10)));
        }

        return acc;
    }

    public static long sumDigits(long number) {
        return sumDigits(BigInteger.valueOf(number)).longValue();
    }

    public static long concatenate(long left, long right) {
        long longTypeDigitLimit = size(Long.MAX_VALUE);
        if (size(left) + size(right) > longTypeDigitLimit) {
            throw new IllegalArgumentException(String.format("Concatenating %s and %s would case a long overflow", left, right));
        }

        long spaceMultiplier = BasicMath.pow(10, size(right));

        return left * spaceMultiplier + right;
    }
}
