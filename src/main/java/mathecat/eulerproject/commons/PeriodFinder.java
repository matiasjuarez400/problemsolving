package mathecat.eulerproject.commons;

public class PeriodFinder {
    public static long[] findLongestPeriod(long[] expansionIndexes) {
        int periodSizeLimit = expansionIndexes.length / 2;

        int longestPeriodSize = -1;

        for (int periodSize = 1; periodSize <= periodSizeLimit; periodSize++) {
            boolean isPeriodCandidate = true;
            for (int i = 0; i < periodSize; i++) {
                if (expansionIndexes[i + 1] != expansionIndexes[periodSize + i + 1]) {
                    isPeriodCandidate = false;
                    break;
                }
            }

            if (isPeriodCandidate) {
                if (!isSubPeriod(periodSize, longestPeriodSize, expansionIndexes)) {
                    longestPeriodSize = periodSize;
                }
            }
        }

        long[] longestPeriod = new long[longestPeriodSize + 1];
        for (int i = 0; i < longestPeriod.length; i++) {
            longestPeriod[i] = expansionIndexes[i];
        }

        return longestPeriod;
    }

    /**
     * Analyses the expansion indexes array in an attempt to find a period. If a period is found, it validates that such period
     * can be used to cover the ratio of expansion indexes specified by matching ratio.
     * @param expansionIndexes - The numbers to be analyzed in order to find the period.
     * @param matchingRatio - Allows to specify how much of the expansionIndexes array should be covered by the peior to be returned
     * @return null if no period with the matching ratio is found or an array containing the period if applicable.
     */
    public static long[] findLongestPeriod(long[] expansionIndexes, double matchingRatio) {
        long[] longestPeriodFound = findLongestPeriod(expansionIndexes);

        int matchingAcc = 0;
        boolean stillMatching = true;

        for (int periodIndex = 0, expansionIndex = 1; stillMatching && expansionIndex < expansionIndexes.length ; periodIndex +=1, expansionIndex += 1) {
            int actualPeriodIndex = periodIndex % (longestPeriodFound.length - 1) + 1;

            if (longestPeriodFound[actualPeriodIndex] == expansionIndexes[expansionIndex]) {
                matchingAcc++;
            } else {
                stillMatching = false;
            }
        }

        double coverageRatio = (matchingAcc * 1.0) / expansionIndexes.length;

        return coverageRatio < matchingRatio && matchingAcc < 150 ? new long[0] : longestPeriodFound;
    }

    public static boolean isSubPeriod(final int currentPeriodSize, final int longestPeriodSize, long[] expansionIndexes) {
        if (longestPeriodSize == -1) return false;

        int subPeriodFittingTimes = currentPeriodSize / longestPeriodSize;
        if (subPeriodFittingTimes * longestPeriodSize != currentPeriodSize) return false;

        boolean isSubPeriod = true;
        for (int i = 0; i < currentPeriodSize; i++) {
            int currentPeriodIndex = i + 1;
            int currentSubPeriodIndex = (i % longestPeriodSize) + 1;

            if (expansionIndexes[currentPeriodIndex] != expansionIndexes[currentSubPeriodIndex]) {
                isSubPeriod = false;
                break;
            }
        }

        return isSubPeriod;
    }
}
