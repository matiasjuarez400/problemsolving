package mathecat.eulerproject.commons.series;

public class PentagonalGenerator extends Generator {
    @Override
    protected long generate(int n) {
        return n * (3L * n - 1L) / 2L;
    }
}
