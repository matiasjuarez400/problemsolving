package mathecat.eulerproject.commons.series;

import java.util.ArrayList;
import java.util.List;

public abstract class Generator {
    private final int DEFAULT_INDEX_SEARCH_STEP = 5000;

    public long get(int n) {
        if (n < 1) {
            throw new IllegalArgumentException("n must be at least 1");
        }

        return generate(n);
    }

    /**
     * Returns the 1-based index of the member of the series that is right before the searchValue param
     * @param searchValue - The value you are interested in
     * @param searchStep - The increment by which the value of 'end' will be modified in order to perform the binary search
     * @return
     */
    public int beforeIndexForValue(long searchValue, int searchStep) {
        if (searchValue <= 1) return 0;

        int start = 1;
        int end = 0;

        long currentValue;

        // Init end for binary search
        do {
            end += searchStep;
            currentValue = generate(end);
        } while (currentValue <= searchValue);

        int middle;
        do {
            middle = (end + start) / 2;
            currentValue = generate(middle);
            if (currentValue < searchValue) start = middle;
            else if (currentValue > searchValue) end = middle;
            else break;
        } while (end - start > 1);

        if (searchValue < currentValue) return middle - 1;
        else return middle;
    }

    public int beforeIndexForValue(long searchValue) {
        return beforeIndexForValue(searchValue, DEFAULT_INDEX_SEARCH_STEP);
    }

    /**
     *
     * @param startValue inclusive
     * @param endValue inclusive
     * @return
     */
    public List<Long> generateValuesForRange(long startValue, long endValue) {
        List<Long> values = new ArrayList<>();

        int nStart = beforeIndexForValue(startValue);
        long currentStartValue = generate(nStart);
        if (currentStartValue < startValue) {
            nStart++;
        }

        int nEnd = beforeIndexForValue(endValue);

        for (int n = nStart; n <= nEnd; n++) {
            values.add(generate(n));
        }

        return values;
    }

    protected abstract long generate(int n);
}
