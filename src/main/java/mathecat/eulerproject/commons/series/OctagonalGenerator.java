package mathecat.eulerproject.commons.series;

public class OctagonalGenerator extends Generator {
    @Override
    protected long generate(int n) {
        return n * (3L * n - 2L);
    }
}
