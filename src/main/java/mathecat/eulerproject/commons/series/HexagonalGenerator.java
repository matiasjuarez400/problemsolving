package mathecat.eulerproject.commons.series;

public class HexagonalGenerator extends Generator {
    @Override
    protected long generate(int n) {
        return n * (2L * n - 1L);
    }
}
