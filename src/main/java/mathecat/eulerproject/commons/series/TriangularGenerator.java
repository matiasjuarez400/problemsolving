package mathecat.eulerproject.commons.series;

public class TriangularGenerator extends Generator {
    @Override
    protected long generate(int n) {
        return (n * (n + 1L) ) / 2L;
    }
}
