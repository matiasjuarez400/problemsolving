package mathecat.eulerproject.commons.series;

public class HeptagonalGenerator extends Generator {
    @Override
    protected long generate(int n) {
        return (n * (5L * n - 3L) ) / 2L;
    }
}
