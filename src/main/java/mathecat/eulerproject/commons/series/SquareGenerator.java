package mathecat.eulerproject.commons.series;

public class SquareGenerator extends Generator {
    @Override
    protected long generate(int n) {
        return (n * n);
    }
}
