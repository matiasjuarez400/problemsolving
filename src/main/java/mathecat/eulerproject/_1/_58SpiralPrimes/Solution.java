package mathecat.eulerproject._1._58SpiralPrimes;

import mathecat.eulerproject.commons.primes.prime_calculator.PrimeCalculator;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.solve(10));
    }

    public long solve(double percentageLimit) {
        PrimeCalculator primeCalculator = new PrimeCalculator(true);

        double primeCount = 0;
        double diagonalElementCount = 1;
        double currentPercentage = 100;

        int iteration = 0;
        long currentStartValue = 1;
        while (currentPercentage > percentageLimit) {
            iteration++;

            long[] diagonalElements = getDiagonalElementsForIteration(iteration, currentStartValue);

            diagonalElementCount += 4;

            for (long diagonalElement : diagonalElements) {
                if (primeCalculator.isPrime(diagonalElement)) primeCount++;
            }

            currentStartValue = diagonalElements[diagonalElements.length - 1];

            currentPercentage = (primeCount / diagonalElementCount) * 100.0;
        }

        primeCalculator.savePrimes();

        return sideLength(iteration);
    }

    private long[] getDiagonalElementsForIteration(int iteration, long startValue) {
        long[] elements = new long[4];

        long prev = startValue;
        for (int i = 0; i < 4; i++) {
            long newValue = prev + sideLength(iteration) - 1;
            elements[i] = newValue;
            prev = newValue;
        }

        return elements;
    }

    private long sideLength(int iteration) {
        return iteration * 2 + 1;
    }

    private long numbersInDiagonal(int iterations) {
        return iterations * 4 + 1;
    }
}
