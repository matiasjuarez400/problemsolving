package mathecat.eulerproject._1._64OddPeriodSquareRoots;

import mathecat.eulerproject.commons.BasicMath;
import mathecat.eulerproject.commons.continuedFractions.ContinuedFractionExpander;
import mathecat.eulerproject.commons.PeriodFinder;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class Calculator {
    private final int rootIterations;
    private final int rootPrecision;
    private final int expansions;
    private final double periodRatio;

    public Calculator(int rootIterations, int rootPrecision, int expansions, double periodRatio) {
        this.rootIterations = rootIterations;
        this.rootPrecision = rootPrecision;
        this.expansions = expansions;
        this.periodRatio = periodRatio;
    }

    public Future<CalculatorTask> submitTask(long inputValue, ExecutorService executorService) {
        return executorService.submit(createTask(inputValue));
    }

    public Callable<CalculatorTask> createTask(long inputValue) {
        return () -> {
            BigDecimal sqrtValue = BasicMath.root(BigDecimal.valueOf(inputValue), rootIterations, 2, rootPrecision);

            long[] expansionIndexes = ContinuedFractionExpander.expand(sqrtValue, expansions);

            MemoryRecord memoryRecord = new MemoryRecord(
                    inputValue,
                    PeriodFinder.findLongestPeriod(expansionIndexes, periodRatio)
            );
            memoryRecord.setMatchingRatioChecked(periodRatio);

            return new CalculatorTask(memoryRecord,this);
        };
    }

    public int getRootIterations() {
        return rootIterations;
    }

    public int getRootPrecision() {
        return rootPrecision;
    }

    public int getExpansions() {
        return expansions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Calculator that = (Calculator) o;
        return rootIterations == that.rootIterations &&
                rootPrecision == that.rootPrecision &&
                expansions == that.expansions;
    }

    @Override
    public int hashCode() {
        return Objects.hash(rootIterations, rootPrecision, expansions);
    }

    @Override
    public String toString() {
        return "Calculator{" +
                "ROOT_ITERATIONS=" + rootIterations +
                ", ROOT_PRECISION=" + rootPrecision +
                ", EXPANSIONS=" + expansions +
                '}';
    }
}
