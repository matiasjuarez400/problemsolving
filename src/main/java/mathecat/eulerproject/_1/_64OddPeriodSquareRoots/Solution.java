package mathecat.eulerproject._1._64OddPeriodSquareRoots;

import java.io.IOException;
import java.util.List;

class Solution {
    private final MemoryManagerMongo memoryManager;
    private final MemoryConsistencyChecker memoryConsistencyChecker;
    private final MainProcessor mainProcessor;
    private final PostProcessor postProcessor;

    private static final int SEARCH_LIMIT = 10_000;
    private static final int TIMEOUT = 60;

    public static void main(String[] args) throws IOException {
        Solution solution = new Solution();
        int answer = solution.solve(SEARCH_LIMIT);

        System.out.println("Odd periods: " + answer);
    }

    public Solution() {
        this.memoryManager = new MemoryManagerMongo();
        this.memoryConsistencyChecker = new MemoryConsistencyChecker(memoryManager);
        this.mainProcessor = new MainProcessor(memoryManager, TIMEOUT);

        this.postProcessor = new PostProcessor(
                memoryManager,
                new MainProcessor(memoryManager, TIMEOUT),
                memoryConsistencyChecker
        );
    }

    public int solve(int searchLimit) throws IOException {
        try {
            List<MemoryRecord> storedResults = memoryConsistencyChecker.checkAndClean();
            mainProcessor.process(determineSearchInitialValue(), searchLimit);
            postProcessor.process(1, searchLimit);

            return (int) storedResults.stream()
                    .filter(record -> (record.getIndexes().length - 1) % 2 != 0)
                    .count();
        } finally {
            shutdown();
        }
    }

    private void shutdown() {
        this.mainProcessor.shutdown();
        this.postProcessor.shutdown();
    }

    private int determineSearchInitialValue() {
        List<MemoryRecord> storedResults = memoryManager.findAll();

        int startValue = 1;
        if (!storedResults.isEmpty()) {
            MemoryRecord lastRecord = storedResults.get(storedResults.size() - 1);
            startValue = (int) lastRecord.getBaseNumber() + 1;
        }

        return startValue;
    }
}
