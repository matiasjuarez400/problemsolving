package mathecat.eulerproject._1._64OddPeriodSquareRoots.unused;

import mathecat.eulerproject._1._64OddPeriodSquareRoots.MemoryManagerMongo;
import mathecat.eulerproject._1._64OddPeriodSquareRoots.MemoryRecord;
import mathecat.eulerproject.commons.fileUtils.SimpleFileWriter;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ExportMongoRecords {
    public static void main(String[] args) throws IOException {
        MemoryManagerMongo memoryManagerMongo = new MemoryManagerMongo();

        List<MemoryRecord> memoryRecords = memoryManagerMongo.findAll();

        List<String> rawRecords = memoryRecords.stream()
                .map(Objects::toString)
                .collect(Collectors.toList());

        SimpleFileWriter fileWriter = new SimpleFileWriter("src/main/java/mathecat/eulerproject/_1/_64OddPeriodSquareRoots/unused/results.txt");

        fileWriter.writeLines(rawRecords, false);
    }
}
