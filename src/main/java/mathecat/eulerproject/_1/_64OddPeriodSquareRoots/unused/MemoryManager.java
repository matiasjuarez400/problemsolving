package mathecat.eulerproject._1._64OddPeriodSquareRoots.unused;

import mathecat.eulerproject._1._64OddPeriodSquareRoots.CalculatorTask;
import mathecat.eulerproject._1._64OddPeriodSquareRoots.MemoryRecord;
import mathecat.eulerproject.commons.fileUtils.SimpleFileReader;
import mathecat.eulerproject.commons.fileUtils.SimpleFileWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class MemoryManager {
    private static final int WRITER_MEMORY_TRIGGER_SIZE = 5;

    private final List<String> writerMemory = new ArrayList<>();
    private final SimpleFileWriter simpleFileWriter;
    private final String memoryPath;

    public MemoryManager(String memoryPath) throws IOException {
        this.memoryPath = memoryPath;
        this.simpleFileWriter = new SimpleFileWriter(memoryPath);
    }

    public MemoryManager() throws IOException {
        this("src/main/java/mathecat/eulerproject/_1/_64OddPeriodSquareRoots/unused/results.txt");
    }

    public void writeToMemory(CalculatorTask calculatorTask) {
        String newRecord = createRawRecord(calculatorTask.getMemoryRecord());

        synchronized (MemoryManager.class) {
            writerMemory.add(newRecord);

            if (writerMemory.size() >= WRITER_MEMORY_TRIGGER_SIZE) {
                flush();
            }
        }
    }

    public void writeToMemory(List<MemoryRecord> memoryRecords, boolean append) {
        List<String> records = memoryRecords.stream()
                .map(this::createRawRecord)
                .collect(Collectors.toList());

        simpleFileWriter.writeLines(records, append);
    }

    public void flush() {
        simpleFileWriter.writeLines(writerMemory, true);
        writerMemory.clear();
    }

    public List<MemoryRecord> loadMemory() throws IOException {
        System.out.println("Loading memory...");

        SimpleFileReader sfr = new SimpleFileReader(memoryPath);

        return sfr.readAllLines().stream()
                .map(this::createRawRecord)
                .collect(Collectors.toList());
    }

    private MemoryRecord createRawRecord(String rawRecord) {
        String[] splittedRecord = rawRecord.split(",");

        long baseNumber = Long.parseLong(splittedRecord[0]);

        long[] indexes = new long[splittedRecord.length - 1];

        for (int i = 0; i < indexes.length; i++) {
            indexes[i] = Long.parseLong(splittedRecord[i + 1]);
        }

        return new MemoryRecord(baseNumber, indexes);
    }

    private String createRawRecord(MemoryRecord memoryRecord) {
        List<String> rawRecord = new ArrayList<>();

        rawRecord.add(Long.toString(memoryRecord.getBaseNumber()));
        rawRecord.add(Arrays.stream(memoryRecord.getIndexes()).boxed().map(Objects::toString).collect(Collectors.joining(",")));

        return String.join(",", rawRecord);
    }
}
