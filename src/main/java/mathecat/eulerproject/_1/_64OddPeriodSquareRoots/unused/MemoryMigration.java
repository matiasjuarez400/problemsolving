package mathecat.eulerproject._1._64OddPeriodSquareRoots.unused;

import mathecat.eulerproject._1._64OddPeriodSquareRoots.MemoryManagerMongo;
import mathecat.eulerproject._1._64OddPeriodSquareRoots.MemoryRecord;

import java.io.IOException;
import java.util.List;

public class MemoryMigration {
    public static void main(String[] args) throws IOException {
        MemoryManager fileManager = new MemoryManager();
        MemoryManagerMongo mongo = new MemoryManagerMongo();

        List<MemoryRecord> memoryRecords = fileManager.loadMemory();

        mongo.store(memoryRecords);

        List<MemoryRecord> mongoRecords = mongo.findAll();

        if (mongoRecords.size() != memoryRecords.size()) {
            throw new IllegalStateException("Something went wrong with the migration");
        }

        for (MemoryRecord memoryRecord : mongoRecords) {
            System.out.println(memoryRecord);
        }
    }
}
