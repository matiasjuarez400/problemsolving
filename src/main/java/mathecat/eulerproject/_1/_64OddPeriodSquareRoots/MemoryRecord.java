package mathecat.eulerproject._1._64OddPeriodSquareRoots;

import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

public class MemoryRecord {
    private static final String OBJECT_ID = "_id";
    private static final String BASE_NUMBER = "baseNumber";
    private static final String INDEXES = "indexes";
    private static final String MATCHING_RATIO_CHECKED = "matchingRatioChecked";

    private ObjectId objectId;
    private long baseNumber;
    private long[] indexes;
    private double matchingRatioChecked;

    public MemoryRecord(long baseNumber, long[] indexes) {
        this.baseNumber = baseNumber;
        this.indexes = indexes;
    }

    public MemoryRecord(MemoryRecord memoryRecord) {
        this.objectId = memoryRecord.objectId;
        this.baseNumber = memoryRecord.baseNumber;
        this.indexes = memoryRecord.indexes;
        this.matchingRatioChecked = memoryRecord.matchingRatioChecked;;
    }

    private MemoryRecord() {}

    public long getBaseNumber() {
        return baseNumber;
    }

    public long[] getIndexes() {
        return indexes;
    }

    public void setBaseNumber(long baseNumber) {
        this.baseNumber = baseNumber;
    }

    public void setIndexes(long[] indexes) {
        this.indexes = indexes;
    }

    public double getMatchingRatioChecked() {
        return matchingRatioChecked;
    }

    public void setMatchingRatioChecked(double matchingRatioChecked) {
        this.matchingRatioChecked = matchingRatioChecked;
    }

    public ObjectId getObjectId() {
        return objectId;
    }

    public void setObjectId(ObjectId objectId) {
        this.objectId = objectId;
    }

    public Document toBsonDocument() {
        Document document = new Document();
        document.append(BASE_NUMBER, baseNumber);
        document.append(INDEXES, Arrays.stream(indexes).boxed().collect(Collectors.toList()));
        document.append(MATCHING_RATIO_CHECKED, matchingRatioChecked);

        if (Objects.nonNull(objectId)) {
            document.append(OBJECT_ID, objectId);
        }

        return document;
    }

    public static MemoryRecord from(Document document) {
        MemoryRecord memoryRecord = new MemoryRecord();
        memoryRecord.setObjectId(document.getObjectId(OBJECT_ID));
        memoryRecord.setBaseNumber(document.get(BASE_NUMBER, Long.class));
        memoryRecord.setIndexes(document.getList(INDEXES, Long.class).stream().mapToLong(l -> l).toArray());
        memoryRecord.setMatchingRatioChecked(document.get(MATCHING_RATIO_CHECKED, Double.class));

        return memoryRecord;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MemoryRecord that = (MemoryRecord) o;
        return baseNumber == that.baseNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(baseNumber);
    }

    @Override
    public String toString() {
        return "MemoryRecord{" +
                "objectId=" + objectId +
                ", baseNumber=" + baseNumber +
                ", indexes=" + Arrays.toString(indexes) +
                ", matchingRatioChecked=" + matchingRatioChecked +
                '}';
    }
}
