package mathecat.eulerproject._1._64OddPeriodSquareRoots;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MainProcessor {
    private final MemoryManagerMongo memoryManager;
    private final int calculationTimeout;
    private final ExecutorService executorService;
    private final CalculatorManager calculatorManager;
    private final Map<Long, String> failedCalculationsByBase;

    public MainProcessor(MemoryManagerMongo memoryManager, int calculationTimeout) {
        this.memoryManager = memoryManager;
        this.calculationTimeout = calculationTimeout;
        this.executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        this.calculatorManager = new CalculatorManager(executorService);
        this.failedCalculationsByBase = new HashMap<>();
    }

    public void process(int searchStart, int searchLimit) {
        if (searchStart == searchLimit) return;

        List<Integer> numbers = IntStream
                .rangeClosed(searchStart, searchLimit)
                .boxed()
                .collect(Collectors.toList());

        process(numbers);
    }

    public void process(List<Integer> numbers) {
        if (numbers.isEmpty()) return;

        System.out.printf("Starting fraction expansions for %s numbers\n", numbers.size());

        int availableCores = Runtime.getRuntime().availableProcessors();

        List<Future<CalculatorTask>> futureList = new ArrayList<>();
        for (int i = 0; i < numbers.size();) {
            futureList.clear();

            while (futureList.size() < availableCores && i < numbers.size()) {
                int remainingValues = numbers.size() - i - 1;
                if (remainingValues % 50 == 0) {
                    System.out.println("Remaining values: " + remainingValues);
                }

                Integer nextNumber = numbers.get(i);

                if (!isSkippable(nextNumber)) {
                    futureList.add(calculatorManager.submitTask(nextNumber));
                }

                i++;
            }

            try {
                processFutureResults(futureList);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        if (!failedCalculationsByBase.isEmpty()) {
            System.out.printf("A total of %s tasks failed: \n", failedCalculationsByBase.size());

            for (Long key : failedCalculationsByBase.keySet()) {
                System.out.printf("%s: %s\n", key, failedCalculationsByBase.get(key));
            }
        }
    }

    public boolean isSkippable(int value) {
        double sqrt = Math.sqrt(value);

        return sqrt % 1 == 0;
    }

    public void shutdown() {
        this.executorService.shutdown();
    }

    private void processFutureResults(List<Future<CalculatorTask>> futureList)
            throws InterruptedException, ExecutionException, TimeoutException {
        if (futureList.isEmpty()) return;

        Map<Long, CalculatorTask> failedTasksByInputValue = new HashMap<>();

        for (Future<CalculatorTask> future : futureList) {
            CalculatorTask calculatorTask = future.get(calculationTimeout, TimeUnit.SECONDS);

            long[] longestPeriod = calculatorTask.getMemoryRecord().getIndexes();
            long input = calculatorTask.getMemoryRecord().getBaseNumber();

            if (longestPeriod.length == 0) {
                System.out.printf("Calculation for value %s failed using calculator: %s\n", input, calculatorTask.getCalculator());

                if (failedTasksByInputValue.containsKey(input)) {
                    Calculator lastUsedCalculator = failedTasksByInputValue.get(input).getCalculator();
                    Calculator currentCalculator = calculatorTask.getCalculator();

                    if (currentCalculator.getRootPrecision() > lastUsedCalculator.getRootPrecision()) {
                        failedTasksByInputValue.put(input, calculatorTask);
                    }
                } else {
                    failedTasksByInputValue.put(input, calculatorTask);
                }
            } else {
                memoryManager.store(calculatorTask.getMemoryRecord());
            }
        }

        List<CalculatorTask> resubmittedTasks = resubmitTasks(failedTasksByInputValue);
        for (CalculatorTask resubmittedTask : resubmittedTasks) {
            memoryManager.store(resubmittedTask.getMemoryRecord());
        }
    }

    private List<CalculatorTask> resubmitTasks(Map<Long, CalculatorTask> failedTasksByInputValue) throws InterruptedException, ExecutionException, TimeoutException {
        List<CalculatorTask> results = new ArrayList<>();

        for (Long failedInput : failedTasksByInputValue.keySet()) {
            CalculatorTask failedTask = failedTasksByInputValue.get(failedInput);

            try {
                results.add(doResubmitTasks(failedTask));
            } catch (Exception e) {
                failedCalculationsByBase.put(failedInput, e.getMessage());
            }
        }

        return results;
    }

    private CalculatorTask doResubmitTasks(CalculatorTask failedTask) throws InterruptedException, ExecutionException, TimeoutException {
        List<Future<CalculatorTask>> resubmittedTasks = new ArrayList<>();

        resubmittedTasks.addAll(calculatorManager.resubmitTask(failedTask, true));

        CalculatorTask taskToResubmit = null;

        for (Future<CalculatorTask> future : resubmittedTasks) {
            CalculatorTask currentTask = future.get(calculationTimeout, TimeUnit.SECONDS);

            long[] longestPeriod = currentTask.getMemoryRecord().getIndexes();

            if (longestPeriod.length == 0) {
                System.out.printf("Calculation for value %s failed using calculator: %s\n",
                        currentTask.getMemoryRecord().getBaseNumber(), currentTask.getCalculator());

                if (taskToResubmit == null) {
                    taskToResubmit = currentTask;
                } else if (currentTask.getCalculator().getRootPrecision() > taskToResubmit.getCalculator().getRootPrecision()) {
                    taskToResubmit = currentTask;
                }
            } else {
                return currentTask;
            }
        }

        return doResubmitTasks(taskToResubmit);
    }
}
