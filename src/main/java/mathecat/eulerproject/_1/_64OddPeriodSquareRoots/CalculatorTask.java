package mathecat.eulerproject._1._64OddPeriodSquareRoots;

import java.util.Objects;

public class CalculatorTask {
    private final MemoryRecord memoryRecord;
    private final Calculator calculator;

    public CalculatorTask(MemoryRecord memoryRecord, Calculator calculator) {
        this.memoryRecord = memoryRecord;
        this.calculator = calculator;
    }

    public MemoryRecord getMemoryRecord() {
        return memoryRecord;
    }

    public Calculator getCalculator() {
        return calculator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalculatorTask that = (CalculatorTask) o;
        return this.getMemoryRecord().getBaseNumber() == that.getMemoryRecord().getBaseNumber();
    }

    @Override
    public int hashCode() {
        return Objects.hash(memoryRecord.getBaseNumber());
    }
}
