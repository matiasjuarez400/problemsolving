package mathecat.eulerproject._1._64OddPeriodSquareRoots;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class PostProcessor {
    private final MemoryManagerMongo memoryManager;
    private final MainProcessor mainProcessor;
    private final MemoryConsistencyChecker memoryConsistencyChecker;
    private static final int SHORT_PERIOD_LENGTH = 15;

    public PostProcessor(MemoryManagerMongo memoryManager, MainProcessor mainProcessor, MemoryConsistencyChecker memoryConsistencyChecker) {
        this.memoryManager = memoryManager;
        this.mainProcessor = mainProcessor;
        this.memoryConsistencyChecker = memoryConsistencyChecker;
    }

    public void process(int startValue, int endValue) {
        System.out.println("Starting memory check...");

        fillMissingValues(startValue, endValue);
        fixWrongPeriods();
        rerunShortPeriods();
        memoryConsistencyChecker.checkAndClean();
    }

    private void rerunShortPeriods() {
        List<MemoryRecord> memoryRecords = memoryManager.findAll();

        List<Integer> reprocessList = memoryRecords.stream()
                .filter(memoryRecord -> memoryRecord.getIndexes().length <= SHORT_PERIOD_LENGTH)
                .filter(memoryRecord -> memoryRecord.getMatchingRatioChecked() <= 0.01)
                .map(MemoryRecord::getBaseNumber)
                .map(Long::intValue)
                .collect(Collectors.toList());

        if (reprocessList.isEmpty()) {
            System.out.println("NO SHORT PERIOD TO PROCESS!!");
        } else {
            System.out.println("Short periods to fix: " + reprocessList.size());
            mainProcessor.process(reprocessList);
        }
    }

    private void fixWrongPeriods() {
        fixRepeatedPeriods(false);
        memoryConsistencyChecker.checkAndClean();
        fixRepeatedPeriods(true);
    }

    private void fixRepeatedPeriods(boolean failOnRepeated) {
        List<MemoryRecord> storedResults = memoryManager.findAll();

        Map<String, List<Long>> basesWithSameIndexes = new HashMap<>();
        storedResults.forEach(memoryRecord -> {
            String key = Arrays.stream(memoryRecord.getIndexes())
                    .boxed()
                    .map(Objects::toString)
                    .collect(Collectors.joining(","));

            List<Long> valuesForKey = basesWithSameIndexes.computeIfAbsent(key, k -> new ArrayList<>());
            valuesForKey.add(memoryRecord.getBaseNumber());
        });

        StringBuilder sb = new StringBuilder();
        List<Integer> valuesToFix = new ArrayList<>();

        for (String key : basesWithSameIndexes.keySet()) {
            List<Long> values = basesWithSameIndexes.get(key);

            if (values.size() <= 1) continue;

            valuesToFix.addAll(values.stream().map(Long::intValue).collect(Collectors.toList()));

            sb.append(String.format("Same period [%s] for values [%s]", key,
                    values.stream()
                            .map(Objects::toString)
                            .collect(Collectors.joining(", "))));

            sb.append("\n");
        }

        if (sb.length() != 0) {
            if (failOnRepeated) {
                throw new IllegalStateException(sb.toString());
            } else {
                System.out.printf("Trying to fix periods for values with same period: [%s]\n",
                        valuesToFix.stream().map(Objects::toString).collect(Collectors.joining(", "))
                );

                mainProcessor.process(valuesToFix);
            }
        }
    }

    private void fillMissingValues(int startValue, int endValue) {
        List<MemoryRecord> storedResults = memoryManager.findAll();
        storedResults.sort(Comparator.comparing(MemoryRecord::getBaseNumber));

        List<Integer> missingValues = new ArrayList<>();
        for (int i = startValue, j = 0; i <= endValue; i++) {
            if (mainProcessor.isSkippable(i)) continue;

            MemoryRecord nextRecord = storedResults.get(j);

            if (nextRecord.getBaseNumber() == i) {
                j++;
            } else{
                System.out.println("Missing value found: " + i);
                missingValues.add(i);
            }
        }

        if (missingValues.isEmpty()) {
            System.out.println("No inconsistencies found!");
        } else {
            mainProcessor.process(missingValues);
        }
    }

    public void shutdown() {
        this.mainProcessor.shutdown();
    }
}
