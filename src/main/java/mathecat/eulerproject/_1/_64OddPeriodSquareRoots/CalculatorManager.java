package mathecat.eulerproject._1._64OddPeriodSquareRoots;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class CalculatorManager {
    private final List<Calculator> calculators = new ArrayList<>();

    private static final int ROOT_ITERATIONS_STEP = 20000;
    private static final int ROOT_PRECISION_STEP = 200;
    private static final int EXPANSIONS_STEP = 200;

    private static final int DEFAULT_MAX_CALCULATORS = 9;
    private static final double DEFAULT_PERIOD_RATIO = 0.15;

    private final ExecutorService executorService;

    public CalculatorManager(ExecutorService executorService) {
        this(executorService, 130000, 800, 801, DEFAULT_MAX_CALCULATORS);
    }

    public CalculatorManager(ExecutorService executorService, int rootIterations, int rootPrecision, int expansions, int maxCalculations) {
        this.executorService = executorService;

        for (int i = 0; i < maxCalculations; i++) {
            this.calculators.add(new Calculator(
                    rootIterations + ROOT_ITERATIONS_STEP * i,
                    rootPrecision + ROOT_PRECISION_STEP * i,
                    expansions + EXPANSIONS_STEP * i,
                    DEFAULT_PERIOD_RATIO
                    )
            );
        }
    }

    public Future<CalculatorTask> submitTask(int input) {
        Calculator calculator = this.calculators.get(0);

        System.out.printf("Task submission for value %s\n", input);

        return calculator.submitTask(input, executorService);
    }

    public Future<CalculatorTask> resubmitTask(CalculatorTask task) {
        Calculator nextCalculator = getNextCalculator(task.getCalculator());

        long baseValue = task.getMemoryRecord().getBaseNumber();

        System.out.printf("Task resubmission for value %s to %s\n", baseValue, nextCalculator);

        return nextCalculator.submitTask(baseValue, executorService);
    }

    public List<Future<CalculatorTask>> resubmitTask(CalculatorTask task, boolean useMultipleCalculators) {
        long baseValue = task.getMemoryRecord().getBaseNumber();

        int howManyCalculators = useMultipleCalculators ? (this.calculators.size() / 3) : 1;

        List<Calculator> calculatorsToUse = getNextCalculators(task.getCalculator(), howManyCalculators);

        System.out.printf("Calculation for %s submitted to %s calculators\n", baseValue, calculatorsToUse.size());

        List<Future<CalculatorTask>> resubmittedTasks = new ArrayList<>();

        for (Calculator calculator : calculatorsToUse) {
            System.out.printf("Task resubmission for value %s to %s\n", baseValue, calculator);

            resubmittedTasks.add(calculator.submitTask(baseValue, executorService));
        }

        return resubmittedTasks;
    }

    private Calculator getNextCalculator(Calculator current) {
        return getNextCalculators(current, 1).get(0);
    }

    private List<Calculator> getNextCalculators(Calculator current, int howMany) {
        int currentIndex = calculators.indexOf(current);

        if (currentIndex < 0) {
            throw new IllegalArgumentException("Calculator " + current + " is not present in the calculators list");
        }

        int nextIndex = currentIndex + 1;

        if (nextIndex >= calculators.size()) {
            throw new IllegalStateException("All calculators have been used!");
        }

        return calculators.subList(nextIndex, Integer.min(calculators.size(), nextIndex + howMany));
    }
}
