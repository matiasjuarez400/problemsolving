package mathecat.eulerproject._1._64OddPeriodSquareRoots;

import mathecat.eulerproject._1._64OddPeriodSquareRoots.unused.MemoryManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MemoryConsistencyChecker {
    private final MemoryManagerMongo memoryManager;

    public MemoryConsistencyChecker(MemoryManagerMongo memoryManager) {
        this.memoryManager = memoryManager;
    }

    public List<MemoryRecord> checkAndClean() {
        List<MemoryRecord> records = memoryManager.findAll();
        records = removeDuplicates(records);
        records.sort(Comparator.comparing(MemoryRecord::getBaseNumber));
        memoryManager.deleteAll();
        memoryManager.store(records);
        return records;
    }

    private List<MemoryRecord> removeDuplicates(List<MemoryRecord> storedRecords) {
        Map<Long, List<MemoryRecord>> recordsByBaseNumber = storedRecords.stream()
                .collect(Collectors.groupingBy(MemoryRecord::getBaseNumber));

        List<MemoryRecord> differentRecords = new ArrayList<>();

        for (Long key : recordsByBaseNumber.keySet()) {
            List<MemoryRecord> recordsForKey = recordsByBaseNumber.get(key);

            MemoryRecord longestRecord = recordsForKey.get(0);

            for (int i = 1; i < recordsForKey.size(); i++) {
                MemoryRecord nextRecord = recordsForKey.get(i);

                if (nextRecord.getIndexes().length > longestRecord.getIndexes().length) {
                    longestRecord = nextRecord;
                } else if (nextRecord.getIndexes().length == longestRecord.getIndexes().length) {
                    for (int j = 0; j < nextRecord.getIndexes().length; j++) {
                        if (longestRecord.getIndexes()[j] != nextRecord.getIndexes()[j]) {
                            String msg = String.format("The following two records for the base number %s have the same length %s but different indexes: \n" +
                                    "Record 1: %s\n" +
                                    "Record 2: %s\n",
                                    longestRecord.getBaseNumber(),
                                    longestRecord.getIndexes().length,
                                    longestRecord,
                                    nextRecord
                                    );

                            throw new IllegalArgumentException(msg);
                        }
                    }
                }
            }

            differentRecords.add(longestRecord);
        }

        return differentRecords;
    }
}
