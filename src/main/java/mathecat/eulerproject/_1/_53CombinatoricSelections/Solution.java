package mathecat.eulerproject._1._53CombinatoricSelections;

import mathecat.eulerproject.commons.combination.CombinationUtils;

import java.math.BigInteger;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.solve());
    }

    public long solve() {
        long count = 0;
        BigInteger bigMillion = BigInteger.valueOf(1_000_000);

        for (int n = 1; n <= 100; n++) {
            BigInteger maxCombinatorics = CombinationUtils.maxCombinatorics(n);

            if (maxCombinatorics.compareTo(bigMillion) < 0) continue;

            long acc = 0;
            for (int k = n / 2; CombinationUtils.combinatorics(n, k).compareTo(bigMillion) > 0; k--) {
                acc++;
            }

            if (n % 2 == 0) {
                count += acc * 2;
            } else {
                count += acc * 2 - 1;
            }
        }

        return count;
    }
}
