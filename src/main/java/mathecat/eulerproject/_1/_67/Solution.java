package mathecat.eulerproject._1._67;

import mathecat.eulerproject.commons.fileUtils.SimpleFileReader;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class Solution {
    public static void main(String[] args) {
        final Solution solution = new Solution();

        final int result = solution.solve(Paths.get("src/main/java/mathecat/eulerproject/_1/_67/p067_triangle.txt"));

        System.out.println(result);
    }

    public int solve(final Path inputFile) {
        final int[][] triangle = loadTriangle(inputFile);

        final int[][] accumulationTriangle = new int[triangle.length][];
        accumulationTriangle[accumulationTriangle.length - 1] = triangle[triangle.length - 1];

        for (int row = triangle.length - 2; row >= 0; row--) {
            accumulationTriangle[row] = new int[triangle[row].length];

            for (int col = 0; col < triangle[row].length; col++) {
                final int currentValue = triangle[row][col];
                final int leftAccumulated = accumulationTriangle[row + 1][col];
                final int rightAccumulated = accumulationTriangle[row + 1][col + 1];

                accumulationTriangle[row][col] = Integer.max(currentValue + leftAccumulated, currentValue + rightAccumulated);
            }
        }

        return accumulationTriangle[0][0];
    }

    private int[][] loadTriangle(final Path inputFile) {
        final SimpleFileReader simpleFileReader = new SimpleFileReader(inputFile);

        final String[] lines = simpleFileReader.readAllLines().toArray(new String[0]);

        final int[][] triangle = new int[lines.length][];

        for (int row = 0; row < lines.length; row++) {
            final int[] rowElements = Arrays.stream(lines[row].split(" "))
                    .map(Integer::parseInt)
                    .mapToInt(i -> i)
                    .toArray();

            triangle[row] = rowElements;
        }

        return triangle;
    }
}
