package mathecat.eulerproject._1._50ConsecutivePrimeSum;

import mathecat.eulerproject.commons.primes.PrimeUtilsWithMemory;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Solution {
    private PrimeUtilsWithMemory primeUtilsWithMemory;

    public static void main(String[] args) {
        solveAndPrint(100);
        solveAndPrint(1000);
        solveAndPrint(1_000_000);
    }

    private static void solveAndPrint(long searchLimit) {
        Solution solution = new Solution(searchLimit);
        Result result = solution.doSolve(searchLimit);

        System.out.println(result);
    }

    public Solution(long searchLimit) {
        this.primeUtilsWithMemory = new PrimeUtilsWithMemory(searchLimit);
    }

    public long solve(long searchLimit) {
        return doSolve(searchLimit).getMaxAccumulated();
    }

    private Result doSolve(long searchLimit) {
        List<Long> availablePrimes = primeUtilsWithMemory.getPrimesInRange(0, searchLimit);

        if (availablePrimes.isEmpty()) return Result.createDefault();

        long[] sums = new long[availablePrimes.size()];
        sums[0] = availablePrimes.get(0);

        for (int i = 1; i < sums.length; i++) {
            sums[i] = sums[i - 1] + availablePrimes.get(i);
        }

        long maxAccumulated = 0;
        int startIndex = -1;
        int endIndex = -1;

        if (sums[0] <= searchLimit && primeUtilsWithMemory.isPrime(sums[0])) {
            maxAccumulated = sums[0];
            startIndex = endIndex = 0;
        }

        for (int i = 1; i < sums.length; i++) {
            long currentSum = sums[i];
            if (currentSum <= searchLimit && primeUtilsWithMemory.isPrime(currentSum) && currentSum > maxAccumulated) {
                   maxAccumulated = currentSum;
                   startIndex = 0;
                   endIndex = i;
            }

            // Only consider element at j if the distance between i and j is greater than the current startIndex and endIndex distance.
            // startIndex and endIndex represent the amount of primes used to build the current max accumulated value
            for (int j = 0; j < i && (i - j) > (endIndex - startIndex); j++) {
                long sumFromJ = currentSum - sums[j];
                if (sumFromJ <= searchLimit && primeUtilsWithMemory.isPrime(sumFromJ) && sumFromJ > maxAccumulated) {
                    maxAccumulated = sumFromJ;
                    startIndex = j + 1;
                    endIndex = i;
                }
            }
        }

        return new Result(maxAccumulated, startIndex, endIndex, availablePrimes);
    }

    private static class Result {
        private final long maxAccumulated;
        private final int startIndex;
        private final int endIndex;
        private final List<Long> availablePrimes;

        public Result(long maxAccumulated, int startIndex, int endIndex, List<Long> availablePrimes) {
            this.maxAccumulated = maxAccumulated;
            this.startIndex = startIndex;
            this.endIndex = endIndex;
            this.availablePrimes = availablePrimes;
        }

        public static Result createDefault() {
            return new Result(0, -1, -1, Collections.emptyList());
        }

        public long getMaxAccumulated() {
            return maxAccumulated;
        }

        public int getStartIndex() {
            return startIndex;
        }

        public int getEndIndex() {
            return endIndex;
        }

        @Override
        public String toString() {
            return String.format("Result{Accumulated: %s. StartIndex: %s. EndIndex: %s. Total elements: %s}\n" +
                    "Used elements: [%s]", maxAccumulated, startIndex, endIndex, endIndex - startIndex + 1,
                    availablePrimes.subList(startIndex, endIndex + 1).stream()
                            .map(Objects::toString).collect(Collectors.joining(", ")));
        }
    }
}
