package mathecat.eulerproject._1._63PowerfulDigitCounts;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        Map<Integer, List<Data>> result = solution.solve();

        int amount = 0;

        for (Integer size : result.keySet()) {
            amount += result.get(size).size();
        }

        System.out.println("Solution found: " + amount);
    }

    public Map<Integer, List<Data>> solve() {
        Map<Integer, List<Data>> map = new HashMap<>();
        map.computeIfAbsent(1, k -> {
            List<Data> list = new ArrayList<>();
            list.add(new Data(1, 1, BigInteger.ONE));
            return list;
        });

        int nextPower = 0;

        boolean stopExploration = false;
        while (!stopExploration) {
            nextPower++;

            /*
             * Whenever you calculate 10 to the power of N, regardless of the value of N, you will get a number of size N+1.
             * That's why 10 is the limit.
             */
            for (int base = 2; base < 10; base++) {
                BigInteger powerResult = BigInteger.valueOf(base).pow(nextPower);
                int numberSize = powerResult.toString().length();

                if (numberSize == nextPower) {
                    List<Data> numbersForSize = map.computeIfAbsent(numberSize, k -> new ArrayList<>());
                    numbersForSize.add(new Data(base, nextPower, powerResult));
                }
            }

            /*
             * If this happens, it means that bases from 2 to 9 to the power of N are unable to reach a number with digits N (nextPower)
             */
            if (!map.containsKey(nextPower)) {
                stopExploration = true;
            }
        }

        return map;
    }

    private static class Data {
        private final long base;
        private final int power;
        private final BigInteger value;

        public Data(long base, int power, BigInteger value) {
            this.base = base;
            this.power = power;
            this.value = value;
        }

        public long getBase() {
            return base;
        }

        public int getPower() {
            return power;
        }

        public BigInteger getValue() {
            return value;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "base=" + base +
                    ", power=" + power +
                    ", value=" + value +
                    '}';
        }
    }
}
