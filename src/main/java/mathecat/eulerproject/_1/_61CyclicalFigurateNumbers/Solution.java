package mathecat.eulerproject._1._61CyclicalFigurateNumbers;

import mathecat.eulerproject.commons.BasicMath;
import mathecat.eulerproject.commons.NumberUtils;
import mathecat.eulerproject.commons.memory.RangeMemory;
import mathecat.eulerproject.commons.permutation.PermutationIterator;
import mathecat.eulerproject.commons.series.Generator;
import mathecat.eulerproject.commons.series.HeptagonalGenerator;
import mathecat.eulerproject.commons.series.HexagonalGenerator;
import mathecat.eulerproject.commons.series.OctagonalGenerator;
import mathecat.eulerproject.commons.series.PentagonalGenerator;
import mathecat.eulerproject.commons.series.SquareGenerator;
import mathecat.eulerproject.commons.series.TriangularGenerator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.solve(4, 6));
    }

    public long solve(int digits, int polygonals) {
        List<Generator> generators = Arrays.asList(
                new TriangularGenerator(),
                new SquareGenerator(),
                new PentagonalGenerator(),
                new HexagonalGenerator(),
                new HeptagonalGenerator(),
                new OctagonalGenerator()
        ).subList(0, polygonals);

        List<MemoryWrapper> memories = generators.stream()
                .map(generator -> createRangeMemory(generator, digits))
                .collect(Collectors.toList());

        PermutationIterator<MemoryWrapper> iterator = new PermutationIterator<>(memories, memories.size(), false);

        while (iterator.hasNext()) {
            List<MemoryWrapper> nextPermutation = iterator.next();

            List<Long> chain = createChain(nextPermutation);

            if (!chain.isEmpty()) {
                return chain.stream().mapToLong(l -> l).sum();
            }
        }

        return -1;
    }

    private List<Long> createChain(List<MemoryWrapper> permutation) {
        return tryCreateChain(permutation, new ArrayList<>());
    }

    private List<Long> tryCreateChain(List<MemoryWrapper> permutation, List<Long> currentChain) {
        if (currentChain.size() == permutation.size()) return currentChain;

        MemoryWrapper memoryToUse = permutation.get(currentChain.size());

        if (currentChain.isEmpty()) {
            for (Long element : memoryToUse.getRangeMemory().getElements()) {
                currentChain.add(element);
                tryCreateChain(permutation, currentChain);

                if (currentChain.size() == permutation.size()) {
                    return currentChain;
                }

                currentChain.remove(element);
            }
        } else {
            Long lastElement = currentChain.get(currentChain.size() - 1);

            int firstDigit = NumberUtils.getDigit(lastElement, 0);
            int secondDigit = NumberUtils.getDigit(lastElement, 1);
            long concatDigits = NumberUtils.concatenate(secondDigit, firstDigit);

            for (Long element : memoryToUse.getRangeMemory().getElementsForRange(concatDigits)) {
                currentChain.add(element);
                tryCreateChain(permutation, currentChain);

                if (currentChain.size() == permutation.size()) {
                    // Check if the first element of the chain is cyclical with the last element of the chain
                    Long firstNumber = currentChain.get(0);
                    Long lastNumber = currentChain.get(currentChain.size() - 1);

                    if (NumberUtils.getDigit(firstNumber, NumberUtils.size(firstNumber) - 1) == NumberUtils.getDigit(lastNumber, 1) &&
                        NumberUtils.getDigit(firstNumber, NumberUtils.size(firstNumber) - 2) == NumberUtils.getDigit(lastNumber, 0)
                    )
                    return currentChain;
                }

                currentChain.remove(element);
            }
        }

        return Collections.emptyList();
    }

    private MemoryWrapper createRangeMemory(Generator generator, int digits) {
        long startValue = BasicMath.pow(10, digits - 1);
        long endValue = startValue * 10 - 1;
        long rangeSize = startValue / 10;

        RangeMemory rangeMemory = new RangeMemory(rangeSize);

        List<Long> generatedValues = generator.generateValuesForRange(startValue, endValue);

        rangeMemory.addAll(generatedValues);

        return new MemoryWrapper(rangeMemory, generator);
    }
}
