package mathecat.eulerproject._1._61CyclicalFigurateNumbers;

import mathecat.eulerproject.commons.memory.RangeMemory;
import mathecat.eulerproject.commons.series.Generator;

class MemoryWrapper {
    private final RangeMemory rangeMemory;
    private final Generator generator;

    public MemoryWrapper(RangeMemory rangeMemory, Generator generator) {
        this.rangeMemory = rangeMemory;
        this.generator = generator;
    }

    public RangeMemory getRangeMemory() {
        return rangeMemory;
    }

    @Override
    public String toString() {
        return generator.getClass().getSimpleName();
    }
}
