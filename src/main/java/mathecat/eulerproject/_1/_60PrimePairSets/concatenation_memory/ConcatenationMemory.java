package mathecat.eulerproject._1._60PrimePairSets.concatenation_memory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ConcatenationMemory {
    private final Map<Long, ConcatenationMemorySlot> memorySlots;

    public static ConcatenationMemory fromMemory() throws IOException {
        return fromMemory(Integer.MAX_VALUE);
    }

    public static ConcatenationMemory fromMemory(int desiredPrimes) throws IOException {
        return PersistenceManager.loadMemory(desiredPrimes);
    }

    public ConcatenationMemory() {
        this.memorySlots = new HashMap<>();
    }

    public ConcatenationMemorySlot getMemorySlot(long prime) {
        return memorySlots.get(prime);
    }

    public ConcatenationMemorySlot getAndInsertMemorySlot(long prime) {
        return memorySlots.computeIfAbsent(prime, ConcatenationMemorySlot::new);
    }

    public void addConcatenable(long prime, long newConcatenable) {
        ConcatenationMemorySlot concatenationMemory = getAndInsertMemorySlot(prime);
        concatenationMemory.addConcatenablePrime(newConcatenable);
    }

    public Set<Long> getPrimesInMemory() {
        return Collections.unmodifiableSet(memorySlots.keySet());
    }

    public List<Long> getPrimesInMemoryAsList() {
        return Collections.unmodifiableList(new ArrayList<>(memorySlots.keySet()));
    }

    public List<ConcatenationMemorySlot> getMemorySlots() {
        return Collections.unmodifiableList(new ArrayList<>(memorySlots.values()));
    }

    public void save() throws IOException {
        PersistenceManager.saveMemory(this);
    }

    public int size() {
        return memorySlots.size();
    }
}
