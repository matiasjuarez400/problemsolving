package mathecat.eulerproject._1._60PrimePairSets.concatenation_memory;

import mathecat.eulerproject.commons.fileUtils.SimpleFileReader;
import mathecat.eulerproject.commons.fileUtils.SimpleFileWriter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PersistenceManager {
    private static final String FILES_FOLDER_PATH = "src/main/java/mathecat/eulerproject/_1/_60PrimePairSets/concatenation_memory/files";

    public static ConcatenationMemory loadMemory(int desiredPrimes) throws IOException {
        ConcatenationMemory memory = new ConcatenationMemory();

        try (Stream<Path> paths = Files.walk(Paths.get(FILES_FOLDER_PATH))) {
            List<Path> files = paths.collect(Collectors.toList());

            for (Path file : files) {
                if (Files.isDirectory(file)) continue;

                if (memory.size() >= desiredPrimes) break;

                SimpleFileReader simpleFileReader = new SimpleFileReader(file);
                List<String> lines = simpleFileReader.readAllLines();

                long basePrime = Long.parseLong(lines.get(1));
                for (int i = 2; i < lines.size(); i++) {
                    long storedPrime = Long.parseLong(lines.get(i));
                    memory.addConcatenable(basePrime, storedPrime);
                }
            }

        }

        return memory;
    }

    public static void saveMemory(ConcatenationMemory memory) throws IOException {
        for (ConcatenationMemorySlot slot : memory.getMemorySlots()) {
            Path slotPath = Paths.get(FILES_FOLDER_PATH, Long.toString(slot.getPrime()) + ".txt");
            SimpleFileWriter simpleFileWriter = new SimpleFileWriter(slotPath);

            if (Files.exists(slotPath)) {
                SimpleFileReader reader = new SimpleFileReader(slotPath);
                int storedPrimesCount = Integer.parseInt(reader.getFirstLine());

                if (storedPrimesCount >= slot.size()) continue;
            }

            List<String> slotInfo = new ArrayList<>();
            slotInfo.add(Integer.toString(slot.size()));
            slotInfo.add(Long.toString(slot.getPrime()));

            for (Long concatPrime : slot.getOrderedConcatenablePrimes()) {
                slotInfo.add(Long.toString(concatPrime));
            }

            simpleFileWriter.writeLines(slotInfo);
        }
    }
}
