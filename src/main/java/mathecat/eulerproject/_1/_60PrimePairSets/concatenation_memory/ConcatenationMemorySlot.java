package mathecat.eulerproject._1._60PrimePairSets.concatenation_memory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class ConcatenationMemorySlot {
    private final long prime;
    private final List<Long> orderedConcatenablePrimes;
    private final Set<Long> knownConcatenablePrimes;

    public ConcatenationMemorySlot(long prime) {
        this.prime = prime;
        this.orderedConcatenablePrimes = new ArrayList<>();
        this.knownConcatenablePrimes = new HashSet<>();
    }

    public void addConcatenablePrime(long prime) {
        if (knownConcatenablePrimes.contains(prime)) return;

        orderedConcatenablePrimes.add(prime);
        knownConcatenablePrimes.add(prime);
    }

    public List<Long> getOrderedConcatenablePrimes() {
        return Collections.unmodifiableList(orderedConcatenablePrimes);
    }

    public long getPrime() {
        return prime;
    }

    public long getBiggestKnownConcatenable() {
        if (orderedConcatenablePrimes.isEmpty()) return -1;

        return orderedConcatenablePrimes.get(orderedConcatenablePrimes.size() - 1);
    }

    public boolean isConcatenable(long value) {
        if (knownConcatenablePrimes.contains(value)) return true;

        if (value > getBiggestKnownConcatenable()) {
            String msg = String.format("The incoming value %s is greater than the last known concatenable %s", value, getBiggestKnownConcatenable());
            throw new IllegalArgumentException(msg);
        }

        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConcatenationMemorySlot that = (ConcatenationMemorySlot) o;
        return prime == that.prime;
    }

    @Override
    public int hashCode() {
        return Objects.hash(prime);
    }

    @Override
    public String toString() {
        return String.format("Prime %s. Known concatenable size: %s. Last known concatenable: %s",
                prime, knownConcatenablePrimes.size(), getBiggestKnownConcatenable());
    }

    public int size() {
        return this.knownConcatenablePrimes.size();
    }
}
