package mathecat.eulerproject._1._60PrimePairSets;

import mathecat.eulerproject._1._60PrimePairSets.concatenation_memory.ConcatenationMemory;
import mathecat.eulerproject._1._60PrimePairSets.concatenation_memory.ConcatenationMemorySlot;
import mathecat.eulerproject.commons.NumberUtils;
import mathecat.eulerproject.commons.primes.prime_calculator.PrimeCalculator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

class Solution {
    private final PrimeCalculator primeCalculator;
    private final int CONCATENABLE_PRIMES_PER_STEP = 10;
    private final int INITIALIZATION_ITERATIONS = 10;
    private final int TOTAL_BASE_PRIMES = 2000;
    private final int TOTAL_CONCATENABLE_PRIMES_PER_BASE = 100;

    public static void main(String[] args) throws IOException {
        Solution solution = new Solution();

        System.out.println("Found solution: " + solution.solve(5));
    }

    public Solution() throws IOException {
        this.primeCalculator = new PrimeCalculator(true);
    }

    public long solve(int familySize) throws IOException {
        ConcatenationMemory concatenationMemory = startUpMemory();

        int iteration = 0;

        while (iteration++ < INITIALIZATION_ITERATIONS) {
            System.out.print("Starting iteration " + iteration);
            long startTime = System.currentTimeMillis();
            runMemoryIncreaseIteration(concatenationMemory);
            long endTime = System.currentTimeMillis();
            System.out.printf(". Finished in %s seconds.\n", (endTime - startTime) / 1000.0);

            primeCalculator.savePrimes();
            concatenationMemory.save();
        }

        System.out.println("Starting concatenation chaining");
        Memory memory = buildConcatenationChain(familySize, concatenationMemory);

        if (memory.getChainsFound().isEmpty()) {
            String msg = "Unable to solve problem with following settings. FamilySize: " + familySize +
                    ". CONCATENABLE_PRIMES_PER_STEP: " + CONCATENABLE_PRIMES_PER_STEP +
                    ". INITIALIZATION_ITERATIONS: " + INITIALIZATION_ITERATIONS +
                    ". TOTAL_BASE_PRIMES: " + TOTAL_BASE_PRIMES +
                    ". TOTAL_CONCATENABLE_PRIMES_PER_BASE: " + TOTAL_CONCATENABLE_PRIMES_PER_BASE;
            throw new IllegalStateException(msg);
        } else {
            System.out.println("Min sum found: " + memory.getLowestSumFound());
            System.out.println("Min chain: " + getChainAsString(memory.getLowestChain()));

            System.out.println("Additional chains found: " + (memory.getChainsFound().size() - 1));
            for (List<Long> chain : memory.getChainsFound()) {
                if (chain.equals(memory.getLowestChain())) continue;

                System.out.println("Chain sum: " + getChainSum(chain));
                System.out.println("Chain members: " + getChainAsString(chain));
            }

            return memory.getLowestSumFound();
        }
    }

    private ConcatenationMemory startUpMemory() throws IOException {
        ConcatenationMemory concatenationMemory = ConcatenationMemory.fromMemory(TOTAL_BASE_PRIMES);

        if (concatenationMemory.size() < TOTAL_BASE_PRIMES) {
            long startPrimeSearch = concatenationMemory.getPrimesInMemory().stream()
                    .max(Long::compareTo).orElse(2L);

            while (concatenationMemory.size() < TOTAL_BASE_PRIMES) {
                long nextPrime = primeCalculator.nextPrime(startPrimeSearch);
                if (nextPrime == 5) {
                    startPrimeSearch = nextPrime;
                    continue;
                };

                concatenationMemory.getAndInsertMemorySlot(nextPrime);
                startPrimeSearch = nextPrime;
            }
        }

        return concatenationMemory;
    }

    private void runMemoryIncreaseIteration(ConcatenationMemory concatenationMemory) {
        for (ConcatenationMemorySlot slot : concatenationMemory.getMemorySlots()) {
            if (slot.size() >= TOTAL_CONCATENABLE_PRIMES_PER_BASE) continue;

            long searchStart = Long.max(slot.getBiggestKnownConcatenable(), slot.getPrime());

            for (int i = 0; i < CONCATENABLE_PRIMES_PER_STEP;) {
                long nextPrime = primeCalculator.nextPrime(searchStart);

                if (checkConcatenationPropertiesBothWays(slot.getPrime(), nextPrime)) {
                    slot.addConcatenablePrime(nextPrime);
                    if (slot.size() >= TOTAL_CONCATENABLE_PRIMES_PER_BASE) break;
                    i++;
                }

                searchStart = nextPrime;
            }
        }
    }

    private String getChainAsString(List<Long> chain) {
        return chain.stream().map(Objects::toString).collect(Collectors.joining(", "));
    }

    private long getChainSum(List<Long> chain) {
        return chain.stream().mapToLong(l -> l).sum();
    }

    private static class Memory {
        private final int familySize;
        private final List<List<Long>> chainsFound;
        private List<Long> lowestChain;
        private long lowestSumFound;
        private final ConcatenationMemory concatenationMemory;

        public Memory(int familySize, ConcatenationMemory concatenationMemory) {
            this.familySize = familySize;
            this.chainsFound = new ArrayList<>();
            this.lowestSumFound = Long.MAX_VALUE;
            this.concatenationMemory = concatenationMemory;
        }

        public int getFamilySize() {
            return familySize;
        }

        public List<List<Long>> getChainsFound() {
            return chainsFound;
        }

        public long getLowestSumFound() {
            return lowestSumFound;
        }

        public List<Long> getLowestChain() {
            return lowestChain;
        }

        public ConcatenationMemory getConcatenationMemory() {
            return concatenationMemory;
        }

        public void addNewChain(final List<Long> chain) {
            List<Long> localChain = new ArrayList<>(chain);
            chainsFound.add(localChain);

            long chainSum = localChain.stream().mapToLong(l -> l).sum();

            if (chainSum < lowestSumFound) {
                lowestSumFound = chainSum;
                lowestChain = localChain;
            }
        }
    }

    private Memory buildConcatenationChain(int familySize, ConcatenationMemory concatenationMemory) {
        Memory memory = new Memory(familySize, concatenationMemory);
        tryCreateConcatenationChain(concatenationMemory.getPrimesInMemoryAsList(), new ArrayList<>(), memory);

        return memory;
    }

    private void tryCreateConcatenationChain(List<Long> candidates, List<Long> concatenatedPrimes, Memory memory) {
        for (long candidate : candidates) {
            ConcatenationMemorySlot slot = memory.getConcatenationMemory().getMemorySlot(candidate);
            if (slot == null) continue;

            if (concatenatedPrimes.isEmpty() || checkPrimeConcatenationProperties(concatenatedPrimes, candidate)) {
                concatenatedPrimes.add(candidate);
                long sumConcatenated = getChainSum(concatenatedPrimes);
                // Remove this IF to allow multiple chains to be found
                if (sumConcatenated > memory.getLowestSumFound()) {
                    concatenatedPrimes.remove(concatenatedPrimes.size() - 1);
                    continue;
                }

                if (concatenatedPrimes.size() == memory.getFamilySize()) {
                    memory.addNewChain(concatenatedPrimes);
                } else {
                    tryCreateConcatenationChain(slot.getOrderedConcatenablePrimes(), concatenatedPrimes, memory);
                }

                concatenatedPrimes.remove(concatenatedPrimes.size() - 1);
            }
        }
    }

    private boolean checkConcatenationPropertiesBothWays(long left, long right) {
        long leftConcatenation = NumberUtils.concatenate(left, right);

        if (!primeCalculator.isPrime(leftConcatenation)) return false;

        long rightConcatenation = NumberUtils.concatenate(right, left);

        if (!primeCalculator.isPrime(rightConcatenation)) return false;

        return true;
    }

    private boolean checkPrimeConcatenationProperties(List<Long> existingPrimes, long newPrime) {
        for (long existingPrime : existingPrimes) {
            if (!checkConcatenationPropertiesBothWays(newPrime, existingPrime)) return false;
        }

        return true;
    }
}
