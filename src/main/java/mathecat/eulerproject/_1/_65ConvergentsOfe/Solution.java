package mathecat.eulerproject._1._65ConvergentsOfe;

import mathecat.eulerproject.commons.concepts.BigFraction;
import mathecat.eulerproject.commons.continuedFractions.ContinuedFractionExpander;
import mathecat.eulerproject.commons.continuedFractions.ConvergentCalculator;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println("Solution: " + solution.solve());
    }

    public BigInteger solve() {
        BigFraction e = getApproximationWithFactorial(1000);

        List<Long> expansionIndexes = Arrays
                .stream(ContinuedFractionExpander.expand(e, 101, 5000))
                .boxed().collect(Collectors.toList());

        List<BigFraction> convergentList = new ArrayList<>();
        convergentList.add(BigFraction.of(expansionIndexes.get(0)));
        for (int maxIndex = 1; maxIndex < expansionIndexes.size(); maxIndex++) {
            convergentList.add(ConvergentCalculator
                    .findConvergent(
                            expansionIndexes.get(0),
                            expansionIndexes.subList(1, maxIndex + 1)
                    )
            );
        }

        BigInteger numeratorsSum = BigInteger.ZERO;

        String numeratorDigits = convergentList.get(99).getNumerator().toString();
        for (char c : numeratorDigits.toCharArray()) {
            numeratorsSum = numeratorsSum.add(BigInteger.valueOf(Character.digit(c, 10)));
        }

        return numeratorsSum;
    }

    // This doesn't work
    private BigFraction getApproximationWithPowers(int n) {
        return BigFraction.ONE.add(BigFraction.of(1, n)).pow(n);
    }

    private BigFraction getApproximationWithFactorial(int n) {
        BigInteger currentFactorial = BigInteger.ONE;

        BigFraction e = BigFraction.ZERO;

        for (int i = 1; i <= n; i++) {
            e = e.add(BigFraction.ONE.divide(currentFactorial));
            currentFactorial = currentFactorial.multiply(BigInteger.valueOf(i));
        }

        return e;
    }
}
