package mathecat.eulerproject._1._57SquareRootConvergents;

import mathecat.eulerproject.commons.concepts.BigFraction;
import mathecat.eulerproject.commons.NumberUtils;

import java.util.ArrayList;
import java.util.List;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        List<BigFraction> fractions = solution.solve(1000);

        System.out.println("Found the following expansions: " + fractions.size());
        for (BigFraction fraction : fractions) System.out.println(fraction);
    }

    /**
     * Find fractions with more digits in the numerator compared to the denominator
     * @param expansions
     * @return
     */
    public List<BigFraction> solve(int expansions) {
        BigFraction currentDenominator = BigFraction.of(2);

        List<BigFraction> matchingFractions = new ArrayList<>();

        for (int expansion = 0; expansion < expansions; expansion++) {
            BigFraction currentExpansion = expand(currentDenominator);
            if (NumberUtils.size(currentExpansion.getNumerator()) > NumberUtils.size(currentExpansion.getDenominator())) {
                matchingFractions.add(currentExpansion);
            }

            currentDenominator = nextDenominator(currentDenominator);
        }

        return matchingFractions;
    }

    private BigFraction expand(BigFraction currentDenominator) {
        return BigFraction.of(1).add(BigFraction.of(1).divide(currentDenominator));
    }

    private BigFraction nextDenominator(BigFraction currentDenominator) {
        return BigFraction.of(2).add(BigFraction.of(1).divide(currentDenominator));
    }
}
