package mathecat.eulerproject._1._62CubicPermutations;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        List<BigInteger> result = solution.solve(5);

        System.out.println("Found solution: " + result.get(0));
        System.out.println("All permutations found are: ");
        for (BigInteger permutation : result) {
            System.out.println(permutation);
        }
    }

    public List<BigInteger> solve(int desiredPermutations) {
        Map<String, List<BigInteger>> cubesByDigits = new HashMap<>();

        BigInteger nextValue = BigInteger.ONE;
        while (true) {
            BigInteger currentCube = nextValue.pow(3);

            char[] currentCubeDigits = currentCube.toString().toCharArray();
            Arrays.sort(currentCubeDigits);

            String cubeKey = new String(currentCubeDigits);

            List<BigInteger> cubesForKey = cubesByDigits.computeIfAbsent(cubeKey, k -> new ArrayList<>());
            cubesForKey.add(currentCube);

            if (cubesForKey.size() == desiredPermutations) {
                return cubesForKey;
            }

            nextValue = nextValue.add(BigInteger.ONE);
        }
    }
}
