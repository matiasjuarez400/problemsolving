package mathecat.eulerproject._1._51PrimeDigitReplacements;

import mathecat.eulerproject.commons.BasicMath;
import mathecat.eulerproject.commons.NumberUtils;
import mathecat.eulerproject.commons.primes.PrimeUtilsWithMemory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

class Solution {
    private PrimeUtilsWithMemory primeUtilsWithMemory;

    public static void main(String[] args) {
        Solution solution = new Solution();

        solution.solve(6);
        solution.solve(7);
        solution.solve(8);
    }

    public Solution() {
        this.primeUtilsWithMemory = new PrimeUtilsWithMemory();
    }

    public long solve(int desiredFamilySize) {
        Result result = doSolve(desiredFamilySize);

        if (result.getFamily().size() != desiredFamilySize) {
            throw new IllegalStateException("Unable to find family of desired size!");
        }

        System.out.println(result);

        return result.getSmallestPrime();
    }

    private Result doSolve(int desiredFamilySize) {
        Result result = new Result();

        int digitSize = 1;
        while (digitSize <= 10) {
            List<Long> primes = getPrimesWithDigits(digitSize);
            List<Long> family = new ArrayList<>(findPrimeFamily(primes, desiredFamilySize));
            Collections.sort(family);

            if (family.size() == desiredFamilySize) {
                result.setFamily(family);
                break;
            }

            digitSize++;
        }

        return result;
    }

    private Set<Long> findPrimeFamily(List<Long> primes, int desiredFamilySize) {
        Set<Long> family = new HashSet<>();

        for (long rawPrime : primes) {
            int[] complement = NumberUtils.toArray(NumberUtils.nineComplement(rawPrime));
            int[] prime = NumberUtils.toArray(rawPrime);
            int[] workingArray = new int[prime.length];
            long workingNumber;

            for (int digitPosition = 0; digitPosition < complement.length; digitPosition++) {
                // Check if there is enough room for the family to be built
                if (complement[digitPosition] + 1 < desiredFamilySize) continue;

                boolean[] combination = new boolean[complement.length];

                do {
                    family.clear();

                    // The digit we are going to use as replacement
                    for (int replacementDigit = prime[digitPosition]; replacementDigit <= 9; replacementDigit++) {
                        for (int combinationIndex = 0; combinationIndex < combination.length; combinationIndex++) {
                            // If the value at combination[i] is active, it means we want to replace the prime's digit at position i
                            if (combination[combinationIndex]) {
                                workingArray[combinationIndex] = replacementDigit;
                            } else {
                                // If not, we keep the current prime's digit
                                workingArray[combinationIndex] = prime[combinationIndex];
                            }
                        }

                        workingNumber = NumberUtils.toNumber(workingArray);
                        if (primeUtilsWithMemory.isPrime(workingNumber)) {
                            family.add(workingNumber);
                            if (family.size() == desiredFamilySize) return family;
                        }
                    }
                } while(generateNextCombination(combination, digitPosition));
            }
        }

        return family;
    }

    private boolean generateNextCombination(boolean[] current, int start) {
        for (int i = start; i < current.length; i++) {
            if (current[i]) {
                current[i] = false;
                // This means we already covered the last combination
                if (i == current.length - 1) {
                    return false;
                }
            } else {
                current[i] = true;
                break;
            }
        }

        return true;
    }

    private List<Long> getPrimesWithDigits(int howManyDigits) {
        long leftLimit = BasicMath.pow(10, howManyDigits - 1);
        long rightLimit = leftLimit * 10 - 1;

        return primeUtilsWithMemory.getPrimesInRange(leftLimit, rightLimit);
    }

    private static class Result {
        private List<Long> family;

        public void setFamily(List<Long> family) {
            this.family = family;
            Collections.sort(family);
        }

        public long getSmallestPrime() {
            return family.get(0);
        }

        public List<Long> getFamily() {
            return family;
        }

        @Override
        public String toString() {
            return String.format("Result{size: %s\n", family.size()) +
                   String.format("Members: [%s]}", family.stream().map(Objects::toString).collect(Collectors.joining(", ")));
        }
    }
}
