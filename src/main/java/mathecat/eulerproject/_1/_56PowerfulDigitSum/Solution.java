package mathecat.eulerproject._1._56PowerfulDigitSum;

import mathecat.eulerproject.commons.NumberUtils;

import java.math.BigInteger;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        System.out.println(solution.solve(100));
    }

    public BigInteger solve(int limit) {
        BigInteger maxDigitSum = BigInteger.ZERO;

        for (int i = 1; i < limit; i++) {
            for (int k = 1; k < limit; k++) {
                BigInteger current = BigInteger.valueOf(i).pow(k);
                BigInteger currentDigitSum = NumberUtils.sumDigits(current);
                if (currentDigitSum.compareTo(maxDigitSum) > 0) maxDigitSum = currentDigitSum;
            }
        }

        return maxDigitSum;
    }
}
