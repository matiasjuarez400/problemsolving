package mathecat.eulerproject._1._54PokerHands;

import mathecat.eulerproject._1._54PokerHands.model.Game;
import mathecat.eulerproject._1._54PokerHands.model.Rank;
import mathecat.eulerproject._1._54PokerHands.rank_analyzer.RankAnalyzer;

import java.io.File;
import java.io.FileNotFoundException;

class Solution {
    public static void main(String[] args) throws FileNotFoundException {
        Solution solution = new Solution();

        File file = new File("src/main/java/mathecat/eulerproject/_1/_54PokerHands/p054_poker.txt");

        System.out.printf("Game won by player one: %s\n", solution.solve(file));
    }

    public int solve(File file) throws FileNotFoundException {
        InputParser inputParser = new InputParser(file);
        RankAnalyzer rankAnalyzer = new RankAnalyzer();

        int winsPlayerOne = 0;

        while (inputParser.hasNext()) {
            Game game = inputParser.next();

            Rank playerOneRank = rankAnalyzer.analyze(game.getLeftPlayerHand());
            Rank playerTwoRank = rankAnalyzer.analyze(game.getRightPlayerHand());

            if (playerOneRank.compareTo(playerTwoRank) > 0) winsPlayerOne++;
        }

        return winsPlayerOne;
    }
}
