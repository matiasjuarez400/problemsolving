package mathecat.eulerproject._1._54PokerHands.rank_analyzer;

import mathecat.eulerproject._1._54PokerHands.model.Card;
import mathecat.eulerproject._1._54PokerHands.model.Group;
import mathecat.eulerproject._1._54PokerHands.model.RankType;

import java.util.List;
import java.util.Set;

class RoyalFlushAnalyzer implements Analyzer {

    @Override
    public RankType process(Set<Card> cards, boolean areConsecutiveCards, int suitCount, List<Group> groupList) {
        if (areConsecutiveCards && suitCount == 1 && Utils.hasAce(cards)) return RankType.ROYAL_FLUSH;
        return null;
    }
}
