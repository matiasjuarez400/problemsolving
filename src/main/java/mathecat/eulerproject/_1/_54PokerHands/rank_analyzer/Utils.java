package mathecat.eulerproject._1._54PokerHands.rank_analyzer;

import mathecat.eulerproject._1._54PokerHands.model.Card;
import mathecat.eulerproject._1._54PokerHands.model.CardValue;
import mathecat.eulerproject._1._54PokerHands.model.Group;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

class Utils {
    public static boolean areConsecutive(Set<Card> cards) {
        return areConsecutive(new ArrayList<>(cards));
    }

    public static boolean areConsecutive(List<Card> cards) {
        List<Card> sortedCards = cards.stream()
                .sorted(Comparator.comparing((Card card) -> card.getCardValue().getOrder()))
                .collect(Collectors.toList());

        for (int i = 1; i < sortedCards.size(); i++) {
            Card previous = sortedCards.get(i - 1);
            Card current = sortedCards.get(i);

            if (previous.getCardValue().getOrder() != current.getCardValue().getOrder() -1) {
                return false;
            }
        }

        return true;
    }

    public static int countSuits(Set<Card> cards) {
        return countSuits(new ArrayList<>(cards));
    }

    public static int countSuits(List<Card> cards) {
        return cards.stream()
                .map(Card::getSuit)
                .collect(Collectors.toSet())
                .size();
    }

    public static List<Group> groupByValue(Set<Card> cards) {
        Map<CardValue, List<Card>> groupsByValue = cards.stream().collect(Collectors.groupingBy(Card::getCardValue));

        List<Group> groupList = new ArrayList<>();

        for (CardValue cardValue : groupsByValue.keySet()) {
            groupList.add(new Group(groupsByValue.get(cardValue)));
        }

        return groupList.stream().filter(group -> group.size() > 1).collect(Collectors.toList());
    }

    public static Map<CardValue, List<Card>> groupByCardValueOld(Set<Card> cards) {
        return groupByCardValueOld(new ArrayList<>(cards));
    }

    public static Map<CardValue, List<Card>> groupByCardValueOld(List<Card> cards) {
        return cards.stream().collect(Collectors.groupingBy(Card::getCardValue));
    }

    public static Map<Integer, List<List<Card>>> groupCardValueGroupsBySize(Set<Card> cards) {
        return groupCardValueGroupsBySize(new ArrayList<>(cards));
    }

    public static Map<Integer, List<List<Card>>> groupCardValueGroupsBySize(List<Card> cards) {
        Map<CardValue, List<Card>> cardsByCardValue = groupByCardValueOld(cards);

        Map<Integer, List<List<Card>>> groupsBySize = new HashMap<>();
        for (CardValue cardValue : cardsByCardValue.keySet()) {
            List<Card> cardsWithCardValue = cardsByCardValue.get(cardValue);

            int groupSize = cardsWithCardValue.size();
            List<List<Card>> groupWithSize = groupsBySize.computeIfAbsent(groupSize, k -> new ArrayList<>());

            groupWithSize.add(cardsWithCardValue);
        }

        return groupsBySize;
    }

    public static boolean hasAce(Set<Card> cards) {
        return hasCardValue(cards, CardValue.ACE);
    }

    public static boolean hasCardValue(Set<Card> cards, CardValue value) {
        return cards.stream().anyMatch(card -> card.getCardValue() == value);
    }
}
