package mathecat.eulerproject._1._54PokerHands.rank_analyzer;

import mathecat.eulerproject._1._54PokerHands.model.Card;
import mathecat.eulerproject._1._54PokerHands.model.Group;
import mathecat.eulerproject._1._54PokerHands.model.Hand;
import mathecat.eulerproject._1._54PokerHands.model.Rank;
import mathecat.eulerproject._1._54PokerHands.model.RankType;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class RankAnalyzer {
    private final List<Analyzer> analyzers;

    public RankAnalyzer() {
        this.analyzers = Arrays.asList(
            new RoyalFlushAnalyzer(),
            new StraightFlushAnalyzer(),
            new FourOfAKindAnalyzer(),
            new FullHouseAnalyzer(),
            new FlushAnalyzer(),
            new StraightAnalyzer(),
            new ThreeOfAKindAnalyzer(),
            new TwoPairsAnalyzer(),
            new OnePairAnalyzer(),
            new HighCardAnalyzer()
        );
    }

    public Rank analyze(final Hand hand) {
        Set<Card> cards = hand.getCards();
        boolean areConsecutiveCards = Utils.areConsecutive(cards);
        int suitCount = Utils.countSuits(cards);
        List<Group> groups = Utils.groupByValue(cards);

        for (Analyzer analyzer : analyzers) {
            RankType rankType = analyzer.process(cards, areConsecutiveCards, suitCount, groups);

            if (rankType != null) {
                return new Rank(rankType, cards, groups);
            }
        }

        throw new IllegalStateException("Unable to determine Rank Type");
    }
}
