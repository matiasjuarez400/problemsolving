package mathecat.eulerproject._1._54PokerHands.rank_analyzer;

import mathecat.eulerproject._1._54PokerHands.model.Card;
import mathecat.eulerproject._1._54PokerHands.model.Group;
import mathecat.eulerproject._1._54PokerHands.model.RankType;

import java.util.List;
import java.util.Set;

class FourOfAKindAnalyzer implements Analyzer {
    @Override
    public RankType process(Set<Card> cards, boolean areConsecutiveCards, int suitCount, List<Group> groupList) {
        if (groupList.size() == 1 && groupList.get(0).size() == 4) return RankType.FOUR_OF_A_KIND;
        return null;
    }
}
