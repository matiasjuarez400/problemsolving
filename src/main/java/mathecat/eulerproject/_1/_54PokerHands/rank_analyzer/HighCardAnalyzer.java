package mathecat.eulerproject._1._54PokerHands.rank_analyzer;

import mathecat.eulerproject._1._54PokerHands.model.Card;
import mathecat.eulerproject._1._54PokerHands.model.Group;
import mathecat.eulerproject._1._54PokerHands.model.RankType;

import java.util.List;
import java.util.Set;

class HighCardAnalyzer implements Analyzer {
    @Override
    public RankType process(Set<Card> cards, boolean areConsecutiveCards, int suitCount, List<Group> groupList) {
        if (!areConsecutiveCards && suitCount > 1 && groupList.size() == 0) return RankType.HIGH_CARD;
        return null;
    }
}
