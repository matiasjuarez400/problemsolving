package mathecat.eulerproject._1._54PokerHands.model;

import java.util.Objects;

public class Card implements Comparable<Card> {
    private final CardValue cardValue;
    private final Suit suit;

    public Card(CardValue cardValue, Suit suit) {
        if (cardValue == null || suit == null) {
            String msg = String.format("Can not build a card with the following values: [Value: %s. Suit: %s]", cardValue, suit);
            throw new IllegalArgumentException(msg);
        }

        this.cardValue = cardValue;
        this.suit = suit;
    }

    public CardValue getCardValue() {
        return cardValue;
    }

    public Suit getSuit() {
        return suit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return cardValue == card.cardValue &&
                suit == card.suit;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardValue, suit);
    }

    @Override
    public String toString() {
        return cardValue.getName() + suit.getValue();
    }

    @Override
    public int compareTo(Card o) {
        return Integer.compare(this.getCardValue().getOrder(), o.getCardValue().getOrder());
    }
}
