package mathecat.eulerproject._1._54PokerHands.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Group {
    private final Set<Card> cardSet;

    public Group(List<Card> cards) {
        this.cardSet = new HashSet<>(cards);

        if (this.cardSet.size() != cards.size()) {
            throw new IllegalArgumentException("There are repeated cards in the list");
        }
    }

    public Group(Card... cards) {
        this(Arrays.asList(cards));
    }

    public List<Card> getCards() {
        return Collections.unmodifiableList(new ArrayList<>(cardSet));
    }

    public int size() {
        return cardSet.size();
    }

    public boolean contains(Card card) {
        return cardSet.contains(card);
    }
}
