package mathecat.eulerproject._1._54PokerHands.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Hand {
    private final Set<Card> cards;

    public Hand(Card... cards) {
        this(Arrays.asList(cards));
    }

    public Hand(List<Card> cards) {
        this.cards = Collections.unmodifiableSet(new HashSet<>(cards));

        if (this.cards.size() != 5) {
            String msg = String.format("A hand should consist of 5 different cards. Got [%s]",
                    Stream.of(cards).map(Objects::toString).collect(Collectors.joining(",")));

            throw new IllegalArgumentException(msg);
        }
    }

    public Set<Card> getCards() {
        return cards;
    }

    public List<Card> getCardsList() {
        return Collections.unmodifiableList(new ArrayList<>(cards));
    }

    @Override
    public String toString() {
        return String.format("[%s]", cards.stream().map(Objects::toString).collect(Collectors.joining(", ")));
    }
}
