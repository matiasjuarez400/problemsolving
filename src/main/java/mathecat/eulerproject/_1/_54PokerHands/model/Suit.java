package mathecat.eulerproject._1._54PokerHands.model;

public enum Suit {
    HEART("H"),
    DIAMOND("D"),
    CLUB("C"),
    SPADE("S");

    private final String value;

    Suit(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
