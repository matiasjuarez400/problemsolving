package mathecat.eulerproject._1._54PokerHands.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class Rank implements Comparable<Rank> {
    private final RankType rankType;
    private final Set<Card> cards;
    private final List<Group> groups;

    public Rank(RankType rankType, Set<Card> cards, List<Group> groups) {
        this.rankType = rankType;
        this.cards = new HashSet<>(cards);
        this.groups = new ArrayList<>(groups);

        if (this.cards.size() != 5) {
            throw new IllegalArgumentException("Each rank has to be made of 5 cards");
        }

        for (Group group : this.groups) {
            for (Card groupCard : group.getCards()) {
                if (!this.cards.contains(groupCard)) {
                    throw new IllegalArgumentException("The groups param has some cards that are not present in the cards param");
                }
            }
        }
    }

    public List<Card> getCards() {
        return Collections.unmodifiableList(new ArrayList<>(cards));
    }

    public RankType getRankType() {
        return rankType;
    }

    @Override
    public int compareTo(Rank that) {
        if (!this.getRankType().equals(that.getRankType())) {
            return Integer.compare(getRankType().getValue(), that.getRankType().getValue());
        }

        switch (getRankType()) {
            case HIGH_CARD:
            case ONE_PAIR:
            case THREE_OF_A_KIND:
            case STRAIGHT:
            case FLUSH:
            case FOUR_OF_A_KIND:
            case STRAIGHT_FLUSH: {
                if (this.getGroups().size() > 0) {
                    int groupsComparison = compareByHighestCardValue(this.getGroups().get(0).getCards(), that.getGroups().get(0).getCards());
                    if (groupsComparison != 0) return groupsComparison;
                }

                return compareByHighestCardValue(getUngroupedCards(this), getUngroupedCards(that));
            }
            case TWO_PAIRS: {
                int groupComparison = compareByHighestCardValue(flattenCardGroups(this), flattenCardGroups(that));
                if (groupComparison != 0) return groupComparison;

                return compareByHighestCardValue(getUngroupedCards(this), getUngroupedCards(that));
            }
            case FULL_HOUSE: {
                List<Group> playerOneGroups = getGroupsSorted(this);
                List<Group> playerTwoGroups = getGroupsSorted(that);

                for (int i = 0; i < playerOneGroups.size(); i++) {
                    int result = compareByHighestCardValue(playerOneGroups.get(i).getCards(), playerTwoGroups.get(i).getCards());

                    if (result != 0) return result;
                }

                return 0;
            }
            case ROYAL_FLUSH: return 0;
            default: throw new IllegalArgumentException("Unknown RankType");
        }
    }

    private List<Card> getUngroupedCards(Rank rank) {
        List<Card> ungroupedCards = new ArrayList<>();

        for (Card card : rank.getCards()) {
            boolean isUngrouped = true;
            for (Group cardGroup : rank.getGroups()) {
                if (cardGroup.contains(card)) {
                    isUngrouped = false;
                    break;
                }
            }

            if (isUngrouped) ungroupedCards.add(card);
        }

        return ungroupedCards;
    }

    private List<Card> flattenCardGroups(Rank rank) {
        List<Card> groupedCards = new ArrayList<>();
        for (Group rankGroup : rank.getGroups()) {
            groupedCards.addAll(rankGroup.getCards());
        }

        return groupedCards;
    }

    private List<Group> getGroupsSorted(Rank rank) {
        return rank.getGroups().stream().sorted(Comparator.comparing((Group group) -> group.size()).reversed()).collect(Collectors.toList());
    }

    private int compareByHighestCardValue(List<Card> thisRankCards, List<Card> thatRankCards) {
        Card playerOneHighestCard = findHighestCardByValue(thisRankCards);
        Card playerTwoHighestCard = findHighestCardByValue(thatRankCards);

        return playerOneHighestCard.compareTo(playerTwoHighestCard);
    }

    private Card findHighestCardByValue(List<Card> cardList) {
        return cardList.stream()
                .max(Card::compareTo)
                .orElseThrow(() -> new IllegalArgumentException("CardList can not be empty"));
    }

    @Override
    public String toString() {
        String groups = getGroups().stream()
                .map(group -> group.getCards().stream()
                    .map(Objects::toString)
                    .collect(Collectors.joining(","))
                ).collect(Collectors.joining(" | "));

        return String.format("%s. [%s]", rankType, groups);
    }

    public List<Group> getGroups() {
        return groups;
    }
}
