package mathecat.eulerproject._1._54PokerHands.model;

public class Game {
    private final Hand leftPlayerHand;
    private final Hand rightPlayerHand;

    public Game(Hand leftPlayerHand, Hand rightPlayerHand) {
        this.leftPlayerHand = leftPlayerHand;
        this.rightPlayerHand = rightPlayerHand;
    }

    public Hand getLeftPlayerHand() {
        return leftPlayerHand;
    }

    public Hand getRightPlayerHand() {
        return rightPlayerHand;
    }
}
