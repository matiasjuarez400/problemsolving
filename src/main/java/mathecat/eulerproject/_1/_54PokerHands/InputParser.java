package mathecat.eulerproject._1._54PokerHands;

import mathecat.eulerproject._1._54PokerHands.model.Card;
import mathecat.eulerproject._1._54PokerHands.model.CardValue;
import mathecat.eulerproject._1._54PokerHands.model.Game;
import mathecat.eulerproject._1._54PokerHands.model.Hand;
import mathecat.eulerproject._1._54PokerHands.model.Suit;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class InputParser implements Iterator<Game> {
    private final Scanner scanner;

    public InputParser(File file) throws FileNotFoundException {
        if (file == null) {
            throw new IllegalArgumentException("File can not be null");
        }

        if (!file.exists()) {
            throw new IllegalArgumentException(String.format("Unable to read file %s", file.getAbsolutePath()));
        }

        this.scanner = new Scanner(file);
    }

    public InputParser(String filePath) throws FileNotFoundException {
        this(new File(filePath));
    }

    @Override
    public Game next() {
        if (!hasNext()) return null;

        String nextGame = scanner.nextLine().trim();

        String[] cards = nextGame.split(" ");

        if (cards.length != 10) {
            throw new IllegalArgumentException("Received line we an amount of cards that's not 10: " + nextGame);
        }

        return new Game(
            buildHand(cards, 0, 5),
            buildHand(cards, 5, 10)
        );
    }

    public static List<Card> parseCardList(String rawCards) {
        String[] rawCardsArray = rawCards.split(" ");

        List<Card> cardList = new ArrayList<>();
        for (String rawCard : rawCardsArray) {
            cardList.add(parseCard(rawCard));
        }

        return cardList;
    }

    public static Card parseCard(String rawCard) {
        String rawValue = rawCard.substring(0, 1).toUpperCase();
        String rawSuit = rawCard.substring(1).toUpperCase();

        CardValue cardValue = Arrays.stream(CardValue.values())
                .filter(nextCardValue -> nextCardValue.getName().equals(rawValue))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Unable to find CardValue for " + rawValue));

        Suit suit = Arrays.stream(Suit.values())
                .filter(nextSuit -> nextSuit.getValue().equals(rawSuit))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Unable to find Suit for " + rawSuit));

        return new Card(cardValue, suit);
    }

    private Hand buildHand(String[] cards, int start, int end) {
        List<Card> cardList = new ArrayList<>();

        for (int i = start; i < end; i++) {
            String rawCard = cards[i];

            cardList.add(parseCard(rawCard));
        }

        return new Hand(cardList);
    }

    @Override
    public boolean hasNext() {
        return scanner.hasNextLine();
    }
}
