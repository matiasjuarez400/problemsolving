package mathecat.eulerproject._1._52PermutedMultiples;

import mathecat.eulerproject.commons.NumberUtils;

import java.util.stream.Collectors;
import java.util.stream.LongStream;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        long result = solution.solve();

        if (result == -1) {
            throw new IllegalStateException("Unable to find solution");
        }

        System.out.printf("Found solution for %s: \n", result);

        System.out.printf("[%s]", getMultiples(result));
    }

    public long solve() {
        for (long n = 1; n <= Long.MAX_VALUE / 6L; n++) {
            if (NumberUtils.size(n) != NumberUtils.size(n * 6)) continue;

            int[] currentDigits = NumberUtils.toArray(n);
            int[] multipleDigits = new int[currentDigits.length];

            boolean multipliersMatchForCurrentNumber = false;

            for (int multiplier = 6; multiplier >= 2; multiplier--) {
                NumberUtils.toArray(n * multiplier, multipleDigits);

                boolean digitsMatch = false;
                int matchingDigits = 0;
                for (int k = 0; k < currentDigits.length; k++) {
                    for (int l = 0; l < multipleDigits.length; l++) {
                        if (currentDigits[k] == multipleDigits[l]) {
                            multipleDigits[l] = -1;
                            matchingDigits++;
                            break;
                        }
                    }

                    // This means that we didn't find a matching digit in the multipleDigits array for the current k
                    if (matchingDigits < k + 1) {
                        break;
                    } else if (matchingDigits == currentDigits.length) {
                        digitsMatch = true;
                    } else if (multiplier == 2) {
                        // We were able to match the lowest multiplier
                        multipliersMatchForCurrentNumber = true;
                    }
                }

                if (!digitsMatch) break;
            }

            if (multipliersMatchForCurrentNumber) return n;
        }

        return -1;
    }

    private static String getMultiples(long number) {
        return LongStream.rangeClosed(2, 6).mapToObj(i -> Long.toString(i * number)).collect(Collectors.joining(", "));
    }
}
