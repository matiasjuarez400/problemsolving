package mathecat.eulerproject._1._66DiophantineEquation;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        DiophantineResult diophantineResult = solution.solve(1000);
        System.out.println("Largest value of X found: ");
        System.out.println(diophantineResult);
    }

    public DiophantineResult solve(int maxD) {
        List<DiophantineResult> diophantineResults = diophantineResults(maxD);

        return diophantineResults.stream()
                .max(Comparator.comparing(DiophantineResult::getX))
                .orElseThrow(() -> new RuntimeException("What"));
    }

    private List<DiophantineResult> diophantineResults(int maxD) {
        List<DiophantineResult> diophantineResults = new ArrayList<>();

        MemoryManagerMongo memoryManagerMongo = new MemoryManagerMongo();
        CalculationCoordinator coordinator = new CalculationCoordinator(memoryManagerMongo);

        List<MemoryRecord> records = memoryManagerMongo.findAll();
        records.sort(Comparator.comparing(MemoryRecord::getD));

        int startD = 1;
        BigInteger xStart = BigInteger.ONE;
        if (!records.isEmpty()) {
            MemoryRecord lastRecord = records.get(records.size() - 1);

            if (lastRecord.getLastExplored() != null) {
                startD = lastRecord.getD().intValue();
                xStart = lastRecord.getLastExplored();
            } else {
                startD = lastRecord.getD().add(BigInteger.ONE).intValue();
            }
        }

        for (int d = startD; d <= maxD; d++) {
            DiophantineResult result = coordinator.findMinimumSolution(d, xStart);

            if (result != null) {
                System.out.println(result);

                diophantineResults.add(result);
            }
        }

        return diophantineResults;
    }
}
