package mathecat.eulerproject._1._66DiophantineEquation;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MemoryManagerMongo {
    private final MongoClient mongoClient;
    private final MongoDatabase database;
    private final MongoCollection<Document> collection;

    private static final String DEFAULT_DB_PATH = "localhost";
    private static final String DATABASE_NAME = "euler66";

    public MemoryManagerMongo(String dbPath, String databaseName) {
        this.mongoClient = new MongoClient(dbPath);
        this.database = mongoClient.getDatabase(databaseName);
        this.collection = database.getCollection(MemoryRecord.class.getSimpleName());
    }

    public MemoryManagerMongo() {
        this(DEFAULT_DB_PATH, DATABASE_NAME);
    }

    public MemoryManagerMongo(String databaseName) {
        this(DEFAULT_DB_PATH, databaseName);
    }

    public void store(MemoryRecord memoryRecord) {
        Bson filter = Filters.eq(MemoryRecord.D_COLUMN, memoryRecord.getD().toString());

        Document values = memoryRecord.toBsonDocument();

        BasicDBObject updateQuery = new BasicDBObject();
        updateQuery.append("$set", values);

        UpdateOptions options = new UpdateOptions().upsert(true);

        collection.updateOne(filter, updateQuery, options);
    }

    public void store(List<MemoryRecord> memoryRecords) {
        collection.insertMany(memoryRecords.stream().map(MemoryRecord::toBsonDocument).collect(Collectors.toList()));
    }

    public void update(MemoryRecord current, MemoryRecord updated) {
        if (!current.getD().equals(updated.getD())) {
            throw new IllegalArgumentException(String.format("The current record and the updated record point to different D: [%s, %s]",
                    current.getD(), updated.getD()));
        }

        Bson filter = Filters.eq(MemoryRecord.D_COLUMN, current.getD().toString());

        Document newValues = updated.toBsonDocument();

        BasicDBObject updateQuery = new BasicDBObject();
        updateQuery.append("$set", newValues);

        collection.updateOne(filter, updateQuery);
    }

    public List<MemoryRecord> findAll() {
        FindIterable<Document> documents = collection.find();

        List<MemoryRecord> memoryRecords = new ArrayList<>();
        for (Document document : documents) {
            memoryRecords.add(MemoryRecord.from(document));
        }

        return memoryRecords;
    }

    public void deleteAll() {
        collection.drop();
    }
}
