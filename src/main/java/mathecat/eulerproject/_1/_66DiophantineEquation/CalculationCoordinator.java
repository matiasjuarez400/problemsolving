package mathecat.eulerproject._1._66DiophantineEquation;

import lombok.Getter;
import mathecat.eulerproject.commons.MathUtils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CalculationCoordinator {
    private final ExecutorService executorService;
    private final int threads;
    private final MathUtils mathUtils = new MathUtils();
    private final int chunkSize = 100000;
    private final int threadCheck = (int) (chunkSize * 0.01);
    private final Set<BigInteger> foundDs = new HashSet<>();
    private final MemoryManagerMongo memoryManagerMongo;

    public CalculationCoordinator(MemoryManagerMongo memoryManagerMongo) {
        this.memoryManagerMongo = memoryManagerMongo;
        this.threads = Runtime.getRuntime().availableProcessors();
        this.executorService = Executors.newFixedThreadPool(threads);
    }

    public DiophantineResult findMinimumSolution(int intD, BigInteger startX) {
        if (Math.sqrt(intD) % 1 == 0) return null;

        System.out.println("Starting calculation for " + intD);

        BigInteger d = BigInteger.valueOf(intD);

        BigInteger lowerLimit = startX;

        while (!foundDs.contains(d)) {
            List<Future<SearchResult>> futureList = new ArrayList<>();

            for (int i = 0; i < threads; i++) {
                final BigInteger finalLowerLimit = lowerLimit;
                BigInteger upperLimit = lowerLimit.add(BigInteger.valueOf(chunkSize));

                futureList.add(executorService.submit(() -> calculateInLimit(finalLowerLimit, upperLimit, d)));

                lowerLimit = upperLimit;
            }

            System.out.printf("Exploring x=%s, d=%s\n", lowerLimit, d);

            BigInteger biggestX = BigInteger.ZERO;
            for (Future<SearchResult> future : futureList) {
                try {
                    SearchResult searchResult = future.get();
                    DiophantineResult diophantineResult = searchResult.getDiophantineResult();

                    if (searchResult.getXEnd().compareTo(biggestX) > 0) biggestX = searchResult.getXEnd();

                    if (diophantineResult != null) {
                        foundDs.add(d);

                        memoryManagerMongo.store(new MemoryRecord(diophantineResult));

                        return diophantineResult;
                    }
                } catch (InterruptedException | ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }

            memoryManagerMongo.store(new MemoryRecord(d, biggestX));
        }

        throw new IllegalStateException("We should never reach this line");
    }

    // xEnd is exclusive. xStart is inclusive
    private SearchResult calculateInLimit(BigInteger xStart, BigInteger xEnd, BigInteger d) {
        BigInteger x = xStart;

        int acc = 0;
        while (x.compareTo(xEnd) < 0) {
            if (acc % threadCheck == 0) {
                if (foundDs.contains(d)) break;
            }

            DiophantineResult result = calculateFor(x, d);

            if (result != null) {
                return new SearchResult(xStart, xEnd, d, result);
            }

            x = x.add(BigInteger.ONE);
            acc++;
        }

        return new SearchResult(xStart, xEnd, d, null);
    }

    private DiophantineResult calculateFor(BigInteger x, BigInteger d) {
        BigInteger xSquare = x.pow(2);

        BigInteger[] ySquareData = xSquare.subtract(BigInteger.ONE).divideAndRemainder(d);

        if (ySquareData[1].equals(BigInteger.ZERO)) {
            BigInteger y = mathUtils.findBase(ySquareData[0], 2);

            if (y == null) {
                throw new IllegalStateException("Unable to calculate the base of Y^2");
            }

            return new DiophantineResult(x, y, d);
        }

        return null;
    }

    @Getter
    private static class SearchResult {
        BigInteger xStart;
        BigInteger xEnd;
        BigInteger d;
        DiophantineResult diophantineResult;

        public SearchResult(BigInteger xStart, BigInteger xEnd, BigInteger d, DiophantineResult diophantineResult) {
            this.xStart = xStart;
            this.xEnd = xEnd;
            this.d = d;
            this.diophantineResult = diophantineResult;
        }
    }
}
