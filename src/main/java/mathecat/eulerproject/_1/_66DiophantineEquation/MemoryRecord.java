package mathecat.eulerproject._1._66DiophantineEquation;

import lombok.Getter;
import lombok.Setter;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.math.BigInteger;
import java.util.Objects;

@Getter
@Setter
public class MemoryRecord {
    public static final String OBJECT_ID = "_id";
    public static final String X_COLUMN = "x";
    public static final String Y_COLUMN = "y";
    public static final String D_COLUMN = "d";
    public static final String LAST_EXPLORED_COLUMN = "lastExplored";

    private ObjectId objectId;
    private String X;
    private String Y;
    private String D;
    private String lastExplored;

    public MemoryRecord(BigInteger x, BigInteger y, BigInteger d) {
        X = x.toString();
        Y = y.toString();
        D = d.toString();
    }

    public MemoryRecord(BigInteger d, BigInteger lastExplored) {
        this.D = d.toString();
        this.lastExplored = lastExplored.toString();
    }

    private MemoryRecord() {}

    public MemoryRecord(DiophantineResult diophantineResult) {
        this.X = diophantineResult.getX().toString();
        this.Y = diophantineResult.getY().toString();
        this.D = diophantineResult.getD().toString();
    }

    public Document toBsonDocument() {
        Document document = new Document();

        if (Objects.nonNull(objectId)) {
            document.append(OBJECT_ID, objectId);
        }

        if (lastExplored != null) {
            document.append(LAST_EXPLORED_COLUMN, lastExplored);
        } else {
            document.append(X_COLUMN, X);
            document.append(Y_COLUMN, Y);
            document.append(D_COLUMN, D);
        }

        return document;
    }

    public static MemoryRecord from(Document document) {
        MemoryRecord memoryRecord = new MemoryRecord();
        memoryRecord.setObjectId(document.getObjectId(OBJECT_ID));
        memoryRecord.setX(document.get(X_COLUMN, String.class));
        memoryRecord.setY(document.get(Y_COLUMN, String.class));
        memoryRecord.setD(document.get(D_COLUMN, String.class));
        memoryRecord.setLastExplored(document.get(LAST_EXPLORED_COLUMN, String.class));

        return memoryRecord;
    }

    public BigInteger getX() {
        return toBigInteger(X);
    }

    public BigInteger getY() {
        return toBigInteger(Y);
    }

    public BigInteger getD() {
        return toBigInteger(D);
    }

    public BigInteger getLastExplored() {
        return toBigInteger(lastExplored);
    }

    private BigInteger toBigInteger(String s) {
        return s == null ? null : new BigInteger(s);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MemoryRecord that = (MemoryRecord) o;
        return Objects.equals(X, that.X) &&
                Objects.equals(Y, that.Y) &&
                Objects.equals(D, that.D) &&
                Objects.equals(lastExplored, that.lastExplored);
    }

    @Override
    public int hashCode() {
        return Objects.hash(X, Y, D, lastExplored);
    }

    @Override
    public String toString() {
        return "MemoryRecord{" +
                "objectId=" + objectId +
                ", X=" + X +
                ", Y=" + Y +
                ", D=" + D +
                ", lastExplored=" + lastExplored +
                '}';
    }
}
