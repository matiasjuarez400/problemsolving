package mathecat.eulerproject._1._66DiophantineEquation;

import lombok.Getter;

import java.math.BigInteger;

@Getter
public class DiophantineResult {
    private BigInteger x;
    private BigInteger y;
    private BigInteger ySquare;
    private BigInteger d;

    public DiophantineResult(BigInteger x, BigInteger y, BigInteger d) {
        this.x = x;
        this.y = y;
        this.d = d;
        this.ySquare = y.pow(2);
    }

    private DiophantineResult() {}

    public static DiophantineResult ofFailure(BigInteger d) {
        DiophantineResult result = new DiophantineResult();
        result.d = d;
        result.x = BigInteger.valueOf(-1);
        result.y = BigInteger.valueOf(-1);

        return result;
    }

    public static DiophantineResult ofYSquare(BigInteger x, BigInteger ySquare, BigInteger d) {
        DiophantineResult result = new DiophantineResult();

        result.x = x;
        result.y = null;
        result.ySquare = ySquare;
        result.d = d;

        return result;
    }

    public boolean isFailure() {
        return this.x.equals(BigInteger.valueOf(-1)) && this.y.equals(BigInteger.valueOf(-1));
    }

    @Override
    public String toString() {
        return "DiophantineResult{" +
                "x=" + x +
                ", y=" + y +
                ", ySquare=" + ySquare +
                ", d=" + d +
                '}';
    }
}
