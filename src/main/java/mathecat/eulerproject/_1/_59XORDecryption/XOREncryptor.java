package mathecat.eulerproject._1._59XORDecryption;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class XOREncryptor {
    public String encrypt(String message, String key) {
        int[] cipherArray = new int[message.length()];
        char[] keyArray = key.toCharArray();

        for (int i = 0; i < cipherArray.length; i++) {
            cipherArray[i] = message.charAt(i) ^ keyArray[i % keyArray.length];
        }

        return Arrays.stream(cipherArray).mapToObj(Objects::toString).collect(Collectors.joining(","));
    }

    public String decrypt(String cipher, String key, Set<Character> allowedCharacters) {
        List<String> messageOutput = new ArrayList<>();

        int nextStartIndex = 0;
        int iteration = 0;
        boolean lastSearch = false;
        while (!lastSearch) {
            int endSubstring = cipher.indexOf(',', nextStartIndex);
            if (endSubstring == -1) {
                endSubstring = cipher.length();
                lastSearch = true;
            }

            int nextCipherCode = Integer.parseInt(cipher.substring(nextStartIndex, endSubstring));
            char nextKeyChar = key.charAt(iteration++ % key.length());

            int xorCode =  nextCipherCode ^ nextKeyChar;

            nextStartIndex = endSubstring + 1;

            if (allowedCharacters != null) {
                if (!allowedCharacters.contains((char) xorCode)) return null;
            }

            messageOutput.add(Character.toString((char) xorCode));
        }

        return String.join("", messageOutput);
    }

    public String decrypt(String cipher, String key) {
        return decrypt(cipher, key, null);
    }
}
