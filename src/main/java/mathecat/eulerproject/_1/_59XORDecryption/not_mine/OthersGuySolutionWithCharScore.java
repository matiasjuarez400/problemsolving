package mathecat.eulerproject._1._59XORDecryption.not_mine;

import mathecat.eulerproject.commons.fileUtils.SimpleFileReader;

import java.io.IOException;

public class OthersGuySolutionWithCharScore {
    public static void main(String[] args) throws IOException {
        OthersGuySolutionWithCharScore othersGuySolutionWithCharScore = new OthersGuySolutionWithCharScore();

        String result = othersGuySolutionWithCharScore.run();

        System.out.println(result);
    }

    public String run() throws IOException {
        byte[] CIPHERTEXT = readCipher();

        byte[] bestKey = null;
        byte[] bestDecrypted = null;
        double bestScore = Double.NaN;
        for (byte x = 'a'; x <= 'z'; x++) {
            for (byte y = 'a'; y <= 'z'; y++) {
                for (byte z = 'a'; z <= 'z'; z++) {
                    byte[] key = {x, y, z};
                    byte[] decrypted = decrypt(CIPHERTEXT, key);
                    double score = score(decrypted);
                    if (bestKey == null || score > bestScore) {
                        bestKey = key;
                        bestDecrypted = decrypted;
                        bestScore = score;
                    }
                }
            }
        }

        int sum = 0;
        for (int i = 0; i < bestDecrypted.length; i++)
            sum += bestDecrypted[i];
        return Integer.toString(sum);
    }

    private static double score(byte[] b) {
        double sum = 0;
        for (int i = 0; i < b.length; i++) {
            char c = (char)b[i];
            if ('A' <= c && c <= 'Z')
                sum += 1;  // Uppercase letters are good
            else if ('a' <= c && c <= 'z')
                sum += 2;  // Lowercase letters are excellent
            else if (c < 0x20 || c == 0x7F)
                sum -= 10;  // Control characters are very bad
        }
        return sum;
    }


    private static byte[] decrypt(byte[] ciphertext, byte[] key) {
        byte[] plaintext = new byte[ciphertext.length];
        for (int i = 0; i < ciphertext.length; i++)
            plaintext[i] = (byte)(ciphertext[i] ^ key[i % key.length]);
        return plaintext;
    }

    private byte[] readCipher() throws IOException {
        SimpleFileReader simpleFileReader = new SimpleFileReader("src/main/java/mathecat/eulerproject/_1/_59XORDecryption/cipher.txt");

        String text = simpleFileReader.next();

        String[] rawCodes = text.split(",");

        byte[] output = new byte[rawCodes.length];
        for (int i = 0; i < rawCodes.length; i++) {
            output[i] = Byte.parseByte(rawCodes[i]);
        }

        return output;
    }
}
