package mathecat.eulerproject._1._59XORDecryption;

import mathecat.eulerproject.commons.fileUtils.SimpleFileReader;
import mathecat.eulerproject.commons.permutation.PermutationIterator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

class Solution {
    private final Set<Character> ALLOWED_CHARS = allowedCharacters();
    public static void main(String[] args) throws IOException {
        Solution solution = new Solution();

        System.out.println(solution.solve());
    }

    private static Set<Character> allowedCharacters() {
        Set<Character> allowed = new HashSet<>();

        // numbers
        for (int i = 48; i <= 57; i++) allowed.add((char) i);
        // lower case
        for (int i = 97; i <= 122; i++) allowed.add((char) i);
        // upper case
        for (int i = 65; i <= 90; i++) allowed.add((char) i);
        // symbols and space
        for (int i = 32; i <= 47; i++) allowed.add((char) i);
        // more symbols
        for (int i = 58; i <= 63; i++) allowed.add((char) i);

        allowed.addAll(Arrays.asList('[', ']'));

        return allowed;
    }

    private long solve() throws IOException {
        int[] cipherCodes = readCipher();
        String cipherText = Arrays.stream(cipherCodes).mapToObj(Objects::toString).collect(Collectors.joining(","));

        XOREncryptor encryptor = new XOREncryptor();
        PermutationIterator<String> combinator = new PermutationIterator<>(getLowerCaseLetters(), 3);

        while (combinator.hasNext()) {
            String nextPass = String.join("", combinator.next());

            String decryptionResult = encryptor.decrypt(cipherText, nextPass);

            if (!validateDecryptionResult(decryptionResult)) continue;

            System.out.println("Key found: " + nextPass);

            long acc = 0;
            for (char c : decryptionResult.toCharArray()) {
                acc += (int) c;
            }

            return acc;
        }

        throw new IllegalStateException("Unable to solve the problem");
    }

    private boolean validateDecryptionResult(String result) {
        for (char c : result.toCharArray()) {
            if (!ALLOWED_CHARS.contains(c)) {
                return false;
            }
        }

        return true;
    }

    private int[] readCipher() throws IOException {
        SimpleFileReader simpleFileReader = new SimpleFileReader("src/main/java/mathecat/eulerproject/_1/_59XORDecryption/cipher.txt");

        String text = simpleFileReader.next();

        String[] rawCodes = text.split(",");

        return Arrays.stream(rawCodes).map(Integer::parseInt).mapToInt(Integer::intValue).toArray();
    }

    private List<String> getLowerCaseLetters() {
        List<String> letters = new ArrayList<>();

        for (char c = 'a'; c <= 'z'; c++) {
            letters.add(Character.toString(c));
        }

        return letters;
    }
}
