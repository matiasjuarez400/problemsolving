package mathecat.eulerproject._1._55LychrelNumbers;

import mathecat.eulerproject.commons.NumberUtils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();

        int searchUpTo = 10_000;
        int iterationLimit = 50;

        Result result = solution.solve(searchUpTo, iterationLimit);

        System.out.println("Lychrel numbers total: " + result.getLychrelNumbers().size());
        System.out.println("Lychrel numbers: " + result.getLychrelNumbers().stream().map(Objects::toString).collect(Collectors.joining(", ")));

        System.out.println("Found the following palindromic numbers: ");
        result.getPalindromicNumbers().forEach(System.out::println);
    }

    public Result solve(int searchUpTo, int iterationLimit) {
        Result result = new Result();

        for (int i = 1; i <= searchUpTo; i++) {
            BigInteger currentValue = BigInteger.valueOf(i);

            boolean isPalindrome = false;
            for (int j = 0; j < iterationLimit; j++) {
                currentValue = currentValue.add(NumberUtils.reverse(currentValue));
                if (NumberUtils.isPalindrome(currentValue)) {
                    result.addPalindromic(new PalindromicNumber(BigInteger.valueOf(i), currentValue, j + 1));
                    isPalindrome = true;
                    break;
                }
            }

            if (!isPalindrome) {
                result.addLychrel((long) i);
            }
        }

        return result;
    }

    private static class Result {
        private final List<PalindromicNumber> palindromicNumbers;
        private final List<Long> lychrelNumbers;

        public Result() {
            this.palindromicNumbers = new ArrayList<>();
            this.lychrelNumbers = new ArrayList<>();
        }

        public List<PalindromicNumber> getPalindromicNumbers() {
            return palindromicNumbers;
        }

        public List<Long> getLychrelNumbers() {
            return lychrelNumbers;
        }

        public void addPalindromic(PalindromicNumber palindromicNumber) {
            this.palindromicNumbers.add(palindromicNumber);
        }

        public void addLychrel(Long lychrel) {
            this.lychrelNumbers.add(lychrel);
        }
    }

    private static class PalindromicNumber {
        private final BigInteger start;
        private final BigInteger end;
        private final int iterations;

        public PalindromicNumber(BigInteger start, BigInteger end, int iterations) {
            this.start = start;
            this.end = end;
            this.iterations = iterations;
        }

        public BigInteger getStart() {
            return start;
        }

        public BigInteger getEnd() {
            return end;
        }

        public int getIterations() {
            return iterations;
        }

        @Override
        public String toString() {
            return "Result{" +
                    "start=" + start +
                    ", end=" + end +
                    ", iterations=" + iterations +
                    '}';
        }
    }
}
