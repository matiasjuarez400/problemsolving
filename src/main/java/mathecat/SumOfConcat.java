package mathecat;

public class SumOfConcat {
    public static void main(String[] args) {
        System.out.println(solve(new int[]{10, 2}));
    }

    public static int solve(int[] input) {
        int result = 0;
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input.length; j++) {
                int left = input[i];
                int right = input[j];
//
//                int partialResult = (int) (left * Math.pow(10, calculateDigits(right)) + right);

                result += (int) (left * Math.pow(10, calculateDigits(right)) + right);

//                System.out.println(String.format("LEFT %s - RIGHT %s - PARTIAL %s - RESULT %s", left, right, partialResult, result));
            }
        }

        return result;
    }

    private static int calculateDigits(int number) {
        return (int) Math.log10(number) + 1;
    }
}
