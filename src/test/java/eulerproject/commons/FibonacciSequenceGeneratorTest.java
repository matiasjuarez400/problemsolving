package eulerproject.commons;

import mathecat.eulerproject.commons.generators.FibonacciSequenceGenerator;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FibonacciSequenceGeneratorTest {
    @Test
    public void testGenerateFirst10Terms() {
        List<Integer> generatedTerms = new ArrayList<>();

        FibonacciSequenceGenerator generator = new FibonacciSequenceGenerator();

        for (int i = 0; i < 10; i++) {
            generatedTerms.add(generator.next());
        }

        List<Integer> expectedTerms = Arrays.asList(
            1, 2, 3, 5, 8, 13, 21, 34, 55, 89
        );

        for (int i = 0; i < expectedTerms.size(); i++) {
            Assert.assertEquals(expectedTerms.get(i), generatedTerms.get(i));
        }
    }
}
