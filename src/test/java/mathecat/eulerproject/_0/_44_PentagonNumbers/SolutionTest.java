package mathecat.eulerproject._0._44_PentagonNumbers;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SolutionTest {
    @Test
    public void testIsGoodRightPentagon() {
        Solution solution = new Solution();

        assertTrue(solution.shouldAnalyzePosition(12, 3));
        assertFalse(solution.shouldAnalyzePosition(12, 4));

        assertTrue(solution.shouldAnalyzePosition(22, 4));
        assertTrue(solution.shouldAnalyzePosition(22, 5));
        assertTrue(solution.shouldAnalyzePosition(22, 6));
        assertTrue(solution.shouldAnalyzePosition(22, 7));
        assertFalse(solution.shouldAnalyzePosition(22, 8));
    }
}