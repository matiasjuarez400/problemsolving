package mathecat.eulerproject._0._44_PentagonNumbers;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PentagonNumberGeneratorTest {
    @Test
    public void testPentagonNumberGeneration() {
        PentagonNumberGenerator generator = new PentagonNumberGenerator();

        assertEquals(1, generator.generate(0));
        assertEquals(5, generator.generate(1));
        assertEquals(12, generator.generate(2));
        assertEquals(22, generator.generate(3));
        assertEquals(35, generator.generate(4));
        assertEquals(51, generator.generate(5));
        assertEquals(70, generator.generate(6));
        assertEquals(92, generator.generate(7));
        assertEquals(117, generator.generate(8));
        assertEquals(145, generator.generate(9));
    }

    @Test
    public void testDistanceToPreviousPentagon() {
        PentagonNumberGenerator generator = new PentagonNumberGenerator();

        assertEquals(16, generator.distanceToPreviousPentagon(5));
        assertEquals(4, generator.distanceToPreviousPentagon(1));
        assertEquals(25, generator.distanceToPreviousPentagon(8));
        assertEquals(10, generator.distanceToPreviousPentagon(3));
    }
}