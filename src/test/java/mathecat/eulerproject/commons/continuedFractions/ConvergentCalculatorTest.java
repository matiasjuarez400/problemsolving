package mathecat.eulerproject.commons.continuedFractions;

import mathecat.eulerproject.commons.concepts.BigFraction;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ConvergentCalculatorTest {
    @Test
    public void testCalculateConvergentForSquare2() {
        long base = 1;
        List<Long> indexes = new ArrayList<>();

        indexes.add(2L);
        assertEquals(BigFraction.of(3, 2), ConvergentCalculator.findConvergent(base, indexes));

        indexes.add(2L);
        assertEquals(BigFraction.of(7, 5), ConvergentCalculator.findConvergent(base, indexes));

        indexes.add(2L);
        assertEquals(BigFraction.of(17, 12), ConvergentCalculator.findConvergent(base, indexes));

        indexes.add(2L);
        assertEquals(BigFraction.of(41, 29), ConvergentCalculator.findConvergent(base, indexes));

        indexes.add(2L);
        assertEquals(BigFraction.of(99, 70), ConvergentCalculator.findConvergent(base, indexes));

        indexes.add(2L);
        assertEquals(BigFraction.of(239, 169), ConvergentCalculator.findConvergent(base, indexes));

        indexes.add(2L);
        assertEquals(BigFraction.of(577, 408), ConvergentCalculator.findConvergent(base, indexes));

        indexes.add(2L);
        assertEquals(BigFraction.of(1393, 985), ConvergentCalculator.findConvergent(base, indexes));

        indexes.add(2L);
        assertEquals(BigFraction.of(3363, 2378), ConvergentCalculator.findConvergent(base, indexes));
    }
}