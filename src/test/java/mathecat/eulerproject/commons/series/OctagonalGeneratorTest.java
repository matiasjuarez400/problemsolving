package mathecat.eulerproject.commons.series;

import org.junit.Test;

import static org.junit.Assert.*;

public class OctagonalGeneratorTest {
    @Test
    public void generate() {
        Generator generator = new OctagonalGenerator();

        assertEquals(1, generator.get(1));
        assertEquals(8, generator.get(2));
        assertEquals(21, generator.get(3));
        assertEquals(40, generator.get(4));
        assertEquals(65, generator.get(5));
    }
}