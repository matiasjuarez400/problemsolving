package mathecat.eulerproject.commons.series;

import org.junit.Test;

import static org.junit.Assert.*;

public class SquareGeneratorTest {
    @Test
    public void generate() {
        Generator generator = new SquareGenerator();

        assertEquals(1, generator.get(1));
        assertEquals(4, generator.get(2));
        assertEquals(9, generator.get(3));
        assertEquals(16, generator.get(4));
        assertEquals(25, generator.get(5));
    }
}