package mathecat.eulerproject.commons.series;

import org.junit.Test;

import static org.junit.Assert.*;

public class HexagonalGeneratorTest {
    @Test
    public void generate() {
        Generator generator = new HexagonalGenerator();

        assertEquals(1, generator.get(1));
        assertEquals(6, generator.get(2));
        assertEquals(15, generator.get(3));
        assertEquals(28, generator.get(4));
        assertEquals(45, generator.get(5));
    }
}