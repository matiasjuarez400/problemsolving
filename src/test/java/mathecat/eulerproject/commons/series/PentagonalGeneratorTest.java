package mathecat.eulerproject.commons.series;

import org.junit.Test;

import static org.junit.Assert.*;

public class PentagonalGeneratorTest {
    @Test
    public void generate() {
        Generator generator = new PentagonalGenerator();

        assertEquals(1, generator.get(1));
        assertEquals(5, generator.get(2));
        assertEquals(12, generator.get(3));
        assertEquals(22, generator.get(4));
        assertEquals(35, generator.get(5));
    }
}