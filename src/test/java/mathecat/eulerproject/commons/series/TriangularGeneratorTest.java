package mathecat.eulerproject.commons.series;

import org.junit.Test;

import static org.junit.Assert.*;

public class TriangularGeneratorTest {
    @Test
    public void generate() {
        Generator generator = new TriangularGenerator();

        assertEquals(1, generator.get(1));
        assertEquals(3, generator.get(2));
        assertEquals(6, generator.get(3));
        assertEquals(10, generator.get(4));
        assertEquals(15, generator.get(5));
    }
}