package mathecat.eulerproject.commons.series;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class HeptagonalGeneratorTest {

    @Test
    public void generate() {
        Generator generator = new HeptagonalGenerator();

        assertEquals(1, generator.get(1));
        assertEquals(7, generator.get(2));
        assertEquals(18, generator.get(3));
        assertEquals(34, generator.get(4));
        assertEquals(55, generator.get(5));
    }

    @Test
    public void indexForValue() {
        Generator generator = new HeptagonalGenerator();

        for (int searchStep = 1; searchStep <= 100; searchStep++) {
            assertForRange(-10, 1, 0, searchStep, generator);
            assertForRange(2, 7, 1, searchStep, generator);
            assertForRange(7, 18, 2, searchStep, generator);
            assertForRange(18, 34, 3, searchStep, generator);
            assertForRange(34, 55, 4, searchStep, generator);
        }
    }

    @Test
    public void generateValuesForRange() {
        Generator generator = new HeptagonalGenerator();

        List<Long> expectedValues = Arrays.asList(7L, 18L, 34L);
        assertCollection(expectedValues, generator.generateValuesForRange(7, 34));
        assertCollection(expectedValues, generator.generateValuesForRange(7, 35));
        assertCollection(expectedValues, generator.generateValuesForRange(6, 34));
        assertCollection(expectedValues, generator.generateValuesForRange(6, 36));
        assertCollection(expectedValues, generator.generateValuesForRange(6, 54));

        expectedValues = Arrays.asList(18L, 34L);
        assertCollection(expectedValues, generator.generateValuesForRange(8, 34));

        expectedValues = Arrays.asList(1L, 7L, 18L, 34L);
        assertCollection(expectedValues, generator.generateValuesForRange(1, 35));
        assertCollection(expectedValues, generator.generateValuesForRange(1, 36));
        assertCollection(expectedValues, generator.generateValuesForRange(1, 37));

        expectedValues = Arrays.asList(7L, 18L, 34L, 55L);
        assertCollection(expectedValues, generator.generateValuesForRange(7, 55));
        assertCollection(expectedValues, generator.generateValuesForRange(6, 55));
        assertCollection(expectedValues, generator.generateValuesForRange(6, 56));

        expectedValues = Arrays.asList(1L, 7L, 18L, 34L, 55L);
        assertCollection(expectedValues, generator.generateValuesForRange(1, 55));
    }

    public void assertCollection(List<Long> expected, List<Long> actual) {
        assertEquals(expected.size(), actual.size());

        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), actual.get(i));
        }
    }

    private void assertForRange(int start, int end, int expected, int searchStep, Generator generator) {
        for (int i = start; i < end; i++) {
            String errorMessage = String.format("Failed with [SEARCH_STEP: %s, CURRENT_VALUE: %s, EXPECTED: %s]", searchStep, i, expected);
            assertEquals(errorMessage, expected, generator.beforeIndexForValue(i, searchStep));
        }
    }
}