package mathecat.eulerproject.commons;

import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.*;

public class BasicMathTest {
    @Test
    public void testFactorial() {
        assertEquals(BigInteger.valueOf(1), BasicMath.factorial(0));
        assertEquals(BigInteger.valueOf(1), BasicMath.factorial(1));
        assertEquals(BigInteger.valueOf(2), BasicMath.factorial(2));
        assertEquals(BigInteger.valueOf(6), BasicMath.factorial(3));
        assertEquals(BigInteger.valueOf(24), BasicMath.factorial(4));
        assertEquals(BigInteger.valueOf(120), BasicMath.factorial(5));
    }
}