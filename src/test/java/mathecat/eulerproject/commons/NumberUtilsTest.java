package mathecat.eulerproject.commons;

import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.*;

public class NumberUtilsTest {
    @Test
    public void testSize() {
        assertEquals(5, NumberUtils.size(23168));
        assertEquals(4, NumberUtils.size(2316));
        assertEquals(3, NumberUtils.size(231));
        assertEquals(2, NumberUtils.size(23));
        assertEquals(1, NumberUtils.size(2));
        assertEquals(1, NumberUtils.size(1));
        assertEquals(1, NumberUtils.size(0));
        assertEquals(1, NumberUtils.size(-1));
    }

    @Test
    public void testGetDigit() {
        assertEquals(2, NumberUtils.getDigit(23168, 4));
        assertEquals(3, NumberUtils.getDigit(23168, 3));
        assertEquals(1, NumberUtils.getDigit(23168, 2));
        assertEquals(6, NumberUtils.getDigit(23168, 1));
        assertEquals(8, NumberUtils.getDigit(23168, 0));

        try {
            assertEquals(2, NumberUtils.getDigit(23168, 5));
            fail("Position is out of range. It should fail");
        } catch (Exception e) {
            assertEquals(String.format("Position %s is out of range for number %s", 5, 23168), e.getMessage());
        }

        try {
            assertEquals(2, NumberUtils.getDigit(23168, -5));
            fail("Position index is negative. It should fail");
        } catch (Exception e) {
            assertEquals("Position can not be negative", e.getMessage());
        }
    }

    @Test
    public void testGet9Complement() {
        test9ComplementBothWays(89, 10);
        test9ComplementBothWays(2, 7);
        test9ComplementBothWays(7, 2);
        test9ComplementBothWays(345, 654);
        test9ComplementBothWays(123456789, 876543210);
    }

    private void test9ComplementBothWays(long left, long right) {
        assertEquals(left, NumberUtils.nineComplement(right));
        assertEquals(right, NumberUtils.nineComplement(left));
    }

    @Test
    public void testToArray() {
        assertArrayEquals(new int[] {4, 5, 1}, NumberUtils.toArray(451));
        assertArrayEquals(new int[] {4, 5}, NumberUtils.toArray(45));
        assertArrayEquals(new int[] {5}, NumberUtils.toArray(5));
        assertArrayEquals(new int[] {0}, NumberUtils.toArray(0));
        assertArrayEquals(new int[] {5, 7, 8, 1, 3, 9, 6}, NumberUtils.toArray(5781396));
    }

    @Test
    public void testToNumber() {
        assertEquals(523, NumberUtils.toNumber(new int[] {5, 2, 3}));
        assertEquals(23, NumberUtils.toNumber(new int[] {2, 3}));
        assertEquals(4, NumberUtils.toNumber(new int[] {4}));
        assertEquals(9, NumberUtils.toNumber(new int[] {9}));
        assertEquals(1, NumberUtils.toNumber(new int[] {1}));
        assertEquals(0, NumberUtils.toNumber(new int[] {0}));
    }

    @Test
    public void testIsPalindrome() {
        assertTrue(NumberUtils.isPalindrome(424));
        assertTrue(NumberUtils.isPalindrome(4224));
        assertFalse(NumberUtils.isPalindrome(4243));

        assertTrue(NumberUtils.isPalindrome(7));
        assertTrue(NumberUtils.isPalindrome(55));
        assertFalse(NumberUtils.isPalindrome(78));

        assertTrue(NumberUtils.isPalindrome(42124));
        assertTrue(NumberUtils.isPalindrome(421124));
        assertTrue(NumberUtils.isPalindrome(4210124));
    }

    @Test
    public void testReverse() {
        assertEquals(1234, NumberUtils.reverse(4321));
        assertEquals(4321, NumberUtils.reverse(1234));
        assertEquals(4, NumberUtils.reverse(4));
        assertEquals(51, NumberUtils.reverse(15));
    }

    @Test
    public void testSumDigits() {
        assertEquals(9, NumberUtils.sumDigits(432));
        assertEquals(16, NumberUtils.sumDigits(4327));
        assertEquals(2, NumberUtils.sumDigits(2));
        assertEquals(0, NumberUtils.sumDigits(0));
    }

    @Test
    public void testConcatenate() {
        assertEquals(173, NumberUtils.concatenate(1, 73));
        assertEquals(173, NumberUtils.concatenate(17, 3));
        assertEquals(731, NumberUtils.concatenate(73, 1));
        assertEquals(731, NumberUtils.concatenate(7, 31));

        assertEquals(73, NumberUtils.concatenate(0, 73));
        assertEquals(730, NumberUtils.concatenate(73, 0));

        assertEquals(562498, NumberUtils.concatenate(562, 498));
        assertEquals(498562, NumberUtils.concatenate(498, 562));

        try {
            NumberUtils.concatenate(1234567890, 1876543210);
            fail("Concatenating these two numbers would exceed long limits");
        } catch (Exception e) {
            assertEquals("Concatenating 1234567890 and 1876543210 would case a long overflow", e.getMessage());
        }
    }
}