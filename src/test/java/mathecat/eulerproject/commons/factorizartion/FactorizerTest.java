package mathecat.eulerproject.commons.factorizartion;

import org.junit.Test;

import static org.junit.Assert.*;

public class FactorizerTest {
    @Test
    public void testFindLeastCommonMultiple() {
        assertEquals(6, Factorizer.findLeastCommonMultiple(2, 3));
        assertEquals(8, Factorizer.findLeastCommonMultiple(4, 8));
        assertEquals(8, Factorizer.findLeastCommonMultiple(2, 8));
        assertEquals(40, Factorizer.findLeastCommonMultiple(5, 8));
        assertEquals(24, Factorizer.findLeastCommonMultiple(3, 8));
    }
}