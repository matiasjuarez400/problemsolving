package mathecat.eulerproject.commons;

import mathecat.eulerproject.commons.concepts.Range;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class RangeTest {

    @Test
    public void isOverlapping() {
        Range pivot = new Range(20, 30);

        for (int i = 20; i <= 35; i++) {
            testIsOverlappingBothWays(pivot, new Range(10, i), true);
        }

        for (int i = 30; i >= 15; i--) {
            testIsOverlappingBothWays(pivot, new Range(i, 50), true);
        }

        testIsOverlappingBothWays(pivot, new Range(25, 25), true);
        testIsOverlappingBothWays(pivot, new Range(10, 19), false);
        testIsOverlappingBothWays(pivot, new Range(31, 40), false);
    }

    private void testIsOverlappingBothWays(Range left, Range right, boolean expectedResult) {
        assertEquals(String.format("%s, %s", left, right), expectedResult, left.isOverlapping(right));
        assertEquals(String.format("%s, %s", left, right), expectedResult, right.isOverlapping(left));
    }

    @Test
    public void isAdjoined() {
        Range pivot = new Range(20, 30);

        for (int i = 20; i <= 35; i++) {
            Range testRange = new Range(10, i);
            testIsAdjoinedBothWays(testRange, pivot, false);
        }

        for (int i = 30; i >= 15; i--) {
            Range testRange = new Range(i, 50);
            testIsAdjoinedBothWays(testRange, pivot, false);
        }

        testIsAdjoinedBothWays(pivot, new Range(25, 25), false);
        testIsAdjoinedBothWays(pivot, new Range(21, 29), false);
        testIsAdjoinedBothWays(pivot, new Range(10, 19), true);
        testIsAdjoinedBothWays(pivot, new Range(31, 40), true);
    }

    private void testIsAdjoinedBothWays(Range left, Range right, boolean expectedResult) {
        assertEquals(String.format("%s, %s", left, right), expectedResult, left.isAdjoined(right));
        assertEquals(String.format("%s, %s", left, right), expectedResult, right.isAdjoined(left));
    }

    @Test
    public void canMerge() {
        Range pivot = new Range(20, 30);

        assertTrue(pivot.canMerge(new Range(10, 19)));
        assertTrue(pivot.canMerge(new Range(10, 20)));
        assertTrue(pivot.canMerge(new Range(10, 21)));

        assertTrue(pivot.canMerge(new Range(29, 40)));
        assertTrue(pivot.canMerge(new Range(30, 40)));
        assertTrue(pivot.canMerge(new Range(31, 40)));

        assertTrue(pivot.canMerge(new Range(21, 29)));
        assertTrue(pivot.canMerge(new Range(22, 28)));
        assertTrue(pivot.canMerge(new Range(25, 25)));

        assertFalse(pivot.canMerge(new Range(10, 10)));
        assertFalse(pivot.canMerge(new Range(40, 40)));
        assertFalse(pivot.canMerge(new Range(10, 18)));
        assertFalse(pivot.canMerge(new Range(32, 40)));

    }

    @Test
    public void mergeManyRanges() {
        Range left = new Range(10, 20);
        Range middle = new Range(21, 29);
        Range right = new Range(30, 40);
        Range farRange = new Range(100, 200);

        assertEquals(new Range(10, 40), left.merge(middle, right));
        assertEquals(new Range(10, 40), left.merge(right, middle));

        assertEquals(new Range(10, 40), middle.merge(left, right));
        assertEquals(new Range(10, 40), middle.merge(right, left));

        assertEquals(new Range(10, 40), right.merge(left, middle));
        assertEquals(new Range(10, 40), right.merge(middle, left));

        assertUnableToMergeRanges(left, middle, right, farRange);
        assertUnableToMergeRanges(farRange, middle, right, left);
        assertUnableToMergeRanges(left, farRange, right, middle);
        assertUnableToMergeRanges(left, middle, farRange, right);
        assertUnableToMergeRanges(left, right);
    }

    private void assertUnableToMergeRanges(Range pivotRange, Range... mergeRanges) {
        try {
            pivotRange.merge(mergeRanges);
            fail("Expected to be unable to merge all ranges");
        } catch (Exception e) {
            assertTrue(e.getMessage().contains("The maximum ranged that was created is"));
            assertTrue(e.getMessage().contains("Could not merge these ranges"));
        }
    }

    @Test
    public void mergeOneRange() {
        Range origin = new Range(20, 40);

        assertRangeCantMerge(origin, new Range(1, 18));
        assertRangeCantMerge(origin, new Range(1, 1));
        assertRangeCantMerge(origin, new Range(42, 42));
        assertRangeCantMerge(origin, new Range(42, 100));

        assertEquals(new Range(10, 40), origin.merge(new Range(10, 19)));
        assertEquals(new Range(10, 40), origin.merge(new Range(10, 20)));
        assertEquals(new Range(10, 40), origin.merge(new Range(10, 21)));
        assertEquals(new Range(10, 40), origin.merge(new Range(10, 22)));
        assertEquals(new Range(10, 40), origin.merge(new Range(10, 40)));

        assertEquals(new Range(10, 50), origin.merge(new Range(10, 50)));

        assertEquals(new Range(20, 70), origin.merge(new Range(20, 70)));
        assertEquals(new Range(20, 70), origin.merge(new Range(21, 70)));
        assertEquals(new Range(20, 70), origin.merge(new Range(39, 70)));
        assertEquals(new Range(20, 70), origin.merge(new Range(40, 70)));
        assertEquals(new Range(20, 70), origin.merge(new Range(41, 70)));
    }

    private void assertRangeCantMerge(Range left, Range right) {
        try {
            left.merge(right);
            fail("Should not merge");
        } catch (Exception e) {
            assertEquals(
                    String.format("Can not merge ranges. Reason: they don't overlap nor are adjoined. [%s | %s]", left, right),
                    e.getMessage()
            );
        }
    }

    @Test
    public void gap() {
        Range left = new Range(10, 15);
        Range right = new Range(20, 30);

        Range gap = left.gap(right);

        assertEquals(gap, right.gap(left));
        assertEquals(gap, new Range(16, 19));

        right = new Range(16, 30);
        gap = left.gap(right);
        assertNull(gap);

        right = new Range(15, 30);
        gap = left.gap(right);
        assertNull(gap);

        right = new Range(14, 30);
        gap = left.gap(right);
        assertNull(gap);

        right = new Range(10, 30);
        gap = left.gap(right);
        assertNull(gap);

        right = new Range(9, 30);
        gap = left.gap(right);
        assertNull(gap);

        right = new Range(1, 30);
        gap = left.gap(right);
        assertNull(gap);
    }

    @Test
    public void createMaxRange() {
        Range workingRange = Range.createMaxRange(new Range(5, 15), new Range(30, 60));
        assertEquals(5, workingRange.getStart());
        assertEquals(60, workingRange.getEnd());

        workingRange = Range.createMaxRange(new Range(20, 40), new Range(10, 20));
        assertEquals(10, workingRange.getStart());
        assertEquals(40, workingRange.getEnd());

        workingRange = Range.createMaxRange(new Range(20, 40), new Range(10, 19));
        assertEquals(10, workingRange.getStart());
        assertEquals(40, workingRange.getEnd());

        workingRange = Range.createMaxRange(new Range(20, 70), new Range(25, 30));
        assertEquals(20, workingRange.getStart());
        assertEquals(70, workingRange.getEnd());
    }

    @Test
    public void size() {
        assertEquals(1, new Range(10, 10).size());
        assertEquals(2, new Range(10, 11).size());
        assertEquals(11, new Range(10, 20).size());
    }

    @Test
    public void includesValue() {
        Range range = new Range(50, 65);

        assertFalse(range.includes(5));
        assertFalse(range.includes(49));
        assertFalse(range.includes(66));
        assertFalse(range.includes(888));

        assertTrue(range.includes(50));
        assertTrue(range.includes(55));
        assertTrue(range.includes(65));
    }

    @Test
    public void includesRange() {
        Range pivotRange = new Range(20, 40);

        assertFalse(pivotRange.includes(new Range(0, 10)));
        assertFalse(pivotRange.includes(new Range(0, 20)));
        assertFalse(pivotRange.includes(new Range(0, 30)));
        assertFalse(pivotRange.includes(new Range(0, 40)));
        assertFalse(pivotRange.includes(new Range(0, 50)));

        assertTrue(pivotRange.includes(new Range(20, 30)));
        assertTrue(pivotRange.includes(new Range(20, 40)));
        assertFalse(pivotRange.includes(new Range(20, 50)));

        assertFalse(pivotRange.includes(new Range(40, 50)));
        assertFalse(pivotRange.includes(new Range(50, 60)));

        assertTrue(pivotRange.includes(new Range(25, 28)));
        assertTrue(pivotRange.includes(new Range(25, 40)));

        assertFalse(pivotRange.includes(new Range(2, 2)));
        assertFalse(pivotRange.includes(new Range(200, 200)));
        assertTrue(pivotRange.includes(new Range(25, 25)));
    }

    @Test
    public void getStart() {
        assertEquals(1, new Range(1, 5).getStart());
    }

    @Test
    public void getEnd() {
        assertEquals(5, new Range(1, 5).getEnd());
    }
}