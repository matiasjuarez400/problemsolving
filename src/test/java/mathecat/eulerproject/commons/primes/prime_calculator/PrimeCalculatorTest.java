package mathecat.eulerproject.commons.primes.prime_calculator;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PrimeCalculatorTest {
    @Test
    public void testIsPrimeWithoutInitialRange() {
        PrimeCalculator primeCalculator = new PrimeCalculator();

        assertTrue(primeCalculator.isPrime(3));
        assertTrue(primeCalculator.isPrime(2));
        assertFalse(primeCalculator.isPrime(4));
        assertFalse(primeCalculator.isPrime(20));
        assertTrue(primeCalculator.isPrime(5));
        assertFalse(primeCalculator.isPrime(0));
        assertFalse(primeCalculator.isPrime(1));
        assertTrue(primeCalculator.isPrime(113));
        assertTrue(primeCalculator.isPrime(37));
    }

    @Test
    public void testGetPrimesUnderOneThousand() {
        List<Long> expectedPrimes = Stream.of(
                2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43,
                47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103,
                107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163,
                167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227,
                229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281,
                283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353,
                359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421,
                431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487,
                491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569,
                571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631,
                641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701,
                709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773,
                787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857,
                859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937,
                941, 947, 953, 967, 971, 977, 983, 991, 997
        ).map(Long::valueOf).collect(Collectors.toList());

        PrimeCalculator primeCalculator = new PrimeCalculator();
        List<Long> calculatedPrimes = LongStream.range(0, 1001).boxed()
                .filter(primeCalculator::isPrime)
                .collect(Collectors.toList());

        assertEquals(expectedPrimes.size(), calculatedPrimes.size());

        for (int i = 0; i < expectedPrimes.size(); i++) {
            assertEquals(expectedPrimes.get(i), calculatedPrimes.get(i));
        }
    }

    @Test
    public void testCreatePrimeCalculatorFromMemory() {
        PrimeCalculator primeCalculator = new PrimeCalculator("src/test/java/mathecat/eulerproject/commons/primes/prime_calculator/primes_test_memory.txt");

        List<Long> loadedPrimes = primeCalculator.getKnownPrimes();
        List<Long> expectedPrimes = Stream.of(2, 3, 5, 7, 11, 13).map(Long::valueOf).collect(Collectors.toList());

        assertEquals(expectedPrimes.size(), loadedPrimes.size());

        for (int i = 0; i < expectedPrimes.size(); i++) {
            assertEquals(loadedPrimes.get(i), expectedPrimes.get(i));
        }
    }

    @Test
    public void testSavePrimesToFile() throws IOException {
        File tempFile = File.createTempFile("writting_primes_test", ".txt");

        PrimeCalculator loadedPrimeCalculator = new PrimeCalculator(tempFile.getAbsolutePath());
        assertEquals(2, loadedPrimeCalculator.getKnownPrimes().size());

        PrimeCalculator writerCalculator = new PrimeCalculator();
        assertEquals(2, writerCalculator.getKnownPrimes().size());

        writerCalculator.isPrime(841);

        List<Long> expectedPrimes = Stream.of(2, 3, 5, 7, 11, 13, 17, 19, 23, 29).map(Long::valueOf).collect(Collectors.toList());

        List<Long> knownPrimes = writerCalculator.getKnownPrimes();

        assertEquals(expectedPrimes.size(), knownPrimes.size());
        for (int i = 0; i < expectedPrimes.size(); i++) {
            assertEquals(expectedPrimes.get(i), knownPrimes.get(i));
        }

        writerCalculator.savePrimes(tempFile.getAbsolutePath());

        assertEquals(2, loadedPrimeCalculator.getKnownPrimes().size());
        loadedPrimeCalculator = new PrimeCalculator(tempFile.getAbsolutePath());

        assertEquals(expectedPrimes.size(), loadedPrimeCalculator.getKnownPrimes().size());
        List<Long> loadedPrimes = loadedPrimeCalculator.getKnownPrimes();

        for (int i = 0; i < expectedPrimes.size(); i++) {
            assertEquals(expectedPrimes.get(i), loadedPrimes.get(i));
        }
    }

    @Test
    public void testMultipleSaves_expectSaveOnlyWhenTheAmountOfPrimesIsGreater() throws IOException {
        File tempFile = File.createTempFile("writting_primes_test", ".txt");
        String tempFilePath = tempFile.getAbsolutePath();

        PrimeCalculator calculator = new PrimeCalculator(tempFilePath);
        assertEquals(2, calculator.getKnownPrimes().size());

        calculator.isPrime(25);
        List<Long> expectedPrimes = Stream.of(2, 3, 5).map(Long::valueOf).collect(Collectors.toList());
        calculator.savePrimes();

        calculator = new PrimeCalculator(tempFilePath);
        List<Long> loadedPrimes = calculator.getKnownPrimes();

        validateWithExpectedPrimes(expectedPrimes, loadedPrimes);

        calculator.isPrime(841);
        expectedPrimes = Stream.of(2, 3, 5, 7, 11, 13, 17, 19, 23, 29).map(Long::valueOf).collect(Collectors.toList());
        calculator.savePrimes();

        calculator = new PrimeCalculator(tempFilePath);
        loadedPrimes = calculator.getKnownPrimes();

        validateWithExpectedPrimes(expectedPrimes, loadedPrimes);


        calculator = new PrimeCalculator();
        expectedPrimes = Arrays.asList(2L, 3L);
        validateWithExpectedPrimes(expectedPrimes, calculator.getKnownPrimes());

        calculator.savePrimes(tempFilePath);

        calculator = new PrimeCalculator(tempFilePath);
        expectedPrimes = Stream.of(2, 3, 5, 7, 11, 13, 17, 19, 23, 29).map(Long::valueOf).collect(Collectors.toList());
        loadedPrimes = calculator.getKnownPrimes();

        validateWithExpectedPrimes(expectedPrimes, loadedPrimes);
    }

    @Test
    public void testNextPrime() {
        PrimeCalculator primeCalculator = new PrimeCalculator();

        assertEquals(2, primeCalculator.getKnownPrimes().size());

        assertEquals(2, primeCalculator.nextPrime(-1));
        assertEquals(2, primeCalculator.nextPrime(0));
        assertEquals(2, primeCalculator.nextPrime(1));
        assertEquals(3, primeCalculator.nextPrime(2));
        assertEquals(5, primeCalculator.nextPrime(3));
        assertEquals(5, primeCalculator.nextPrime(4));
        assertEquals(7, primeCalculator.nextPrime(5));
        assertEquals(7, primeCalculator.nextPrime(6));
        assertEquals(11, primeCalculator.nextPrime(7));
        assertEquals(11, primeCalculator.nextPrime(8));
        assertEquals(11, primeCalculator.nextPrime(9));
        assertEquals(11, primeCalculator.nextPrime(10));
        assertEquals(13, primeCalculator.nextPrime(11));
        assertEquals(13, primeCalculator.nextPrime(12));
        assertEquals(17, primeCalculator.nextPrime(13));
        assertEquals(17, primeCalculator.nextPrime(14));
        assertEquals(17, primeCalculator.nextPrime(15));
        assertEquals(17, primeCalculator.nextPrime(16));
        assertEquals(19, primeCalculator.nextPrime(17));
        assertEquals(19, primeCalculator.nextPrime(18));
        assertEquals(23, primeCalculator.nextPrime(19));
        assertEquals(23, primeCalculator.nextPrime(20));

        assertEquals(2, primeCalculator.nextPrime(-1));
        assertEquals(2, primeCalculator.nextPrime(0));
        assertEquals(2, primeCalculator.nextPrime(1));
        assertEquals(3, primeCalculator.nextPrime(2));
        assertEquals(5, primeCalculator.nextPrime(3));
        assertEquals(5, primeCalculator.nextPrime(4));
        assertEquals(7, primeCalculator.nextPrime(5));
        assertEquals(7, primeCalculator.nextPrime(6));
        assertEquals(11, primeCalculator.nextPrime(7));
        assertEquals(11, primeCalculator.nextPrime(8));
        assertEquals(11, primeCalculator.nextPrime(9));
        assertEquals(11, primeCalculator.nextPrime(10));
        assertEquals(13, primeCalculator.nextPrime(11));
        assertEquals(13, primeCalculator.nextPrime(12));
        assertEquals(17, primeCalculator.nextPrime(13));
        assertEquals(17, primeCalculator.nextPrime(14));
        assertEquals(17, primeCalculator.nextPrime(15));
        assertEquals(17, primeCalculator.nextPrime(16));
        assertEquals(19, primeCalculator.nextPrime(17));
        assertEquals(19, primeCalculator.nextPrime(18));
        assertEquals(23, primeCalculator.nextPrime(19));
        assertEquals(23, primeCalculator.nextPrime(20));
    }

    private void validateWithExpectedPrimes(List<Long> expected, List<Long> actual) {
        assertEquals(expected.size(), actual.size());

        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), actual.get(i));
        }
    }
}