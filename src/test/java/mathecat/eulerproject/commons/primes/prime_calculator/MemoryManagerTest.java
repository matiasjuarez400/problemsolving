package mathecat.eulerproject.commons.primes.prime_calculator;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class MemoryManagerTest {
    @Test
    public void testLoadPrimes() {
        MemoryManager memoryManager = new MemoryManager("src/test/java/mathecat/eulerproject/commons/primes/prime_calculator/primes_test_memory.txt");

        List<Long> loadedPrimes = memoryManager.loadPrimes();
        List<Long> expectedPrimes = Stream.of(2, 3, 5, 7, 11, 13).map(Long::valueOf).collect(Collectors.toList());

        assertEquals(expectedPrimes.size(), loadedPrimes.size());

        for (int i = 0; i < expectedPrimes.size(); i++) {
            assertEquals(expectedPrimes.get(i), loadedPrimes.get(i));
        }
    }

    @Test
    public void testSavePrimes() throws IOException {
        File tempFile = File.createTempFile("writting_primes_test", ".txt");

        MemoryManager memory = new MemoryManager(tempFile);
        assertEquals(0, memory.loadPrimes().size());

        memory.savePrimes(Arrays.asList(5L, 7L, 11L, 13L));

        List<Long> loadedPrimes = memory.loadPrimes();
        List<Long> expectedPrimes = Stream.of(5, 7, 11, 13).map(Long::valueOf).collect(Collectors.toList());


        assertEquals(expectedPrimes.size(), loadedPrimes.size());
        for (int i = 0; i < expectedPrimes.size(); i++) {
            assertEquals(expectedPrimes.get(i), loadedPrimes.get(i));
        }
    }
}