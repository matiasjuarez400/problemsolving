package mathecat.eulerproject.commons.primes;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PrimeUtilsWithMemoryTest {
    @Test
    public void testIsPrimeWithoutInitialRange() {
        PrimeUtilsWithMemory primeUtilsWithMemory = new PrimeUtilsWithMemory();

        assertTrue(primeUtilsWithMemory.isPrime(3));
        assertTrue(primeUtilsWithMemory.isPrime(2));
        assertFalse(primeUtilsWithMemory.isPrime(4));
        assertFalse(primeUtilsWithMemory.isPrime(20));
        assertTrue(primeUtilsWithMemory.isPrime(5));
        assertFalse(primeUtilsWithMemory.isPrime(0));
        assertFalse(primeUtilsWithMemory.isPrime(1));
        assertTrue(primeUtilsWithMemory.isPrime(113));
    }

    @Test
    public void testIsPrimeWithInitialRange() {
        PrimeUtilsWithMemory primeUtilsWithMemory = new PrimeUtilsWithMemory(200);

        assertTrue(primeUtilsWithMemory.isPrime(3));
        assertTrue(primeUtilsWithMemory.isPrime(2));
        assertFalse(primeUtilsWithMemory.isPrime(4));
        assertFalse(primeUtilsWithMemory.isPrime(20));
        assertTrue(primeUtilsWithMemory.isPrime(5));
        assertFalse(primeUtilsWithMemory.isPrime(0));
        assertFalse(primeUtilsWithMemory.isPrime(1));
        assertTrue(primeUtilsWithMemory.isPrime(113));
    }

    @Test
    public void testGetPrimesInRange() {
        PrimeUtilsWithMemory primeUtilsWithMemory = new PrimeUtilsWithMemory(200);

        List<Long> primes = primeUtilsWithMemory.getPrimesInRange(4, 20);

        assertEquals(6, primes.size());
        assertTrue(primes.containsAll(Arrays.asList(
                5L, 7L, 11L, 13L, 17L, 19L
        )));

        primes = primeUtilsWithMemory.getPrimesInRange(10, 20);
        assertEquals(4, primes.size());
        assertTrue(primes.containsAll(Arrays.asList(
                11L, 13L, 17L, 19L
        )));
    }

    @Test
    public void testGetPrimesFromDisjointRanges() {
        PrimeUtilsWithMemory primeUtilsWithMemory = new PrimeUtilsWithMemory();

        List<Long> primes = primeUtilsWithMemory.getPrimesInRange(1, 10);

        assertEquals(4, primes.size());
        assertTrue(primes.containsAll(Arrays.asList(
                2L, 3L, 5L, 7L
        )));

        primes = primeUtilsWithMemory.getPrimesInRange(3, 6);
        assertEquals(2, primes.size());
        assertTrue(primes.containsAll(Arrays.asList(
                3L, 5L
        )));

        primes = primeUtilsWithMemory.getPrimesInRange(30, 40);
        assertEquals(2, primes.size());
        assertTrue(primes.containsAll(Arrays.asList(
                31L, 37L
        )));

        primes = primeUtilsWithMemory.getPrimesInRange(30, 35);
        assertEquals(1, primes.size());
        assertTrue(primes.contains(31L));


        primes = primeUtilsWithMemory.getPrimesInRange(11, 29);
        assertEquals(6, primes.size());
        assertTrue(primes.containsAll(Arrays.asList(
                11L, 13L, 17L, 19L, 23L, 29L
        )));
    }

    @Test
    public void testGetPrimesFromDisjointRangesAndThenCreateOverlappingIntermediateRange() {
        PrimeUtilsWithMemory primeUtilsWithMemory = new PrimeUtilsWithMemory();

        List<Long> primes = primeUtilsWithMemory.getPrimesInRange(1, 10);

        assertEquals(4, primes.size());
        assertTrue(primes.containsAll(Arrays.asList(
                2L, 3L, 5L, 7L
        )));

        primes = primeUtilsWithMemory.getPrimesInRange(3, 6);
        assertEquals(2, primes.size());
        assertTrue(primes.containsAll(Arrays.asList(
                3L, 5L
        )));

        primes = primeUtilsWithMemory.getPrimesInRange(30, 40);
        assertEquals(2, primes.size());
        assertTrue(primes.containsAll(Arrays.asList(
                31L, 37L
        )));

        primes = primeUtilsWithMemory.getPrimesInRange(30, 35);
        assertEquals(1, primes.size());
        assertTrue(primes.contains(31L));


        primes = primeUtilsWithMemory.getPrimesInRange(7, 31);
        assertEquals(8, primes.size());
        assertTrue(primes.containsAll(Arrays.asList(
                7L, 11L, 13L, 17L, 19L, 23L, 29L, 31L
        )));
    }

    @Test
    public void testSearchInBiggerRangeComparedToTheOneStored() {
        PrimeUtilsWithMemory primeUtilsWithMemory = new PrimeUtilsWithMemory();

        List<Long> primes = primeUtilsWithMemory.getPrimesInRange(10, 20);

        assertEquals(4, primes.size());
        assertTrue(primes.containsAll(Arrays.asList(
                11L, 13L, 17L, 19L
        )));

        primes = primeUtilsWithMemory.getPrimesInRange(1, 30);

        assertEquals(10, primes.size());
        assertTrue(primes.containsAll(Arrays.asList(
                2L, 3L, 5L, 7L, 11L, 13L, 17L, 19L, 23L, 29L
        )));
    }

    @Test
    public void testSearchInOverlappingRangeToTheRightComparedToTheOneStored() {
        PrimeUtilsWithMemory primeUtilsWithMemory = new PrimeUtilsWithMemory();

        List<Long> primes = primeUtilsWithMemory.getPrimesInRange(10, 20);

        assertEquals(4, primes.size());
        assertTrue(primes.containsAll(Arrays.asList(
                11L, 13L, 17L, 19L
        )));

        primes = primeUtilsWithMemory.getPrimesInRange(17, 30);

        assertEquals(4, primes.size());
        assertTrue(primes.containsAll(Arrays.asList(
                17L, 19L, 23L, 29L
        )));
    }

    @Test
    public void testSearchInOverlappingRangeToTheLeftComparedToTheOneStored() {
        PrimeUtilsWithMemory primeUtilsWithMemory = new PrimeUtilsWithMemory();

        List<Long> primes = primeUtilsWithMemory.getPrimesInRange(10, 20);

        assertEquals(4, primes.size());
        assertTrue(primes.containsAll(Arrays.asList(
                11L, 13L, 17L, 19L
        )));

        primes = primeUtilsWithMemory.getPrimesInRange(2, 15);

        assertEquals(6, primes.size());
        assertTrue(primes.containsAll(Arrays.asList(
                2L, 3L, 5L, 7L, 11L, 13L
        )));
    }
}