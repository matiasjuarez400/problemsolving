package mathecat.eulerproject.commons;

import mathecat.eulerproject.commons.continuedFractions.ContinuedFractionExpander;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class ContinuedFractionExpanderTest {
    @Test
    public void testExpand() {
        assertArrayEquals(new long[] {1}, getExpansion(2, 1));
        assertArrayEquals(new long[] {1, 2}, getExpansion(2, 2));
        assertArrayEquals(new long[] {1, 2, 2}, getExpansion(2, 3));
        assertArrayEquals(new long[] {1, 2, 2, 2}, getExpansion(2, 4));

        assertArrayEquals(new long[] {1}, getExpansion(3, 1));
        assertArrayEquals(new long[] {1, 1}, getExpansion(3, 2));
        assertArrayEquals(new long[] {1, 1, 2}, getExpansion(3, 3));
        assertArrayEquals(new long[] {1, 1, 2, 1}, getExpansion(3, 4));
        assertArrayEquals(new long[] {1, 1, 2, 1, 2}, getExpansion(3, 5));

        assertArrayEquals(new long[] {2}, getExpansion(5, 1));
        assertArrayEquals(new long[] {2, 4}, getExpansion(5, 2));
        assertArrayEquals(new long[] {2, 4, 4}, getExpansion(5, 3));

        assertArrayEquals(new long[] {2}, getExpansion(6, 1));
        assertArrayEquals(new long[] {2, 2}, getExpansion(6, 2));
        assertArrayEquals(new long[] {2, 2, 4}, getExpansion(6, 3));
        assertArrayEquals(new long[] {2, 2, 4, 2}, getExpansion(6, 4));
        assertArrayEquals(new long[] {2, 2, 4, 2, 4}, getExpansion(6, 5));

        assertArrayEquals(new long[] {2}, getExpansion(7, 1));
        assertArrayEquals(new long[] {2, 1}, getExpansion(7, 2));
        assertArrayEquals(new long[] {2, 1, 1}, getExpansion(7, 3));
        assertArrayEquals(new long[] {2, 1, 1, 1}, getExpansion(7, 4));
        assertArrayEquals(new long[] {2, 1, 1, 1, 4}, getExpansion(7, 5));
        assertArrayEquals(new long[] {2, 1, 1, 1, 4, 1}, getExpansion(7, 6));
        assertArrayEquals(new long[] {2, 1, 1, 1, 4, 1, 1}, getExpansion(7, 7));
        assertArrayEquals(new long[] {2, 1, 1, 1, 4, 1, 1, 1}, getExpansion(7, 8));
        assertArrayEquals(new long[] {2, 1, 1, 1, 4, 1, 1, 1, 4}, getExpansion(7, 9));
        assertArrayEquals(new long[] {2, 1, 1, 1, 4, 1, 1, 1, 4, 1}, getExpansion(7, 10));

        assertArrayEquals(new long[] {2, 1, 4, 1, 4, 1, 4}, getExpansion(8, 7));
        assertArrayEquals(new long[] {3, 6, 6, 6, 6, 6, 6}, getExpansion(10, 7));
        assertArrayEquals(new long[] {3, 3, 6, 3, 6, 3, 6}, getExpansion(11, 7));
        assertArrayEquals(new long[] {3, 2, 6, 2, 6, 2, 6}, getExpansion(12, 7));

        assertArrayEquals(new long[] {3, 1, 1, 1, 1, 6, 1, 1, 1, 1, 6, 1, 1, 1, 1, 6, 1, 1, 1, 1, 6, 1, 1, 1, 1, 6, 1, 1, 1, 1, 6, 1, 1, 1, 1, 6, 1, 1, 1, 1, 6},
                getExpansion(13, 41));
    }

    @Test
    public void testExpandSqrt() {
        assertArrayEquals(new long[] {1}, getExpansionSqrt(2, 1));
        assertArrayEquals(new long[] {1, 2}, getExpansionSqrt(2, 2));
        assertArrayEquals(new long[] {1, 2, 2}, getExpansionSqrt(2, 3));
        assertArrayEquals(new long[] {1, 2, 2, 2}, getExpansionSqrt(2, 4));

        assertArrayEquals(new long[] {1}, getExpansionSqrt(3, 1));
        assertArrayEquals(new long[] {1, 1}, getExpansionSqrt(3, 2));
        assertArrayEquals(new long[] {1, 1, 2}, getExpansionSqrt(3, 3));
        assertArrayEquals(new long[] {1, 1, 2, 1}, getExpansionSqrt(3, 4));
        assertArrayEquals(new long[] {1, 1, 2, 1, 2}, getExpansionSqrt(3, 5));

        assertArrayEquals(new long[] {2}, getExpansionSqrt(5, 1));
        assertArrayEquals(new long[] {2, 4}, getExpansionSqrt(5, 2));
        assertArrayEquals(new long[] {2, 4, 4}, getExpansionSqrt(5, 3));

        assertArrayEquals(new long[] {2}, getExpansionSqrt(6, 1));
        assertArrayEquals(new long[] {2, 2}, getExpansionSqrt(6, 2));
        assertArrayEquals(new long[] {2, 2, 4}, getExpansionSqrt(6, 3));
        assertArrayEquals(new long[] {2, 2, 4, 2}, getExpansionSqrt(6, 4));
        assertArrayEquals(new long[] {2, 2, 4, 2, 4}, getExpansionSqrt(6, 5));

        assertArrayEquals(new long[] {2}, getExpansionSqrt(7, 1));
        assertArrayEquals(new long[] {2, 1}, getExpansionSqrt(7, 2));
        assertArrayEquals(new long[] {2, 1, 1}, getExpansionSqrt(7, 3));
        assertArrayEquals(new long[] {2, 1, 1, 1}, getExpansionSqrt(7, 4));
        assertArrayEquals(new long[] {2, 1, 1, 1, 4}, getExpansionSqrt(7, 5));
        assertArrayEquals(new long[] {2, 1, 1, 1, 4, 1}, getExpansionSqrt(7, 6));
        assertArrayEquals(new long[] {2, 1, 1, 1, 4, 1, 1}, getExpansionSqrt(7, 7));
        assertArrayEquals(new long[] {2, 1, 1, 1, 4, 1, 1, 1}, getExpansionSqrt(7, 8));
        assertArrayEquals(new long[] {2, 1, 1, 1, 4, 1, 1, 1, 4}, getExpansionSqrt(7, 9));
        assertArrayEquals(new long[] {2, 1, 1, 1, 4, 1, 1, 1, 4, 1}, getExpansionSqrt(7, 10));

        assertArrayEquals(new long[] {2, 1, 4, 1, 4, 1, 4}, getExpansionSqrt(8, 7));
        assertArrayEquals(new long[] {3, 6, 6, 6, 6, 6, 6}, getExpansionSqrt(10, 7));
        assertArrayEquals(new long[] {3, 3, 6, 3, 6, 3, 6}, getExpansionSqrt(11, 7));
        assertArrayEquals(new long[] {3, 2, 6, 2, 6, 2, 6}, getExpansionSqrt(12, 7));

        assertArrayEquals(new long[] {3, 1, 1, 1, 1, 6, 1, 1, 1, 1, 6, 1, 1, 1, 1, 6, 1, 1, 1, 1, 6, 1, 1, 1, 1, 6, 1, 1, 1, 1, 6, 1, 1, 1, 1, 6, 1, 1, 1, 1, 6},
                getExpansionSqrt(13, 41));
    }

    private long[] getExpansionSqrt(long input, int iterations) {
        return ContinuedFractionExpander.expandSqrt(input, 600, iterations, 200);
    }

    private long[] getExpansion(double input, int iterations) {
        return ContinuedFractionExpander.expand(BasicMath.root(BigDecimal.valueOf(input), 600, 2, 200), iterations);
    }
}