package mathecat.eulerproject.commons;

import mathecat.eulerproject.commons.combination.CombinationUtils;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.*;

public class CombinationUtilsTest {
    @Test
    public void testCombinations() {
        assertEquals(BigInteger.valueOf(10), CombinationUtils.combinatorics(5, 3));
        assertEquals(BigInteger.valueOf(1144066), CombinationUtils.combinatorics(23, 10));
    }

    @Test
    public void testMaxCombinatorics() {
        assertEquals(BigInteger.valueOf(184756), CombinationUtils.maxCombinatorics(20));
        assertEquals(BigInteger.valueOf(92378), CombinationUtils.maxCombinatorics(19));
        assertEquals(BigInteger.valueOf(48620), CombinationUtils.maxCombinatorics(18));
        assertEquals(BigInteger.valueOf(24310), CombinationUtils.maxCombinatorics(17));

        assertEquals(BigInteger.valueOf(1), CombinationUtils.maxCombinatorics(1));
        assertEquals(BigInteger.valueOf(2), CombinationUtils.maxCombinatorics(2));
        assertEquals(BigInteger.valueOf(3), CombinationUtils.maxCombinatorics(3));
        assertEquals(BigInteger.valueOf(6), CombinationUtils.maxCombinatorics(4));
    }

    @Test
    public void findRForMaxCombinatorics() {
        for (int n = 1; n <= 20; n++) {
            BigInteger maxValue = BigInteger.ZERO;
            int bestR = -1;
            for (int r = 1;  r <= n; r++) {
                BigInteger combinatoric = CombinationUtils.combinatorics(n, r);
                if (maxValue.compareTo(combinatoric) < 0) {
                    maxValue = combinatoric;
                    bestR = r;
                }
            }

            System.out.printf("N: %s. R: %s\n", n, bestR);
        }
    }

    @Test
    public void printCombinatorics() {
        for (int n = 1; n <= 20; n++) {
            System.out.println("------------------------------------------");
            System.out.println("N: " + n);
            for (int r = 1;  r <= n; r++) {
                BigInteger combinatoric = CombinationUtils.combinatorics(n, r);

                System.out.printf("R %02d: %s\n", r, combinatoric);
            }

            System.out.println("------------------------------------------");
        }
    }
}