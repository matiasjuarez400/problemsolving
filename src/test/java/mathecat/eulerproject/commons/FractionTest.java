package mathecat.eulerproject.commons;

import mathecat.eulerproject.commons.concepts.Fraction;
import org.junit.Test;

import static org.junit.Assert.*;

public class FractionTest {
    @Test
    public void testAdd() {
        Fraction left = Fraction.of(2, 3);
        Fraction right = Fraction.of(4, 5);

        Fraction addition = left.add(right);
        validateFraction(22, 15, addition);


        left = Fraction.of(3, 7);
        right = Fraction.of(6, 7);

        addition = left.add(right);
        validateFraction(9, 7, addition);
    }

    @Test
    public void testDiff() {
        Fraction left = Fraction.of(2, 3);
        Fraction right = Fraction.of(4, 5);

        validateFraction(-2, 15, left.diff(right));
        validateFraction(2, 15, right.diff(left));


        left = Fraction.of(3, 7);
        right = Fraction.of(6, 7);

        validateFraction(-3, 7, left.diff(right));
        validateFraction(3, 7, right.diff(left));
    }

    @Test
    public void testTimes() {
        Fraction left = Fraction.of(2, 3);
        Fraction right = Fraction.of(4, 5);

        validateFraction(8, 15, left.times(right));


        left = Fraction.of(3, 7);
        right = Fraction.of(6, 7);

        validateFraction(18, 49, left.times(right));
    }

    @Test
    public void testDivide() {
        Fraction left = Fraction.of(2, 3);
        Fraction right = Fraction.of(4, 5);

        validateFraction(5, 6, left.divide(right));


        left = Fraction.of(3, 7);
        right = Fraction.of(6, 7);

        validateFraction(1, 2, left.divide(right));
    }

    private void validateFraction(long expectedNumerator, long expectedDenominator, Fraction fraction) {
        assertEquals(expectedNumerator, fraction.getNumerator());
        assertEquals(expectedDenominator, fraction.getDenominator());
    }
}