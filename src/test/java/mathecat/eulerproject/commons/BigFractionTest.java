package mathecat.eulerproject.commons;

import mathecat.eulerproject.commons.concepts.BigFraction;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;

import static org.junit.Assert.*;

public class BigFractionTest {
    @Test
    public void testAdd() {
        BigFraction left = BigFraction.of(BigInteger.valueOf(2), BigInteger.valueOf(3));
        BigFraction right = BigFraction.of(BigInteger.valueOf(4), BigInteger.valueOf(5));

        BigFraction addition = left.add(right);
        validateFraction(22, 15, addition);


        left = BigFraction.of(BigInteger.valueOf(3), BigInteger.valueOf(7));
        right = BigFraction.of(BigInteger.valueOf(6), BigInteger.valueOf(7));

        addition = left.add(right);
        validateFraction(9, 7, addition);
    }

    @Test
    public void testDiff() {
        BigFraction left = BigFraction.of(BigInteger.valueOf(2), BigInteger.valueOf(3));
        BigFraction right = BigFraction.of(BigInteger.valueOf(4), BigInteger.valueOf(5));

        validateFraction(-2, 15, left.diff(right));
        validateFraction(2, 15, right.diff(left));


        left = BigFraction.of(BigInteger.valueOf(3), BigInteger.valueOf(7));
        right = BigFraction.of(BigInteger.valueOf(6), BigInteger.valueOf(7));

        validateFraction(-3, 7, left.diff(right));
        validateFraction(3, 7, right.diff(left));
    }

    @Test
    public void testTimes() {
        BigFraction left = BigFraction.of(BigInteger.valueOf(2), BigInteger.valueOf(3));
        BigFraction right = BigFraction.of(BigInteger.valueOf(4), BigInteger.valueOf(5));

        validateFraction(8, 15, left.times(right));


        left = BigFraction.of(BigInteger.valueOf(3), BigInteger.valueOf(7));
        right = BigFraction.of(BigInteger.valueOf(6), BigInteger.valueOf(7));

        validateFraction(18, 49, left.times(right));
    }

    @Test
    public void testDivide() {
        BigFraction left = BigFraction.of(BigInteger.valueOf(2), BigInteger.valueOf(3));
        BigFraction right = BigFraction.of(BigInteger.valueOf(4), BigInteger.valueOf(5));

        validateFraction(10, 12, left.divide(right));


        left = BigFraction.of(BigInteger.valueOf(3), BigInteger.valueOf(7));
        right = BigFraction.of(BigInteger.valueOf(6), BigInteger.valueOf(7));

        validateFraction(21, 42, left.divide(right));
    }

    @Test
    public void testToDecimal() {
        double delta = 0.0001;

        assertEquals(231, new BigFraction(231, 1).toDecimal().doubleValue(), delta);
        assertEquals(23.1, new BigFraction(231, 10).toDecimal().doubleValue(), delta);
        assertEquals(2.31, new BigFraction(231, 100).toDecimal().doubleValue(), delta);
        assertEquals(0.231, new BigFraction(231, 1000).toDecimal().doubleValue(), delta);

        assertEquals(2.32, new BigFraction(58, 25).toDecimal().doubleValue(), delta);
    }

    @Test
    public void testCreateFromDecimal() {
        validateFractionFromDecimal(23, 1, 23.0);
        validateFractionFromDecimal(231, 10, 23.1);
        validateFractionFromDecimal(116, 5, 23.2);
    }

    @Test
    public void testInvert() {
        validateFraction(2, 5, BigFraction.of(5, 2).invert());
        validateFraction(2, 1, BigFraction.of(5, 10).invert());
    }

    @Test
    public void testDivideAndRemainder() {
        validateDivideAndRemainder(2, 1, 2, BigFraction.of(5, 2).divideAndRemainder());
        validateDivideAndRemainder(0, 1, 2, BigFraction.of(5, 10).divideAndRemainder());
        validateDivideAndRemainder(3, 17, 50, BigFraction.of(334, 100).divideAndRemainder());
        validateDivideAndRemainder(1, 1, 500, BigFraction.of(501, 500).divideAndRemainder());
        validateDivideAndRemainder(2, 0, 1, BigFraction.of(6, 3).divideAndRemainder());
    }

    private void validateDivideAndRemainder(long expectedInteger, long expectedRemainedNumerator, long expectedRemainderDenominator, BigFraction[] actualResult) {
        assertEquals(expectedInteger, actualResult[0].getNumerator().longValue());
        assertEquals(1, actualResult[0].getDenominator().longValue());
        assertEquals(expectedRemainedNumerator, actualResult[1].getNumerator().longValue());
        assertEquals(expectedRemainderDenominator, actualResult[1].getDenominator().longValue());
    }

    private void validateFractionFromDecimal(long expectedNumerator, long expectedDenominator, double original) {
        BigDecimal input = BigDecimal.valueOf(original);
        BigFraction fraction = BigFraction.of(input);

        assertEquals(original, fraction.toDecimal().doubleValue(), 0.001);

        validateFraction(expectedNumerator, expectedDenominator, fraction);
    }

    private void validateFraction(long expectedNumerator, long expectedDenominator, BigFraction fraction) {
        assertEquals(BigInteger.valueOf(expectedNumerator), fraction.getNumerator());
        assertEquals(BigInteger.valueOf(expectedDenominator), fraction.getDenominator());
    }
}