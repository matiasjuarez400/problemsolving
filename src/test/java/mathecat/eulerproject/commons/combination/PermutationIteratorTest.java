package mathecat.eulerproject.commons.combination;

import mathecat.eulerproject.commons.BasicMath;
import mathecat.eulerproject.commons.permutation.PermutationIterator;
import mathecat.eulerproject.commons.permutation.PermutationUtils;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class PermutationIteratorTest {
    // REPETITIONS
    @Test
    public void testGetAllPossibleCombinations_withRepetitions() {
        List<Integer> values = Arrays.asList(4, 3, 2);
        int combinationSize = 3;

        PermutationIterator<Integer> iterator = new PermutationIterator<>(values, combinationSize);

        Set<String> combinationsFound = new HashSet<>();

        while (iterator.hasNext()) {
            List<Integer> nextCombination = iterator.next();
            String nextCombinationString = nextCombination.stream().map(Objects::toString).collect(Collectors.joining());
            combinationsFound.add(nextCombinationString);
        }

        assertEquals(BasicMath.pow(values.size(), combinationSize), combinationsFound.size());
    }

    @Test
    public void testGetAllPossibleCombinations_withCombinationSizeGreaterThanTotalElements_withRepetitions() {
        List<Integer> values = Arrays.asList(4, 3, 2);
        int combinationSize = 4;

        PermutationIterator<Integer> iterator = new PermutationIterator<>(values, combinationSize);

        Set<String> combinationsFound = new HashSet<>();

        while (iterator.hasNext()) {
            List<Integer> nextCombination = iterator.next();
            String nextCombinationString = nextCombination.stream().map(Objects::toString).collect(Collectors.joining());
            combinationsFound.add(nextCombinationString);
        }

        assertEquals(BasicMath.pow(values.size(), combinationSize), combinationsFound.size());
    }

    @Test
    public void testGetAllPossibleCombinations_withCombinationSizeLessThanTotalElements_withRepetitions() {
        List<Integer> values = Arrays.asList(4, 3, 2);
        int combinationSize = 2;

        PermutationIterator<Integer> iterator = new PermutationIterator<>(values, combinationSize);

        Set<String> combinationsFound = new HashSet<>();

        while (iterator.hasNext()) {
            List<Integer> nextCombination = iterator.next();
            String nextCombinationString = nextCombination.stream().map(Objects::toString).collect(Collectors.joining());
            combinationsFound.add(nextCombinationString);
        }

        assertEquals(BasicMath.pow(values.size(), combinationSize), combinationsFound.size());
    }

    // NO REPETITIONS
    @Test
    public void testGetAllPossibleCombinations_noRepetitions() {
        List<Integer> values = Arrays.asList(4, 3, 2);
        int combinationSize = 3;

        PermutationIterator<Integer> iterator = new PermutationIterator<>(values, combinationSize, false);

        Set<String> combinationsFound = new HashSet<>();

        while (iterator.hasNext()) {
            List<Integer> nextCombination = iterator.next();
            String nextCombinationString = nextCombination.stream().map(Objects::toString).collect(Collectors.joining());
            combinationsFound.add(nextCombinationString);
        }

        assertEquals(PermutationUtils.getTotalPermutations(values.size(), combinationSize, false).intValue(), combinationsFound.size());
    }

    @Test
    public void testGetAllPossibleCombinations_withCombinationSizeLessThanTotalElements_noRepetitions() {
        List<Integer> values = Arrays.asList(4, 3, 2);
        int combinationSize = 2;

        PermutationIterator<Integer> iterator = new PermutationIterator<>(values, combinationSize, false);

        Set<String> combinationsFound = new HashSet<>();

        while (iterator.hasNext()) {
            List<Integer> nextCombination = iterator.next();
            String nextCombinationString = nextCombination.stream().map(Objects::toString).collect(Collectors.joining());
            combinationsFound.add(nextCombinationString);
        }

        assertEquals(PermutationUtils.getTotalPermutations(values.size(), combinationSize, false).intValue(), combinationsFound.size());
    }

    @Test
    public void testGetAllPossibleCombinations_withCombinationSizeLessThanTotalElements_noRepetitions_4elements() {
        List<Integer> values = Arrays.asList(4, 3, 2, 1);
        int combinationSize = 2;

        PermutationIterator<Integer> iterator = new PermutationIterator<>(values, combinationSize, false);

        Set<String> combinationsFound = new HashSet<>();

        while (iterator.hasNext()) {
            List<Integer> nextCombination = iterator.next();
            String nextCombinationString = nextCombination.stream().map(Objects::toString).collect(Collectors.joining());
            combinationsFound.add(nextCombinationString);
        }

        assertEquals(PermutationUtils.getTotalPermutations(values.size(), combinationSize, false).intValue(), combinationsFound.size());
    }
}