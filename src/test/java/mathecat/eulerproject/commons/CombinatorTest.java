package mathecat.eulerproject.commons;

import junit.framework.TestCase;
import mathecat.eulerproject.commons.combination.Combinator;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CombinatorTest extends TestCase {
    @Test
    public void testFindPermutationsInputSize3() {
        Combinator<Integer> combinator = new Combinator<>();

        List<List<Integer>> permutations = combinator.findPermutations(Arrays.asList(1, 2, 3));

        Set<String> expectedPermutations = new HashSet<>(Arrays.asList(
           "123", "132",
           "213", "231",
           "312", "321"
        ));

        Set<String> createdPermutations = permutations.stream()
                .map(permutationList -> permutationList.stream()
                    .map(Object::toString)
                    .collect(Collectors.joining())
                ).collect(Collectors.toSet());

        assertEquals(expectedPermutations.size(), createdPermutations.size());

        for (String expectedPermutation : expectedPermutations) {
            assertTrue(String.format("Permutation %s is not contained in generated permutations", expectedPermutation),
                    createdPermutations.contains(expectedPermutation));
        }
    }

    @Test
    public void testFindPermutationsInputSize4() {
        Combinator<Integer> combinator = new Combinator<>();

        List<List<Integer>> permutations = combinator.findPermutations(Arrays.asList(1, 2, 3, 4));

        Set<String> expectedPermutations = new HashSet<>(Arrays.asList(
                "1234", "1243", "1324", "1342", "1423", "1432",
                "2134", "2143", "2314", "2341", "2413", "2431",
                "3124", "3142", "3214", "3241", "3412", "3421",
                "4123", "4132", "4213", "4231", "4312", "4321"
        ));

        Set<String> createdPermutations = permutations.stream()
                .map(permutationList -> permutationList.stream()
                        .map(Object::toString)
                        .collect(Collectors.joining())
                ).collect(Collectors.toSet());

        assertEquals(expectedPermutations.size(), createdPermutations.size());

        for (String expectedPermutation : expectedPermutations) {
            assertTrue(String.format("Permutation %s is not contained in generated permutations", expectedPermutation),
                    createdPermutations.contains(expectedPermutation));
        }
    }
}