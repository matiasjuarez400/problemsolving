package mathecat.eulerproject.commons.memory;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class RangeMemoryTest {

    @Test
    public void size() {
        RangeMemory rangeMemory = new RangeMemory(10);

        rangeMemory.add(5L);
        assertEquals(1, rangeMemory.size());

        rangeMemory.add(7L);
        assertEquals(2, rangeMemory.size());

        rangeMemory.add(879L);
        assertEquals(3, rangeMemory.size());

        rangeMemory.remove(2L);
        assertEquals(3, rangeMemory.size());

        rangeMemory.remove(7L);
        assertEquals(2, rangeMemory.size());
    }

    @Test
    public void contains() {
        RangeMemory rangeMemory = new RangeMemory(10);
        assertFalse(rangeMemory.contains(5L));

        rangeMemory.add(5L);
        assertTrue(rangeMemory.contains(5L));

        rangeMemory.remove(7L);
        assertTrue(rangeMemory.contains(5L));

        rangeMemory.remove(5L);
        assertFalse(rangeMemory.contains(5L));
    }

    @Test
    public void add() {
        RangeMemory rangeMemory = new RangeMemory(10);

        for (long i = 1; i <= 100; i++) {
            assertTrue(rangeMemory.add(i));
        }

        assertEquals(100, rangeMemory.size());
    }

    @Test
    public void remove() {
        RangeMemory rangeMemory = new RangeMemory(10);
        assertFalse(rangeMemory.remove(9L));

        rangeMemory.add(9L);
        assertTrue(rangeMemory.remove(9L));
        assertFalse(rangeMemory.remove(9L));
    }

    @Test
    public void get() {
        RangeMemory rangeMemory = new RangeMemory(10);

        rangeMemory.addAll(Arrays.asList(2L, 20L, 13L, 3L, 10L, 14L, 21L));

        assertEquals(7, rangeMemory.size());

        assertEquals(2L, (long) rangeMemory.get(0));
        assertEquals(3L, (long) rangeMemory.get(1));
        assertEquals(10L, (long) rangeMemory.get(2));
        assertEquals(13L, (long) rangeMemory.get(3));
        assertEquals(14L, (long) rangeMemory.get(4));
        assertEquals(20L, (long) rangeMemory.get(5));
        assertEquals(21L, (long) rangeMemory.get(6));

        try {
            rangeMemory.get(7);
            fail("Index is out of founds. Should fail");
        } catch (Exception e) {
            assertEquals("Element count: 7. Asked for index: 7", e.getMessage());
        }
    }

    @Test
    public void getRange() {
        RangeMemory rangeMemory = new RangeMemory(10);

        // 2, 3, 10, 13, 14, 20, 21
        rangeMemory.addAll(Arrays.asList(2L, 20L, 13L, 3L, 10L, 14L, 21L));

        assertEquals(7, rangeMemory.size());

        assertCollection(Arrays.asList(3L, 10L), rangeMemory.get(1, 3));
        assertCollection(Arrays.asList(3L, 10L, 13L), rangeMemory.get(1, 4));
        assertCollection(Arrays.asList(3L, 10L, 13L, 14L, 20L, 21L), rangeMemory.get(1, 7));
        assertCollection(Arrays.asList(3L, 10L, 13L, 14L, 20L, 21L), rangeMemory.get(1, rangeMemory.size()));
        assertCollection(Arrays.asList(2L, 3L, 10L, 13L, 14L, 20L, 21L), rangeMemory.get(0, rangeMemory.size()));

        try {
            rangeMemory.get(0, rangeMemory.size() + 1);
            fail("'end' is out of range");
        } catch (Exception e) {
            assertEquals("Element count: 7. Asked for indexes: 0 to 8", e.getMessage());
        }
    }

    private void assertCollection(List<Long> expected, Collection<Long> actual) {
        assertEquals(expected.size(), actual.size());

        List<Long> actualList = new ArrayList<>(actual);
        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), actualList.get(i));
        }
    }

    @Test
    public void doGetElements() {
        RangeMemory rangeMemory = new RangeMemory(10);

        // 2, 3, 10, 13, 14, 20, 21
        List<Long> elementsAdded = Arrays.asList(2L, 3L, 10L, 13L, 14L, 20L, 21L);
        rangeMemory.addAll(elementsAdded);

        assertCollection(elementsAdded, rangeMemory.doGetElements());
    }

    @Test
    public void getElementsForRange() {
        RangeMemory rangeMemory = new RangeMemory(10);

        // 2, 3, 10, 13, 14, 20, 21
        List<Long> elementsAdded = Arrays.asList(2L, 3L, 10L, 13L, 14L, 20L, 21L);
        rangeMemory.addAll(elementsAdded);

        assertCollection(Arrays.asList(2L, 3L), rangeMemory.getElementsForRange(0L));
        assertCollection(Arrays.asList(10L, 13L, 14L), rangeMemory.getElementsForRange(1L));
        assertCollection(Arrays.asList(20L, 21L), rangeMemory.getElementsForRange(2L));

        assertCollection(Collections.emptyList(), rangeMemory.getElementsForRange(-1L));
        assertCollection(Collections.emptyList(), rangeMemory.getElementsForRange(3L));
    }
}