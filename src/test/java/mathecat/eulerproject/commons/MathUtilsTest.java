package mathecat.eulerproject.commons;

import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.*;

public class MathUtilsTest {
    private MathUtils mathUtils = new MathUtils();

    @Test
    public void testFindBase() {
        validateFindBase(2, 4, 2);
        validateFindBase(3, 9, 2);
        validateFindBase(4, 16, 2);
        validateFindBase(5, 25, 2);

        validateFindBase(2, 8, 3);
        validateFindBase(3, 27, 3);
        validateFindBase(4, 64, 3);
        validateFindBase(5, 125, 3);

        assertNull(mathUtils.findBase(BigInteger.valueOf(3), 2));
        assertNull(mathUtils.findBase(BigInteger.valueOf(200), 4));
    }

    private void validateFindBase(long expectedBase, long inputValue, int power) {
        BigInteger foundBase = mathUtils.findBase(BigInteger.valueOf(inputValue), power);

        assertEquals(expectedBase, foundBase.longValue());
    }
}