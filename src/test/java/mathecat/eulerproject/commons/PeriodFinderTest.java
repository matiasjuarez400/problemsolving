package mathecat.eulerproject.commons;

import mathecat.eulerproject.commons.continuedFractions.ContinuedFractionExpander;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PeriodFinderTest {
    @Test
    public void findLongestPeriod7312() {
        TestInput testInput = createTestInput(7312, 60000, 600, 601);

        String expectedIndexes = "85,1,1,23,1,13,3,2,2,1,1,3,18,1,2,1,1,1,1,2,9,1,2,10,2,1,9,2,1,1,1,1,2,1,18,3,1,1,2,2,3,13,1,23,1,1,170";

        long[] foundIndexes = PeriodFinder.findLongestPeriod(testInput.getExpansionIndexes());

        validateIndexes(expectedIndexes, foundIndexes);
    }

    @Test
    public void findLongestPeriod7313() {
        TestInput testInput = createTestInput(7313, 60000, 600, 601);

        String expectedIndexes = "85,1,1,15,21,3,5,1,1,3,10,2,2,5,8,1,4,2,4,1,8,5,2,2,10,3,1,1,5,3,21,15,1,1,170";

        long[] foundIndexes = PeriodFinder.findLongestPeriod(testInput.getExpansionIndexes());

        validateIndexes(expectedIndexes, foundIndexes);
    }

    @Test
    public void findLongestPeriod2810() {
        TestInput testInput = createTestInput(2810, 100000, 1000, 1001);

        String expectedIndexes = "85,1,1,15,21,3,5,1,1,3,10,2,2,5,8,1,4,2,4,1,8,5,2,2,10,3,1,1,5,3,21,15,1,1,170";

        long[] foundIndexes = PeriodFinder.findLongestPeriod(testInput.getExpansionIndexes(), 0.15);

        validateIndexes(expectedIndexes, foundIndexes);
    }

    @Test
    public void findLongestPeriod9706() {
        TestInput testInput = createTestInput(9706, 60000, 600, 601);

        String expectedIndexes = "98,1,1,12,1,1,1,2,1,3,1,27,2,1,3,1,1,11,32,1,3,19,2,4,1,2,3,4,2,1,1,5,2,1,1,1,2,1,21,5,1,12,3,3,7,1,1,2,1,1,2,8,1,1,3,8,3,1,1,8,2,1,1,2,1,1,7,3,3,12,1,5,21,1,2,1,1,1,2,5,1,1,2,4,3,2,1,4,2,19,3,1,32,11,1,1,3,1,2,27,1,3,1,2,1,1,1,12,1,1,196";

        long[] foundIndexes = PeriodFinder.findLongestPeriod(testInput.getExpansionIndexes());

        validateIndexes(expectedIndexes, foundIndexes);
    }

    @Test
    public void findLongestPeriodWithMatchingRatio9706() {
        TestInput testInput = createTestInput(9706, 60000, 600, 601);

        String expectedIndexes = "98,1,1,12,1,1,1,2,1,3,1,27,2,1,3,1,1,11,32,1,3,19,2,4,1,2,3,4,2,1,1,5,2,1,1,1,2,1,21,5,1,12,3,3,7,1,1,2,1,1,2,8,1,1,3,8,3,1,1,8,2,1,1,2,1,1,7,3,3,12,1,5,21,1,2,1,1,1,2,5,1,1,2,4,3,2,1,4,2,19,3,1,32,11,1,1,3,1,2,27,1,3,1,2,1,1,1,12,1,1,196";

        validateIndexes(expectedIndexes, PeriodFinder.findLongestPeriod(testInput.getExpansionIndexes(), 0));
        validateIndexes(expectedIndexes, PeriodFinder.findLongestPeriod(testInput.getExpansionIndexes(), 0.1));
        validateIndexes(expectedIndexes, PeriodFinder.findLongestPeriod(testInput.getExpansionIndexes(), 0.2));
        validateIndexes(expectedIndexes, PeriodFinder.findLongestPeriod(testInput.getExpansionIndexes(), 0.3));
        validateIndexes("", PeriodFinder.findLongestPeriod(testInput.getExpansionIndexes(), 0.4));
    }

    private void validateIndexes(String expectedPeriod, long[] foundPeriod) {
        String foundPeriodString = Arrays.stream(foundPeriod).boxed().map(Objects::toString).collect(Collectors.joining(","));
        assertEquals(expectedPeriod, foundPeriodString);
    }

    private TestInput createTestInput(int number, int rootIterations, int rootPrecision, int expansions) {
        BigDecimal sqrtValue = BasicMath.root(BigDecimal.valueOf(number), rootIterations, 2, rootPrecision);

        long[] expansionIndexes = ContinuedFractionExpander.expand(sqrtValue, expansions);

        return new TestInput(sqrtValue, expansionIndexes);
    }

    private static class TestInput {
        private final BigDecimal sqrt;
        private final long[] expansionIndexes;
        private final String concatenatedIndexes;

        public TestInput(BigDecimal sqrt, long[] expansionIndexes) {
            this.sqrt = sqrt;
            this.expansionIndexes = expansionIndexes;
            this.concatenatedIndexes = Arrays.stream(expansionIndexes).boxed().map(Objects::toString).collect(Collectors.joining(","));
        }

        public BigDecimal getSqrt() {
            return sqrt;
        }

        public long[] getExpansionIndexes() {
            return expansionIndexes;
        }

        public String getConcatenatedIndexes() {
            return concatenatedIndexes;
        }

        @Override
        public String toString() {
            return concatenatedIndexes;
        }
    }
}