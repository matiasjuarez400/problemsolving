package mathecat.eulerproject._1._61CyclicalFigurateNumbers;

import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    @Test
    public void solve_size4() {
        Solution solution = new Solution();

        long result = solution.solve(4, 3);

        assertEquals(8128 + 2882 + 8281, result);
    }

    @Test
    public void solve_size6() {
        Solution solution = new Solution();

        long result = solution.solve(4, 6);

        assertEquals(28684, result);
    }

}