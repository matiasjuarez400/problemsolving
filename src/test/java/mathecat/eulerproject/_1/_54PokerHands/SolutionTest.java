package mathecat.eulerproject._1._54PokerHands;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.Assert.*;

public class SolutionTest {
    private Solution solution = new Solution();

    @Test
    public void testSolve() throws IOException {
        assertEquals(0, solve("5H 5C 6S 7S KD 2C 3S 8S 8D TD"));
        assertEquals(1, solve("5D 8C 9S JS AC 2C 5C 7D 8S QH"));
        assertEquals(0, solve("2D 9C AS AH AC 3D 6D 7D TD QD"));
        assertEquals(1, solve("4D 6S 9H QH QC 3D 6D 7H QD QS"));
        assertEquals(1, solve("2H 2D 4C 4D 4S 3C 3D 3S 9S 9D"));
    }

    private int solve(String input) throws IOException {
        return solution.solve(createFileFromContent(input));
    }

    private File createFileFromContent(String content) throws IOException {
        Path tempFile = Files.createTempFile(null, null);

        Files.write(tempFile, content.getBytes(StandardCharsets.UTF_8));

        return tempFile.toFile();
    }
}