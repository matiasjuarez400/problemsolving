package mathecat.eulerproject._1._54PokerHands.rank_analyzer;

import mathecat.eulerproject._1._54PokerHands.InputParser;
import mathecat.eulerproject._1._54PokerHands.model.Card;
import mathecat.eulerproject._1._54PokerHands.model.Hand;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static mathecat.eulerproject._1._54PokerHands.model.RankType.FLUSH;
import static mathecat.eulerproject._1._54PokerHands.model.RankType.FOUR_OF_A_KIND;
import static mathecat.eulerproject._1._54PokerHands.model.RankType.FULL_HOUSE;
import static mathecat.eulerproject._1._54PokerHands.model.RankType.HIGH_CARD;
import static mathecat.eulerproject._1._54PokerHands.model.RankType.ONE_PAIR;
import static mathecat.eulerproject._1._54PokerHands.model.RankType.ROYAL_FLUSH;
import static mathecat.eulerproject._1._54PokerHands.model.RankType.STRAIGHT;
import static mathecat.eulerproject._1._54PokerHands.model.RankType.STRAIGHT_FLUSH;
import static mathecat.eulerproject._1._54PokerHands.model.RankType.THREE_OF_A_KIND;
import static mathecat.eulerproject._1._54PokerHands.model.RankType.TWO_PAIRS;
import static org.junit.Assert.assertEquals;

public class RankAnalyzerTest {
    @Test
    public void testAnalyze() {
        RankAnalyzer analyzer = new RankAnalyzer();

        assertEquals(ONE_PAIR, analyzer.analyze(create("5H 5C 6S 7S KD")).getRankType());
        assertEquals(ONE_PAIR, analyzer.analyze(create("2C 3S 8S 8D TD")).getRankType());

        assertEquals(HIGH_CARD, analyzer.analyze(create("5D 8C 9S JS AC")).getRankType());
        assertEquals(HIGH_CARD, analyzer.analyze(create("2C 5C 7D 8S QH")).getRankType());

        assertEquals(THREE_OF_A_KIND, analyzer.analyze(create("2D 9C AS AH AC")).getRankType());
        assertEquals(FLUSH, analyzer.analyze(create("3D 6D 7D TD QD")).getRankType());

        assertEquals(ONE_PAIR, analyzer.analyze(create("4D 6S 9H QH QC")).getRankType());
        assertEquals(ONE_PAIR, analyzer.analyze(create("3D 6D 7H QD QS")).getRankType());

        assertEquals(FULL_HOUSE, analyzer.analyze(create("2H 2D 4C 4D 4S")).getRankType());
        assertEquals(FULL_HOUSE, analyzer.analyze(create("3C 3D 3S 9S 9D")).getRankType());

        assertEquals(TWO_PAIRS, analyzer.analyze(create("5H 5C 6S 6D KD")).getRankType());
        assertEquals(STRAIGHT, analyzer.analyze(create("2H 3C 4S 5D 6D")).getRankType());
        assertEquals(STRAIGHT, analyzer.analyze(create("6H 3C 2S 4D 5D")).getRankType());
        assertEquals(FOUR_OF_A_KIND, analyzer.analyze(create("6H 6C 6S 6D 5D")).getRankType());
        assertEquals(STRAIGHT_FLUSH, analyzer.analyze(create("2H 3H 4H 5H 6H")).getRankType());
        assertEquals(ROYAL_FLUSH, analyzer.analyze(create("JH TH AH KH QH")).getRankType());

        assertEquals(STRAIGHT, analyzer.analyze(create("AD JH QD KH TH")).getRankType());
    }

    private Hand create(String rawCards) {
        List<Card> cards = Arrays.stream(rawCards.split(" "))
                .map(this::createCard)
                .collect(Collectors.toList());

        return new Hand(cards);
    }

    private Card createCard(String rawCard) {
        return InputParser.parseCard(rawCard);
    }
}