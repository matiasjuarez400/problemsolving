package mathecat.eulerproject._1._54PokerHands.rank_analyzer;

import mathecat.eulerproject._1._54PokerHands.InputParser;
import mathecat.eulerproject._1._54PokerHands.model.Card;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class UtilsTest {

    @Test
    public void testAreConsecutive() {
        List<Card> cards = InputParser.parseCardList("2H 3H 4H 5H 6H");
        assertTrue(Utils.areConsecutive(cards));

        cards = InputParser.parseCardList("4H 2H 6H 5H 3H");
        assertTrue(Utils.areConsecutive(cards));

        cards = InputParser.parseCardList("4H 2H 7H 5H 3H");
        assertFalse(Utils.areConsecutive(cards));
    }

    @Test
    public void testCountSuits() {
        List<Card> cards = InputParser.parseCardList("2H 3H 4H 5H 6H");
        assertEquals(1, Utils.countSuits(cards));

        cards = InputParser.parseCardList("4H 2H 6H 5H 3H");
        assertEquals(1, Utils.countSuits(cards));

        cards = InputParser.parseCardList("4H 2H 7D 5H 3H");
        assertEquals(2, Utils.countSuits(cards));

        cards = InputParser.parseCardList("4H 2H 7D 5S 3H");
        assertEquals(3, Utils.countSuits(cards));

        cards = InputParser.parseCardList("4H 2H 7D 5S 3C");
        assertEquals(4, Utils.countSuits(cards));
    }

    @Test
    public void testGroupCardValueGroupsBySize() {
        List<Card> cards = InputParser.parseCardList("2H 3H 4H 5H 6H");
        Map<Integer, List<List<Card>>> groupsByCardValue = Utils.groupCardValueGroupsBySize(cards);

        assertEquals(1, groupsByCardValue.size());
        assertFalse(groupsByCardValue.containsKey(2));
        assertFalse(groupsByCardValue.containsKey(3));
        assertFalse(groupsByCardValue.containsKey(4));
        assertFalse(groupsByCardValue.containsKey(5));

        assertTrue(groupsByCardValue.containsKey(1));
        assertEquals(5, groupsByCardValue.get(1).size());


        cards = InputParser.parseCardList("2H 2D 4H 5H 6H");
        groupsByCardValue = Utils.groupCardValueGroupsBySize(cards);

        assertEquals(2, groupsByCardValue.size());
        assertFalse(groupsByCardValue.containsKey(3));
        assertFalse(groupsByCardValue.containsKey(4));
        assertFalse(groupsByCardValue.containsKey(5));

        assertTrue(groupsByCardValue.containsKey(1));
        assertEquals(3, groupsByCardValue.get(1).size());
        assertTrue(groupsByCardValue.containsKey(2));
        assertEquals(1, groupsByCardValue.get(2).size());


        cards = InputParser.parseCardList("2H 2D 2S 5H 6H");
        groupsByCardValue = Utils.groupCardValueGroupsBySize(cards);

        assertEquals(2, groupsByCardValue.size());
        assertFalse(groupsByCardValue.containsKey(2));
        assertFalse(groupsByCardValue.containsKey(4));
        assertFalse(groupsByCardValue.containsKey(5));

        assertTrue(groupsByCardValue.containsKey(1));
        assertEquals(2, groupsByCardValue.get(1).size());
        assertTrue(groupsByCardValue.containsKey(3));
        assertEquals(1, groupsByCardValue.get(3).size());


        cards = InputParser.parseCardList("2H 2D 2S 2C 6H");
        groupsByCardValue = Utils.groupCardValueGroupsBySize(cards);

        assertEquals(2, groupsByCardValue.size());
        assertFalse(groupsByCardValue.containsKey(2));
        assertFalse(groupsByCardValue.containsKey(3));
        assertFalse(groupsByCardValue.containsKey(5));

        assertTrue(groupsByCardValue.containsKey(1));
        assertEquals(1, groupsByCardValue.get(1).size());
        assertTrue(groupsByCardValue.containsKey(4));
        assertEquals(1, groupsByCardValue.get(4).size());
    }
}