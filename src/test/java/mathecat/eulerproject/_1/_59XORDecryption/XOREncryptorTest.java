package mathecat.eulerproject._1._59XORDecryption;

import mathecat.eulerproject.commons.permutation.PermutationIterator;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

public class XOREncryptorTest {
    @Test
    public void testEncryptAndDecrypt() {
        XOREncryptor encryptor = new XOREncryptor();

        String msg = "Hi. This is a private message for my cat, my dog and my beloved 'friend'." +
                "I have 4 eggs and 50 (pluri) toys! Would y4 like to come home?";

        String pass = "cdt";

        String encrypted = encryptor.encrypt(msg, pass);

        assertNotEquals(encrypted, msg);
        assertNotEquals(msg, encryptor.decrypt(encrypted, "bca"));
        assertNotEquals(msg, encryptor.decrypt(encrypted, "zqr"));
        assertNotEquals(msg, encryptor.decrypt(encrypted, "tdc"));

        assertEquals(msg, encryptor.decrypt(encrypted, "cdt"));
    }

    @Test
    public void testTryDecryptWithAllPossibleKeys() {
        XOREncryptor encryptor = new XOREncryptor();

        String msg = "Hi. This is a private message for my cat, my dog and my beloved 'friend'." +
                "I have 4 eggs and 50 (pluri) toys! Would y4 like to come home?";
        String originalPass = "cdt";
        String encrypted = encryptor.encrypt(msg, originalPass);

        PermutationIterator<String> combinator = new PermutationIterator<>(getLowerCaseLetters(), 3);

        Set<String> exploredCombinations = new HashSet<>();
        while (combinator.hasNext()) {
            String nextPass = String.join("", combinator.next());

            if (!exploredCombinations.add(nextPass)) {
                fail("There is a repeated combination: " + nextPass);
            }

            String decryptedMessageAttempt = encryptor.decrypt(encrypted, nextPass);
            if (nextPass.equals(originalPass)) {
                assertEquals(msg, decryptedMessageAttempt);
            } else {
                assertNotEquals(msg, decryptedMessageAttempt);
            }
        }

        assertEquals(26*26*26, exploredCombinations.size());
    }

    private List<String> getLowerCaseLetters() {
        List<String> letters = new ArrayList<>();

        for (char c = 'a'; c <= 'z'; c++) {
            letters.add(Character.toString(c));
        }

        return letters;
    }
}