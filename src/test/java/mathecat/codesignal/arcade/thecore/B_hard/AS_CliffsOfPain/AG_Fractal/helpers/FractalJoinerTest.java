package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import org.junit.Test;

import java.util.List;

import static mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers.TestHelper.containsBridge;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class FractalJoinerTest {
    private static final ConnectionAnalyzer CONNECTION_ANALYZER = new ConnectionAnalyzer();
    private static final FractalUnionsFinder FRACTAL_UNIONS_FINDER = new FractalUnionsFinder(CONNECTION_ANALYZER);
    private static final FractalJoiner FRACTAL_JOINER = new FractalJoiner(CONNECTION_ANALYZER);

    @Test
    public void testJoinTwoFractals1() {
        final FractalModel fractalModel1 = new FractalModel(new char[][]
                {
                        {'.', '_', '.'},
                        {'.', '_', '|'}
                }
        );

        final FractalModel fractalModel2 = new FractalModel(new char[][]
                {
                        {'.', '.', '.'},
                        {'|', '_', '|'}
                }
        );

        final BridgeInformation bridgeInformation = FRACTAL_JOINER.joinFractals(
                new FractalUnionPointsInformationRelative(
                        FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel1),
                        new Point(0, 0)
                ),
                new FractalUnionPointsInformationRelative(
                        FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel2),
                        new Point(0, 2)
                )
        );

        assertNotNull(bridgeInformation);
        assertTrue(bridgeInformation.bridgeExists());

        assertEquals(new Point(0, 2), bridgeInformation.getShortestBridge().getFirstAnchor().getGlobalRelativePosition());
        assertEquals(CharManager.getModelVertical(), bridgeInformation.getShortestBridge().getConnector());
    }

    @Test
    public void testJoinTwoFractals2() {
        final FractalModel fractalModel1 = new FractalModel(new char[][]
                {
                        {'.', '_', '.'},
                        {'.', '_', '|'}
                }
        );

        final FractalModel fractalModel2 = new FractalModel(new char[][]
                {
                        {'.', '_', '.'},
                        {'|', '.', '|'}
                }
        );

        final BridgeInformation bridgeInformation = FRACTAL_JOINER.joinFractals(
                new FractalUnionPointsInformationRelative(
                        FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel1),
                        new Point(0, 0)
                ),
                new FractalUnionPointsInformationRelative(
                        FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel2),
                        new Point(0, 2)
                )
        );

        assertFalse(bridgeInformation.bridgeExists());
    }

    @Test
    public void testJoinTwoFractals3() {
        final FractalModel fractalModel1 = new FractalModel(new char[][]
                {
                        {'.', '_', '.'},
                        {'|', '.', '|'}
                }
        );

        final FractalModel fractalModel2 = new FractalModel(new char[][]
                {
                        {'.', '.', '.'},
                        {'|', '_', '|'}
                }
        );

        final BridgeInformation bridgeInformation = FRACTAL_JOINER.joinFractals(
                new FractalUnionPointsInformationRelative(
                        FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel1),
                        new Point(0, 0)
                ),
                new FractalUnionPointsInformationRelative(
                        FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel2),
                        new Point(0, 2)
                )
        );

        assertNotNull(bridgeInformation);
        assertTrue(bridgeInformation.bridgeExists());

        assertEquals(2, bridgeInformation.getBridgesBetweenFractals().size());

        char connector1 = bridgeInformation.getBridgesBetweenFractals().get(0).getConnector();
        assertEquals(CharManager.getModelVertical(), connector1);

        char connector2 = bridgeInformation.getBridgesBetweenFractals().get(1).getConnector();
        assertEquals(CharManager.getModelVertical(), connector2);
    }

    @Test
    public void testCreateBridges() {
        final FractalModel fractalModel1 = new FractalModel(new char[][]
                {
                        {'.', '_', '.'},
                        {'.', '_', '|'}
                }
        );
        FractalUnionPointsInformationRelative information1 = new FractalUnionPointsInformationRelative(
                FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel1),
                new Point(0, 0)
        );

        final FractalModel fractalModel2 = new FractalModel(new char[][]
                {
                        {'.', '_', '.'},
                        {'|', '.', '|'}
                }
        );
        FractalUnionPointsInformationRelative information2 = new FractalUnionPointsInformationRelative(
                FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel2),
                new Point(0, 2)
        );

        List<Bridge> bridges = FRACTAL_JOINER.createBridges(
                information1, information2
        );

        assertEquals(3, bridges.size());
        assertTrue(containsBridge(new Point(0, 0), new Point(0, 4), CharManager.getModelVertical(), bridges));
        assertTrue(containsBridge(new Point(0, 1), new Point(0, 4), CharManager.getModelVertical(), bridges));
        assertTrue(containsBridge(new Point(0, 2), new Point(0, 4), CharManager.getModelVertical(), bridges));

        List<Bridge> validBridges = FRACTAL_JOINER.createValidBridges(information1, information2);
        assertEquals(0, validBridges.size());
    }

    @Test
    public void testCreateBridges2() {
        final FractalModel fractalModel1 = new FractalModel(new char[][]
                {
                        {'.', '_', '.'},
                        {'.', '_', '|'}
                }
        );
        FractalUnionPointsInformationRelative information1 = new FractalUnionPointsInformationRelative(
                FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel1),
                new Point(0, 0)
        );

        final FractalModel fractalModel2 = new FractalModel(new char[][]
                {
                        {'.', '.', '.'},
                        {'|', '_', '|'}
                }
        );
        FractalUnionPointsInformationRelative information2 = new FractalUnionPointsInformationRelative(
                FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel2),
                new Point(0, 2)
        );

        List<Bridge> bridges = FRACTAL_JOINER.createBridges(information1, information2);

        assertEquals(3, bridges.size());
        assertTrue(containsBridge(new Point(0, 0), new Point(0, 2), CharManager.getModelVertical(), bridges));
        assertTrue(containsBridge(new Point(0, 1), new Point(0, 2), CharManager.getModelVertical(), bridges));
        assertTrue(containsBridge(new Point(0, 2), new Point(0, 2), CharManager.getModelVertical(), bridges));
    }

    @Test
    public void testJoinFractals() {
        final FractalModel fractalModel1 = new FractalModel(new char[][]
                {
                        {'.', '_', '.', '.', '.', '_', '.'},
                        {'.', '_', '|', '.', '|', '_', '.'},
                        {'|', '.', '.', '_', '.', '.', '|'},
                        {'|', '_', '|', '.', '|', '_', '|'}
                }
        );

        fractalModel1.getRotations();

        FractalUnionPointsInformationRelative information1 = new FractalUnionPointsInformationRelative(
                FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel1),
                new Point(0, 0)
        );

        final FractalModel fractalModel2 = new FractalModel(new char[][]
                {
                        {'.', '.', '.', '_', '.', '_', '.'},
                        {'|', '_', '|', '.', '.', '_', '|'},
                        {'.', '_', '.', '.', '|', '_', '.'},
                        {'|', '.', '|', '_', '.', '_', '|'}
                }
        );
        FractalUnionPointsInformationRelative information2 = new FractalUnionPointsInformationRelative(
                FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel2),
                new Point(8, 0)
        );

        BridgeInformation bridgeInformation = FRACTAL_JOINER.joinFractals(information1, information2);

        assertNotNull(bridgeInformation);
        assertEquals(1, bridgeInformation.getBridgesBetweenFractals().size());
        assertTrue(TestHelper.containsBridge(new Point(6, 0), new Point(7, 0),
                CharManager.getModelHorizontal(), bridgeInformation.getBridgesBetweenFractals()));
    }

    @Test
    public void testJoinFractals2() {
        final FractalModel fractalModel1 = new FractalModel(new char[][]
                {
                        {'.', '_', '.', '.', '.', '_', '.'},
                        {'|', '.', '|', '_', '|', '.', '|'},
                        {'|', '_', '.', '.', '.', '_', '|'},
                        {'.', '_', '|', '.', '|', '_', '.'}
                }
        );

        fractalModel1.getRotations();

        FractalUnionPointsInformationRelative information1 = new FractalUnionPointsInformationRelative(
                FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel1),
                new Point(0, 5)
        );

        final FractalModel fractalModel2 = new FractalModel(new char[][]
                {
                        {'.', '.', '.', '_', '_', '_', '.'},
                        {'|', '_', '|', '.', '.', '_', '|'},
                        {'.', '_', '.', '.', '|', '_', '.'},
                        {'|', '.', '|', '_', '_', '_', '|'}
                }
        );
        FractalUnionPointsInformationRelative information2 = new FractalUnionPointsInformationRelative(
                FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel2),
                new Point(8, 5)
        );

        BridgeInformation bridgeInformation = FRACTAL_JOINER.joinFractals(information1, information2);

        assertNotNull(bridgeInformation);
        assertEquals(1, bridgeInformation.getBridgesBetweenFractals().size());
        assertTrue(TestHelper.containsBridge(new Point(6, 8), new Point(7, 8),
                CharManager.getModelHorizontal(), bridgeInformation.getBridgesBetweenFractals()));
    }
}
