package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class FractalUnionPointsInformationWithRelativePositionTest {
    private static final ConnectionAnalyzer connectionAnalyzer = new ConnectionAnalyzer();
    private static final FractalUnionsFinder fractalUnionsFinder = new FractalUnionsFinder(connectionAnalyzer);

    @Test
    public void testChangeInPositionsForUnionAndAnchors() {
        char[][] matrix = {
                {'.', '_', '.'},
                {'.', '_', '|'}
        };

        FractalModel fractalModel = new FractalModel(matrix);

        FractalUnionPointsInformation fractalUnionPointsInformation = fractalUnionsFinder.analyzeFractalModel(fractalModel);

        FractalUnionPointsInformationRelative fractalUnionPointsInformationRelative =
                new FractalUnionPointsInformationRelative(fractalUnionPointsInformation, new Point(2, 1));

        List<UnionPointWithAnchorsRelative> relativeUnionsWithAnchors = fractalUnionPointsInformationRelative.getUnionsWithRelativePosition();

        final UnionPointWithAnchorsRelative firstRelativeUnion = relativeUnionsWithAnchors.stream()
                .filter(unionWithAnchors -> unionWithAnchors.getGlobalRelativePosition().equals(new Point(3, 1)))
                .findFirst().orElse(null);

        assertNotNull(firstRelativeUnion);

        final List<Point> expectedFirstUnionAnchorLocations = Arrays.asList(
                new Point(2, 1)
        );

        final List<Point> actualFirstUnionAnchorLocations = firstRelativeUnion.getAnchorRelatives().stream()
                .map(AnchorRelative::getGlobalRelativePosition)
                .collect(Collectors.toList());

        assertEquals(expectedFirstUnionAnchorLocations.size(), actualFirstUnionAnchorLocations.size());
        assertTrue(expectedFirstUnionAnchorLocations.containsAll(actualFirstUnionAnchorLocations));
        assertTrue(actualFirstUnionAnchorLocations.containsAll(expectedFirstUnionAnchorLocations));
    }
}
