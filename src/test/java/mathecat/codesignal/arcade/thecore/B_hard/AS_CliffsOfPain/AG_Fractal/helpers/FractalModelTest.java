package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import org.junit.Test;

import java.util.List;

import static mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers.TestHelper.areMatrixEqual;
import static org.junit.Assert.assertTrue;

public class FractalModelTest {
    @Test
    public void testExpand() {
        String testName = "expand";

        char[][] matrix = {
                {'.', '_', '.'},
                {'.', '_', '|'}
        };

        FractalModel fractalModel = new FractalModel(matrix);

        FractalModel expandedModel = new FractalModel(fractalModel.expand(fractalModel.getModel()));

        char[][] expected = {
                {'°', '°', '°', '°'},
                {'.', '_', '.', '°'},
                {'°', '°', '|', '°'},
                {'.', '_', '°', '°'}
        };

        TestHelper.printExpectedVsActual(
                testName,
                FractalModelPrinter.printModelRepresentation(expected),
                FractalModelPrinter.printModelRepresentation(expandedModel)
        );

        assertTrue(areMatrixEqual(expected, expandedModel.getModel()));
    }

    @Test
    public void testCompress() {
        String testName = "compress";

        char[][] expandedModel = {
                {'°', '°', '°', '°'},
                {'.', '_', '.', '°'},
                {'°', '°', '|', '°'},
                {'.', '_', '°', '°'}
        };

        FractalModel fractalModel = new FractalModel(expandedModel);

        final char[][] compressedModel = fractalModel.compress(fractalModel.getModel());

        final char[][] expected = {
                {'.', '_', '.'},
                {'.', '_', '|'}
        };

        TestHelper.printExpectedVsActual(
                testName,
                FractalModelPrinter.printModelRepresentation(expected),
                FractalModelPrinter.printModelRepresentation(compressedModel)
        );

        assertTrue(areMatrixEqual(expected, compressedModel));
    }

    @Test
    public void testBreath() {
        String testName = "breath";

        final char[][] base = {
                {'.', '_', '.'},
                {'.', '_', '|'}
        };

        FractalModel breath = new FractalModel(base);

        // Breath 1
        breath = new FractalModel(breath.breath());

        char[][] expected = {
                {'.', '_', '.'},
                {'|', '.', '|'}
        };

        TestHelper.printExpectedVsActual(
                testName,
                FractalModelPrinter.printModelRepresentation(expected),
                FractalModelPrinter.printModelRepresentation(breath.getModel())
        );

        assertTrue(areMatrixEqual(expected, breath.getModel()));

        // Breath 2
        breath = new FractalModel(breath.breath());

        expected = new char[][]{
                {'.', '_', '.'},
                {'|', '_', '.'}
        };

        TestHelper.printExpectedVsActual(
                testName,
                FractalModelPrinter.printModelRepresentation(expected),
                FractalModelPrinter.printModelRepresentation(breath.getModel())
        );

        assertTrue(areMatrixEqual(expected, breath.getModel()));

        // Breath 3
        breath = new FractalModel(breath.breath());

        expected = new char[][]{
                {'.', '.', '.'},
                {'|', '_', '|'}
        };

        TestHelper.printExpectedVsActual(
                testName,
                FractalModelPrinter.printModelRepresentation(expected),
                FractalModelPrinter.printModelRepresentation(breath.getModel())
        );

        assertTrue(areMatrixEqual(expected, breath.getModel()));
    }

    @Test
    public void testGetAllRotations() {
        String testName = "allRotations";

        final FractalModel fractalModel = new FractalModel(new char[][]
                {
                        {'.', '_', '.', '.', '.', '_', '.'},
                        {'.', '_', '|', '.', '|', '_', '.'},
                        {'|', '.', '.', '_', '.', '.', '|'},
                        {'|', '_', '|', '.', '|', '_', '|'}
                }
        );

        List<FractalModel> rotations = fractalModel.getRotations();

        final FractalModel expectedRotated2 = new FractalModel(new char[][]
                {
                        {'.', '.', '.', '_', '_', '_', '.'},
                        {'|', '_', '|', '.', '.', '_', '|'},
                        {'.', '_', '.', '.', '|', '_', '.'},
                        {'|', '.', '|', '_', '_', '_', '|'}
                }
        );

        TestHelper.printExpectedVsActual(
                testName,
                FractalModelPrinter.printModelRepresentation(expectedRotated2),
                FractalModelPrinter.printModelRepresentation(rotations.get(1))
        );


        assertTrue(areMatrixEqual(expectedRotated2.getModel(), rotations.get(1).getModel()));
    }
}
