package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FractalExpansionLogicTest {
    private static final ConnectionAnalyzer connectionAnalyzer = new ConnectionAnalyzer();
    private static final FractalUnionsFinder fractalUnionsFinder = new FractalUnionsFinder(connectionAnalyzer);
    private static final FractalJoiner fractalJoiner = new FractalJoiner(connectionAnalyzer);
    private static final NextLevelFractalJoiner nextLevelFractalJoiner = new NextLevelFractalJoiner(fractalJoiner);
    private static final FractalExpansionLogic fractalExpansionLogic = new FractalExpansionLogic(fractalUnionsFinder, nextLevelFractalJoiner);

    @Test
    public void testExpandFromLevel1ToLevel2() {
        char[][] matrix = {
                {' ', '_', ' '},
                {' ', '_', '|'}
        };

        FractalModel firstLevel = new FractalModel(matrix);

        FractalModel secondLevel = fractalExpansionLogic.expand(firstLevel);

        String expectedSecondLevel =
                ". _ . . . _ . \n" +
                ". _ | . | _ . \n" +
                "| . . _ . . | \n" +
                "| _ | . | _ |";

        assertEquals(expectedSecondLevel.trim(), secondLevel.toString().trim());
    }
}
