package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class FractalUnionsFinderTest {
    private static final ConnectionAnalyzer CONNECTION_ANALYZER = new ConnectionAnalyzer();
    private static final FractalUnionsFinder FRACTAL_UNIONS_FINDER = new FractalUnionsFinder(CONNECTION_ANALYZER);

    @Test
    public void testCalculateUnionPoints1() {
        final char[][] base = {
                {'.', '_', '.'},
                {'.', '_', '|'}
        };

        FractalModel fractalModel = new FractalModel(base);

        FractalUnionPointsInformation fractalUnionPointsInformation = FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel);

        boolean[][] unionPoints = fractalUnionPointsInformation.getUnionPointsMatrix();

        final boolean[][] expected = {
                {false, true, false},
                {false, true, false}
        };

        assertTrue(TestHelper.areMatrixEqual(expected, unionPoints));
    }

    @Test
    public void testCalculateUnionPoints2() {
        final char[][] base = {
                {'.', '_', '.'},
                {'|', '.', '|'}
        };

        FractalModel fractalModel = new FractalModel(base);

        FractalUnionPointsInformation fractalUnionPointsInformation = FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel);

        boolean[][] unionPoints = fractalUnionPointsInformation.getUnionPointsMatrix();

        final boolean[][] expected = {
                {false, false, false},
                {true, false, true}
        };

        assertTrue(TestHelper.areMatrixEqual(expected, unionPoints));
    }

    @Test
    public void testCalculateUnionPoints3() {
        final char[][] base = {
                {'.', '_', '.'},
                {'|', '_', '.'}
        };

        FractalModel fractalModel = new FractalModel(base);

        FractalUnionPointsInformation fractalUnionPointsInformation = FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel);

        boolean[][] unionPoints = fractalUnionPointsInformation.getUnionPointsMatrix();

        final boolean[][] expected = {
                {false, true, false},
                {false, true, false}
        };

        assertTrue(TestHelper.areMatrixEqual(expected, unionPoints));
    }

    @Test
    public void testCalculateUnionPoints4() {
        final char[][] base = {
                {'.', '.', '.'},
                {'|', '_', '|'}
        };

        FractalModel fractalModel = new FractalModel(base);

        FractalUnionPointsInformation fractalUnionPointsInformation = FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel);

        boolean[][] unionPoints = fractalUnionPointsInformation.getUnionPointsMatrix();

        final boolean[][] expected = {
                {false, false, false},
                {true, false, true}
        };

        assertTrue(TestHelper.areMatrixEqual(expected, unionPoints));
    }

    @Test
    public void testCalculateUnionPoints5() {
        final char[][] base = {
              {' ', '_', ' ', ' ', ' ', '_', ' '},      
              {' ', '_', '|', ' ', '|', '_', ' '},                                  
              {'|', ' ', ' ', '_', ' ', ' ', '|'},                                 
              {'|', '_', '|', ' ', '|', '_', '|'}
        };

        FractalModel fractalModel = new FractalModel(base);

        FractalUnionPointsInformation fractalUnionPointsInformation = FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel);

        boolean[][] unionPoints = fractalUnionPointsInformation.getUnionPointsMatrix();

        final boolean[][] expected = {
                {false, true, false, false, false, true, false},
                {false, false, false, false, false, false, false},
                {false, false, false, false, false, false, false},
                {false, false, false, false, false, false, false}
        };

        assertTrue(TestHelper.areMatrixEqual(expected, unionPoints));
    }

    @Test
    public void testCalculateUnionPoints6() {
        final char[][] base = {
                {'.', '.', '.', '_', '_', '_', '.'},
                {'|', '_', '|', '.', '.', '_', '|'},
                {'.', '_', '.', '.', '|', '_', '.'},
                {'|', '.', '|', '_', '_', '_', '|'}
        };

        FractalModel fractalModel = new FractalModel(base);

        FractalUnionPointsInformation fractalUnionPointsInformation = FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel);

        boolean[][] unionPoints = fractalUnionPointsInformation.getUnionPointsMatrix();

        final boolean[][] expected = {
                {false, false, false, false, false, false, false},
                {true, false, false, false, false, false, false},
                {false, false, false, false, false, false, false},
                {true, false, false, false, false, false, false}
        };

        assertTrue(TestHelper.areMatrixEqual(expected, unionPoints));
    }

    @Test
    public void testAnchorCreationLogic() {
        char[][] matrix = {
                {'.', '_', '.'},
                {'.', '_', '|'}
        };

        FractalModel fractalModel = new FractalModel(matrix);

        FractalUnionPointsInformation fractalUnionPointsInformation = FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel);

        final List<Point> expectedUnionPoints = Arrays.asList(
                new Point(1, 0),
                new Point(1, 1)
        );

        final List<Point> actualUnionPoints = fractalUnionPointsInformation.getUnionPointsWithAnchors().stream()
                .map(UnionPointWithAnchors::getUnionLocation)
                .collect(Collectors.toList());

        assertEquals(expectedUnionPoints.size(), actualUnionPoints.size());
        assertTrue(expectedUnionPoints.containsAll(actualUnionPoints));
        assertTrue(actualUnionPoints.containsAll(expectedUnionPoints));


        final UnionPointWithAnchors firstUnion = fractalUnionPointsInformation.getUnionPointsWithAnchors().stream()
                .filter(unionWithAnchors -> unionWithAnchors.getUnionLocation().equals(expectedUnionPoints.get(0)))
                .findFirst().orElse(null);

        assertNotNull(firstUnion);
        assertEquals(1, firstUnion.getAnchors().size());

        firstUnion.getAnchors().forEach(anchor -> {
            assertEquals(anchor.getAnchorOwner(), firstUnion);
        });

        assertEquals(CharManager.getModelHorizontal(), (char) firstUnion.getUnionSymbol());

        final List<Point> expectedFirstUnionAnchorLocations = Arrays.asList(
                new Point(0, 0)
        );

        final List<Point> actualFirstUnionAnchorLocations = firstUnion.getAnchors().stream()
                .map(Anchor::getAnchorLocation)
                .collect(Collectors.toList());

        assertEquals(expectedFirstUnionAnchorLocations.size(), actualFirstUnionAnchorLocations.size());
        assertTrue(expectedFirstUnionAnchorLocations.containsAll(actualFirstUnionAnchorLocations));
        assertTrue(actualFirstUnionAnchorLocations.containsAll(expectedFirstUnionAnchorLocations));


        final UnionPointWithAnchors secondUnion = fractalUnionPointsInformation.getUnionPointsWithAnchors().stream()
                .filter(unionWithAnchors -> unionWithAnchors.getUnionLocation().equals(expectedUnionPoints.get(1)))
                .findFirst().orElse(null);

        assertNotNull(secondUnion);
        assertEquals(2, secondUnion.getAnchors().size());

        secondUnion.getAnchors().forEach(anchor -> {
            assertEquals(anchor.getAnchorOwner(), secondUnion);
        });

        assertEquals(CharManager.getModelHorizontal(), (char) secondUnion.getUnionSymbol());

        final List<Point> expectedSecondUnionAnchorLocations = Arrays.asList(
                new Point(0, 1),
                new Point(0, 2)
        );

        final List<Point> actualSecondUnionAnchorLocations = secondUnion.getAnchors().stream()
                .map(Anchor::getAnchorLocation)
                .collect(Collectors.toList());

        assertEquals(expectedSecondUnionAnchorLocations.size(), actualSecondUnionAnchorLocations.size());
        assertTrue(expectedSecondUnionAnchorLocations.containsAll(actualSecondUnionAnchorLocations));
        assertTrue(actualSecondUnionAnchorLocations.containsAll(expectedSecondUnionAnchorLocations));
    }

    @Test
    public void testAnalyzePossibleValuesForAnchor() {
        char[][] matrix = {
                {'.', '_', '.'},
                {'.', '_', '|'}
        };

        FractalModel fractalModel = new FractalModel(matrix);

        Point firstUnionLocation = new Point(1, 0);
        Point secondUnionLocation = new Point(1, 1);

        List<Character> possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(0, 0), firstUnionLocation, fractalModel);
        assertEquals(2, possibleValuesForAnchor.size());
        assertTrue(possibleValuesForAnchor.containsAll(CharManager.getConnectionChars()));

        possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(0, 1), secondUnionLocation, fractalModel);
        assertEquals(1, possibleValuesForAnchor.size());
        assertTrue(possibleValuesForAnchor.contains(CharManager.getModelHorizontal()));

        possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(0, 1), firstUnionLocation, fractalModel);
        assertEquals(0, possibleValuesForAnchor.size());

        possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(2, 0), firstUnionLocation, fractalModel);
        assertEquals(0, possibleValuesForAnchor.size());

        possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(2, 0), secondUnionLocation, fractalModel);
        assertEquals(0, possibleValuesForAnchor.size());

        possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(0, 2), firstUnionLocation, fractalModel);
        assertEquals(0, possibleValuesForAnchor.size());

        possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(0, 2), secondUnionLocation, fractalModel);
        assertEquals(1, possibleValuesForAnchor.size());
        assertTrue(possibleValuesForAnchor.contains(CharManager.getModelVertical()));

        possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(2, 2), secondUnionLocation, fractalModel);
        assertEquals(0, possibleValuesForAnchor.size());

        possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(2, 2), firstUnionLocation, fractalModel);
        assertEquals(0, possibleValuesForAnchor.size());

        possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(3, 0), firstUnionLocation, fractalModel);
        assertEquals(0, possibleValuesForAnchor.size());

        possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(3, 0), secondUnionLocation, fractalModel);
        assertEquals(0, possibleValuesForAnchor.size());
    }

    @Test
    public void testAnalyzePossibleValuesForAnchor2() {
        char[][] matrix = {
                {'.', '.', '.'},
                {'|', '_', '|'}
        };

        FractalModel fractalModel = new FractalModel(matrix);

        Point firstUnionLocation = new Point(0, 1);
        Point secondUnionLocation = new Point(2, 1);

        List<Character> possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(0, 0), firstUnionLocation, fractalModel);
        assertEquals(1, possibleValuesForAnchor.size());
        assertTrue(possibleValuesForAnchor.contains(CharManager.getModelVertical()));

        possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(-1, 0), firstUnionLocation, fractalModel);
        assertEquals(1, possibleValuesForAnchor.size());
        assertTrue(possibleValuesForAnchor.contains(CharManager.getModelHorizontal()));

        possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(-1, 1), firstUnionLocation, fractalModel);
        assertEquals(0, possibleValuesForAnchor.size());

        possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(0, 2), firstUnionLocation, fractalModel);
        assertEquals(0, possibleValuesForAnchor.size());

        possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(1, 0), firstUnionLocation, fractalModel);
        assertEquals(0, possibleValuesForAnchor.size());


        possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(2, 0), secondUnionLocation, fractalModel);
        assertEquals(1, possibleValuesForAnchor.size());
        assertTrue(possibleValuesForAnchor.contains(CharManager.getModelVertical()));

        possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(3, 0), secondUnionLocation, fractalModel);
        assertEquals(1, possibleValuesForAnchor.size());
        assertTrue(possibleValuesForAnchor.contains(CharManager.getModelHorizontal()));

        possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(3, 1), secondUnionLocation, fractalModel);
        assertEquals(0, possibleValuesForAnchor.size());

        possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(2, 2), secondUnionLocation, fractalModel);
        assertEquals(0, possibleValuesForAnchor.size());

        possibleValuesForAnchor = FRACTAL_UNIONS_FINDER.analyzePossibleValuesForAnchor(new Point(1, 0), secondUnionLocation, fractalModel);
        assertEquals(0, possibleValuesForAnchor.size());
    }

    @Test
    public void testAnalyzePossibleValuesForAnchor3() {
        final char[][] base = {
                {'.', '.', '.', '_', '_', '_', '.'},
                {'|', '_', '|', '.', '.', '_', '|'},
                {'.', '_', '.', '.', '|', '_', '.'},
                {'|', '.', '|', '_', '_', '_', '|'}
        };

        FractalModel fractalModel = new FractalModel(base);

        FractalUnionPointsInformation fractalUnionPointsInformation = FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel);
        assertEquals(2, fractalUnionPointsInformation.getUnionPointsWithAnchors().size());

        UnionPointWithAnchors bottomLeftUnionPoint = fractalUnionPointsInformation.getUnionPointsWithAnchors().stream()
                .filter(unionPointWithAnchors -> unionPointWithAnchors.getUnionLocation().equals(new Point(0, 3)))
                .findFirst()
                .orElse(null);

        assertNotNull(bottomLeftUnionPoint);

        assertEquals(2, bottomLeftUnionPoint.getAnchors().size());

        Point firstAnchorLocation = new Point(-1, 3);
        Anchor firstAnchor = bottomLeftUnionPoint.getAnchors().stream()
                .filter(anchor -> anchor.getAnchorLocation().equals(firstAnchorLocation))
                .findFirst().orElse(null);

        assertNotNull(firstAnchor);
        assertEquals(1, firstAnchor.getValidConnectors().size());
        assertEquals(CharManager.getModelHorizontal(), (char) firstAnchor.getValidConnectors().get(0));

        Point secondAnchorLocation = new Point(0, 4);
        Anchor secondAnchor = bottomLeftUnionPoint.getAnchors().stream()
                .filter(anchor -> anchor.getAnchorLocation().equals(secondAnchorLocation))
                .findFirst().orElse(null);

        assertNotNull(secondAnchor);
        assertEquals(1, secondAnchor.getValidConnectors().size());
        assertEquals(CharManager.getModelVertical(), (char) secondAnchor.getValidConnectors().get(0));
    }
}
