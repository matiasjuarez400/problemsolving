package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class NextLevelFractalJoinerTest {
    private static final ConnectionAnalyzer CONNECTION_ANALYZER = new ConnectionAnalyzer();
    private static final FractalUnionsFinder FRACTAL_UNIONS_FINDER = new FractalUnionsFinder(CONNECTION_ANALYZER);
    private static final FractalJoiner FRACTAL_JOINER = new FractalJoiner(CONNECTION_ANALYZER);
    private static final NextLevelFractalJoiner NEXT_LEVEL_FRACTAL_JOINER = new NextLevelFractalJoiner(FRACTAL_JOINER);

    @Test
    public void testJoinFractals() {
        final FractalModel fractalModel1 = new FractalModel(new char[][]
                {
                        {'.', '_', '.'},
                        {'.', '_', '|'}
                }
        );

        final FractalModel fractalModel2 = new FractalModel(new char[][]
                {
                        {'.', '.', '.'},
                        {'|', '_', '|'}
                }
        );

        final FractalModel fractalModel3 = new FractalModel(new char[][]
                {
                        {'.', '.', '.'},
                        {'|', '_', '|'}
                }
        );

        final FractalModel fractalModel4 = new FractalModel(new char[][]
                {
                        {'.', '_', '.'},
                        {'|', '_', '.'}
                }
        );

        final List<FractalUnionPointsInformationRelative> fractalModelList = Arrays.asList(
                new FractalUnionPointsInformationRelative(
                        FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel1),
                        new Point(0, 0)
                ),
                new FractalUnionPointsInformationRelative(
                        FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel2),
                        new Point(0, 2)
                ),
                new FractalUnionPointsInformationRelative(
                        FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel3),
                        new Point(3, 2)
                ),
                new FractalUnionPointsInformationRelative(
                        FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel4),
                        new Point(3, 0)
                )
        );

        final NextLevelFractalData nextLevelFractalData = NEXT_LEVEL_FRACTAL_JOINER.join(fractalModelList);

        assertNull(nextLevelFractalData);
    }

    @Test
    public void testJoinFractals2() {
        final FractalModel fractalModel1 = new FractalModel(new char[][]
                {
                        {'.', '_', '.'},
                        {'.', '_', '|'}
                }
        );

        final FractalModel fractalModel2 = new FractalModel(new char[][]
                {
                        {'.', '.', '.'},
                        {'|', '_', '|'}
                }
        );

        final FractalModel fractalModel3 = new FractalModel(new char[][]
                {
                        {'.', '.', '.'},
                        {'|', '_', '|'}
                }
        );

        final FractalModel fractalModel4 = new FractalModel(new char[][]
                {
                        {'.', '_', '.'},
                        {'|', '_', '.'}
                }
        );

        final List<FractalUnionPointsInformationRelative> fractalModelList = Arrays.asList(
                new FractalUnionPointsInformationRelative(
                        FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel1),
                        new Point(0, 0)
                ),
                new FractalUnionPointsInformationRelative(
                        FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel2),
                        new Point(0, 2)
                ),
                new FractalUnionPointsInformationRelative(
                        FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel3),
                        new Point(4, 2)
                ),
                new FractalUnionPointsInformationRelative(
                        FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel4),
                        new Point(4, 0)
                )
        );

        final NextLevelFractalData nextLevelFractalData = NEXT_LEVEL_FRACTAL_JOINER.join(fractalModelList);

        assertNotNull(nextLevelFractalData);
        assertEquals(3, nextLevelFractalData.getUsedBridges().size());
        assertTrue(TestHelper.containsBridge(new Point(0, 2), new Point(0, 2),
                CharManager.getModelVertical(), nextLevelFractalData.getUsedBridges()));
        assertTrue(TestHelper.containsBridge(new Point(3, 2), new Point(3, 2),
                CharManager.getModelHorizontal(), nextLevelFractalData.getUsedBridges()));
        assertTrue(TestHelper.containsBridge(new Point(6, 2), new Point(6, 2),
                CharManager.getModelVertical(), nextLevelFractalData.getUsedBridges()));
    }

    @Test
    public void testJoinFractals3() {
        final FractalModel fractalModel1 = new FractalModel(new char[][]
                {
                        {'.', '_', '.', '.', '.', '_', '.'},
                        {'.', '_', '|', '.', '|', '_', '.'},
                        {'|', '.', '.', '_', '.', '.', '|'},
                        {'|', '_', '|', '.', '|', '_', '|'}
                }
        );

        final FractalModel fractalModel2 = new FractalModel(new char[][]
                {
                        {'.', '_', '.', '.', '.', '_', '.'},
                        {'|', '.', '|', '_', '|', '.', '|'},
                        {'|', '_', '.', '.', '.', '_', '|'},
                        {'.', '_', '|', '.', '|', '_', '.'}
                }
        );

        final FractalModel fractalModel3 = new FractalModel(new char[][]
                {
                        {'.', '.', '.', '_', '_', '_', '.'},
                        {'|', '_', '|', '.', '.', '_', '|'},
                        {'.', '_', '.', '.', '|', '_', '.'},
                        {'|', '.', '|', '_', '_', '_', '|'}
                }
        );

        final FractalModel fractalModel4 = new FractalModel(new char[][]
                {
                        {'.', '.', '.', '_', '_', '_', '.'},
                        {'|', '_', '|', '.', '.', '_', '|'},
                        {'.', '_', '.', '.', '|', '_', '.'},
                        {'|', '.', '|', '_', '_', '_', '|'}
                }
        );

        final List<FractalUnionPointsInformationRelative> fractalModelList = Arrays.asList(
                new FractalUnionPointsInformationRelative(
                        FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel1),
                        new Point(0, 0)
                ),
                new FractalUnionPointsInformationRelative(
                        FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel2),
                        new Point(0, 4)
                ),
                new FractalUnionPointsInformationRelative(
                        FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel3),
                        new Point(8, 4)
                ),
                new FractalUnionPointsInformationRelative(
                        FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel4),
                        new Point(8, 0)
                )
        );

        final NextLevelFractalData nextLevelFractalData = NEXT_LEVEL_FRACTAL_JOINER.join(fractalModelList);

        assertNotNull(nextLevelFractalData);
        assertEquals(3, nextLevelFractalData.getUsedBridges().size());
        assertTrue(TestHelper.containsBridge(new Point(6, 0), new Point(7, 0),
                CharManager.getModelHorizontal(), nextLevelFractalData.getUsedBridges()));
        assertTrue(TestHelper.containsBridge(new Point(8, 4), new Point(8, 4),
                CharManager.getModelVertical(), nextLevelFractalData.getUsedBridges()));
        assertTrue(TestHelper.containsBridge(new Point(6, 7), new Point(7, 7),
                CharManager.getModelHorizontal(), nextLevelFractalData.getUsedBridges()));
    }
}
