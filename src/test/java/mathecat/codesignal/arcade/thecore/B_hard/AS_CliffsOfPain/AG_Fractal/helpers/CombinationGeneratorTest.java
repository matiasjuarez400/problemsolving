package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class CombinationGeneratorTest {
    @Test
    public void testCombinationGeneration() {
        CombinationGenerator combinationGenerator = new CombinationGenerator(2, 3);

        List<int[]> combinationsFound = new ArrayList<>();
        combinationsFound.add(combinationGenerator.getCurrentCombination());

        while (combinationGenerator.hasNext()) {
            combinationsFound.add(combinationGenerator.getAndGenerate());
        }

        assertEquals(28, combinationsFound.size());
        assertArrayEquals(new int[]{2, 2, 2}, combinationsFound.get(combinationsFound.size() - 1));
    }

    @Test
    public void testCombinationGeneration2() {
        CombinationGenerator combinationGenerator = new CombinationGenerator(new int[]{2, 3, 4});

        List<int[]> combinationsFound = new ArrayList<>();
        combinationsFound.add(combinationGenerator.getCurrentCombination());

        while (combinationGenerator.hasNext()) {
            combinationsFound.add(combinationGenerator.getAndGenerate());
        }

        assertEquals(61, combinationsFound.size());
        assertArrayEquals(new int[]{2, 3, 4}, combinationsFound.get(combinationsFound.size() - 1));
    }

    @Test
    public void testCombinationGeneration3() {
        CombinationGenerator combinationGenerator = new CombinationGenerator(1, 2);

        List<int[]> combinationsFound = new ArrayList<>();

        while (combinationGenerator.hasNext()) {
            combinationsFound.add(combinationGenerator.getAndGenerate());
        }

        assertEquals(4, combinationsFound.size());
        assertArrayEquals(new int[] {0, 0}, combinationsFound.get(0));
        assertArrayEquals(new int[] {1, 0}, combinationsFound.get(1));
        assertArrayEquals(new int[] {0, 1}, combinationsFound.get(2));
        assertArrayEquals(new int[] {1, 1}, combinationsFound.get(3));
    }
}
