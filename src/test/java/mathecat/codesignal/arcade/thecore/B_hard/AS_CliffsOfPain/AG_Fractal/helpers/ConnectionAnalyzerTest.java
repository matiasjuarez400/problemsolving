package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ConnectionAnalyzerTest {
    private final static ConnectionAnalyzer analyzer = new ConnectionAnalyzer();

    @Test
    public void testConnectHorizontalElementWithHorizontalElement() {
        FractalModelElement base = new FractalModelElement(CharManager.getModelHorizontal(), 1, 1);
        FractalModelElement target = new FractalModelElement(CharManager.getModelHorizontal(), 1, 1);

        target.moveTo(0, 1);
        assertTrue(analyzer.canConnectWithElement(base, target));

        target.moveTo(2, 1);
        assertTrue(analyzer.canConnectWithElement(base, target));

        target.moveTo(3, 1);
        assertFalse(analyzer.canConnectWithElement(base, target));

        target.moveTo(2, 3);
        assertFalse(analyzer.canConnectWithElement(base, target));

        for (int col = 0; col < 3; col++) {
            target.moveTo(col, 0);
            assertFalse(analyzer.canConnectWithElement(base, target));

            target.moveTo(col, 2);
            assertFalse(analyzer.canConnectWithElement(base, target));
        }
    }

    @Test
    public void testConnectHorizontalElementWithVerticalElement() {
        FractalModelElement base = new FractalModelElement(CharManager.getModelHorizontal(), 1, 1);
        FractalModelElement target = new FractalModelElement(CharManager.getModelVertical(), 1, 1);

        target.moveTo(0, 1);
        assertTrue(analyzer.canConnectWithElement(base, target));

        target.moveTo(2, 1);
        assertTrue(analyzer.canConnectWithElement(base, target));

        target.moveTo(0, 2);
        assertTrue(analyzer.canConnectWithElement(base, target));

        target.moveTo(2, 2);
        assertTrue(analyzer.canConnectWithElement(base, target));

        target.moveTo(1, 2);
        assertFalse(analyzer.canConnectWithElement(base, target));

        target.moveTo(1, 3);
        assertFalse(analyzer.canConnectWithElement(base, target));

        target.moveTo(-1, -1);
        assertFalse(analyzer.canConnectWithElement(base, target));

        for (int col = 0; col < 3; col++) {
            target.moveTo(col, 0);
            assertFalse(analyzer.canConnectWithElement(base, target));
        }
    }

    @Test
    public void testConnectVerticalElementWithVerticalElement() {
        FractalModelElement base = new FractalModelElement(CharManager.getModelVertical(), 1, 1);
        FractalModelElement target = new FractalModelElement(CharManager.getModelVertical(), 1, 1);

        target.moveTo(1, 0);
        assertTrue(analyzer.canConnectWithElement(base, target));

        target.moveTo(1, 2);
        assertTrue(analyzer.canConnectWithElement(base, target));

        for (int col = -1; col < 3; col++) {
            for (int row = -1; row < 3; row++) {
                target.moveTo(col, row);

                if (col == 1) {
                    if (row == 0 || row == 2) {
                        assertTrue(analyzer.canConnectWithElement(base, target));
                    } else {
                        assertFalse(analyzer.canConnectWithElement(base, target));
                    }
                } else {
                    assertFalse(analyzer.canConnectWithElement(base, target));
                }
            }
        }
    }

    @Test
    public void testConnectVerticalElementWithHorizontalElement() {
        FractalModelElement base = new FractalModelElement(CharManager.getModelVertical(), 1, 1);
        FractalModelElement target = new FractalModelElement(CharManager.getModelHorizontal(), 1, 1);

        target.moveTo(0, 0);
        assertTrue(analyzer.canConnectWithElement(base, target));

        target.moveTo(0, 1);
        assertTrue(analyzer.canConnectWithElement(base, target));

        target.moveTo(2, 0);
        assertTrue(analyzer.canConnectWithElement(base, target));

        target.moveTo(2, 1);
        assertTrue(analyzer.canConnectWithElement(base, target));


        target.moveTo(1, 0);
        assertFalse(analyzer.canConnectWithElement(base, target));

        for (int col = 0; col < 3; col++) {
            target.moveTo(col, 2);
            assertFalse(analyzer.canConnectWithElement(base, target));
        }
    }

    @Test
    public void testConnectionLogicForFractalModelElementWihSurroundings() {
        char[][] matrix = {
                {'.', '.', '.'},
                {'|', '_', '|'}
        };

        FractalModel fractalModel = new FractalModel(matrix);

        FractalModelElement fractalModelElement = new FractalModelElement(CharManager.getModelHorizontal(), -1, 1);
        FractalModelElementWithSurroundings fractalModelElementWithSurroundings = fractalModel.getFractalModelElementWithSurroundings(0, 1);
        assertTrue(analyzer.canConnectWithElement(fractalModelElement, fractalModelElementWithSurroundings.getCentralElement()));
        assertTrue(analyzer.canConnectWithElement(fractalModelElementWithSurroundings.getCentralElement(), fractalModelElement));
        assertFalse(analyzer.canConnectWithElement(fractalModelElementWithSurroundings, fractalModelElement));

        fractalModelElement = new FractalModelElement(CharManager.getModelHorizontal(), -1, 0);
        fractalModelElementWithSurroundings = fractalModel.getFractalModelElementWithSurroundings(0, 1);
        assertTrue(analyzer.canConnectWithElement(fractalModelElement, fractalModelElementWithSurroundings.getCentralElement()));
        assertTrue(analyzer.canConnectWithElement(fractalModelElementWithSurroundings.getCentralElement(), fractalModelElement));
        assertTrue(analyzer.canConnectWithElement(fractalModelElementWithSurroundings, fractalModelElement));

        fractalModelElement = new FractalModelElement(CharManager.getModelVertical(), 0, 0);
        fractalModelElementWithSurroundings = fractalModel.getFractalModelElementWithSurroundings(0, 1);
        assertTrue(analyzer.canConnectWithElement(fractalModelElement, fractalModelElementWithSurroundings.getCentralElement()));
        assertTrue(analyzer.canConnectWithElement(fractalModelElementWithSurroundings.getCentralElement(), fractalModelElement));
        assertTrue(analyzer.canConnectWithElement(fractalModelElementWithSurroundings, fractalModelElement));

        fractalModelElement = new FractalModelElement(CharManager.getModelVertical(), 2, 0);
        fractalModelElementWithSurroundings = fractalModel.getFractalModelElementWithSurroundings(2, 1);
        assertTrue(analyzer.canConnectWithElement(fractalModelElement, fractalModelElementWithSurroundings.getCentralElement()));
        assertTrue(analyzer.canConnectWithElement(fractalModelElementWithSurroundings.getCentralElement(), fractalModelElement));
        assertTrue(analyzer.canConnectWithElement(fractalModelElementWithSurroundings, fractalModelElement));

        fractalModelElement = new FractalModelElement(CharManager.getModelHorizontal(), 3, 0);
        fractalModelElementWithSurroundings = fractalModel.getFractalModelElementWithSurroundings(2, 1);
        assertTrue(analyzer.canConnectWithElement(fractalModelElement, fractalModelElementWithSurroundings.getCentralElement()));
        assertTrue(analyzer.canConnectWithElement(fractalModelElementWithSurroundings.getCentralElement(), fractalModelElement));
        assertTrue(analyzer.canConnectWithElement(fractalModelElementWithSurroundings, fractalModelElement));

        fractalModelElement = new FractalModelElement(CharManager.getModelHorizontal(), 3, 1);
        fractalModelElementWithSurroundings = fractalModel.getFractalModelElementWithSurroundings(2, 1);
        assertTrue(analyzer.canConnectWithElement(fractalModelElement, fractalModelElementWithSurroundings.getCentralElement()));
        assertTrue(analyzer.canConnectWithElement(fractalModelElementWithSurroundings.getCentralElement(), fractalModelElement));
        assertFalse(analyzer.canConnectWithElement(fractalModelElementWithSurroundings, fractalModelElement));

        fractalModelElement = new FractalModelElement(CharManager.getModelHorizontal(), 1, 0);
        fractalModelElementWithSurroundings = fractalModel.getFractalModelElementWithSurroundings(2, 1);
        assertTrue(analyzer.canConnectWithElement(fractalModelElement, fractalModelElementWithSurroundings.getCentralElement()));
        assertTrue(analyzer.canConnectWithElement(fractalModelElementWithSurroundings.getCentralElement(), fractalModelElement));
        assertTrue(analyzer.canConnectWithElement(fractalModelElementWithSurroundings, fractalModelElement));
    }
}
