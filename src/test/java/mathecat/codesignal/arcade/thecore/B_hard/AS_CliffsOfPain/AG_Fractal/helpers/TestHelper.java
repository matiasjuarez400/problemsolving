package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import java.util.List;

public class TestHelper {
    public static void printTestOutput(String testName, String output) {
        StringBuilder msg = new StringBuilder();
        msg.append(createMessageSeparator()).append("\n");
        msg.append(testName).append("\n\n");
        msg.append(output);
        msg.append(createMessageSeparator());

        System.out.println(msg.toString());
    }

    public static void printExpectedVsActual(String testName, String expected, String actual) {
        StringBuilder msg = new StringBuilder();
        msg.append("EXPECTED: \n\n")
                .append(expected)
                .append(repeatChar('-', 30)).append("\n")
                .append("ACTUAL: ").append("\n\n")
                .append(actual);

        printTestOutput(testName, msg.toString());
    }

    public static boolean areMatrixEqual(char[][] base, char[][] target) {
        if (base.length != target.length) return false;

        for (int row = 0; row < base.length; row++) {
            if (base[row].length != target[row].length) return false;

            for (int col = 0; col < base[row].length; col++) {
                if (base[row][col] != target[row][col]) return false;
            }
        }

        return true;
    }

    public static boolean areMatrixEqual(boolean[][] base, boolean[][] target) {
        if (base.length != target.length) return false;

        for (int row = 0; row < base.length; row++) {
            if (base[row].length != target[row].length) return false;

            for (int col = 0; col < base[row].length; col++) {
                if (base[row][col] != target[row][col]) return false;
            }
        }

        return true;
    }

    private static String createMessageSeparator() {
        return repeatChar('*', 30);
    }

    private static String repeatChar(char c, int times) {
        return new String(new char[times]).replace('\0', c);
    }

    public static boolean containsBridge(Point start, Point end, char connector, List<Bridge> bridges) {
        boolean found = false;
        for (Bridge bridge : bridges) {
            if (bridge.getConnector() == connector) {
                Point firstAnchorPosition = bridge.getFirstAnchor().getGlobalRelativePosition();
                Point secondAnchorPosition = bridge.getSecondAnchor().getGlobalRelativePosition();

                if (firstAnchorPosition.equals(start) && secondAnchorPosition.equals(end) ||
                        firstAnchorPosition.equals(end) && secondAnchorPosition.equals(start)
                ) {
                    found = true;
                    break;
                }
            }
        }

        return found;
    }
}
