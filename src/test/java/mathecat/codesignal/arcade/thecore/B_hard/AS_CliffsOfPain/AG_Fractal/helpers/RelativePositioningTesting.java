package mathecat.codesignal.arcade.thecore.B_hard.AS_CliffsOfPain.AG_Fractal.helpers;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RelativePositioningTesting {
    private static final ConnectionAnalyzer CONNECTION_ANALYZER = new ConnectionAnalyzer();
    private static final FractalUnionsFinder FRACTAL_UNIONS_FINDER = new FractalUnionsFinder(CONNECTION_ANALYZER);

    @Test
    public void testUnionPointWithAnchorsRelative() {
        FractalUnionPointsInformation fractalUnionPointsInformation = createFractalUnionPointInformation();

        final Point relativePosition = new Point(3, 1);

        FractalUnionPointsInformationRelative relative =
                new FractalUnionPointsInformationRelative(fractalUnionPointsInformation, relativePosition);

        assertEquals(relativePosition, relative.getGlobalRelativePosition());

        List<UnionPointWithAnchorsRelative> relativeUnions = relative.getUnionsWithRelativePosition();

        assertEquals(2, relativeUnions.size());

        UnionPointWithAnchorsRelative first = relativeUnions.get(0);
        assertEquals(new Point(2, 0), first.getLocalPosition());
        assertEquals(new Point(5, 1), first.getGlobalRelativePosition());

        UnionPointWithAnchorsRelative second = relativeUnions.get(1);
        assertEquals(new Point(0, 1), second.getLocalPosition());
        assertEquals(new Point(3, 2), second.getGlobalRelativePosition());
    }

    @Test
    public void testAnchorsRelativePositioning() {
        FractalUnionPointsInformation fractalUnionPointsInformation = createFractalUnionPointInformation();

        final Point relativePosition = new Point(3, 4);

        FractalUnionPointsInformationRelative relative =
                new FractalUnionPointsInformationRelative(fractalUnionPointsInformation, relativePosition);

        List<UnionPointWithAnchorsRelative> relativeUnions = relative.getUnionsWithRelativePosition();

        assertEquals(2, relativeUnions.size());

        UnionPointWithAnchorsRelative first = relativeUnions.get(0);
        assertEquals(new Point(2, 0), first.getLocalPosition());
        List<AnchorRelative> firstAnchorRelatives = first.getAnchorRelatives();

        List<Point> firstExpectedAnchorLocalPosition = Arrays.asList(
            new Point(3, 0),
            new Point(3, 1)
        );

        List<Point> firstActualAnchorLocalPosition = firstAnchorRelatives.stream()
                .map(AnchorRelative::getLocalPosition)
                .collect(Collectors.toList());

        List<Point> firstExpectedAnchorRelativePosition = Arrays.asList(
                new Point(6, 4),
                new Point(6, 5)
        );

        List<Point> firstActualAnchorRelativePosition = firstAnchorRelatives.stream()
                .map(AnchorRelative::getGlobalRelativePosition)
                .collect(Collectors.toList());

        assertEquals(firstExpectedAnchorLocalPosition.size(), firstAnchorRelatives.size());
        assertTrue(firstExpectedAnchorLocalPosition.containsAll(firstActualAnchorLocalPosition));
        assertTrue(firstActualAnchorLocalPosition.containsAll(firstExpectedAnchorLocalPosition));

        assertEquals(firstExpectedAnchorRelativePosition.size(), firstActualAnchorRelativePosition.size());
        assertTrue(firstExpectedAnchorRelativePosition.containsAll(firstActualAnchorRelativePosition));
        assertTrue(firstActualAnchorRelativePosition.containsAll(firstExpectedAnchorRelativePosition));
    }

    private FractalUnionPointsInformation createFractalUnionPointInformation() {
        final FractalModel fractalModel1 = new FractalModel(new char[][]
                {
                        {'.', '_', '_'},
                        {'_', '_', '|'}
                }
        );

        FractalUnionPointsInformation fractalUnionPointsInformation =
                FRACTAL_UNIONS_FINDER.analyzeFractalModel(fractalModel1);

        return fractalUnionPointsInformation;
    }
}
