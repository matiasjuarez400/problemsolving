package mathecat.codility.training._5_prefix_sums.genomic_range_query;

import junit.framework.TestCase;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class SolutionTest extends TestCase {
    private Solution solution = new Solution();

    @Test
    public void testA() {
        String S = "CAGCCTA";
        int[] P = {2, 5, 0};
        int[] Q = {4, 5, 6};

        int[] result = solution.solution(S, P, Q);
        int[] expected = {2, 4, 1};

        assertArrayEquals(expected, result);
    }

    @Test
    public void testB() {
        String S = "ATGCA";
        int[] P = {2, 0, 2};
        int[] Q = {4, 1, 3};

        int[] result = solution.solution(S, P, Q);
        int[] expected = {1, 1, 2};

        assertArrayEquals(expected, result);
    }
}