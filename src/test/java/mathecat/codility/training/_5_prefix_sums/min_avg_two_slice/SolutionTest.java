package mathecat.codility.training._5_prefix_sums.min_avg_two_slice;

import junit.framework.TestCase;
import org.junit.Test;

import java.util.List;

public class SolutionTest extends TestCase {
    int[] A = {4, 2, 2, 5, 1, 5, 8};
    Solution solution = new Solution();

    @Test
    public void testBruteForce() {
        List<Solution.Result> result = solution.bruteForce(A);

        Solution.Result first = result.get(0);

        assertEquals(1, first.start);
    }

    @Test
    public void testAccSumWithSlidingWindow() {
        List<Solution.Result> oracle = solution.bruteForce(A);

        List<Solution.Result> result = solution.prefixSumWithWindowSlide(A);

        assertEquals(oracle.get(0).getStart(), result.get(0).getStart());
        assertEquals(oracle.get(0).getAvg(), result.get(0).getAvg());
    }
}