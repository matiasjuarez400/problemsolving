package mathecat.codility.training._5_prefix_sums.count_div;

import junit.framework.TestCase;
import org.junit.Test;

public class SolutionTest extends TestCase {
    private  Solution solution = new Solution();

    @Test
    public void testA() {
        int result = solution.solution(1, 4, 2);

        assertEquals(2, result);
    }

    @Test
    public void testB() {
        int result = solution.solution(5, 9, 3);

        assertEquals(2, result);
    }

    @Test
    public void testC() {
        int result = solution.solution(6, 12, 3);

        assertEquals(3, result);
    }

    @Test
    public void testD() {
        int result = solution.solution(7, 8, 3);

        assertEquals(0, result);
    }

    @Test
    public void testE() {
        int result = solution.solution(0, 1, 11);

        assertEquals(1, result);
    }

    @Test
    public void testF() {
        int result = solution.solution(0, 10, 1);

        assertEquals(11, result);
    }
}