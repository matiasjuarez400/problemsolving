package mathecat.codility.training._4_counting_elements.frog_river_one;

import junit.framework.TestCase;
import org.junit.Test;

public class SolutionTest extends TestCase {
    private Solution solution = new Solution();

    @Test
    public void testDefaultTest() {
        int riverSize = 5;
        int[] leaves = {1, 3, 1, 4, 2, 3, 5, 4};

        int result = solution.run(riverSize, leaves);

        assertEquals(6, result);
    }
}