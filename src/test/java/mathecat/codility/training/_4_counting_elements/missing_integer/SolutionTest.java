package mathecat.codility.training._4_counting_elements.missing_integer;

import junit.framework.TestCase;
import org.junit.Test;

public class SolutionTest extends TestCase {
    private Solution solution = new Solution();

    @Test
    public void testBetweenCase() {
        int[] A = {1, 3, 6, 4, 1, 2};

        int result = solution.solution(A);

        assertEquals(5, result);
    }

    @Test
    public void testAfterCase() {
        int[] A = {1, 2, 3};

        int result = solution.solution(A);

        assertEquals(4, result);
    }

    @Test
    public void testNegativeCase() {
        int[] A = {-1, -3};

        int result = solution.solution(A);

        assertEquals(1, result);
    }

}