package mathecat.codility.training._4_counting_elements.perm_check;

import junit.framework.TestCase;
import org.junit.Test;

public class SolutionTest extends TestCase {
    Solution solution = new Solution();

    @Test
    public void testPositivePermutation() {
        int[] A = {4, 1, 3, 2};

        int result = solution.solution(A);

        assertEquals(1, result);
    }

    @Test
    public void testNegativePermutation() {
        int[] A = {4, 1, 3};

        int result = solution.solution(A);

        assertEquals(0, result);
    }
}