package mathecat.codility.training._4_counting_elements.max_counters;

import junit.framework.TestCase;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class SolutionTest extends TestCase {
    private Solution solution = new Solution();

    @Test
    public void testDefault() {
        int N = 5;
        int[] operations = {3, 4, 4, 6, 1, 4, 4};

        int[] result = solution.solution(N, operations);

        int[] expected = {3, 2, 2, 4, 2};

        assertArrayEquals(expected, result);
    }

    @Test
    public void testDoubleMax() {
        int N = 2;
        int[] operations = {1, 2, 2, 3, 2, 3, 1};
        /*
        1, 0
        1, 1
        1, 2
        2, 2
        2, 3
        3, 3
        4, 3
         */

        int[] result = solution.solution(N, operations);

        int[] expected = {4, 3};

        assertArrayEquals(expected, result);
    }
}